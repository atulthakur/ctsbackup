<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>false</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Name</field>
        <field>JJ_ANZ_Type__c</field>
        <field>JJ_ANZ_Therapeutic_Area__c</field>
        <field>JJ_ANZ_Product__c</field>
        <field>JJ_ANZ_Start_Date__c</field>
        <field>JJ_ANZ_End_Date__c</field>
        <field>Owner</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>JJ_ANZ_BUCL_Review__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>BUCL to approve if a BUCL Reviewer is provided</description>
        <entryCriteria>
            <formula>NOT(ISBLANK(JJ_ANZ_BUCL_Review__c ))</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>BUCL Review</label>
        <name>BUCL_Review</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>This will go to first line manager for approval.</description>
        <label>First Line Manager Approval</label>
        <name>First_Line_Manager_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>HCC</name>
                <type>queue</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>HCC to review to complete the approval process</description>
        <label>HCC Review</label>
        <name>HCC_Review</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Approval Process for the new Consulting Group. 
Follows a similar design to Promotional (Janssen) Events, where it is:
- locked on approval
- approved by HCC
- when Approved/Rejected it updates the Process Status field and sends an email alert

v2 - added Line Manager Approval
v3 - added BUCL Review Approval</description>
    <emailTemplate>Consulting_Group/Consulting_Group_Approval_Request</emailTemplate>
    <enableMobileDeviceAccess>false</enableMobileDeviceAccess>
    <entryCriteria>
        <formula>(TEXT( JJ_ANZ_Consulting_Group_Status__c) = &apos;Draft&apos;
|| TEXT (JJ_ANZ_Consulting_Group_Status__c) = &apos;Rejected&apos;)
&amp;&amp;
 JJ_ANZ_Approval_Validations_Passed__c</formula>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>JJ_ANZ_Consulting_Group_Approved_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Set_RT_to_Standard_Approved</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Set_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>false</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>JJ_ANZ_Consulting_Group_Rejected_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Set_Status_to_Rejected</name>
            <type>FieldUpdate</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>JJ_ANZ_Consulting_Group_Submitted_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Set_Status_to_Awaiting_Approval</name>
            <type>FieldUpdate</type>
        </action>
    </initialSubmissionActions>
    <label>Consulting Group v3</label>
    <nextAutomatedApprover>
        <useApproverFieldOfRecordOwner>false</useApproverFieldOfRecordOwner>
        <userHierarchyField>Manager</userHierarchyField>
    </nextAutomatedApprover>
    <recallActions>
        <action>
            <name>Set_Status_to_Draft</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOnly</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
