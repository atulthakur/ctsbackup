<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>true</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Name</field>
        <field>Owner</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>JJ_ANZ_BUCL_Review__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>Unanimous</whenMultipleApprovers>
        </assignedApprover>
        <description>BUCL to approve if a BUCL Reviewer is provided</description>
        <entryCriteria>
            <formula>NOT(ISBLANK(JJ_ANZ_BUCL_Review__c ))</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>BUCL Review</label>
        <name>BUCL_Review</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>JJ_ANZ_Owner_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>Approval step if BUCL review occurs. Requires using the Owner Manager field, as the SFDC &apos;Manager&apos; will use the BUCL reviewer&apos;s manager, not the Submitter Manager 

//06/2016 - FB204</description>
        <entryCriteria>
            <formula>NOT(ISBLANK(JJ_ANZ_BUCL_Review__c ))</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>First Line Manager approval (after BUCL)</label>
        <name>JJ_ANZ_First_Line_Manager_approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>Assign to Manager for approval</description>
        <entryCriteria>
            <formula>ISBLANK(JJ_ANZ_BUCL_Review__c )</formula>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Manager</label>
        <name>JJ_ANZ_Review_Speaker_Details</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>HCC</name>
                <type>queue</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <description>Assign to HCC for approval</description>
        <label>HCC</label>
        <name>HCC</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Events Simplification 1 - Unique Agreement Approval Process.
Assign to Line Manager, then HCC approval
04/2016 BUCL Review added
06/2016 BUCL Review updates - use Owner Manager in First Line Manager approval after BUCL</description>
    <emailTemplate>JJ_ANZ_Event_Emails/Speaker_Approval_Request</emailTemplate>
    <enableMobileDeviceAccess>false</enableMobileDeviceAccess>
    <entryCriteria>
        <formula>RecordType.DeveloperName = &apos;JJ_ANZ_Unique_Agreement&apos; 
&amp;&amp; (ISPICKVAL(JJ_ANZ_Speaker_Status__c, &apos;Draft&apos;)||ISPICKVAL(JJ_ANZ_Speaker_Status__c, &apos;Rejected&apos;)) &amp;&amp; JJ_ANZ_Validations_Passed__c = True</formula>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>JJ_ANZ_Set_Engagement_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>JJ_ANZ_Speaker_Approved_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>JJ_ANZ_Update_RT_to_Unique_Approved</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Set_Agreement_Status_to_Pending</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>false</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>JJ_ANZ_Set_Engagement_Status_to_Rejected</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>JJ_ANZ_Speaker_Rejected_Alert</name>
            <type>Alert</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>JJ_ANZ_Engage_Status_Awaiting_Approval</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>JJ_ANZ_Speaker_Submitted_Alert</name>
            <type>Alert</type>
        </action>
    </initialSubmissionActions>
    <label>Unique Agreement Approval Process v3</label>
    <nextAutomatedApprover>
        <useApproverFieldOfRecordOwner>false</useApproverFieldOfRecordOwner>
        <userHierarchyField>Manager</userHierarchyField>
    </nextAutomatedApprover>
    <recallActions>
        <action>
            <name>JJ_ANZ_Set_Engagement_Status_to_Draft</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOrCurrentApprover</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
