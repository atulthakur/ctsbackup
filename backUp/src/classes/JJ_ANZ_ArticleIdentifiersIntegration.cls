/**
*      @author Keerthana Thallam
*   
       @description    Scheduler for the batch class JJ_ANZ_Medcomms Integration
       

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------

**/
global class JJ_ANZ_ArticleIdentifiersIntegration implements schedulable
{
 global void execute(SchedulableContext sc)
    {
    JJ_ANZ_ArticlesIntegration batch = new JJ_ANZ_ArticlesIntegration(); //ur batch class
      database.executebatch(batch);
    }
}