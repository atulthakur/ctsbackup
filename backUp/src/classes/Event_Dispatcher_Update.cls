/**
* Extension controllerfor the meeting event VF page
* Created: June 17 2013
* Modified June 22 2013
* Bluewolf
**/

/****************************************************************
Related requirements: ALL - Redirects to the appropriate page or 
					  layout on event modification
*****************************************************************/
public with sharing class Event_Dispatcher_Update {
	private ApexPages.StandardController myCont;
	
	public Event_Dispatcher_Update(ApexPages.StandardController controller){myCont=controller;}
	
	public PageReference dispathUpd(){ 
		
			PageReference newRef=null;			
			
			String recordTypeName=[SELECT RecordType.Name FROM Medical_Event_vod__c WHERE Id=:myCont.getRecord().Id].get(0).RecordType.Name ;
	        
	        if(recordTypeName.contains('Dinner Event')){ 
		        if(recordTypeName.contains('Approved')){
		        	newRef=new Pagereference('/'+myCont.getRecord().Id+'/e?retURL='+myCont.getRecord().Id+'&nooverride=1');
		            newRef.setRedirect(True);
		        }
		        else{		        
	           		newRef=new Pagereference('/apex/ME_Creation?id='+myCont.getRecord().Id+'&retURL='+myCont.getRecord().Id);
	           		newRef.setRedirect(True);
		        }
	        }
	        else{ 
	           newRef=new Pagereference('/'+myCont.getRecord().Id+'/e?retURL='+myCont.getRecord().Id+'&nooverride=1');
	           newRef.setRedirect(True);           
	        }      
	        return newRef;          
     } 
}