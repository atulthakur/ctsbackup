/**
*	Created by: Khodabocus Zameer (ICBWORKS)
*	Last Date Modified: 20.03.2014
*	Email: zameer.khodabocus@icbworks.com
*	
*	Description: This batch process is scheduled to run every night to delete users' search history on the Invitee tab in the Janssen Event wizard. 
**/
global with sharing class BatchDeleteAllInviteeSearchResults implements Database.Batchable<SObject>{
	
	String query;
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		query = 'select Id from JJ_Invitee_Search_Result__c';
		
    	return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<JJ_Invitee_Search_Result__c> resultsList = (List<JJ_Invitee_Search_Result__c>) scope;
        
        deleteResults(resultsList);
    }
    
    global void finish(Database.BatchableContext BC){
         
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {'zameer.khodabocus@icbworks.com'}; 
        
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Batch Processing');
    	mail.setPlainTextBody('The BatchDeleteAllInviteeSearchResults has run.');
    	mail.setSubject('The Search Results history for today has been deleted');

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
    
    global void deleteResults(List<JJ_Invitee_Search_Result__c> resultsList){
    	if(resultsList.size() > 0){
    		delete resultsList;
    	}
    }
}