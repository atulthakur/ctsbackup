public class findCurrentuser{
    private final List<User> users;

    public findCurrentuser() {
        users = [select Id, Name,UserRole.Name from User where Id = :UserInfo.getUserId()];
    }

    public List<User> getCurrentuser() {
        return users;
    }
}