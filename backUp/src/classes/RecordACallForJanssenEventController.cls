/*****
*** 	Developed by: Khodabocus Zameer (ICBWORKS)
***		Description: This Class is used as a controller for the visualforce page, RecordACallForJanssenEvent. 
***					 This VF page is used in the button, Record a Call, on Janssen Event to prevent users from Recording calls when
***					 at least one call has already been recorded. 
***					 The message 'Call has already been recorded for this Event' is displayed if at least one call has already been recorded,
***					 else the page redirects to the Standard Record a Call Veeva page.
*****/


public with sharing class RecordACallForJanssenEventController {
    
    public String eventId {get; set;}
    public date eventStartDate {get; set;}
    public boolean hasCallRecorded {get; set;} 
    
    public RecordACallForJanssenEventController(Apexpages.StandardController stdControl){
    	//Getting the Event Id from the page URL
        eventId = Apexpages.currentPage().getParameters().get('Id');
    }
    
    public pageReference redirectPage(){
        pagereference pg = null;
        
        if(eventId != null){
        	//Getting the Event start date
            List<Medical_Event_vod__c> janssenEvent = [select Id, Start_Date_vod__c from Medical_Event_vod__c where Id = :eventId];
            
            if(janssenEvent.size() > 0){
                eventStartDate = janssenEvent[0].Start_Date_vod__c;
            }
            
            //Counting the numbers calls recorded for this Event
            integer countEventCalls = [select count() from Call2_vod__c where Medical_Event_vod__c = :eventId];
            
            if(countEventCalls > 0){
                hasCallRecorded = true;
            }else{
                hasCallRecorded = false;
                
                //Standard Veeva Record a Call page URL
                pg = new pageReference('/servlet/servlet.Integration?scontrolCaching=1&lid=00b90000001HFfC&eid=' + eventId + '&ic=1');
            }
        }
        
        return pg;
    }
    
    public pageReference goBack(){
        pagereference pg = new pageReference('/' + eventId);
        
        return pg;
    }
}