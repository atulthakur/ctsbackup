/**
* Class containing remote actions for the meeting 
* event process (Cancellation and invite print)
* Created: June 22 2013
* Modified July 22 2013
* Bluewolf
**/

/*********************************************************************
Related requirements: - CSP1 to CSP9: Updates made to the event on
                       cancellation trigger workflows and notifications
                      - DMP7
                      - ALPHA REVIEW COMMENTS
**********************************************************************/
global class Global_Meeting_Actions {
    @RemoteAction
    webService static void cancelEvent(String ident, String r) {
        SavePoint sp=Database.setSavepoint();
        try{
            Medical_Event_vod__c cancelEv = [SELECT Id, Process_Status__c,Active_vod__c, RecordType.Name 
                       FROM Medical_Event_vod__c WHERE Id=:ident];
                       
            ID rtId;//=[SELECT ID FROM RecordType WHERE Name='Dinner Event Cancelled'].get(0).Id;
            
            for(RecordType rt:[SELECT ID, Name FROM RecordType WHERE Name like: '%Cancelled' and SobjectType=:'Medical_Event_vod__c' ]){
                if(cancelEv.RecordType.Name.startsWith(rt.Name.removeEnd(' Cancelled'))){
                    rtId = rt.Id;
                }
            }
            
            
            cancelEv.Active_vod__c=False;
            cancelEv.Process_Status__c='Cancelled';
            cancelEv.JJ_ANZ_Event_Status__c='Cancelled';
            cancelEv.RecordTypeId=rtId;
            cancelEv.JJ_Cancellation_Reason__c=r;
            
            
            update cancelEv;        
        }catch(Exception e){
            Database.rollback(sp);
        }
    }  
    @RemoteAction
    webService static void rollbackEvent(String ident, String r) {
        SavePoint sp=Database.setSavepoint();
        try{
            Medical_Event_vod__c rollbackEv = [SELECT Id, Process_Status__c,Active_vod__c, RecordType.Name 
                       FROM Medical_Event_vod__c WHERE Id=:ident];
                       
            ID rtId;//=[SELECT ID FROM RecordType WHERE Name='Dinner Event Cancelled'].get(0).Id;
            
            //ID rtId=[SELECT ID FROM RecordType WHERE Name='Janssen Event Reconciliation'].get(0).Id;
            
            for(RecordType rt:[SELECT ID, Name FROM RecordType WHERE Name like: '%liation%' and SobjectType=:'Medical_Event_vod__c' ]){
                if(rollbackEv.RecordType.Name.startsWith(rt.Name.removeEnd(' Reconciliation'))){
                    rtId = rt.Id;
                }
                if(rollbackEv.RecordType.Name.startsWith(rt.Name.removeEnd(' Reconcilliation'))){
                    rtId = rt.Id;
                }
            }                      
           
            rollbackEv.JJ_ANZ_Event_Status__c='Reconciliation';
            rollbackEv.JJ_Post_Meeting_Reconciliation_Complete__c=False;
            rollbackEv.RecordTypeId=rtId;
            rollbackEv.JJ_Rollback_Reason__c=r;
            
            update rollbackEv;        
        }catch(Exception e){
            Database.rollback(sp);
        }
    }  
    @RemoteAction
    webService static Integer markGeneratedInvites(String ident) {
        SavePoint sp=Database.setSavepoint();
        try{
            Medical_Event_vod__c ev = [SELECT Id, JJ_Min_Invite_Printed__c,JJ_Max_Invite_Printed__c,JJ_Needs_Invite_Printing__c
                       FROM Medical_Event_vod__c WHERE Id = :ident];
            String max='',min='';
            List<sObject> result=new List<sObject>();
            
            if(ev.JJ_Min_Invite_Printed__c!=NULL&&ev.JJ_Max_Invite_Printed__c!=NULL){
                max=ev.JJ_Min_Invite_Printed__c;
                min=ev.JJ_Max_Invite_Printed__c;
                result=[SELECT MIN(Name) BottomVal,MAX(Name) TopVal 
                                      FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c=:ident AND Name>:min];                            
            }
            else{
                result=[SELECT MIN(Name) BottomVal,MAX(Name) TopVal 
                                      FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c=:ident];            
            }
            for(sObject obj:result){
                max=(String)obj.get('TopVal');
                min=(String)obj.get('BottomVal');
            }
            if(max!=NULL&&min!=NULL){
                ev.JJ_Min_Invite_Printed__c=min;
                ev.JJ_Max_Invite_Printed__c=max;
                ev.JJ_Needs_Invite_Printing__c=False;
                update ev;
            }       
            Integer totalInvites=[SELECT Name FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c=:ident AND Name>=:min AND Name<=:max].size();       
            return  totalInvites;
        }catch(Exception e){
            Database.rollback(sp);
        }
        return -1;
    }      
}