/**
*   Created by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 29.07.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description:  This batch process is used to align all Accounts, having checkbox JJ_ANZ_Callout_Trigger__c = true, to their required territories. The batch
*				  processes 500 Accounts at a time and reschedules itself as long as there are Accounts left to be processed. 
**/


global with sharing class BatchRunTerritoryAlignmentOnAccounts implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {
    
    global String query;  
    global String sessionId;
    global Id userId;
     
    global BatchRunTerritoryAlignmentOnAccounts(String sID, Id uId){
        sessionId = sID;
        userId = uId;
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        query = 'Select Id, JJ_ANZ_Callout_Trigger__c from Account where JJ_ANZ_Callout_Trigger__c = true limit 500';

        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<SObject> scope){
        List<Account> accountsList = (List<Account>) scope;
        
        runRules(accountsList);
    }
    
    global void finish(Database.BatchableContext BC){
        
        List<Account_Territory_Alignment_Utilities__c> atlSetting = [select Id, Batch_Scheduled__c, Error__c From Account_Territory_Alignment_Utilities__c limit 1];
        
        if(atlSetting.size() > 0){
        	if(atlSetting[0].Error__c == false){ 
        		
        		integer countAccounts = 0;
        		
		        countAccounts = [select count() from Account where JJ_ANZ_Callout_Trigger__c = true limit 1];
		        
		        if(countAccounts > 0){
		            Datetime sysTime = System.now();
		            sysTime = sysTime.addSeconds(120);
		            
		            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		            system.debug(chron_exp);
		            
		           	SchedulerBatchRunTerritoryAlignmentAcc newSched = new SchedulerBatchRunTerritoryAlignmentAcc(sessionId, userId);
		            //Schedule the next job, and give it the system time so name is unique
		            System.schedule('new Schedule Job' + sysTime.getTime(), chron_exp, newSched);
		            
		            for(CronTrigger c:[Select State,Id,EndTime,CronExpression,OwnerId From CronTrigger where  
		                               NextFireTime = null AND State = 'DELETED' AND OwnerId = :userId]){
		
		            	System.abortJob(c.id);
		            }
		            
		        }else{
		        	for(CronTrigger c:[Select State,Id,EndTime,CronExpression,OwnerId From CronTrigger where  
		                               NextFireTime = null AND State = 'DELETED' AND OwnerId = :userId]){
		
		            	System.abortJob(c.id);
		            }
		            
		    		if(atlSetting.size() > 0){
		    			atlSetting[0].Batch_Scheduled__c = false;
				            
				        update atlSetting[0];
		    		}
		    		
		    		List<EmailTemplate> terrSuccessTemplate = [select Id from EmailTemplate where DeveloperName = 'Territory_Alignment_Success_Template'];
		    		
		    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
			        if(terrSuccessTemplate.size() > 0){
			        	mail.setTemplateId(terrSuccessTemplate[0].Id);
			        	mail.setSaveAsActivity(false);
			        	
			        	List<User> currentUser = [select Id, Email from User where Id = :userId];
			        	
			        	if(currentUser.size() > 0){
				        	mail.setTargetObjectId(currentUser[0].Id);
				        }else{
				        	List<User> sysAdmin = [select Id, Email from User where Profile.Name = 'System Administrator' AND isActive = true limit 1];
				        	
				        	mail.setTargetObjectId(sysAdmin[0].Id);
				        }
			        }else{
			        	mail.setPlainTextBody('The Territory Alignment of Accounts has been completed successfully.');
			        	mail.setSubject('Territory Alignment Scheduled Batch has run sucessfully');
			        	
			        	String[] toAddresses = new String[] {'zameer.khodabocus@icbworks.com'};
			        
			        	mail.setToAddresses(toAddresses);
			        }
			        
			        mail.setReplyTo('no-reply@salesforce.com');
			        
			        mail.setSenderDisplayName('Salesforce Territory Alignment Processing');
			        
			        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
		        }
        	}
        }
    }
    
    global void runRules(List<Account> accountsList){
        
        if(accountsList != null && accountsList.size() > 0){
        	
        	List<Account_Territory_Alignment_Utilities__c> atlSetting = [select Id, Error__c, Batch_Scheduled__c 
        															  	 From Account_Territory_Alignment_Utilities__c limit 1];
        	
        	JJ_ANZ_Process_Futures_Util.setnofutureprocess(true);
        	
        	boolean isError = false;
            String accIds = '';
            List<Account> accountsUpdateList = new List<Account>();
            integer counter = 0;
            integer accountListSize = accountsList.size();
            
            integer callOutLimit = math.mod(accountListSize, 50);
            
            if(callOutLimit > 0){
            	callOutLimit = (math.floor(accountListSize * 0.02)).intValue() + 1;
            }else{
            	callOutLimit = (accountListSize * 0.02).intValue();
            }
            
            for(integer i = 0; i < callOutLimit; i++){
                
                List<Account> acctsList = new List<Account>();
                integer index = 0;
                integer accLimit = counter + 50;
                
                if(accLimit > accountListSize){
                    accLimit = accountListSize;
                }
                
                for(integer j = counter; j < accLimit; j++){
                    if(index == 0){
                        accIds = accountsList[j].Id;
                        index++;
                    }else{
                        accIds += ',' + accountsList[j].Id;
                    }
                    
                    accountsList[j].JJ_ANZ_Callout_Trigger__c = false;
                    
                    acctsList.add(accountsList[j]);
                    
                    counter++;
                }
                
                String VOD_SERVER_URL = VOD_Common.VOD_SERVER_URL;
                String VOD_VERSION = VOD_Common.VOD_VERSION;
                String sessID = sessionId;
                
                System.debug('hk: VSU = ' + VOD_SERVER_URL);
                System.debug('hk: VV = ' + VOD_VERSION);
        
                // Get the base URL
                String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        
                // For some reason, we soemtimes get this with http rather than https.  Fix if necessary
                if (!baseURL.contains('https')){
                    baseURL = baseURL.replace('http','https');
                }
        
                // Now put it all together
                String urlx= VOD_SERVER_URL + '/' + VOD_VERSION +
                '?VER=' + 
                VOD_VERSION +
                '&ses=' + sessID + '&url=' +
                EncodingUtil.urlEncode(baseURL + '/services/Soap/u/24.0/' + UserInfo.getOrganizationId(), 'UTF-8') +
                '&SSID=' + sessID +
                '&oType=assignterritory&accountIds=' + accIds;
                system.debug (urlx);
        
                HttpRequest req = new HttpRequest();
                req.setEndpoint(urlx);
                req.setMethod('GET');
        
                Http http = new Http();
                HTTPResponse res = http.send(req);
                System.debug('hk:' + res.getBody());
                
                if(!res.getBody().contains('Success')){
                	isError = true;
                	
                	acctsList = new List<Account>();
                }else{
                	accountsUpdateList.addAll(acctsList);
                }
            }
            
            if(isError == false){
            	if(accountsUpdateList.size() > 0){
	            	try{
	                	update accountsUpdateList;
	            	}catch(Exception e){
		            	if(atlSetting.size() > 0){
			            	atlSetting[0].Batch_Scheduled__c = false;
			            	
			            	update atlSetting[0];
		            	}
	            	}
	            }
            }else{
            	if(atlSetting.size() > 0){
	            	atlSetting[0].Error__c = true;
	            	atlSetting[0].Batch_Scheduled__c = false;
	            	
	            	update atlSetting[0];
            	}
            	
            	List<EmailTemplate> terrErrorTemplate = [select Id from EmailTemplate where DeveloperName = 'Territory_Alignment_Failed_Template'];
            	
            	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
		        if(terrErrorTemplate.size() > 0){
		        	mail.setTemplateId(terrErrorTemplate[0].Id);
		        	mail.setSaveAsActivity(false);
		        	
		        	List<User> currentUser = [select Id, Email from User where Id = :userId];
		        	
		        	if(currentUser.size() > 0){
			        	mail.setTargetObjectId(currentUser[0].Id);
			        }else{
			        	List<User> sysAdmin = [select Id, Email from User where Profile.Name = 'System Administrator' AND isActive = true limit 1];
			        	
			        	mail.setTargetObjectId(sysAdmin[0].Id);
			        }
		        }else{
		        	mail.setPlainTextBody('An Error has occured, please re run the batch process from your homepage link.');
		        	mail.setSubject('Territory Alignment Scheduled Batch Error!');
		        	
		        	String[] toAddresses = new String[] {'zameer.khodabocus@icbworks.com'};
		        
		        	mail.setToAddresses(toAddresses);
		        }
		        
		        mail.setReplyTo('no-reply@salesforce.com');
		        
		        mail.setSenderDisplayName('Salesforce Territory Alignment Processing');
		        
		        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
		        
		        for(CronTrigger c:[Select State,Id,EndTime,CronExpression,OwnerId From CronTrigger where  
	                               NextFireTime = null AND State = 'DELETED' AND OwnerId = :userId]){
	
	            	System.abortJob(c.id);
	            }
	            
	            return;
            }
            
            JJ_ANZ_Process_Futures_Util.setnofutureprocess(false);
        }
    }
}