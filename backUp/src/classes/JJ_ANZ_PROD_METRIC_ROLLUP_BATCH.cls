global class JJ_ANZ_PROD_METRIC_ROLLUP_BATCH implements Database.Batchable<sObject>{
    private final String initialstate;
    private datetime timenow;
    String query;
    global JJ_ANZ_PROD_METRIC_ROLLUP_BATCH(){
        
        timenow = system.now();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
         
        
        Janssen_Settings__c vsc = Janssen_Settings__c.getOrgDefaults();
		datetime timethen = vsc.JJ_ANZ_Prod_Batch_Last_Run__c; 
        query = 'select id,' +
            	'	(select id, products_vod__c , jj_classification__c' +
            	'	 from Product_metrics_vod__r),' +
            	'	JJ_ANZ_Janssen_Territory_Rule__c' +
				'   from account' +
            	'   where id in (select account_vod__c from product_metrics_vod__c' +
				'	where systemmodstamp >=' + timethen.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') +')'; 
            				
        
            return Database.GetQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC , List<sObject> batch){
        JJ_ANZ_Process_Futures_Util.setnofutureprocess(true);
        system.debug(JJ_ANZ_Process_Futures_Util.getnofutureprocess());
       
        
        /* The goal here is to first create a map of SFE metric records with keys of product id and 
		values of JJ_ANZ_Team__c (string format). Google "Apex Map" for more info. 
		Next we will parse through all the product metrics records gathered (those that have been modified since the last run time). 
		The code will check whether the product is contained within the SFE metric map, and if so will check whether the classification
		is either A, B , or C and if so will ensure that all Team values in the SFE metric map associated with the product key
		are contained within the Janssen_Territory_Rules__c account field to the account associated with the product metrics record.
		If the classification is Z , U, or null,  the associated Teams will be removed from the Janssen_Territory_Rules field.
		In the end this will result in accounts containing Team values stamped onto the Janssen_Territory_Rules field for any A,B,C classification
		for a product associated with a team. Where there may be multiple teams on one field, the teams will be managed in a semicolon delimited list.
		This allows for salesforce territory rules to be created to allign accounts to a territory with rules such as:
		JJ_ANZ_Janssen_Territory_Rules__c contains Neuroscience */
        date thisday = date.today();
        List<account> acctupdates = new List<account>();
        Map<id , list<string>> SFEmap = new Map<id , list<string>>(); 
           List<JJ_ANZ_SFE_Metrics__c> SFE = new List<JJ_ANZ_SFE_Metrics__c>([select id , JJ_ANZ_Product__c , JJ_ANZ_Team__c
																				from JJ_ANZ_SFE_Metrics__c where JJ_ANZ_Start_Date__c <= :thisday
                                                                                AND JJ_ANZ_End_Date__c >= :thisday]); 
        for (JJ_ANZ_SFE_Metrics__c mex : SFE) {
            List<string> prodteams = SFEmap.get(mex.JJ_ANZ_Product__c);
            If (prodteams == null) {
                prodteams = new List<string>();
                SFEmap.put(mex.JJ_ANZ_Product__c , prodteams);
            }
            prodteams.add(mex.JJ_ANZ_Team__c);
        }
        for (sObject obj : batch) {
            string teamlist = ';';
            account acct = (account)obj;
            list<product_metrics_vod__c> acctPMs = acct.product_metrics_vod__r;
            for (product_metrics_vod__c PM : acctPMs){
                list<string> team; 
                if(PM.jj_classification__c == 'A' || PM.jj_classification__c == 'B' || PM.jj_classification__c == 'C'){
                	team = SFEmap.get(PM.products_vod__c);
                    if(team == null){
                        continue;
                    }
                    for(string T : Team){
                        if(teamlist.contains(T)){
                            continue;
                        }
                        teamlist += T + ';';
                    }
                }
            }
            acct.JJ_ANZ_Janssen_Territory_Rule__c = teamlist;
            acct.jj_anz_callout_trigger__c = true;
            acctupdates.add(acct);
        }
        update acctupdates;
    }
    global void finish(Database.BatchableContext BC) {
    Janssen_Settings__c vsc = Janssen_Settings__c.getOrgDefaults();
    vsc.JJ_ANZ_Prod_Batch_Last_Run__c = timenow;    
        update vsc;
    JJ_ANZ_Process_Futures_Util.setnofutureprocess(false);
    }
}