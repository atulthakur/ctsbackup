global class JJ_ANZ_Account_Tactics_Batch_Update implements Database.Batchable<sObject>,Schedulable  {

   /**************************************************************************
        Ingrid Suprana
        September 11, 2014
        Version 1.0
        
        Summary
        --------------
        This routine update Tactic Due status in the Account Tactics.
        Required for Account Plan roll up formula to count number of Account Tactics Due.
        FE-0000044
                    
        How to Run
        --------------
        Option 1. Run manually via the SFDC Developer Console:
        database.executebatch(new JJ_ANZ_Account_Tactics_Batch_Update(),200); 
        
        Option 2. Schedule a nightly routine 
        Setup > Develop > Apex Classes > Schedule Apex
    **************************************************************************/
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, JJ_ANZ_End_Date__c, JJ_ANZ_Tactic_Due_Check__c ' + 
                    ' FROM Account_Tactic_vod__c ' +
                    ' WHERE (JJ_ANZ_End_Date__c < TODAY and JJ_ANZ_Tactic_Due_Check__c = false) ' +
                    ' OR (JJ_ANZ_End_Date__c > TODAY and JJ_ANZ_Tactic_Due_Check__c = true) ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext ctx) { 
        database.executebatch(new JJ_ANZ_Account_Tactics_Batch_Update(),200); 
    } 
   
    global void execute(Database.BatchableContext BC, List<Account_Tactic_vod__c> scope) {
         List<Account_Tactic_vod__c> tacticsToUpdate = new List<Account_Tactic_vod__c>();
         for(Account_Tactic_vod__c a : scope)
         {
             if (a.JJ_ANZ_End_Date__c < system.today()) {
                a.JJ_ANZ_Tactic_Due_Check__c = true;               
             } else {
                a.JJ_ANZ_Tactic_Due_Check__c = false;
             }             
             tacticsToUpdate.add(a); 
         }
         //System.debug('about to update =' + tacticsToUpdate);
         update tacticsToUpdate;
         //System.debug('sucessfully updated =' + tacticsToUpdate);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }

}