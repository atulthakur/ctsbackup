/**
* Class to dispatch actions to the appropriate VF page or Salesfroce.com
* standard page
* Created: June 17 2013
* Modified June 22 2013
* Bluewolf 
**/

/****************************************************************
Related requirements: ALL - Redirects to the appropriate page or 
					  layout on event creation
*****************************************************************/
public with sharing class Event_Action_Dispatcher {
	public Event_Action_Dispatcher(ApexPages.StandardController controller){}
	
	public PageReference dispathNew(){ 
		
		Pagereference newRef=null;
		String recordTypeName=[SELECT Name From RecordType r WHERE ID=:ApexPages.currentPage().getParameters().get('RecordType')].get(0).Name;
        Schema.DescribeSObjectResult res = Medical_Event_vod__c.SObjectType.getDescribe();
        
        if(!recordTypeName.contains('Approved')&&!recordTypeName.contains('Locked')){        	                 
	        if(recordTypeName.contains('Dinner Event')){ 
	           newRef=new Pagereference('/apex/ME_Creation?RecordType='+ApexPages.currentPage().getParameters().get('RecordType')+'&save_new=1&sfdc.override=1');//'&ent=01I900000016Tg4&save_new=1&sfdc.override=1');
	           newRef.setRedirect(True);
	        }       
	        else{
	           newRef=new Pagereference('/'+res.getKeyPrefix()+'/e?RecordType='+ApexPages.currentPage().getParameters().get('RecordType')+'&nooverride=1');//+'&ent=01I900000016Tg4&nooverride=1');
	           newRef.setRedirect(True);           
	        }  
        }
        else{        	
	           newRef=new Pagereference('/'+res.getKeyPrefix());
	           newRef.setRedirect(True);          	
        }
        
        return newRef;                      
     } 
}