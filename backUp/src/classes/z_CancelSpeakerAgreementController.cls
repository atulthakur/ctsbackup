/**
 * Noel Lim 3/08/2015 - Decommissioned as part of Enhancement FB148, moved to Workflows
 */
public with sharing class z_CancelSpeakerAgreementController {
    
    public Speaker_Agreement__c spkagree;
    
    public z_CancelSpeakerAgreementController(ApexPages.StandardController controller) {
        spkagree = [Select Id, RecordTypeId, JJ_ANZ_Speaker_Status__c, JJ_Start_Date__c, JJ_End_Date__c from Speaker_Agreement__c where Id =: ApexPages.CurrentPage().getParameters().get('Id')];
    }
    
    public pageReference cancelspkagr(){
        Id recordtypeid = [SELECT ID, Name FROM RecordType WHERE Name =: 'Expired Speaker' and SobjectType=:'Speaker_Agreement__c'].Id;
    
        spkagree.RecordTypeId = recordtypeid ;
        spkagree.JJ_ANZ_Speaker_Status__c = 'Cancelled';   
        spkagree.JJ_Start_Date__c = (spkagree.JJ_Start_Date__c > date.today())?date.today() - 1:spkagree.JJ_Start_Date__c;
        spkagree.JJ_End_Date__c = date.today();
        update spkagree;
        return new PageReference('/' + spkagree.Id).setRedirect(true);
    }

}