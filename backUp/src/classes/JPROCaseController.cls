@RestResource(urlMapping='/CreateCase/*')
global with sharing class JPROCaseController
{
    @HttpPost 
    global static String createParentCase(){
        
        RestRequest     request    = RestContext.request;
        RestResponse    response   = RestContext.response;    
        response.addHeader('Content-Type','application/json');
        
        String Checked = 'Yes';
        String casenum='';     
        String returnJSON='';
        String errorMessage='';
        String errorField='';
        Attachment a = new Attachment();        
        Case newCase = new Case();
        Case childCase = new Case(); 
        List<Case> createParentCase = new List<Case>();
        List<Case> createChildCase = new List<Case>();
           
        // Query Record type of case object
        
        Map<String,String> caseRecordTypes = new Map<String,String>{};
        
        List<RecordType> rtypes = [Select Name, Id From RecordType 
                                   where sObjectType='Case' and isActive=true];
        
        for(RecordType rt: rtypes){
            caseRecordTypes.put(rt.Name,rt.Id);
        }           
        
        Map<String,String> JPROtoParentCase=new Map<String,String>{}; // Contains String of Custom Meta data
                
        List<JJ_JPRO_to_Parent_Case_Mapping__mdt> parentMapping = [SELECT JJ_Field_Name__c, JJ_Json_Response__c FROM JJ_JPRO_to_Parent_Case_Mapping__mdt];
            
            for(JJ_JPRO_to_Parent_Case_Mapping__mdt jpro : parentMapping)
            {
            JPROtoParentCase.put(jpro.JJ_Field_Name__c,jpro.JJ_Json_Response__c );
            }        
        
        Map<String,String> JPROtoChildCase=new Map<String,String>{}; // Contains String of Custom Meta data Child
                
        List<JJ_JPRO_to_Child_Case_Mapping__mdt> childMapping = [SELECT JJ_Field_Name__c, JJ_Json_Response__c FROM JJ_JPRO_to_Child_Case_Mapping__mdt];
            
            for(JJ_JPRO_to_Child_Case_Mapping__mdt jpro : childMapping)
            {
            JPROtoChildCase.put(jpro.JJ_Field_Name__c,jpro.JJ_Json_Response__c );
            }
                        
        Map<String,String> decimalCheck=new Map<String,String>{};
        
        List<JJ_JPRO_Decimal_Field__mdt> decimalField=[SELECt JJ_Field_Name__c,JJ_Decimal_Field__c FROM JJ_JPRO_Decimal_Field__mdt];
        
        for(JJ_JPRO_Decimal_Field__mdt decimalFieldSet : decimalField){
            decimalCheck.put(decimalFieldSet.JJ_Field_Name__c,decimalFieldSet.JJ_Decimal_Field__c);
        }
        
        Map<String,String> jsonBody  = (Map<String,String>) JSON.deserialize(request.requestBody.toString(),Map<String,String>.class);
        
        if(jsonBody!=null && !jsonBody.isEmpty())
        { 
            newCase.JJ_ANZ_CreatePAR__c=Checked;
            newCase.Origin = 'JPRO';
            newCase.RecordTypeId = caseRecordTypes.get('Case Information');  
            for(String caseMapping :JPROtoParentCase.keySet())
            {
            String value=JPROtoParentCase.get(caseMapping);
            newCase.put(caseMapping,jsonBody.get(value));
            }
            
            Savepoint sp = Database.setSavepoint();
            
            createParentCase.add(newCase);
            
            if(!createParentCase.isEmpty() && createParentCase!=null)
                {
                    try
                    {
                        insert createParentCase;
                        system.debug('caseinsert'+createParentCase);
                    }
                    catch(DMLException de)
                    {
                        // Log a DML Exception
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c = 'This is Regarding Parent Case ' +de.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                        newErrorRecord.JJ_Error_in_Child_Case__c='PAR';
                        errorMessage='This is Regarding Parent Case ' +de.getMessage();
                        errorField=errorMessage.substringBetween('[',']');                        
                        errorField=errorField.left(errorField.length()-3);
                        Database.insert(newErrorRecord,false);      
                    }
                    catch(Exception e)
                    {
                        // Log an Exception
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c =  e.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                        Database.insert(newErrorRecord,false);          
                    }
                    
                }
            if(newCase.Id!=null && newCase.JJ_ANZ_CreatePAR__c==Checked )
                {
                     List<Case> caseNumber = [Select caseNumber from Case where Id=:newCase.id];
                     for(Case childNumber :caseNumber)
                     {
                     casenum=childNumber.caseNumber;
                     }
                    childCase.Subject = newCase.Subject;
                    childCase.ParentId = newCase.Id; 
                    childCase.RecordTypeId = caseRecordTypes.get('Product Access Request');
                    childCase.JJ_ANZ_ChildCaseNumber__c = casenum+ '-PAR';
                    childCase.Origin=newCase.Origin;
                    childCase.Status='In-Queue';
                    
                     for(String caseMapping :JPROtoChildCase.keySet())
                        {
                        String value=JPROtoChildCase.get(caseMapping);
                        System.debug('key contain' +jsonBody.containsKey(caseMapping));
                        String decimalValue=decimalCheck.get(caseMapping);
                        
                        if( jsonBody.containsKey(decimalValue))
                        {
                            childCase.put(caseMapping,Decimal.valueof(jsonBody.get(value)));
                        }
                        
                         else
                             {
                                 childCase.put(caseMapping,jsonBody.get(value));
                             }
                        
                        }
                    createChildCase.add(childCase);
     
                }
                    
                    if(!createChildCase.isEmpty() && createChildCase!=null)
                {
                    try
                    {
                        insert createChildCase;
                    }
                    catch(DMLException de)
                    {
                        // Log a DML Exception
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c = 'This is Regarding Child Case ' +de.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();                        
                        newErrorRecord.JJ_Error_in_Child_Case__c='PAR';
                        errorMessage='This is Regarding Child Case ' +de.getMessage();
                        errorField=errorMessage.substringBetween('[',']');                        
                        errorField=errorField.left(errorField.length()-3);
                        Database.insert(newErrorRecord,false);      
                    }
                    catch(Exception e)
                    {
                        
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                        Database.insert(newErrorRecord,false);          
                    }
                    
                }
                    
                    
         }     
                           
            Blob pic ;//Blob variable to capture decoded image
                
            if(newCase.Id!=null&&jsonBody!=null && jsonBody.get('AttachmentEncoded')!=null) {
            pic=EncodingUtil.base64Decode(jsonBody.get('AttachmentEncoded'));//Decode the base64 encoded image 
            
            a.ParentId = newCase.Id;
            a.Body = pic;
            a.ContentType = 'application/pdf';
            a.Name = jsonBody.get('AttachmentName');
            
            if(a!=null)
            {
                    try
                    {
                        insert a;
                        system.debug('attachment'+a);
                    }
                    catch(DMLException de)
                    {
                        // Log a DML Exception
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c = 'This is Regarding Attachment ' +de.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                        newErrorRecord.JJ_Error_in_Child_Case__c='PAR';
                        errorMessage='This is Regarding Attachment ' +de.getMessage();
                        errorField=errorMessage.substringBetween('[',']');
                        errorField=errorField.left(errorField.length()-3);
                        Database.insert(newErrorRecord,false);      
                        returnJSON=de.getMessage();
                        
                    }
                    catch(Exception e)
                    {
                        // Log an Exception
                        JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                        newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                        newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                        Database.insert(newErrorRecord,false); 
                                 
                    }
                    
             }  
             }
                if(newCase.Id!=null ){
                    JSONGenerator jsonBod = JSON.createGenerator(true);
                    jsonBod.writeStartObject();
                    jsonBod.writeStringField('Case ID' , newCase.Id+'');
                    jsonBod.writeStringField('Parent Case Number' , casenum+'');
                    jsonBod.writeStringField('Child Case ID', childCase.Id+'');
                    if(childCase.Id!=null){
                    jsonBod.writeStringField('Child Case Number', childCase.JJ_ANZ_ChildCaseNumber__c+'');
                        }
                    jsonBod.writeStringField('Attachment ID', a.Id+'');
                    if(String.isNotBlank(errorMessage)){
                    jsonBod.writeStringField('Error Message', errorMessage+'');
                    jsonBod.writeStringField('Error Field', errorField+'');
                    }
                    jsonBod.writeEndObject();
                    returnJSON=jsonBod.getAsString();
            }
        
     
   
       return returnJSON;
       }
      
}