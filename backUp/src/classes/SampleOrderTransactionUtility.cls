/*
* This class is a utility class for the Sample Order Transaction
* Author: Hasseeb Mungroo
* Date Created: 20/03/2014
* Change:
* Created class setSampleDeliveryDetails to copy Sample delivery details from the Call record on to the SOT
* Author: Paul Hosking 26/06/2014
*/
public class SampleOrderTransactionUtility{

     /**
     * Method to fill out the Practice Name on the Sample Transaction Order
     * The practice name is obtained by:
     *  1. The Parent Account of the Account attached to the Sample Transaction Order
     *  2. The Address of the Parent Account equals to the Address of the Sample Transaction Order
     */
    public static void setPracticeName(List<Sample_Order_Transaction_vod__c> newSOTs) {
        Set<Id> sampleOrderTransactionAccountId = new Set<Id>();
        Set<Id> sampleOrderTransactionId = new Set<Id>();
        Set<String> addresses = new Set<String>();
        
        //Get the Accounts and addresses of the records that is being edited/added
        for (Sample_Order_Transaction_vod__c sampleOrderTransaction : newSOTs) {
            sampleOrderTransactionId.add(sampleOrderTransaction.Id);
            sampleOrderTransactionAccountId.add(sampleOrderTransaction.Account_vod__c);
            addresses.add(sampleOrderTransaction.Ship_Address_Line_1_vod__c);
        }
         
       // List<Sample_Order_Transaction_vod__c> sotToBeUpdated = [select id,Account_vod__c,Practice_Name__c from Sample_Order_Transaction_vod__c where Id in : sampleOrderTransactionId];
        List<Sample_Order_Transaction_vod__c> sotToBeUpdated = newSOTs; 
        //Get the Parent Accounts of the SOT account
        Set<Id> parentAccountIds = new Set<Id>();
        
        //Create a map that will store the Parent to Child Accounts
        Map<Id, String> parenttoChildMap = new Map<Id, String>();    
        String childAccountsStr = '';
        List<Child_Account_vod__c> childAccounts = [select id, Parent_Account_vod__c, Child_Account_vod__c from Child_Account_vod__c where Child_Account_vod__c in :sampleOrderTransactionAccountId];
        for (Child_Account_vod__c childAccount : childAccounts) {       
            parentAccountIds.add(childAccount.Parent_Account_vod__c);
            
            //Check if there in an existing child in the map, if it is append to the string else it is a new one
            childAccountsStr = parenttoChildMap.get(childAccount.Parent_Account_vod__c);
            if (childAccountsStr != null) {
                childAccountsStr = parenttoChildMap.get(childAccount.Parent_Account_vod__c) + ':' + childAccount.Child_Account_vod__c;
            } else {
                childAccountsStr = childAccount.Child_Account_vod__c;
            }
            parenttoChildMap.put(childAccount.Parent_Account_vod__c, childAccountsStr);
        }    
        
        //Get the Addresses which are the same as the SOT  for the Parent Account of the SOT Account
        List<Address_vod__c> addressList = [select Id, Account_vod__c, Account_vod__r.Name  from Address_vod__c where Account_vod__c in :parentAccountIds and Name in :addresses];
        List<Sample_Order_Transaction_vod__c> updatedSOTs = new List<Sample_Order_Transaction_vod__c>();
        if (addressList != null && addressList.size() > 0) {
            for (Address_vod__c address : addressList) {
                
                //Get the child account which is the same as the parent address
                childAccountsStr = parenttoChildMap.get(address.Account_vod__c);
                for (Sample_Order_Transaction_vod__c sampleOrderTransaction : sotToBeUpdated) {
                    if (childAccountsStr.contains(sampleOrderTransaction.Account_vod__c)) {
                        sampleOrderTransaction.Practice_Name__c = address.Account_vod__r.Name;
                       
                    }
                }
                  
                
            }
        }         
    }
 
    /**
     * Method to create Sample BRC Tracking for the Sample Order Transaction
     */      
    public static void createSampleBRCTracking(List<Sample_Order_Transaction_vod__c> newSOTs) {
        List<Sample_BRC_Tracking__c> sampleBRCTrackerList = new List<Sample_BRC_Tracking__c>();
    
        //For each SOT Created by the trigger, add a new Sample_BRC_Tracking record for the specified SOT.  
        for (Sample_Order_Transaction_vod__c newSample_BRC_Tracking: newSOTs) {
   
     
                sampleBRCTrackerList .add(new Sample_BRC_Tracking__c(
    
                Transaction_Id__c = newSample_BRC_Tracking.Id,
                //SAP_Order_Number__c = newSample_BRC_Tracking.JJ_ANZ_SAP_Order_Number__c,
                Account__c = newSample_BRC_Tracking.Account_vod__c
                //CreatedById = newSample_BRC_Tracking.CreatedById,
                //Sample__c = newSample_BRC_Tracking.Sample_vod__c,
                //Quantity__c = newSample_BRC_Tracking.Quantity_vod__c,
                //Practice_Name__c = newSample_BRC_Tracking.Practice_Name__c,
                //Ship_Address_Line_1__c = newSample_BRC_Tracking.Ship_Address_Line_1_vod__c,
                //Ship_Address_Line_2__c = newSample_BRC_Tracking.Ship_Address_Line_2_vod__c,
                //Ship__c = newSample_BRC_Tracking.Ship_City_vod__c,
                //Call_Date__c = newSample_BRC_Tracking.Call_Date_vod__c,
                //Status__c = newSample_BRC_Tracking.Status_vod__c 
                ));
            
        }
        
        insert sampleBRCTrackerList;        
    }
    
    /**
    * Method to copy the delivery details from the Call object to the Sample Transaction Order
    */
    public static void setSampleDeliveryDetails(List<Sample_Order_Transaction_vod__c> newSOTs) {
        Set<Id> sampleOrderTransactionCallId = new Set<Id>();
        
        //Get the Call ID's for the records being edited/added
        for (Sample_Order_Transaction_vod__c sampleOrderTransaction : newSOTs) {
            sampleOrderTransactionCallId.add(sampleOrderTransaction.Call_Id_vod__c);
        }
                
        // Get Delivery Details from the Call object
        List<Call2_vod__c> deliveryDetails = [select id,JJ_ANZ_Sample_Delivery_Contact__c,JJ_ANZ_Sample_Delivery_Instruction__c from Call2_vod__c where Id in : sampleOrderTransactionCallId];        
        
        // Get list of SOT's
        List<Sample_Order_Transaction_vod__c> sotToBeUpdated = newSOTs; 
        
        if (deliveryDetails != null && deliveryDetails.size() > 0) {
            for (Call2_vod__c deliveryDetail : deliveryDetails) {
                for (Sample_Order_Transaction_vod__c sampleOrderTransaction : sotToBeUpdated) {
                    if (deliveryDetail.ID == sampleOrderTransaction.Call_Id_vod__c) {
                        sampleOrderTransaction.JJ_ANZ_Sample_Delivery_Contact__c = deliveryDetail.JJ_ANZ_Sample_Delivery_Contact__c;
                        sampleOrderTransaction.JJ_ANZ_Sample_Delivery_Instruction__c = deliveryDetail.JJ_ANZ_Sample_Delivery_Instruction__c;                      
                        }
                    }
                }
            }      
                
        }
    }