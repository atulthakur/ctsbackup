/**
 *      @author Simon Roggeman
 *      @date   13/11/2016
        @description    Class for processing Eloqua activities

        Modification Log:
        ------------------------------------------------------------------------------------------------------
        Developer                   Date                Description
        ------------------------------------------------------------------------------------------------------
        Simon Roggeman              13/11/2016          Original version
 */
global class JJ_Core_elq_PurgeActivities implements Database.Batchable<JJ_Core_elq_Activities__c>, Database.Stateful, Schedulable {
    
    //private members
    private Date errorDate;
    private Date successDate;
    private Integer deleteCount;
    private String notificationUserId;


    //constructor
    global JJ_Core_elq_PurgeActivities() {
    
        //get settings
        JJ_Core_elq_Eloqua_Integration_Settings__c settings = JJ_Core_elq_Eloqua_Integration_Settings__c.getInstance();

        //init
        this.notificationUserId = settings.JJ_Core_elq_Notification_User_Id__c;
        this.errorDate = Date.today().addDays(-(settings.JJ_Core_elq_ErrRecMaxAge__c).intValue());
        this.successDate = Date.today().addDays(-(settings.JJ_Core_elq_SuccessRecMaxAge__c).intValue());
        this.deleteCount = 0;
        
        System.debug('Delete errors before ' + errorDate);
        System.debug('Delete processed before ' + successDate);
    }

    //start - get all processed activities from staging object
    global Iterable<JJ_Core_elq_Activities__c> start(Database.BatchableContext BC) {
        return [
            select 
                Id, LastModifiedDate, JJ_Core_elq_Status__c, JJ_Core_elq_Activity_Date__c
            from JJ_Core_elq_Activities__c 
            where JJ_Core_elq_Status__c != 'New'];
    }

    global void execute(Database.BatchableContext BC, list<JJ_Core_elq_Activities__c> activities) {
        
        System.debug(activities.size() + ' records to process');
        
        list<JJ_Core_elq_Activities__c> activitiesForDelete = new list<JJ_Core_elq_Activities__c>();

   
        //1. throw out records for other sfdc instances (source system based)
        for(JJ_Core_elq_Activities__c activity : activities) {
            
            DateTime compareToDate = activity.JJ_Core_elq_Activity_Date__c;
            
            System.debug('Checking activity modified on ' + compareToDate + ' having status ' + activity.JJ_Core_elq_Status__c);
            
            if((activity.JJ_Core_elq_Status__c == 'Error' && compareToDate < errorDate) || (activity.JJ_Core_elq_Status__c == 'Processed' && compareToDate < successDate)) {
                System.debug('delete');
                activitiesForDelete.add(activity);
            } else {
                System.debug('\tdon\'t delete');
            }
        }
        
        System.debug(activitiesForDelete.size() + ' records to delete');

        //delete activities        
        delete activitiesForDelete;
        
        /*
        for(JJ_Core_elq_Activities__c activity : activitiesForDelete) {
            activity.JJ_Core_elq_Status__c = 'deleted';
        }
        
        update activitiesForDelete;
        */
        
        //increment error count
        deleteCount += activitiesForDelete.size();
    }

    global void finish(Database.BatchableContext BC){
        System.debug('Total deleted: ' + deleteCount );
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { this.notificationUserId };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Eloqua outbound integration - summary';
        message.plainTextBody = 'Hi,\n\nresults of today\'s cleanup run:\n\t- staging records deleted: ' + deleteCount + '\n\nKind regards,\nThe Eloqua integration team';
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
    
   global void execute(SchedulableContext sc) {
      JJ_Core_elq_PurgeActivities purgeActivitiesJob = new JJ_Core_elq_PurgeActivities(); 
      database.executebatch(purgeActivitiesJob);
   }
}