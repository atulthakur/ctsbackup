public class Grant_Controller {

    private ApexPages.StandardController controller {get; set;}
    private JJ_ANZ_Grant__c GrantRecordQueried{get;set;}
    public JJ_ANZ_Grant__c GrantRecord {get;set;}

    public Grant_Controller(ApexPages.StandardController controller) {

        this.controller = controller;
        GrantRecord = (JJ_ANZ_Grant__c)controller.getRecord();
        GrantRecordQueried = [select ID,createdbyid
                         from JJ_ANZ_Grant__c
                         where id =: GrantRecord.Id];
    }

    public PageReference cloneGrant() {

         Savepoint sp = Database.setSavepoint();
         JJ_ANZ_Grant__c GrantCloned;
         try {
             GrantCloned = GrantRecordQueried.clone(false,true);
             insert GrantCloned;
         } catch (Exception e){
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }
        return new PageReference('/'+GrantCloned.id);

    }

}