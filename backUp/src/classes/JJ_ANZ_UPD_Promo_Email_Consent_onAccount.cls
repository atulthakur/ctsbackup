/**
*      @author    Prabushanker Kumarasamy     
*      @date      24/11/2016  
       This class is used when the record is inserted or updated in Multichannel Consent object and does the following tasks:
        - Updates the value of Promotional Email Consent to 'Yes' or 'No' , Marketing Contact to true or false for the opt in and opt out scenario respectively.
        - Updates the value of Non Promotional Email Consent to 'Yes' if Promotional Email consent is 'Yes'
        - Updates the Channel Permission object fields - JJ_Core_Account__c, JJ_Core_Channel__c, JJ_Core_Permission__c
*/


public class JJ_ANZ_UPD_Promo_Email_Consent_onAccount
{
    @InvocableMethod
    public static void ChannelPermissionUPD(List<Id> mccIds)
    {
        List<Multichannel_Consent_vod__c> multiChannelConstent =[select id, Account_vod__c, CreatedDate, Capture_Datetime_vod__c,Opt_Type_vod__c From Multichannel_Consent_vod__c Where id in: mccIds];
        
        List<ID> AccIDs = new List<ID>();
        map<id,list<JJ_Core_Channel_Permissions__c>> channelpermissions=new map<id,list<JJ_Core_Channel_Permissions__c>>();
        map<id,list<Multichannel_Consent_vod__c>> AccountMCMap = new map<id,list<Multichannel_Consent_vod__c>>();
        list<JJ_Core_Channel_Permissions__c> ChpList=new list<JJ_Core_Channel_Permissions__c>();
        list<JJ_Core_Channel_Permissions__c> ChpToInsert=new list<JJ_Core_Channel_Permissions__c>();
        Map<Id,JJ_Core_Channel_Permissions__c> ChpToUpdate=new Map<Id,JJ_Core_Channel_Permissions__c>();
        for(Multichannel_Consent_vod__c oMcc :multiChannelConstent)
        {
          AccIDs.add(oMcc.Account_vod__c);
        }
        for(Multichannel_Consent_vod__c Mcc: [select id, Account_vod__c, CreatedDate, Capture_Datetime_vod__c From Multichannel_Consent_vod__c Where Account_vod__c in: AccIDs order by Capture_Datetime_vod__c desc])
        {
            if(!AccountMCMap.containsKey(Mcc.Account_vod__c))
            {  
            AccountMCMap.put(Mcc.Account_vod__c, new list<Multichannel_Consent_vod__c>{Mcc} );   
            } else 
            {
            AccountMCMap.get(Mcc.Account_vod__c). add(Mcc);            
            }   
        }
        
        for(JJ_Core_Channel_Permissions__c chp:[select Id,JJ_Core_Account__c,JJ_Core_Channel__c,JJ_Core_Permission__c,CreatedDate from JJ_Core_Channel_Permissions__c where JJ_Core_Account__c in:AccIDs and JJ_Core_Channel__c='Email' order by CreatedDate desc])
        {
            if(!channelpermissions.containsKey(chp.JJ_Core_Account__c))
            {  
            channelpermissions.put(chp.JJ_Core_Account__c, new list<JJ_Core_Channel_Permissions__c>{chp} );   
            } else 
            {
            channelpermissions.get(chp.JJ_Core_Account__c). add(chp);            
            }   
        }  
      
        List<Account> Accs = new List<Account>([select id, PersonEmail,JJ_ANZ_NonPromo_Email_Consent__c,JJ_ANZ_Promo_Email_Consent__c,JJ_ANZ_NonPromotional_Consent_Modified__c from Account where id in: AccIDs]);
        Map<Id,Account> AccsToUpdate=new Map<Id,Account>();  
      
        for(Multichannel_Consent_vod__c oMcc :multiChannelConstent)
        {
         for (integer i = 0; i < Accs.size(); i++){  
                list<Multichannel_Consent_vod__c> Mcs = new list<Multichannel_Consent_vod__c>();
                Mcs = AccountMCMap.get(Accs[i].id); 
                if(oMcc.id == Mcs[0].id){               
                if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'Yes';
                    
                    Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;
                    if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c == null){
                          Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes';
                         
                    }else{
                          
                          if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c < Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c){
                              
                              Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes';
                                                            
                          }
                    }
                    
                      
                    AccsToUpdate.put(Accs[i].id, Accs[i]);
                }else if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'No'; 
                    
                    Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;
                                         
                    AccsToUpdate.put(Accs[i].id, Accs[i]);
                }  
                }               
            }
            ChpList=channelpermissions.get(oMcc.Account_vod__c);
            if(ChpList==null)
            {
            JJ_Core_Channel_Permissions__c newchp=new JJ_Core_Channel_Permissions__c();
            newchp.JJ_Core_Account__c=oMcc.Account_vod__c;
            newchp.JJ_Core_Channel__c='Email';
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-in';
                }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-out';
                }
            ChpToInsert.add(newchp);
            }
            else
            {
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod')
                {
                    ChpList[0].JJ_Core_Permission__c= 'Opt-in';
                }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod')
                {
                    ChpList[0].JJ_Core_Permission__c= 'Opt-out';
                }         
             ChpToUpdate.put(ChpList[0].Id,ChpList[0]);
            }
        
        }
        
        if(!AccsToUpdate.isEmpty())
        {
            update AccsToUpdate.Values();
        }
        if(!ChpToInsert.isEmpty())
        {
            insert ChpToInsert;
        }
        if(!ChpToUpdate.isEmpty())
        {
            update ChpToUpdate.Values();
        }
    }
}