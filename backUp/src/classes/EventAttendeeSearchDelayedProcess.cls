/**
*   Created by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 20.03.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description: This class runs asynchronously. It is used on the Invitee tab in the Janssen Event Wizard. 
**/

public with sharing class EventAttendeeSearchDelayedProcess {
    
    @future (callout = true)
    public static void runProcess(String jEventID, Id userId, String action){
        List<JJ_Invitee_Search_Result__c> updateResults = new List<JJ_Invitee_Search_Result__c>();
        integer page = 1;
        integer recordCount = 0;
        
        if(action == 'add'){
            List<JJ_Invitee_Search_Result__c> inviteeResults = [select Id, JJ_Page_Number__c  
                                                                From JJ_Invitee_Search_Result__c
                                                                where OwnerId = :userId AND JJ_Janssen_Event_ID__c = :jEventID
                                                                AND JJ_Added__c = false AND JJ_Saved__c = false order by JJ_Account_Name__c asc];
            
            for(integer i = 0; i < inviteeResults.size(); i++){
                if(recordCount == 50){
                    page++;
                    recordCount = 0;
                }
                inviteeResults[i].JJ_Page_Number__c = page;
                
                updateResults.add(inviteeResults[i]);
                
                recordCount++;
            }
            if(updateResults.size() > 0){
                update updateResults;
            }
        }
        
        page = 1;
        recordCount = 0;
        updateResults = new List<JJ_Invitee_Search_Result__c>();
                          
        List<JJ_Invitee_Search_Result__c> existingResults = [select Id, JJ_Page_Number__c  From JJ_Invitee_Search_Result__c
                                                             where OwnerId = :userId AND JJ_Janssen_Event_ID__c = :jEventID
                                                             AND (JJ_Added__c = true OR JJ_Saved__c = true) order by JJ_Account_Name__c asc];
                               
        for(integer i = 0; i < existingResults.size(); i++){
            if(recordCount == 50){
                page++;
                recordCount = 0;
            }
            existingResults[i].JJ_Page_Number__c = page;
            
            updateResults.add(existingResults[i]);
            
            recordCount++;
        }
        
        if(updateResults.size() > 0){
            update updateResults;
        }
    }
    
    //@future (callout = true)
    public static void runDeleteProcess(String jEventID, Id userId, boolean isAfterApproved, boolean isConfirmAtt){
        Set<String> eventAttendeesRemovedId = new Set<String>();
        
        List<JJ_Invitee_Search_Result__c> deleteResultsList = [select Id, JJ_ID__c from JJ_Invitee_Search_Result__c where JJ_To_Remove__c = true 
                                                                   AND JJ_Janssen_Event_ID__c = :jEventID AND OwnerId = :userId]; 
            
        for(JJ_Invitee_Search_Result__c isr : deleteResultsList){
            eventAttendeesRemovedId.add(isr.JJ_ID__c);
        }
        
        if(eventAttendeesRemovedId.size() > 0){
            List<Event_Attendee_vod__c> deleteEventAttendeesList = [select Id from Event_Attendee_vod__c where JJ_ID__c in :eventAttendeesRemovedId];
            
            if(deleteEventAttendeesList.size() > 0){
                delete deleteEventAttendeesList;
            }
        }
        
        if(isAfterApproved == true || isConfirmAtt == true){
            List<JJ_Invitee_Search_Result__c> deleteSearchHistory = [select Id from JJ_Invitee_Search_Result__c where JJ_Janssen_Event_ID__c = :jEventID AND OwnerId = :userId];
            
            if(deleteSearchHistory.size() > 0){
                delete deleteSearchHistory;
            }
        }else{
            if(deleteResultsList.size() > 0){
                delete deleteResultsList;
            } 
        }
    }
}