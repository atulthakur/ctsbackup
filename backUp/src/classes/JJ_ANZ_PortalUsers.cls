Global class JJ_ANZ_PortalUsers{
 webservice static string Approval(id PUid, String country){
    
    JJ_ANZ_Portal_Users__c oPortalUsers = [SELECT id, JJ_ANZ_Email__c,Account_vod__c,JJ_ANZ_Opt_Type__c, JJ_ANZ_Verification_Status__c, JJ_ANZ_Verification_Type__c from JJ_ANZ_Portal_Users__c where id=:PUid];
    JJ_ANZ_Portal_Users_Portals_Associations__c portalsAssociation = [SELECT id, JJ_ANZ_Portals__r.JJ_ANZ_Description__c from JJ_ANZ_Portal_Users_Portals_Associations__c where JJ_ANZ_Portal_Users__c = :PUid];
    String portal = portalsAssociation.JJ_ANZ_Portals__r.JJ_ANZ_Description__c;
    if(portal != ''){
    oPortalUsers.JJ_ANZ_Portal_Approved__c = portal;
    }    
    oPortalUsers.JJ_ANZ_Verification_Status__c = 'Success' ;
    oPortalUsers.JJ_ANZ_Verification_Type__c = 'Manual Verification' ;
    update oPortalUsers;
    
    Map<id,Account> mapAccount= new Map<id,Account>([Select id,PersonEmail,JJ_Portal_Name__c from Account where id =:oPortalUsers.Account_vod__c]);
    JJ_ANZ_Portal_Users_Portals_Associations__c oPortalAssoc = [SELECT id, JJ_ANZ_Portals__c from JJ_ANZ_Portal_Users_Portals_Associations__c where JJ_ANZ_Portal_Users__c = :PUid];
    RecordType recType = [SELECT ID from RecordType  Where SobjectType = 'Multichannel_Consent_vod__c' and Name = 'Approved_Email_vod'];
    List<Account> updateAccounts = new List<Account>();
    
    Account acctoUpdate =mapAccount.get(oPortalUsers.Account_vod__c);
     acctoUpdate.PersonEmail = oPortalUsers.JJ_ANZ_Email__c;
     if(portal != ''){
     acctoUpdate.JJ_Portal_Name__c = portal;
     }
     updateAccounts.add(acctoUpdate);
     //update acctoUpdate;
    
    if(oPortalUsers.JJ_ANZ_Opt_Type__c==true)
    {               
               Multichannel_Consent_vod__c consent = new Multichannel_Consent_vod__c(
                   Account_vod__c = oPortalUsers.Account_vod__c,
                   Channel_Value_vod__c = oPortalUsers.JJ_ANZ_Email__c,
                   Opt_Type_vod__c = 'Opt_In_vod',
                   Signature_vod__c = '000000000',
                   JJ_ANZ_Portals__c = oPortalAssoc.JJ_ANZ_Portals__c,
                   Signature_Datetime_vod__c = DateTime.now(),
                   Capture_Datetime_vod__c =  DateTime.now(),
                   RecordTypeID = recType.ID
               );
               insert consent;
    }
    else{
    Multichannel_Consent_vod__c consent = new Multichannel_Consent_vod__c(
                   Account_vod__c = oPortalUsers.Account_vod__c,
                   Channel_Value_vod__c = oPortalUsers.JJ_ANZ_Email__c,
                   Opt_Type_vod__c = 'Opt_Out_vod',
                   Signature_vod__c = '000000000',
                   JJ_ANZ_Portals__c = oPortalAssoc.JJ_ANZ_Portals__c,
                   Signature_Datetime_vod__c = DateTime.now(),
                   Capture_Datetime_vod__c =  DateTime.now(),
                   RecordTypeID = recType.ID
               );
               insert consent;
    }
    
   update acctoUpdate;
    
try{ 
       if(test.isRunningTest())
       
       database.update(oPortalUsers); 
       return('User approved successfully.'); 
       }
        catch(DmlException e){
        return(e.getdmlMessage(0)); 
        }
    
    }
 
  webservice static String validateIfEmailExist(String emailID){
  
     String result = 'false';
     JJ_ANZ_Portal_Users__c oPortalUsers;
     try{
        oPortalUsers = [select Account_vod__c from JJ_ANZ_Portal_Users__c where JJ_ANZ_Verification_Status__c = 'Success' and JJ_ANZ_Email__c = :emailID];
     }catch(Exception e){
        oPortalUsers = null;
     }
     if(oPortalUsers != null){
        result = 'true';
     }
     return result;
  }
  
  webservice static string sendRequest(string PUID){
                
        Blob cryptoKey = Crypto.generateAesKey(256);
        String cryptoKeyString = EncodingUtil.base64Encode(cryptoKey);
        
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, Blob.valueOf(PUID));
        String encryptedDataString = EncodingUtil.base64Encode(encryptedData);
                
        String encryptedStringKey = cryptoKeyString + ':' + encryptedDataString;
        
        return encryptedStringKey; 
    }
    
    webservice static string getAccountCountry(string PUID){
        
        JJ_ANZ_Portal_Users__c oPortalUsers = [select Account_vod__c from JJ_ANZ_Portal_Users__c where ID = :PUID];
        String AccID = oPortalUsers.Account_vod__c;
        String country = '';
        Address_vod__c address;
        try{
        address = [select ID,Country_vod__c from Address_vod__c where Account_vod__c = :AccID and Primary_vod__c = true];
        }catch(Exception e){
            address = null;        
        }
        if(address != null){
         country = address.Country_vod__c;
        }
        return country;
        
    
    }
    
    webservice static string ValidateCountry(string PUId){
    
        JJ_ANZ_Portal_Users_Portals_Associations__c portalsAssociation;
        String portalCountry = '';
        try{
            portalsAssociation = [select ID, JJ_ANZ_Portals__r.JJ_ANZ_Country__c from JJ_ANZ_Portal_Users_Portals_Associations__c where JJ_ANZ_Portal_Users__c = :PUId];
        }catch(Exception e){
            portalsAssociation = null;
        }
        if(portalsAssociation != null){
            portalCountry = portalsAssociation.JJ_ANZ_Portals__r.JJ_ANZ_Country__c;
        }
        string accountCountry = '';
        string returnValue = '';
        accountCountry = getAccountCountry(PUId);
        if(portalCountry == '' || accountCountry == ''){
            returnValue = 'ERROR';
        } else if(portalCountry == accountCountry){
           returnValue = accountCountry;
        }
        return returnValue;
        
    }
    webservice static String Validateportal(string PUId){
    List<JJ_ANZ_Portal_Users_Portals_Associations__c> portalsAssociation = new List<JJ_ANZ_Portal_Users_Portals_Associations__c>();
    String portalCountry ='';
    try{
          portalsAssociation = [select ID, JJ_ANZ_Portals__r.JJ_ANZ_Description__c from JJ_ANZ_Portal_Users_Portals_Associations__c where JJ_ANZ_Portal_Users__c = :PUId];
        }catch(Exception e){
            portalsAssociation = null;
        }
        if(portalsAssociation.size()>0){
        for(JJ_ANZ_Portal_Users_Portals_Associations__c pupa: portalsAssociation){
        if(pupa.JJ_ANZ_Portals__r.JJ_ANZ_Description__c == Label.TRINZAPFPPortalUser)
            portalCountry = pupa.JJ_ANZ_Portals__r.JJ_ANZ_Description__c;
            }
            return portalCountry;
        }
        return portalCountry;
    }
         
 }