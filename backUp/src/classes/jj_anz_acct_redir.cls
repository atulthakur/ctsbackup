public class jj_anz_acct_redir {

List<account> accts;

public jj_anz_acct_redir() {

String extId = apexpages.currentpage().getparameters().get('extId');
if (extId != null) {
   accts = [select Id from account where external_ID_vod__c = :extId];
}
}

public PageReference redir() {

if (accts.size()==1) {
   PageReference p = new PageReference('/'+accts[0].Id);
   p.setredirect(true);
   return p;
} else {
   apexpages.addmessage(new apexpages.message(apexpages.severity.error, 'account is not found'));
   return null;
}

}

}