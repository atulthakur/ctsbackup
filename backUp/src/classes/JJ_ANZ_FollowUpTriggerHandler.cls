/** Subject  : JJ_ANZ_FollowUpTriggerHandler

Description  : Class to handle all the calls from Case Product Trigger
Author       : Atul Thakur || athaku11@its.jnj.com||26 May 2016

**/

public with sharing class JJ_ANZ_FollowUpTriggerHandler
{
    String value=System.Label.CaseClosed;
     
     
    public void OnBeforeUpdate(List<JJ_ANZ_Follow_Up_Version__c> oldFollowUp,List<JJ_ANZ_Follow_Up_Version__c> newFollowUp,Map<id,JJ_ANZ_Follow_Up_Version__c> oldMapFollowUp)
    {
        
        Case caseStatus;    
        Set<Id> caseIDs= new Set<Id>();    

        for (JJ_ANZ_Follow_Up_Version__c fuv : oldFollowUp) 
        {
            if(fuv.JJ_ANZ_Case__c != null)
            {
            caseIDs.add(fuv.JJ_ANZ_Case__c);
            }
        }

    Map<Id, Case> caseEntries = new Map<Id, Case>(
        [select status from Case where id in :caseIDs]
        );

    for (JJ_ANZ_Follow_Up_Version__c fuv : newFollowUp) 
      {
        if(fuv.JJ_ANZ_Case__c != null)
         {
           caseStatus = caseEntries.get(fuv.JJ_ANZ_Case__c);
         }
        if (caseStatus != null && caseStatus.status==value) 
         {
           fuv.addError('Record cannot be updated after Case is closed');
         }
      }
    } 
       
    public void OnBeforeDelete(List<JJ_ANZ_Follow_Up_Version__c> oldFollowUp,Map<id,JJ_ANZ_Follow_Up_Version__c> oldMapFollowUp)
    {
       Case caseStatus;
       Set<Id> caseIDs= new Set<Id>();    

      for (JJ_ANZ_Follow_Up_Version__c fuv : oldFollowUp) 
        {
            if(fuv.JJ_ANZ_Case__c != null) 
            {
            caseIDs.add(fuv.JJ_ANZ_Case__c);
            }
        }

            Map<Id, Case> caseEntries = new Map<Id, Case>(
            [select status from Case where id in :caseIDs AND status=:value]
              );

            for(JJ_ANZ_Follow_Up_Version__c fuv : oldFollowUp) 
            {
                if(!caseEntries.isEmpty() && caseEntries !=null)
                {
                    JJ_ANZ_Follow_Up_Version__c errObj = oldMapFollowUp.get(fuv.Id);
                    if(errObj != null)
                    { 
                     errObj.addError('Record cannot be deleted after Case is closed');
                    }
                }
            }
    }
}