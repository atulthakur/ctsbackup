/** Subject  : JJ_CaseProduct_Updates

Description  : Class to handle all the calls from Case Product Trigger
Author       : Atul thakur  || athaku11@its.jnj.com || 25/01/2017 

 **/

Public with sharing class JJ_CaseProduct_Updates
{    
    Public static String CaseObject = 'Case';
    Public static String ParentCase = 'Case Information';
    Public static String PQCCase = 'Product Quality Complaints';
    Public static String AECase = 'Adverse Event';
    Public static String MIRCase = 'Medical Enquiry';
    Public static String caseProductTemp='';
    
    public static void removeCountry(List<JJ_Case_Product__c> OnBeforeInsertList){

        for(JJ_Case_Product__c caseProduct: OnBeforeInsertList ){

            if(caseProduct.JJ_ANZ_Detail_Product__c !=null || caseProduct.JJ_ANZ_Other_Product__c !=null){
                String ProdName = caseProduct.JJ_ANZ_Product__c;
                
                if(ProdName.contains('-'))
                {

                    ProdName=ProdName.substring(0,ProdName.indexOf('-'));
                }

                if(ProdName.contains(' AU')){               
                    caseProduct.JJ_ANZ_Product_Name__c= ProdName.replace(' AU',''); 
                }
                else if(ProdName.contains(' NZ')){
                    caseProduct.JJ_ANZ_Product_Name__c= ProdName.replace(' NZ',''); 
                }
                else{
                    caseProduct.JJ_ANZ_Product_Name__c= ProdName;
                }
            }
        }  
    }

    Public static void stampChildCaseID(List<JJ_Case_Product__c> OnBeforeInsertList){
        List<Id> parentCaseIds = new List<Id>();
        List<JJ_Case_Product__c> updateCases = new List<JJ_Case_Product__c>();

        for(JJ_Case_Product__c caseProduct: OnBeforeInsertList ){
            if(caseProduct.JJ_ANZ_Sup_Case__c!=null){
                parentCaseIds.add(caseProduct.JJ_ANZ_Sup_Case__c);
            }
        }

        List<Case> listProducts = [Select id,ParentId,RecordTypeId,Origin from Case where ParentId in:parentCaseIds and (RecordType.Name='Product Quality Complaints' or RecordType.Name='Adverse Event' or RecordType.Name='Medical Enquiry') ];
        Map<Id,List<Case>> mapCases = new Map<Id,List<Case>>();

        for(Case c : listProducts ){

            if(mapCases.containsKey(c.ParentId)){
                mapCases.get(c.ParentId).add(c);
            }
            else{
                mapCases.put(c.ParentId, new  List <Case> { c });
            }
        }           

        for(JJ_Case_Product__c  caseProduct : OnBeforeInsertList){

            if(caseProduct.JJ_ANZ_Sup_Case__c!=null&&mapCases.ContainsKey(caseProduct.JJ_ANZ_Sup_Case__c)){

                List<Case> casesList = mapCases.get(caseProduct.JJ_ANZ_Sup_Case__c);
                if(!casesList.isEmpty()){
                    for(Case childCase : casesList){
                        if(childCase.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase).getRecordTypeId()){
                            caseProduct.JJ_ANZ_AEChildCase__c = childCase.Id;
                        }
                        else if(childCase.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PQCCase).getRecordTypeId()){
                            caseProduct.JJ_ANZ_ChildCase__c = childCase.Id;
                        }
                        else if(childCase.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(MIRCase).getRecordTypeId()){
                            caseProduct.JJ_ANZ_MIRChildCase__c= childCase.Id;
                        }
                    }
                }
            }
        }
    }

    public static void removeProductFromCase(List<JJ_Case_Product__c> onBeforeDeleteList){   

        List<Id> ProductIds = new List<Id>();
        List<Id> CaseIds = new List<Id>();
        String TemplateproductName;
        String TemplateproductNameWithOneFrontSideBar;
        String cpProductName;
        String cpProductNameWithOneFrontSideBar2;
        Map<Id,Id> MapCasesWithProducts = new Map<Id,Id>();

        for(JJ_Case_Product__c caseProduct: onBeforeDeleteList){
        String BeforeCaseProduct= caseProduct.JJ_ANZ_Product__c;
        if(BeforeCaseProduct !=null){
            if(BeforeCaseProduct.contains(' AU')){               
                    BeforeCaseProduct= BeforeCaseProduct.replace(' AU',''); 
                  System.debug('Prodcut Name = ' +BeforeCaseProduct);
                }
                else if(BeforeCaseProduct.contains(' NZ')){
                    BeforeCaseProduct= BeforeCaseProduct.replace(' NZ',''); 
                  //System.debug('Prodcut Name = ' +caseProduct.JJ_ANZ_Product_Name__c);
                }else{
                    BeforeCaseProduct= caseProduct.JJ_ANZ_Product__c;
                }
                }

            if(caseProduct.JJ_ANZ_Sup_Case__c !=null){

                CaseIds.add(caseProduct.JJ_ANZ_Sup_Case__c);

                if(caseProduct.JJ_ANZ_Detail_Product__c !=null){
                    TemplateproductName=caseProduct.JJ_ANZ_Product_Name__c.toUpperCase()+'®';            
                    TemplateproductNameWithOneFrontSideBar='||'+TemplateproductName;
                    
                    cpProductName=BeforeCaseProduct.toUpperCase()+'®';            
                    cpProductNameWithOneFrontSideBar2='||'+cpProductName;
                    System.debug(+TemplateproductName+' '+TemplateproductNameWithOneFrontSideBar+' '+cpProductNameWithOneFrontSideBar2+' '+cpProductName);
                }
                else if(caseProduct.JJ_ANZ_Other_Product__c !=null){
                    TemplateproductName=caseProduct.JJ_ANZ_Product_Name__c.toUpperCase();            
                    TemplateproductNameWithOneFrontSideBar='||'+TemplateproductName;  
                    
                    cpProductName=BeforeCaseProduct.toUpperCase();            
                    cpProductNameWithOneFrontSideBar2='||'+cpProductName;  
                }
                
            } 
        }

        Map<Id,Case> MapCases = new Map<Id,Case>([Select id,JJ_ANZ_CaseProducts__c,JJ_CaseProductTemplate__c,JJ_ANZ_Active_Ingredient__c ,JJ_ANZ_Strength__c,JJ_ANZ_Dose__c,JJ_ANZ_Formulation__c,JJ_ANZ_Cost_Centre_Number__c from Case where id in:CaseIds]);
        List<Case> updateCases = new List<Case>();

        for(JJ_Case_Product__c  cp : onBeforeDeleteList){

            Case updateCase = MapCases.get(cp.JJ_ANZ_Sup_Case__c);

            if(updateCase.JJ_ANZ_CaseProducts__c!=null){
                String updateCaseProducts=updateCase.JJ_ANZ_CaseProducts__c;
                String updateTemplateCaseProducts=updateCase.JJ_CaseProductTemplate__c;

                if(updateCaseProducts.contains(cpProductNameWithOneFrontSideBar2)){
                    
                    updateCase.JJ_ANZ_CaseProducts__c= updateCaseProducts.replace(cpProductNameWithOneFrontSideBar2,'');
                    
                }
                else if(updateCaseProducts.contains(cpProductName)){
                    
                    updateCase.JJ_ANZ_CaseProducts__c= updateCaseProducts.replace(cpProductName,'');
                }
                if(updateTemplateCaseProducts !=null)
                {
                if(updateTemplateCaseProducts.contains(TemplateproductNameWithOneFrontSideBar))
                {
                updateCase.JJ_CaseProductTemplate__c= updateTemplateCaseProducts.replace(TemplateproductNameWithOneFrontSideBar,'');
                }
                else if(updateTemplateCaseProducts.contains(TemplateproductName))
                {
                updateCase.JJ_CaseProductTemplate__c= updateTemplateCaseProducts.replace(TemplateproductName,'');
                }
                }
                
            }
            updateCases.add(updateCase);
        }

        if(!updateCases.isEmpty()){
            try{
                System.debug('Updating Case Information '+updateCases);
                update updateCases;
            }
            catch(DMLException de){
                System.debug('DMLException occurred '+de);
            }
            catch(Exception e){
                System.debug('Exception occurred '+e);
            }
        }
    }

    public static void stampingDataToParentCase(List<JJ_Case_Product__c> OnAfterInsertList){

        List<Id> ProductIds = new List<Id>();
        List<Id> CaseIds = new List<Id>();
        Map<Id,Id> MapCasesWithProducts = new Map<Id,Id>();


        for(JJ_Case_Product__c  cp : OnAfterInsertList){
            if(cp.JJ_ANZ_Detail_Product__c!=null || cp.JJ_ANZ_Other_Product__c !=null){
                ProductIds.add(cp.JJ_ANZ_Detail_Product__c);
            }   
            CaseIds.add(cp.JJ_ANZ_Sup_Case__c);
        }

        Map<Id,JJ_ANZ_MAFOPS_Product__c> MapProducts = new Map<Id,JJ_ANZ_MAFOPS_Product__c>([Select id,Name,JJ_ANZ_Active_Ingredient__c,JJ_ANZ_Strength__c,JJ_ANZ_Dose__c,JJ_ANZ_Formulation__c,JJ_ANZ_Cost_Centre_Number__c  from JJ_ANZ_MAFOPS_Product__c where id in : ProductIds]);
        Map<Id,Case> MapCases = new Map<Id,Case>([Select id,JJ_ANZ_CaseProducts__c,JJ_CaseProductTemplate__c,JJ_ANZ_Checking_VeevaMI__c,Origin,JJ_ANZ_Active_Ingredient__c ,JJ_ANZ_Strength__c,JJ_ANZ_Dose__c,JJ_ANZ_Formulation__c,JJ_ANZ_Cost_Centre_Number__c from Case where id in:CaseIds]);
        List<Case> updateCases = new List<Case>();

        for(JJ_Case_Product__c  cp : OnAfterInsertList){

            Case updateCase = MapCases.get(cp.JJ_ANZ_Sup_Case__c);
            if(MapProducts.size()>0){

                if(cp.JJ_ANZ_Detail_Product__c!=null&&MapProducts.containsKey(cp.JJ_ANZ_Detail_Product__c)&&cp.JJ_ANZ_Sup_Case__c!=null){

                    JJ_ANZ_MAFOPS_Product__c  prod = MapProducts.get(cp.JJ_ANZ_Detail_Product__c);  

                    if(updateCase.JJ_ANZ_Active_Ingredient__c!=null){
                        updateCase.JJ_ANZ_Active_Ingredient__c=updateCase.JJ_ANZ_Active_Ingredient__c+'||'+prod.JJ_ANZ_Active_Ingredient__c;
                    }
                    else{
                        updateCase.JJ_ANZ_Active_Ingredient__c=prod.JJ_ANZ_Active_Ingredient__c;
                    }
                    if(updateCase.JJ_ANZ_Strength__c !=null){
                        updateCase.JJ_ANZ_Strength__c=updateCase.JJ_ANZ_Strength__c+'||'+prod.JJ_ANZ_Strength__c;
                    }
                    else{
                        updateCase.JJ_ANZ_Strength__c=prod.JJ_ANZ_Strength__c;
                    }
                    if(updateCase.JJ_ANZ_Dose__c !=null){
                        updateCase.JJ_ANZ_Dose__c=updateCase.JJ_ANZ_Dose__c+'||'+prod.JJ_ANZ_Dose__c;
                    }
                    else{
                        updateCase.JJ_ANZ_Dose__c=prod.JJ_ANZ_Dose__c;
                    }  
                    if(updateCase.JJ_ANZ_Cost_Centre_Number__c !=null){
                        updateCase.JJ_ANZ_Cost_Centre_Number__c=updateCase.JJ_ANZ_Cost_Centre_Number__c +'||'+prod.JJ_ANZ_Cost_Centre_Number__c ;
                    }
                    else{
                        updateCase.JJ_ANZ_Cost_Centre_Number__c=prod.JJ_ANZ_Cost_Centre_Number__c ;
                    }
                    if(updateCase.JJ_ANZ_Formulation__c !=null){
                        updateCase.JJ_ANZ_Formulation__c=updateCase.JJ_ANZ_Formulation__c+'||'+prod.JJ_ANZ_Formulation__c; 
                    }
                    else{
                        updateCase.JJ_ANZ_Formulation__c=prod.JJ_ANZ_Formulation__c; 
                    }
                }
            }

            if(cp.JJ_ANZ_Product__c!=null&& cp.JJ_ANZ_Sup_Case__c!=null){

                caseProductTemp= cp.JJ_ANZ_Product__c;
                String ProdName = cp.JJ_ANZ_Product_Name__c;

                 if(caseProductTemp.contains(' AU') ){               
                    caseProductTemp= caseProductTemp.replace(' AU','');             
                }
                else if(caseProductTemp.contains(' NZ')){
                    caseProductTemp= caseProductTemp.replace(' NZ',''); 
                }

                if(updateCase.origin !='Veeva MI'){
                    if(updateCase.JJ_ANZ_CaseProducts__c!=null && updateCase.JJ_CaseProductTemplate__c!=null){
                        if(cp.JJ_ANZ_Detail_Product__c !=null)
                        {
                            updateCase.JJ_ANZ_CaseProducts__c = updateCase.JJ_ANZ_CaseProducts__c.toUpperCase()+'||'+ caseProductTemp.toUpperCase()+'®';
                            updateCase.JJ_CaseProductTemplate__c= updateCase.JJ_CaseProductTemplate__c.toUpperCase()+'||'+ ProdName.toUpperCase()+'®';
                            System.debug('it is in top concantenate field');
                        }else
                        {
                            updateCase.JJ_ANZ_CaseProducts__c = updateCase.JJ_ANZ_CaseProducts__c.toUpperCase()+'||'+ caseProductTemp.toUpperCase();
                            updateCase.JJ_CaseProductTemplate__c= updateCase.JJ_CaseProductTemplate__c.toUpperCase()+'||'+ ProdName.toUpperCase();
                        
                        }
                    }
                    else{
                        if(cp.JJ_ANZ_Detail_Product__c !=null)
                        {
                            updateCase.JJ_ANZ_CaseProducts__c = caseProductTemp.toUpperCase()+'®';
                            updateCase.JJ_CaseProductTemplate__c= ProdName.toUpperCase()+'®';
                            System.debug('it is newly created');
                        }
                        else{
                            updateCase.JJ_ANZ_CaseProducts__c = caseProductTemp.toUpperCase();
                            updateCase.JJ_CaseProductTemplate__c= ProdName.toUpperCase();
                        } 
                    }
                } 

                if(updateCase.origin =='Veeva MI' && updatecase.JJ_ANZ_Checking_VeevaMI__c==True ){ 

                    if(updateCase.JJ_ANZ_CaseProducts__c!=null && updateCase.JJ_CaseProductTemplate__c!=null){
                        if(cp.JJ_ANZ_Detail_Product__c !=null)
                        {
                            updateCase.JJ_ANZ_CaseProducts__c =updateCase.JJ_ANZ_CaseProducts__c+'||'+caseProductTemp.toUpperCase()+'®';
                            updateCase.JJ_CaseProductTemplate__c= updateCase.JJ_CaseProductTemplate__c+'||'+ProdName.toUpperCase()+'®';
                        }
                        else{
                            updateCase.JJ_ANZ_CaseProducts__c =updateCase.JJ_ANZ_CaseProducts__c+'||'+caseProductTemp.toUpperCase();
                            updateCase.JJ_CaseProductTemplate__c= updateCase.JJ_CaseProductTemplate__c+'||'+ProdName.toUpperCase();
                        }
                    }
                }

                if(updateCase.origin =='Veeva MI' && updatecase.JJ_ANZ_Checking_VeevaMI__c==False){
                    if(cp.JJ_ANZ_Detail_Product__c !=null)
                    {
                        updateCase.JJ_ANZ_CaseProducts__c = caseProductTemp.toUpperCase()+'®';
                        updateCase.JJ_CaseProductTemplate__c= ProdName.toUpperCase()+'®';
                        updatecase.JJ_ANZ_Checking_VeevaMI__c=True;
                    }
                    else{
                        updateCase.JJ_ANZ_CaseProducts__c = caseProductTemp.toUpperCase();
                        updateCase.JJ_CaseProductTemplate__c = ProdName.toUpperCase();
                        updatecase.JJ_ANZ_Checking_VeevaMI__c=True;
                    }
                }
            }
            updateCases.add(updateCase);
        }

        if(!updateCases.isEmpty()){
            try{
                System.debug('Updating Case Information '+updateCases);
                update updateCases;
            }
            catch(DMLException de){
                System.debug('DMLException occurred '+de);
            }
            catch(Exception e){
                System.debug('Exception occurred '+e);
            }
        }
    }   
}