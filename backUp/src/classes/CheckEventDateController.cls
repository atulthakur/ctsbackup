public with sharing class CheckEventDateController {
	
	String eventID;
	String eventDate;
	public boolean eventIsExpired {get; set;}
	public boolean eventConAtt {get; set;}
	public boolean eventLocked {get; set;}
	
	public CheckEventDateController(){
		eventID = apexPages.currentPage().getParameters().get('ApprovedEventId'); 
		eventDate = apexPages.currentPage().getParameters().get('EventDate');
		
		eventIsExpired = false;
		eventConAtt = false;
		eventLocked = false;
	}
	
	public pageReference redirectPage(){
		pageReference pg = null;
		
		if(eventID != null){
			List<Medical_Event_vod__c> janssenEvent = [select Id, Start_Date_vod__c, JJ_Event_Post_Actions_Completed__c from Medical_Event_vod__c where Id = :eventID];
			
			if(janssenEvent.size() > 0){
				system.debug('===================janssenEvent[0].Start_Date_vod__c======================' + janssenEvent[0].Start_Date_vod__c);
				system.debug('===================eventDate======================' + eventDate);
				if(janssenEvent[0].JJ_Event_Post_Actions_Completed__c == true){
					eventLocked = true;
				}else{
					if(janssenEvent[0].Start_Date_vod__c <= date.today() && eventDate == null){
						eventIsExpired = true;
					}else if(janssenEvent[0].Start_Date_vod__c <= date.today() && eventDate != null){
						pg = new pageReference('/apex/Event_Add_Invitee?ApprovedEventId=' + eventID + '&EventDate=' + janssenEvent[0].Start_Date_vod__c);
					}else if(janssenEvent[0].Start_Date_vod__c >= date.today() && eventDate == null){
						pg = new pageReference('/apex/Event_Add_Invitee?ApprovedEventId=' + eventID);
					}else if(janssenEvent[0].Start_Date_vod__c >= date.today() && eventDate != null){
						eventConAtt = true;
					}
				}
			}
		}
		system.debug('===================pg======================' + pg);
		
		return pg;
	}
	
	public pageReference goBack(){
		pageReference pg = new pagereference('/' + eventID);
		
		return pg;
	}
}