/**
 *      @author Logeswaran Udayakumar
 *      @date   05/23/2013
        @description    Validate Cordis PC records on submit & send email to CHSS Team
 
        Function: Validation controller & Outbound email to CHSS
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Logeswaran Udayakumar           05/23/2013          Original Version
        Navin Rai                       06/28/2013          Modified as per requirement
        Logeswaran Udayakumar           07/29/2013          Implemented Suspected Drug & Concomitant Medication object related changes   
        Rahul Yadav                     08/14/2013          Modified the SOQL for querieg case owner.                                                       
**/
global class JJ_PAROrder{
    
    // Class variables declaration
    public case c {get;set;}                                            // Case record variable - standard controller
    private case rCase;                                                 // Case record variable - custom controller
    public String caseId {get;set;}                                     // Case Id string variable  
    public String instId {get;set;}                                     // Institution Id - for fetching Institution related address information  
    private Id caseRecTypeId;                                           // Case recordType variable
   //public List<JJ_Suspected_Drug__c> sProdList {get;set;}              // Suspected Drug list variable
    //public List<JJ_Concomitant_Medication__c> cProdList {get;set;}      // Concomitant Medication list variable
                                         // targetObjectId variable
    private String chssTemplate = '';                                   // CHSS Email template variable
    private Map<String,String> cDocImgUrlMap;                           // Map<document Name,Image Url> collection variable
    private static List<Document> cImgDocList;                          // List of documents - for images
    private String imgFolder = '';                                      // Image folder id variable
    private String baseUrl = '';                                        // Salesforce.com base url variable
    private String instanceServer = '';                                 // Salesforce.com current server name variable    
    private String orgId = '';                                          // Current saleforce.com organization Id
    private Address_vod__c rAddress;                                    // Institution related address record variable
    public Boolean isSent{get;set;}
    
    // Constructor - standard controller
    public JJ_PAROrder(ApexPages.StandardController controller) {       
        
        // Initialize variables
        isSent = true;
        
        // Fetch case record in context
        this.c = (case)controller.getrecord();
        
        // Fetch case recordType in context
        caseRecTypeId = c.recordtypeID ;  
        
        baseUrl = String.valueOf(URL.getSalesforceBaseUrl());       
        instanceServer = baseUrl.substringAfter('.');
        instanceServer = instanceServer.substringBefore('.');       
        orgId = UserInfo.getOrganizationId(); 
    }
    
   
      
    /*
     * Method name     - getcoCase() 
     * @param name     - N/A
     * @param type     - N/A
     * @Description    - To retrieve & set case field values in the  form.
    */   
    public Case getcoCase(){
        if(caseId != '' && caseId != null){
            rCase = [SELECT JJ_ReferenceId__c,JJ_ANZ_Patient_Initials__c ,AccountId,CaseNumber,ContactEmail,ContactId,ContactPhone,Id,JJ_ANZ_Case_Id__c,JJ_ANZ_ChildCaseNumber__c,
                    JJ_ANZ_EmailTemplateLink__c,JJ_ANZ_Email__c,JJ_ANZ_Name__c,JJ_ANZ_Name__r.name,JJ_ANZ_New_Email__c,JJ_ANZ_Other_Contact_Email__c,JJ_ANZ_Reporter_Type__c,JJ_ANZ_Source__c,
                    JJ_ANZ_Status__c,JJ_ANZ_Title__c,JJ_Contact_Name__c,JJ_Contact_Phone__c,JJ_Email_1__c,JJ_Email_2__c,JJ_Email_3__c,JJ_Location_name__c,JJ_PAR_Case_Product__c,
                    JJ_Product_Strength_1__c,JJ_Product_Strength_2__c,JJ_Quantity_1__c,JJ_Quantity_2__c,OwnerId,RecordTypeId,Status,Subject FROM Case  Where Id =:caseId];                                   
        }
        return rCase;
    }
    
   
       
        
    /*
     * Method name     - sendmail()
     * @param name     - N/A
     * @param type     - N/A
     * @Description    - To send email to  team for  PAR records
    */    
    webservice static void sendmail(id caseId){  
        case c=new case();
         String conEmail = '',chssTemplate ;
         case rCase=new case();    
                c=[SELECT JJ_ReferenceId__c,AccountId,CaseNumber,ContactEmail,ContactId,ContactPhone,Id,JJ_ANZ_Case_Id__c,JJ_ANZ_ChildCaseNumber__c,
                    JJ_ANZ_EmailTemplateLink__c,JJ_ANZ_Email__c,JJ_ANZ_Date_of_Birth__c ,JJ_ANZ_Name__c,JJ_ANZ_Name__r.name,JJ_ANZ_New_Email__c,JJ_ANZ_Other_Contact_Email__c,JJ_ANZ_Reporter_Type__c,JJ_ANZ_Source__c,
                    JJ_ANZ_Status__c,JJ_ANZ_Title__c,JJ_Contact_Name__c,JJ_Contact_Phone__c,JJ_Email_1__c,JJ_Email_2__c,JJ_Email_3__c,JJ_Location_name__c,JJ_PAR_Case_Product__c,
                    JJ_Product_Strength_1__c,JJ_ANZ_Patient_Initials__c ,JJ_Product_Strength_2__c,JJ_Quantity_1__c,JJ_Quantity_2__c,OwnerId,RecordTypeId,Status,Subject FROM Case  Where Id =:caseId];  
              Id caseRecTypeId = c.recordtypeID ;  
        
      
                  
//        Id cordisPcRecTypeId = JJ_StaticConstants.CaseRecTypeMapByName.get(JJ_StaticConstants.RTYPE_CORDIS_PC).getRecordTypeId();
        //system.debug('Check mail$$$'+JJ_StaticConstants.mailSent); 
        //if(!JJ_StaticConstants.mailSent){
            // Check the record in context is Cordis PC record type        
  //          if(caseRecTypeId == cordisPcRecTypeId){
                
                // Fetch Private contact to set targetObjectId
                list<contact> cons= new list<contact>([select Id from Contact where Name = 'PAR Contact' AND Email ='schand63@its.jnj.com' limit 1]);
                
                // Assign private Contact Id to intermediate String variable - To set targetObjectId
                if(cons!=null && !cons.isempty()){
                    conEmail = cons[0].Id;
                }

                

                    
                // Fetch CHSS template to set email template
                chssTemplate = [select Id from EmailTemplate where Name = 'PAR Order Notification update' limit 1].Id;
                    system.debug('Template'+chssTemplate );

                
                Contact conNew;     // Contact variable - if no private contact found for targetObjectId
                if(conEmail == '' || conEmail == null){
                    // Create New private contact once if there is no private contact records with CHSS email in the system.            
                    conNew = new Contact(LastName = 'PAR Contact',Email = 'SChand63@its.jnj.com');                    
                    try{
                        insert conNew;
                    }catch(exception e){
                         //log error if there is any exception
                        //JJ_ErrorLog eLog=new JJ_ErrorLog('Submit CHS', 'Exception: '+e.getMessage());  
                        //eLog.LogException();  
                    }               
                }
                
                //Temporary CC Address - For Testing purpose
                List<String> ccAddrList = new List<String>();  // cc address list variable
                String ccAddr = '';                            // Intermediate variable - For cc address
                
                // Fetching current logged-in User
                User u = [Select Id, Email from User Where Id =:Userinfo.getUserId()];
                ccAddr = u.Email;
                ccAddrList.add(ccAddr);
                
                //Instantiating Messaging class - For sending mail to CHSS
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                // Set mail attributes
                if(conEmail != '' && conEmail != null){
                    mail.setTargetObjectId(conEmail);
                    mail.setTreatTargetObjectAsRecipient(false);
                }else{
                    mail.setTargetObjectId(conNew.Id);
                }
                
                // Private contact set targetObjectId
                    mail.setTreatTargetObjectAsRecipient(false);
                    
    
                //CC Address - For Testing Purpose
                if(!ccAddrList.isEmpty()){
                    mail.setCcAddresses(ccAddrList);  
                }            
                mail.setTemplateId(chssTemplate);
                mail.setWhatId(c.id);
                mail.setReplyTo(System.Label.JJ_EmailtoCase);

                // Specify the name used as the display name. 
                mail.setSenderDisplayName('PAR Case INFO');   
                try
                {
                    // Invoke Email to CHSS

                   EmailTemplate ET = [Select id,Body, HtmlValue,Subject FROM EmailTemplate Where Id =: chssTemplate LIMIT 1];  
                    System.Debug('mail.getHtmlBody()--------'+mail.getPlainTextBody());                                 
                   String emailBody = ET.Body;
                   EmailMessage em = new EmailMessage();
                                   em.subject = ET.subject;
                     
                                   em.TextBody = mail.getPlainTextBody();
                                   //em.HtmlBody = ET.htmlValue;
                                   //em.BccAddress = 'PATnotifications@janau.jnj.com'+';'+'ra-jnjau_custserv@its.jnj.com';
                                   em.CcAddress = c.JJ_Email_2__c != null ? c.JJ_Email_2__c:'' +';'+ c.JJ_Email_3__c != null ? c.JJ_Email_2__c:'';
                                   em.ToAddress = c.JJ_Email_1__c ;
                                   em.ParentId = c.id;
                                    
                    insert em;

                   // mail.setInReplyTo(em.Id);
                   //mail.setReferences('ref:'+(string)em.id);
                                   
                    Messaging.SendEmailResult[] emailSendVar = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });                                   
                    
                    System.Debug('emailSendVar --------'+emailSendVar );
                    // Update Email transmission date field in associated case record
                    //c.JJ_Email_Transmission_datetime__c = System.now();  
                    c.Status = 'Ordered'; 
                    c.JJ_Date_Order_placed__c = system.now();               
                    update c;
                    
                    //JJ_StaticConstants.mailSent = true;
                }catch(exception e){
                    //log error if there is any exception
                    //JJ_ErrorLog eLog=new JJ_ErrorLog('Submit ', 'Exception: '+e.getMessage());  
                    //eLog.LogException();
                    //return null;
                }                                   
            
        //}
        // Navigate to associated case record
        
     pagereference p = new pagereference('https://iconnect-anz--midevelop.cs72.my.salesforce.com/?tsid='+c.id);        
   
       //p.setRedirect(true);
  
       //return p;
        }
    }