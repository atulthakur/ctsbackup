/** Subject  : JJ_ANZ_CaseTriggerHandler

Description  : Class to handle all the calls from Case Trigger
Author       : Mahesh Somineni || msominen@its.jnj.com||1/14/2016
Conga changes and Korea changes
Author       : Atul thakur || athaku11@its.jnj.com||1/1/2017
Description  : Class to handle all the calls from Case Trigger for PAR Case
Author       : Sunita & Shanmuga || schand63@its.jnj.com & SPriyan3@its.jnj.com ||30/11/2017

**/
public with sharing class JJ_ANZ_CaseTriggerHandler {

    private boolean m_isExecuting = false;
    Public static String Checked = 'Yes';
    Public static Boolean UpdateMIRecordscheck = true;
    Public static String LSO = 'LSO';
    Public static String PAR = 'PAR';
    Public static String Phone= 'Phone';
    Public static String Email = 'Email';
    Public static String VeevaMI = 'Veeva MI';
    Public static String newCaseStatus = 'New';
    Public static String assessedCaseStatus = 'Assessed';
    Public static String FollowUpCaseStatus = 'Follow-up';
    Public static String AssignedCaseStatus = 'Assigned';
    Public static String CaseObject = 'Case';
    Public static String ParentCase = 'Case Information';
    Public static String PhoneCase = 'Phone Case Entry';
    Public static String PQCCase = 'Product Quality Complaints';
    Public static String AECase = 'Adverse Event';
    Public static String PARCase = 'Product Access Request';
    Public static String ApprovedCase = 'Approved';
    Public static String PendingCase = 'Pending';
    Public static String MIRCase  ='Medical Enquiry';
    Public Static String GICase = 'General Inquiry';
    Public Static String ClosedMIRCase = 'Closed Medical Enquiry';
    Public Static String ClosedGICase = 'Closed General Inquiry';
    Public static String SecondaryReview = 'Secondary Review';
    
    //Get Record Type Ids
    Public static String AERecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String PARRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String PQCRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String GIRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String MIRRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String PrimaryCaseRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String ClosedAERecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String ClosedPQCRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String ClosedGIRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String ClosedMIRRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    Public static String ClosedPrimaryCaseRecordTypeId = JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase ).getRecordTypeId();
    // get Custom Setting values
    Public static Boolean caseToMISync = JJ_MAFOPSSetting__c.getValues('Case to MI sync').JJ_Case_to_MI_Sync__c;
    Public static Boolean emailSend    = JJ_MAFOPSSetting__c.getValues('Email Send').JJ_Email_Send__c;
    Public static Boolean childToParentSend= JJ_MAFOPSSetting__c.getValues('childToParent').JJ_childToParent__c;
    Public static Boolean congaRestriction= JJ_MAFOPSSetting__c.getValues('congaRestriction').JJ_congaRestriction__c;
    
    public JJ_ANZ_CaseTriggerHandler(boolean isExecuting){
        m_isExecuting = isExecuting;
    }

    
    public void OnBeforeInsert(List<Case> newCases){
        // EXECUTE BEFORE INSERT LOGIC
        try{  
            System.debug('EXECUTE BEFORE INSERT LOGIC');         
            String ProfileId = UserInfo.getProfileId();
            Profile userProfile = [Select Id, Name From Profile where Id = :ProfileId];
            JJ_ANZ_RecursiveTriggers.firstRunOnCaseStatusChange = false;
            List<Case> listCases = new List<Case>();
            set<Id> accid = New set<Id>();
            List<Address_vod__c> caseadds = New List<Address_vod__c>();
            Map<Id,Address_vod__c> mapcaseadds = New Map<Id,Address_vod__c>();
            List<Id> UserIds = new List<Id>();
            for(Case caseRec : newCases)
            {
                UserIds.add(caseRec.OwnerId);
               if(caseRec.Origin==LSO&&caseRec.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())
                {
                    caseRec.JJ_ANZ_CreateAE__c = Checked;
                }
                if(caseRec.Origin==PAR&&caseRec.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())
                {
                    caseRec.JJ_ANZ_CreatePAR__c = Checked;
                }
                 if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId() || caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PhoneCase).getRecordTypeId() )
                {
                    
                     if(caseRec.Status==assessedCaseStatus)
                    {
                        caseRec.JJ_ANZ_Case_Info_Assessed_Date__c = System.now();
                    }
                    if(caseRec.JJ_ANZ_CreateAE__c==Checked)  
                    {
                      caseRec.JJ_ANZ_Create_AE__c = true;
                      caseRec.JJ_ANZ_Initial_review_date__c = System.Now();
                    }              
              
                  if(caseRec.JJ_ANZ_CreatePQC__c==Checked)  
                    {
                      caseRec.JJ_ANZ_Create_PQC__c = true;
                      caseRec.JJ_ANZ_PQCInitialReviewDate__c = System.Now();
                    } 
                    
                    
                    
                    //PAR Case Creation
                    if(caseRec.JJ_ANZ_CreatePAR__c==Checked)  
                    {
                      caseRec.JJ_ANZ_Create_PAR__c = true;
                      
                    }      
                }
                 
                
                //Added on 3/5/2016
                if(caseRec.Origin==VeevaMI&&caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()){
                    caseRec.JJ_ANZ_Follow_up_Consent__c = '';
                }
                if(caseRec.Origin==VeevaMI&&caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()&&caseRec.JJ_ANZ_Name__c!=null&&caseRec.JJ_ANZ_Address__c==null){
                    accid.add(caseRec.JJ_ANZ_Name__c);                
                }
                
            }
            
            Map<Id,User> MapUsers = new Map<Id,User>([Select id,JJ_ANZ_UserRole__c from User where id in:UserIds]);
            
            for(Case caseRec : newCases)
            {
                if(MapUsers.ContainsKey(caseRec.OwnerId))
                {
                    caseRec.JJ_ANZ_UserRole__c = MapUsers.get(caseRec.OwnerId).JJ_ANZ_UserRole__c;
                }
            }
            
            if(!accid.IsEmpty()){
                caseadds = [Select Id,Account_vod__c,Name FROM Address_vod__c where Account_vod__c IN: accid];    
            }
            if(!caseadds.IsEmpty()){
                for(Address_vod__c adds: caseadds ){
                    mapcaseadds.put(adds.Account_vod__c,adds);
                }
            }
            for(case newcs: newCases){
                if(mapcaseadds.containskey(newcs.JJ_ANZ_Name__c)){
                    newcs.JJ_ANZ_Address__c = mapcaseadds.get(newcs.JJ_ANZ_Name__c).Id;     
                }
            }
        }
        Catch(Exception e){

            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
        }             
        
    }

    public void OnAfterInsert(List<Case> newCases){
    // EXECUTE AFTER INSERT LOGIC
    //  EXECUTE AFTER INSERT LOGIC for PAR
    System.debug('EXECUTE AFTER INSERT LOGIC'); 
    List<Case> listCases = new List<Case>();
    List<Case> listInsertedCases = new List<Case>();
    for(Case caseRec : newCases)
        {
            if(caseRec.JJ_ANZ_CreateAE__c==Checked||caseRec.JJ_ANZ_CreateGI__c==Checked||caseRec.JJ_ANZ_CreateMIR__c==Checked||caseRec.JJ_ANZ_CreatePQC__c==Checked ||caseRec.JJ_ANZ_CreatePAR__c==Checked)
            {
                listInsertedCases.add(caseRec);
                if((caseRec.Origin!=VeevaMI||caseRec.origin==VeevaMI)&&(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()||caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PhoneCase).getRecordTypeId()))
                {
                     
                    listCases.add(caseRec);
                }
            }                         
        }
        
        if(JJ_ANZ_RecursiveTriggers.firstRunOnBusinessHours)
        {
         JJ_ANZ_RecursiveTriggers.firstRunOnBusinessHours = false;
      if(!listInsertedCases.isEmpty())
        {
            JJ_ANZ_CreateChildCases.CreateChildCases(listInsertedCases);       
        }
       //**Condition to disable case to MI Flow*******************************************// 

       if(!listCases.isEmpty() && caseToMISync)
        {
             
            JJ_ANZ_CaseToMISync.CreateChildRecords(listCases);
        }
        
    } 
    }    



    public void OnBeforeUpdate(List<Case> oldCases, List<Case> updatedCases,Map<id,Case> oldMapCases)
    {
        // BEFORE UPDATE LOGIC
        System.debug('BEFORE UPDATE LOGIC');
         List<Case> listGIMICases = new List<Case>();
         List<Id> listCaseIds = new List<Id>();
         List<Case> ListParentCases = new List<Case>();
         List<Case> listMIR= new List<Case>();
         List<Id> listChildCaseId=new List<Id>();
         
         

        if(JJ_ANZ_RecursiveTriggers.firstRunOnBusinessHours)
       {
          JJ_ANZ_RecursiveTriggers.firstRunOnBusinessHours = false;
            
       
        
        //String OwnerId = UserInfo.getOwnerId();
        String ProfileId = UserInfo.getProfileId();
        String UserId = UserInfo.getUserId();
        Profile userProfile = [Select Id, Name From Profile where Id = :ProfileId];
        List<Case> listCases = new List<Case>();
        List<Id> UserIds = new List<Id>();
      
        System.debug('For loop Update Call');
        for(Case caseRec : updatedCases)
        {
            System.debug('For Inside loop Update Call');
            UserIds.add(caseRec.OwnerId);
            Case oldCase = oldMapCases.get(caseRec.Id);
            
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()&&caseRec.Status==assessedCaseStatus&&oldCase.Status!=assessedCaseStatus)
            {
                caseRec.JJ_ANZ_Case_Info_Assessed_Date__c = System.now();
            }
            
            if(caseRec.JJ_ANZ_CreateAE__c=='Yes' && caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId() && oldCase.JJ_ANZ_CreateAE__c==SecondaryReview)
            {
                caseRec.JJ_ANZ_AE_New_Case_ListView__c=true;
            }
            /*System.debug('Updating to close the case ' +updatedCases);
            if(oldCase.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get('Case Information').getRecordTypeId()&&oldCase.IsClosed==false&&caseRec.IsClosed==true)
            {
                
                 JJ_ANZ_UpdateCaseStatus.CloseCaseInformation(updatedCases,oldMapCases);
            }*/
            
        
            //this is to close the Parent case when child case is set to close
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()&&oldCase.Status!='Closed')
            {
            System.debug('Enter in the loop for checking child cases');
            listChildCaseId.add(caseRec.Id);
            }


            
            if(JJ_ANZ_RecursiveTriggers.firstRunOnCaseStatusChange){
            JJ_ANZ_RecursiveTriggers.firstRunOnCaseStatusChange =false;
            if(caseRec.Status=='Closed'&&oldCase.Status!='Closed'&&(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(GICase).getRecordTypeId()||caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(MIRCase).getRecordTypeId()))
            {
                listCaseIds.add(caseRec.Id);
                listGIMICases.add(caseRec);
            }
            
                
            
            if(oldCase.status=='In-Queue'&&caseRec.status == 'In-Queue' && UserId.startswith('005')&&(userProfile.Name!=Label.ManagerProfile))
            {

                
                caseRec.Status = AssignedCaseStatus;
            }
               
         
             if(oldCase.status=='New'&&caseRec.status == 'New')
           {

                
              caseRec.Status = AssignedCaseStatus;
            }
           
            
            }
            if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()) && (caseRec.JJ_ANZ_CreateAE__c == 'No' || caseRec.JJ_ANZ_CreatePQC__c=='No'))
               {
                   ListParentCases.add(caseRec);


               }
               
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(MIRCase).getRecordTypeId() ) 
            {
                listMIR.add(caseRec);
             
            }
            
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PQCCase).getRecordTypeId()&&caseRec.JJ_ANZ_ApprovalStatus__c==PendingCase &&oldCase.JJ_ANZ_ApprovalStatus__c!=PendingCase )
            {
                if(userProfile.Name==Label.PQCProfile)
                {
                    caseRec.JJ_ANZ_AssignedtoQueue__c = 'Back Desk PQC';
                }
                else if(userProfile.Name==Label.FrontDeskProfile)
                {
                   caseRec.JJ_ANZ_AssignedtoQueue__c = 'Front Desk';
                }
            }
            
           
            
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PQCCase).getRecordTypeId()&&caseRec.JJ_ANZ_ApprovalStatus__c==ApprovedCase&&oldCase.JJ_ANZ_ApprovalStatus__c!=ApprovedCase)
            {
                caseRec.JJ_ANZ_Approved_Date__c = System.Today();
                caseRec.JJ_ANZ_AssignedtoQueue__c = '';
                
                
            }
            
            
            if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())&&((caseRec.JJ_ANZ_CreateGI__c==Checked&&OldCase.JJ_ANZ_CreateGI__c!=Checked)||(caseRec.JJ_ANZ_CreateMIR__c==Checked&&OldCase.JJ_ANZ_CreateMIR__c!=Checked)))
                {
                   caseRec.JJ_ANZ_Open_Case_for_Front_Desk__c = true;
                }
            System.debug('Before Update Call...');
            if(JJ_ANZ_RecursiveTriggers.firstRunOnBeforeUpdate )
            {
            System.debug('Inside Update Call');
            JJ_ANZ_RecursiveTriggers.firstRunOnBeforeUpdate = false;
                
            if(caseRec.JJ_ANZ_CreateAE__c==Checked||caseRec.JJ_ANZ_CreateGI__c==Checked||caseRec.JJ_ANZ_CreateMIR__c==Checked||caseRec.JJ_ANZ_CreatePQC__c==Checked)
            {
            System.debug('Inside Update Call 259'+caseRec.JJ_ANZ_CreateAE__c+'  '+oldCase.JJ_ANZ_CreateAE__c);
                if(caseRec.JJ_ANZ_CreateAE__c==Checked&&oldCase.JJ_ANZ_CreateAE__c!=Checked)  
              {
                System.debug('Inside Update Call AE'+caseRec.JJ_ANZ_Create_AE__c);
                caseRec.JJ_ANZ_Create_AE__c = true;
                caseRec.JJ_ANZ_Initial_review_date__c = System.Now();
                System.debug('Inside Update Call AE'+caseRec.JJ_ANZ_Create_AE__c);
              }              
              
              if(caseRec.JJ_ANZ_CreatePQC__c==Checked&&oldCase.JJ_ANZ_CreatePQC__c!=Checked)  
              {
                  caseRec.JJ_ANZ_Create_PQC__c = true;
                  caseRec.JJ_ANZ_PQCInitialReviewDate__c = System.Now();
              }                                         
            }
            }
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase).getRecordTypeId())
            {
            
                if((caseRec.JJ_ANZ_Follow_Up_Date_1__c!=null||caseRec.JJ_ANZ_Follow_Up_Date_2__c!=null)&&oldCase.status==AssignedCaseStatus)
                {
                    caseRec.status=FollowUpCaseStatus;    
                }
                
                if(caseRec.JJ_ANZ_Follow_up_Consent__c == 'Consent Declined')
              {
                  caseRec.JJ_ANZ_Follow_Up_Date_1__c= null;
                  caseRec.JJ_ANZ_Follow_Up_Date_2__c =null;
                  caseRec.JJ_ANZ_Follow_Up_Completion_Date_1__c = null;
                  caseRec.JJ_ANZ_Follow_Up_Completion_Date_2__c = null;
                //caseRec.Status = 'Assigned'; RE:INC000015420193(Arno) RE:INC000015474203(Harriet)
              }
                if(caseRec.JJ_ANZ_FU1_Not_Sent_Reason__c!=null)
              {
                  caseRec.JJ_ANZ_Follow_Up_Completion_Date_1__c = null;
              }
              if(caseRec.JJ_ANZ_FU2_Not_Sent_Reason__c!=null)
              {
                  caseRec.JJ_ANZ_Follow_Up_Completion_Date_2__c = null;
              } 
                  
                  
                
            }
            
        
        }
        
        List<JJ_ANZ_Additional_Question__c> listQuestions = new List<JJ_ANZ_Additional_Question__c>();
        List<Case> childCases= new List<Case>();
        
        if(!listCaseIds.isEmpty())
        {
            listQuestions = [Select id,JJ_ANZ_Case__c,JJ_ANZ_Status__c from JJ_ANZ_Additional_Question__c where JJ_ANZ_Case__c IN:listCaseIds];
        }
        //Here we are checking the child cases and storing in object
       if(!listChildCaseId.isEmpty())
        {
            childCases= [Select id,Status,RecordTypeId,IsClosed from Case where ParentId IN:listChildCaseId];
            System.debug(childCases);
        }
        if(!childCases.isEmpty())
        {
            Integer i = 0;
            Integer j = 0;
            for(Case currentChildCase:childCases)
                {
                    if(currentChildCase.IsClosed)
                    {
                    i++;
                    System.debug(i);
                    }
                    if(currentChildCase.Status=='Non-case')
                    {
                    j++;
                    System.debug(j);
                    }
                }
        }


        Map<Id,List<JJ_ANZ_Additional_Question__c>> MapQuestions = new Map<Id,List<JJ_ANZ_Additional_Question__c>>();
        if(!listQuestions.isEmpty())
        {
            for(JJ_ANZ_Additional_Question__c Question : listQuestions )
            {
                if(MapQuestions.ContainsKey(Question.JJ_ANZ_Case__c))
                {
                    MapQuestions.get(Question.JJ_ANZ_Case__c).add(Question);
                }
                else
                {
                    MapQuestions.put(Question.JJ_ANZ_Case__c, new List<JJ_ANZ_Additional_Question__c>{Question});
                }
            }
           
        }
        // **Conga restriction and child to parent stamping happpening from Here***\\
        
         
        //****Condition to disable Conga and enable child to parent stamping*********// 
        
        if(!listMIR.isEmpty())
                 {
                 if(congaRestriction)
                    {
                     JJ_ANZ_UpdateCaseStatus.CongaCheckBox(listMIR);
                    }
                 if(childToParentSend)
                 {
                    JJ_ANZ_UpdateCaseStatus.childTOParent(listMIR);
                 }
                 }
                 
        for(Case caseRec : listGIMICases)
        {
          if(MapQuestions.ContainsKey(caseRec.Id))
          {
              List<JJ_ANZ_Additional_Question__c> Questions = MapQuestions.get(caseRec.Id);
              
              for(JJ_ANZ_Additional_Question__c Que : Questions)
              {
                  if(Que.JJ_ANZ_Status__c!='Closed'&&Que.JJ_ANZ_Status__c!='Completed'&&Que.JJ_ANZ_Status__c!='Cancelled')
                  {
                      caseRec.addError('Please Close the Questions before closing the Case');
                  }
              }
          }
        }
        
        Map<Id,User> MapUsers = new Map<Id,User>([Select id,JJ_ANZ_UserRole__c from User where id in:UserIds]);
            
            for(Case caseRec : updatedCases)
            {
                if(MapUsers.ContainsKey(caseRec.OwnerId))
                {
                    caseRec.JJ_ANZ_UserRole__c = MapUsers.get(caseRec.OwnerId).JJ_ANZ_UserRole__c;
                }
            }
                if(JJ_ANZ_RecursiveTriggers.firstRunOnCaseStatusChange){
                
                if(!ListParentCases.isEmpty())
                {


                    JJ_ANZ_UpdateCaseStatus.CloseParentCases(ListParentCases);
                }
              }
                   
        JJ_ANZ_CreateChildCases.BusinessHoursCalc(updatedCases);
         
           
      }  
    }

    public void OnAfterUpdate(List<Case> oldCases, List<Case> updatedCases,Map<id,Case> oldMapCases){
        // AFTER UPDATE LOGIC
        System.debug('AFTER UPDATE LOGIC');
            List<Case> listCases = new List<Case>();
            List<Case> listAdverse = new List<Case>();               
            List<Case> listUpdatedCases = new List<Case>();
            List<Case> UpdateCases = new List<Case>();
            List<Case> FrontDeskCases = new List<Case>();
            List<Case> BackDeskCases = new List<Case>();
            List<Case> ParentCases = new List<Case>();
            List<Id> ParentCaseIds = new List<Id>();
            List<Case> listPQC= new List<Case>();
            List<Case> listMIR= new List<Case>();
            List<Case> listPAR= new List<Case>();

            
            
            
        
                System.debug('In Update Call if'+updatedCases);
        for(Case caseRec: updatedCases)
        {
        
        if(caseRec.origin == VeevaMI || caseRec.origin !=VeevaMI )
             {
                 UpdateCases.add(caseRec);
             }
        
        //if(JJ_ANZ_RecursiveTriggers.firstRunforAfterUpdate)
        
            //JJ_ANZ_RecursiveTriggers.firstRunforAfterUpdate = false;
            
            Case oldCase = oldMapCases.get(caseRec.Id);
            
            
             
            
            if(oldCase.Status!=AssignedCaseStatus&&caseRec.Status==AssignedCaseStatus)
            {
                ParentCaseIds.add(caseRec.ParentId);
            }
            
            if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ClosedMIRCase).getRecordTypeId())||(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ClosedGICase).getRecordTypeId())||(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(MIRCase).getRecordTypeId())||(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(GICase).getRecordTypeId()))
            {
                if(caseRec.Status=='Closed'&&oldCase.Status!='Closed')
                {
                    System.debug('Front Desk Cases in Handler');
                    FrontDeskCases.add(caseRec);
                }
            } 
            
            
            //if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())&&(caseRec.JJ_ANZ_CreateAE__c==Checked))
            if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())&&(caseRec.JJ_ANZ_CreateAE__c==Checked||caseRec.JJ_ANZ_CreateGI__c==Checked||caseRec.JJ_ANZ_CreateMIR__c==Checked||caseRec.JJ_ANZ_CreatePQC__c==Checked||caseRec.JJ_ANZ_CreatePAR__c==Checked))
            {
               listUpdatedCases.add(caseRec);
                system.debug('PAR'+listUpdatedCases);
            }
            System.debug('In Update Call if'+caseRec);
           /* if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())||(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get('Closed Case Information').getRecordTypeId()))
             {
                 System.debug('Test listCases '+listCases);
                 listCases.add(caseRec);
                  System.debug('Test listCases '+listCases);

             } */
             
             if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase).getRecordTypeId() ) 
             {
             listAdverse.add(caseRec);
             }
             if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PQCCase).getRecordTypeId())
            {
                listPQC.add(caseRec);
            }
            if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(MIRCase).getRecordTypeId())
            {
                listMIR.add(caseRec);
            }
             
             
          
                                                        

           
       if((caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())||(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get('Closed Case Information').getRecordTypeId()))
             {
                 System.debug('Test listCases '+listCases);
                 listCases.add(caseRec);
                  System.debug('Test listCases '+listCases);

             }
        }   
    


        //**Condition to disable case to MI Flow*******************************************// 

        if(!UpdateCases.isEmpty() && caseToMISync)
        {
            if(UpdateMIRecordsCheck)
            {
                JJ_ANZ_CaseToMISync.updateMIRecords(UpdateCases);
                UpdateMIRecordscheck=false;
            }
        }
   
         System.debug('List Updated Cases ...'+listUpdatedCases);
        if(!listUpdatedCases.isEmpty())
        {                
         System.debug('List Updated Cases ...'+listUpdatedCases);
        JJ_ANZ_CreateChildCases.CreateChildCases(listUpdatedCases,oldMapCases);       
        } 
      
        if(!FrontDeskCases.isEmpty())
        {
            System.debug('Front Desk Cases in Handler'+FrontDeskCases);
            JJ_ANZ_UpdateCaseStatus.CloseFrontDeskCase(FrontDeskCases);
        }

            if(!updatedCases.isEmpty())
        {
            if (TriggerDisabler__c.getInstance().ClosePrimaryCase__c) {return;}
            if(JJ_ANZ_RecursiveTriggers.firstRunOnClosePrimaryCase)
            {


                JJ_ANZ_RecursiveTriggers.firstRunOnClosePrimaryCase = false; 
                JJ_ANZ_UpdateCaseStatus.ClosePrimaryCase(updatedCases,oldMapCases);






            }



            
        }





        
        if(!ParentCaseIds.isEmpty())
        {
            if(JJ_ANZ_RecursiveTriggers.firstRunOnAssessPrimaryCases)
           {
               JJ_ANZ_RecursiveTriggers.firstRunOnAssessPrimaryCases = false;    
               JJ_ANZ_UpdateCaseStatus.AssessPrimaryCase(ParentCaseIds);
           }
        }
         System.debug('list of cases on line 445'+listCases);
        if(!listCases.isEmpty() && caseToMISync)
        {             
           if(JJ_ANZ_RecursiveTriggers.firstRunOnCaseInformation)
           {
               JJ_ANZ_RecursiveTriggers.firstRunOnCaseInformation = false;               
               JJ_ANZ_CaseToMISync.CreateChildRecords(listCases,OldMapCases);
               System.debug('list of cases on line 452'+listCases);

               
               
           }
           //JJ_ANZ_UpdateCaseStatus.ParentToChildCaseSync(listCases);
           //RC1:added new static variable for INC000017924941
           if(JJ_ANZ_RecursiveTriggers.firstRunUpdateOnAfterUpdate)
           {
           JJ_ANZ_RecursiveTriggers.firstRunUpdateOnAfterUpdate = false;
           if(!listCases.isEmpty())
                {
            JJ_ANZ_UpdateCaseStatus.ParentToChildCaseSync(listCases);
             System.debug('In Trigger Handler'+listCases);

                } 
           }
        }
      
        
      
        if(!listAdverse.isEmpty())
        {
           
           System.debug('list Adverse is here');
           JJ_ANZ_RecursiveTriggers.firstRunOnAdverseEvent = false; 
           JJ_ANZ_UpdateCaseStatus.SeriousnessCheck(listAdverse,oldMapCases);
           
           
        }
        
        
        
        
       
                               
                  if(!listPQC.isEmpty())
                 {
                     System.debug('list pqc is here');
                    JJ_ANZ_UpdateCaseStatus.StampPQCWholeSaler(listPQC);
                 } 
                 
                              
                  if(!listMIR.isEmpty())
                 {
                   System.debug('list MIR is here');
                    JJ_ANZ_UpdateCaseStatus.StampMIRCaseClosedBy(listMIR);
                 } 
        
        
               
    }

    public void OnBeforeDelete(List<Case> CasesToDelete,Map<id,Case> MapOldCases){
        // BEFORE DELETE LOGIC
    }

    public void OnAfterDelete(List<Case> CasesToDelete,Map<id,Case> MapOldCases){
        // AFTER DELETE LOGIC
    }

    public void OnUndelete(List<Case> unDeletedCases){
        // AFTER UNDELETE LOGIC
    }

    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    Public void populateSeriousness_Email(List<Case> caselst,Map<id,Case> MapOldCases){
        try{
        System.debug('populateSeriousness_Email');
            Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(AECase).getRecordTypeId();
            set<Id> pid = New set<Id>();
            List<case> parentcases = New List<case>();
            Map<Id,string> seriousnessmap = New Map<Id,string>();
            List<Case> updseriouscase = New List<Case>();
            for(Case newcase: caselst){
                if(newcase.RecordTypeId == RecordTypeId&&newcase.JJ_ANZ_Seriousness__c!=MapOldCases.get(newcase.Id).JJ_ANZ_Seriousness__c){
                    pid.add(newcase.ParentId);
                    seriousnessmap.put(newcase.ParentId,newcase.JJ_ANZ_Seriousness__c);    
                }
            }
            if(!pid.IsEmpty()){
                parentcases =[Select Id,JJ_ANZ_Seriousness__c,JJ_ANZ_Seriousness_Email__c from Case Where Id IN: pid];    
            }
            if(!parentcases.IsEmpty()){            
                for(Case cs: parentcases){
                    if(seriousnessmap.containskey(cs.Id)){
                        cs.JJ_ANZ_Seriousness_Email__c = seriousnessmap.get(cs.Id);
                        updseriouscase.add(cs);                       
                    }
                }
                
                if(!updseriouscase.IsEmpty()){
                    Database.update(updseriouscase,false);
                }
            }
        }
        
        Catch(Exception e){

            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
        }    
    }
    
        Public void populatesalution(List<Case> caselst,Map<id,Case> MapOldCases){
       try{
       System.debug('Mahesh 597');
            set<Id> salaccid = New set<Id>();
            List<Account> accountsalution  = New List<Account>();
            List<Address_vod__c> addressPop  = New List<Address_vod__c>();
            Map<Id,Account> salutionmap = New Map<Id,Account>();
            Map<Id,Address_vod__c> addressMap= New Map<Id,Address_vod__c>();
            Id NameID;
            Case oldName;
            
            for(Case caseRec: caselst){
                       
                if(caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId()
                 &&((trigger.IsInsert&&caseRec.JJ_ANZ_Name__c!=null)||(trigger.IsUpdate && (caseRec.JJ_ANZ_Name__c!=null))))
                {
                    salaccid.add(caseRec.JJ_ANZ_Name__c);  
                    System.debug('acc id'+salaccid);              
                }
                
                 //FOR KOREA --> This is added to send email alert with current considering after update
                if(emailSend)
                {
                if(caseRec.JJ_KR_Email_Send__c==False && caseRec.JJ_ANZ_Name__c==null && caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PhoneCase).getRecordTypeId() && (trigger.IsInsert || trigger.IsBefore))
                {
                caseRec.JJ_KR_Email_Send__c=true;
                }
                }
                
            }
            if(!salaccid.IsEmpty()){
                accountsalution = [Select Id,Salutation,Primary_Parent_vod__r.Name,JJ_ANZ_NonPromo_Email_Consent__c,JJ_Core_Professional_Type__c from Account Where Id IN: salaccid];
                addressPop= [Select Id,Account_vod__c,Name from Address_vod__c Where Account_vod__c IN:salaccid AND Primary_vod__c=True];
                System.debug('Addresspop '+addressPop);
            }
            
            if(!accountsalution.IsEmpty()){
                for(Account adds: accountsalution){
                    salutionmap.put(adds.Id,adds);
                }
            }
             if(!addressPop.IsEmpty()){
                for(Address_vod__c address: addressPop){
                    addressMap.put(address.Account_vod__c,address);
                    System.debug('address '+address);
                }
            }
            
            
            
            System.debug('Mahesh Salutation Map'+salutionmap);
            System.debug('Atul addressMap '+addressMap);
            for(case newcs: caselst){
            if(MapOldCases !=null){
            if(MapOldCases.containskey(newcs.Id))
            {
            oldName= MapOldCases.get(newcs.Id);                        
            NameID=oldName.JJ_ANZ_Name__c;
             }
             }
              if(salutionmap !=null || !salutionmap.isEmpty()){
                if(salutionmap.containskey(newcs.JJ_ANZ_Name__c)){
                
                
                    //FOR KOREA --> This is added to send email alert with current considering after update
                    if(emailSend)
                    {
                    if(trigger.Isbefore || trigger.IsInsert)
                    {
                    newcs.JJ_KR_Email_Send__c=true;
                    }
                    }
                    
                    if(trigger.IsInsert && newcs.JJ_ANZ_Title__c==null && salutionmap.get(newcs.JJ_ANZ_Name__c).Salutation !=null && newcs.JJ_ANZ_Send_for_Verification__c==False){

                    System.debug('here is '+addressMap.get(newcs.JJ_ANZ_Name__c).Name);
                    newcs.JJ_ANZ_Title__c = salutionmap.get(newcs.JJ_ANZ_Name__c).Salutation; 
                    newcs.JJ_ANZ_Address__c=addressMap.get(newcs.JJ_ANZ_Name__c).Id;
                    
                    }
                    if(trigger.IsUpdate &&  (newcs.JJ_ANZ_Title__c!=null || newcs.JJ_ANZ_Title__c==null) && salutionmap.get(newcs.JJ_ANZ_Name__c).Salutation !=null && newcs.JJ_ANZ_Send_for_Verification__c==False)
                    {
                    newcs.JJ_ANZ_Title__c = salutionmap.get(newcs.JJ_ANZ_Name__c).Salutation;
                    System.debug('here is '+addressMap.get(newcs.JJ_ANZ_Name__c).Name);                    
                    newcs.JJ_ANZ_Address__c=addressMap.get(newcs.JJ_ANZ_Name__c).Id;
                    }                    
                    if(newcs.JJ_ANZ_Send_for_Verification__c==False){
                    newcs.JJ_ANZ_Practice_Name__c =salutionmap.get(newcs.JJ_ANZ_Name__c).Primary_Parent_vod__r.Name;
                    }
                    if(trigger.IsInsert && newcs.JJ_ANZ_Name__c!=null && salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_ANZ_NonPromo_Email_Consent__c !=null && newcs.JJ_ANZ_Send_for_Verification__c==False)  
                    {
                    newcs.JJ_ANZ_Permission_to_contact_Reporter__c  = salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_ANZ_NonPromo_Email_Consent__c;
                    }
                    else if(trigger.IsUpdate && NameID != newcs.JJ_ANZ_Name__c&&newcs.JJ_ANZ_Name__c!=null && salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_ANZ_NonPromo_Email_Consent__c!=null && newcs.JJ_ANZ_Send_for_Verification__c==False)
                    {
                    newcs.JJ_ANZ_Permission_to_contact_Reporter__c  = salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_ANZ_NonPromo_Email_Consent__c;
                    }
                    if(newcs.JJ_ANZ_Send_for_Verification__c==False && NameID != newcs.JJ_ANZ_Name__c && salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_Core_Professional_Type__c !=null)
                    {
                    newcs.JJ_ANZ_Initial_Reporter_Type__c = salutionmap.get(newcs.JJ_ANZ_Name__c).JJ_Core_Professional_Type__c ;
                    }
                }
            } 
            }   

        }
        
        Catch(Exception e){

           JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
        }  
    } 
    }