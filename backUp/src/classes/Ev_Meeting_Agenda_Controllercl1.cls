/**
* Class to control the behaviour of the custom control for event aganda
* Crested: June 7 2013
* Modified July 25 2013
* Bluewolf 
**/


/****************************************************************
Related requirements: DMP1,ALPHA REVIEW COMMENTS
*****************************************************************/
public with sharing class Ev_Meeting_Agenda_Controllercl1 {
    
    /**
    * Wraps the information about an event, adding the ability to check it and delete
    * it from the list in the VF component
    **/
    class JanssenEventAgendaWrapper{ 
        
        public Boolean selected {get; set;}
        
        public Boolean render=False;
        
        public Janssen_Event_Agenda__c value {get; set;}
        
        public JanssenEventAgendaWrapper(){
            selected=False; 
            value=new Janssen_Event_Agenda__c();
            value.Description__c='Breakfast';
        }
        
        public JanssenEventAgendaWrapper(Janssen_Event_Agenda__c ev){ 
            selected=False; 
            value=ev;
        } 
        
        public Boolean getRender(){
            Boolean ans=False;
            if(value.Description__c!=NULL){
                if(value.Description__c.indexOf('Registration')<0){
                    ans=True;
                }
            }
            return ans;
        }
    }
        
    /**
    * The controller of the main VF page.
    * To set bi directional comm. it calls a SET method on the page controller.
    * In that way, we can call methods of the component from the main page
    **/
    public Meeting_Event_Extension mainPageController=NULL;
    
    /**
    *List of items in the agenda
    **/ 
    public List<JanssenEventAgendaWrapper> items {get; set;}
    
    /**
    * The default registration Item
    **/
    public Janssen_Event_Agenda__c registrationItem;
    
    /**
    *List of items deleted from the agenda
    **/ 
    public List<Janssen_Event_Agenda__c> deleted {get; set;}    
    /**
    *List of items predefined items in the Event Agenda Config Object
    **/ 
    public List<Selectoption> options {get; set;}
    
    /**
    * Map of registration types
    **/
    private Map<String,Event_Reg_Types__c> types=Event_Reg_Types__c.getAll();
    /**
    *List of registration types for the UI
    **/ 
    public List<Selectoption> regTypeList {get; set;}
    /**
    *The selected registration type
    **/
    public String selectedRegType {get;set;}
    
    /*variable to check for validation error*/
    public boolean hasValidError {get;set;}
    
    /*variable to display error on component*/
    public boolean dispError {get;set;}
    
    public boolean svreturn {get;set;}
    
    
    public String EvId;
    
    public List<selectOption> speakeropts  {get;set;}
    public string educntdes  {get;set;}
    
    
    
    public Ev_Meeting_Agenda_Controllercl1(){   
        EvId = ApexPages.currentPage().getParameters().get('EventId'); 
        dispError = false;
        svreturn = false;
        hasValidError = false;
        items=new List<JanssenEventAgendaWrapper>();
        deleted=new List<Janssen_Event_Agenda__c>();    
        regTypeList=new List<SelectOption>();
        for(String key:types.keySet()){
            regTypeList.add(new SelectOption(key,types.get(key).JJ_Type__c));
        }
                
        speakeropts = new List<selectOption>();
        speakeropts.add(new SelectOption('', '--None--'));
        for(JJ_ANZ_Speaker_Association__c a:[Select JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.Salutation,JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.FirstName,JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.LastName From JJ_ANZ_Speaker_Association__c where JJ_ANZ_Medical_Event__c =:EvId]){
            speakeropts.add(new SelectOption(a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.Salutation + ' ' + a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.FirstName + ' ' + a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.LastName, a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.Salutation + ' ' + a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.FirstName + ' ' + a.JJ_ANZ_Speaker_Agreement__r.JJ_Speaker__r.PersonContact.LastName));
        }
        
        educntdes = '';
        for(JJ_ANZ_Janssen_Events__c je:[Select Educational_Content_Descriptions__c From JJ_ANZ_Janssen_Events__c limit 1]){
            educntdes = (je.Educational_Content_Descriptions__c == null)?'':je.Educational_Content_Descriptions__c;
            //educntdes.addAll(s.split(';'));
        }
        load();
    }

    /**
    * Adds and item to the editable list in the VF component
    **/
    public PageReference add(){
        items.add(new JanssenEventAgendaWrapper());
        return null;
    }
    /**
    * Removes an item from the editable list in the VF component
    **/
    public PageReference remove(){
        Integer j = 0;
        while (j < items.size())
        {
          if(items.get(j).selected == True && items.get(j).value.Description__c.indexOf('Registration')==-1)
          {
            deleted.add(items.get(j).value);
            items.remove(j);
          }else{
            j++;
          }
        }       

        return null;
    }
    
    /**
    * Marks all items from the editable list in the VF component
    **/
    public PageReference selectAll(){
        for(Integer i=0; i<items.size();i++){
            items.get(i).selected=True; 
        }
        
        return null;
    }   

    /**
    * Unmarks all items from the editable list in the VF component
    **/
    public PageReference unmarkAll(){
        for(Integer i=0; i<items.size();i++){
            items.get(i).selected=False; 
        }
        
        return null;
    }   
    
    /**
    * Saves/updates/deletes the agenda items
    **/
    public PageReference mySave(){ 
        hasValidError = false;  
        List<Janssen_Event_Agenda__c> newElements=new List<Janssen_Event_Agenda__c>();
        List<Janssen_Event_Agenda__c> updatedElems=new List<Janssen_Event_Agenda__c>(); 
        Savepoint sp = Database.setSavepoint();
        //Clean elements that are left from past error in the UI
        clean();

        //Calculate the actual times... 
        calculateTimes();
        
        //Check for validation error
        Integer timeint = integer.valueOf(items.get(0).value.Time__c.split(':')[0]);
        for(Integer i=1;i<items.size();i++){
            if(items.get(i).value.Description__c == 'Breakfast' && timeint >= 17){
                items.get(i).value.Description__c.addError(Label.Breakfast_Time_Validation);                
                hasValidError = true;
                return null;
            }
            else if(items.get(i).value.Description__c == 'Lunch' && timeint >= 17){
                items.get(i).value.Description__c.addError(Label.Lunch_Time_Validation);                
                hasValidError = true;
                return null;
            }
            else if(items.get(i).value.Description__c == 'Dinner' && timeint < 17){
                items.get(i).value.Description__c.addError(label.Dinner_Time_Validation);               
                hasValidError = true;
                return null;
            }
            if(items.get(i).value.Time_Slot__c == null){
                items.get(i).value.Time_Slot__c.addError(Label.Event_Agenda_Time_Slot_Null_Error);              
                hasValidError = true;
                return null;
            }
        }
        
        if(EvId!=NULL){              
            try{
                //Registration
                items.get(0).value.Description__c=selectedRegType;
                //Assign ID from event if not present
                for(Integer i=0;i<items.size();i++){                    
                    //items.get(i).value.JJ_ANZ_Educational_Content__c = educntdes.contains(items.get(i).value.Description__c);
                
                    if(items.get(i).value.Janssen_Event__c==NULL){
                        items.get(i).value.Janssen_Event__c=EvId;
                    }
                    if(items.get(i).value.id==NULL){//New elems                         
                        newElements.add(items.get(i).value);
                    }
                    else{//For update
                        updatedElems.add(items.get(i).value);
                    }
                }       

                try{
                    //Delete elements that have been deleted from the list
                    delete deleted;   
                }catch(Exception e){          
                    
                }
                //Create new agenda items               
                insert newElements;             
                //Update existing items
                update updatedElems;
                //Reset values
                deleted=new List<Janssen_Event_Agenda__c>();                    
                load();

                if(svreturn){
                    return new PageReference('/' + EvId);
                }
            }catch(Exception e){
                Database.rollback(sp);
                //getMainPageController().setError=True;
                deleted=new List<Janssen_Event_Agenda__c>();
                load();         
                ApexPages.addMessages(e);
                hasValidError = true;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.VALIDATION_AGENDA_SAVE_EVENT));
        }
        return null;
    }       
    
    /**
    *Setter for the main page controller
    */
    public void setMainPageController(Meeting_Event_Extension ext){
        if(ext!=NULL&&mainPageController==NULL){//Avoid calling it once the main page cont. is set.....
            mainPageController=ext;//
            mainPageController.setAgendaControllercl1(this);               
            load();                 
        }               
    }
    /**
    *Getter for the mai page controller
    */  
    public Meeting_Event_Extension getMainPageController(){return mainPageController;}
    
    /**
    * Reloads the agenda information
    **/
    public void load(){
        items.clear();  
        if(EvId!=NULL){              
            for(Janssen_Event_Agenda__c value:[SELECT Id,Description__c,Time__c,Time_Slot__c,Janssen_Event__c,JJ_ANZ_Educational_Content__c ,JJ_ANZ_Speaker_Name__c FROM Janssen_Event_Agenda__c
                   WHERE Janssen_Event__c =:EvId ORDER BY Name ASC]){
                    if(value.Description__c.indexOf('Registration')>-1)
                        selectedRegType=value.Description__c;
                    items.add(new JanssenEventAgendaWrapper(value));
            }                              
        }
    }

    /**
    * Adds the default registration to the agenda
    **/
    public PageReference addDefaultRegistration(){       
        try{        
            //Default registration
            registrationItem=new Janssen_Event_Agenda__c();
            registrationItem.Description__c=types.get('Registration').JJ_Type__c;
            registrationItem.Janssen_Event__c=EvId;
            registrationItem.Time__c='17:00';       
            registrationItem.Time_Slot__c='15';             
            insert registrationItem;            
            items.add(new JanssenEventAgendaWrapper(registrationItem));
        }catch(DMLException e){
            //getMainPageController().setError=True;
            ApexPages.addMessages(e);
        }
        return null;
    }
    
    /**
    * Inserts the default registration item
    **/
    public Janssen_Event_Agenda__c getRegistrationItem(){
        if(items.size()>0){
            return items.get(0).value;
        }
        else{
            items=new List<JanssenEventAgendaWrapper>();
            registrationItem=new Janssen_Event_Agenda__c();
            registrationItem.Description__c=types.get('Registration').JJ_Type__c;
            registrationItem.Janssen_Event__c=EvId;
            registrationItem.Time__c='17:00';           
            registrationItem.Time_Slot__c='15'; 
            items.add(new JanssenEventAgendaWrapper(registrationItem));
        }           
        return registrationItem;
    }
    
    /**
    * Calculates the actual time of each event based on the time slots assigned
    **/
    private void calculateTimes(){
        if(items.size()>1){
            //Get the registration time and create a Time instance
            String[] tmp=items.get(0).value.Time__c.split(':');
            Integer lastSlot=Integer.valueOf(items.get(0).value.Time_Slot__c);//15;//Integer.valueOf(items.get(0).value.Time_Slot__c);
            
            Datetime lastTimeSet=Datetime.newInstance(2008, 12, 1, Integer.valueOf(tmp[0]),Integer.valueOf(tmp[1]), 0);//Time.newInstance(Integer.valueOf(tmp[0]),Integer.valueOf(tmp[1]),0,0);
            Datetime newTime=null;
            
            //For all the items add the number of timeslots*15 as minutes
            for(Integer i=1;i<items.size();i++){
                
                /* Defect correction: "Meeting Agenda" tab Allow "None value against" allocated Time
                *
                if(items.get(i).value.Time_Slot__c==NULL){
                    items.get(i).value.Time_Slot__c='15';//Default
                }
                */
                //if(items.get(i).value.Time_Slot__c!=NULL){
                    newTime=lastTimeSet.addMinutes(Integer.valueOf(lastSlot));
                    lastSlot= (items.get(i).value.Time_Slot__c == null)?0:Integer.valueOf(items.get(i).value.Time_Slot__c);
                    items.get(i).value.Time__c=newTime.format('HH:mm'); 
                    lastTimeSet=newTime;
                    newTime=null;
                //}
            }
        }
    }
    
    private void clean(){
        Integer j = 1;
        while (j < items.size())
        {
          if(items.get(j).value.Description__c==NULL)
          {
            items.remove(j);
          }else{
            j++;
          }
        }       
    }
}