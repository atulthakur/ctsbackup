/**Description  : Class to display PAR tab
Author       : spriyan3@its.jnj.com||12/18/2017



*/
public class JJ_RelatedCaseSection
    {
    
    public Integer AECnt {get;set;}
    public Integer MIRCnt {get;set;}
    public Integer GICnt {get;set;}
    public Integer PQCCnt {get;set;}
    public Integer PARCnt {get;set;}
    public Integer ClosedAECnt {get;set;}
    public Integer ClosedMIRCnt {get;set;}
    public Integer ClosedGICnt {get;set;}
    public Integer ClosedPQCCnt {get;set;}
    public Integer ClosedPARCnt {get;set;}
    public Boolean AEUser{get;set;}
    public Boolean MIRUser{get;set;}
    public Boolean GIUser{get;set;}
    public Boolean PQCUser{get;set;}
    public Boolean PARUser{get;set;}
    public Boolean ClosedAEUser{get;set;}
    public Boolean ClosedMIRUser{get;set;}
    public Boolean ClosedGIUser{get;set;}
    public Boolean ClosedPQCUser{get;set;}
    public Boolean ClosedPARUser{get;set;}
    public Boolean GenericUser{get;set;}
    public List<Case> AdverseEvent{get;set;}
    public List<Case> MIR{get;set;}
    public List<Case> GI{get;set;}
    public List<Case> PQC{get;set;}
    public List<Case> PAR{get;set;}
    public List<Case> ClosedAE{get;set;}
    public List<Case> ClosedMIR{get;set;}
    public List<Case> ClosedGI{get;set;}
    public List<Case> ClosedPAR{get;set;}
    Public Id ClosedGIRec{get;set;}
    public List<Case> ClosedPQC{get;set;}
    public transient List<Case> ChildCases{get;set;}
    public Case currentRecord {get;set;}
    Public Id UserId {get;set;}
    public String tabInFocus{get;set;}
    Public String CurrentTab{get;set;}


        public JJ_RelatedCaseSection(ApexPages.StandardController controller)
        {
            currentRecord  =(Case)controller.getRecord();
            User currentUser = [Select id,JJ_DefaultChildCase__c from User where id=:UserInfo.getUserId()];
            if(currentUser.JJ_DefaultChildCase__c!=null)
            {
                tabInFocus = currentUser.JJ_DefaultChildCase__c;
            }
            else
            {
                tabInFocus = 'AE';
            }
            
            UserId = UserInfo.getUserId();
            AEUser = false;
            GIUser= false;
            MIRUser = false;
            PQCUser = false;
            PARUser = false;
            ClosedPARUser =false;
            ClosedAEUser = false;
            ClosedGIUser = false;
            ClosedMIRUser = false;
            ClosedPQCUser = false;
            GenericUser = false;
            User u = [Select id,JJ_DefaultChildCase__c from User where id=:UserId];
            if(u.JJ_DefaultChildCase__c=='AE')
            {
                AEUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='GI')
            {
                GIUser= true;
            }
            else if(u.JJ_DefaultChildCase__c=='MIR')
            {
                MIRUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='PQC')
            {
                PQCUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='PAR')
            {
                PARUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='Closed AE')
            {
                ClosedAEUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='Closed GI')
            {
                ClosedGIUser= true;
            }
            else if(u.JJ_DefaultChildCase__c=='Closed MIR')
            {
                ClosedMIRUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='Closed PQC')
            {
                ClosedPQCUser = true;
            }
            else if(u.JJ_DefaultChildCase__c=='Closed PAR')
            {
                ClosedPARUser = true;
            }
            
            if(!AEUser&&!GIUser&&!MIRUser&&!PQCUser&&!ClosedAEUser&&!ClosedGIUser&&!ClosedMIRUser&&!ClosedPQCUser&&!ClosedPARUser)
            {
                GenericUser = true;
            }
            AECnt = 0;
            MIRCnt = 0;
            GICnt = 0;
            PQCCnt = 0;
            PARCnt=0;
            ClosedAECnt = 0;
            ClosedMIRCnt = 0;
            ClosedGICnt = 0;
            ClosedPQCCnt = 0;
            ClosedPARCnt =0;
            
            AdverseEvent = new List<Case>();
            MIR = new List<Case>();
            GI = new List<Case>();
            PQC = new List<Case>();
            PAR=new List<Case>();
            ClosedAE = new List<Case>();            
            ClosedGI = new List<case>();
            ClosedMIR = new List<Case>();
            ClosedPQC = new List<Case>();
            ClosedPAR = new List<Case>();
            
            if(currentRecord.ID!=null)
            {           
            ChildCases = [select id,CaseNumber,recordtype.name,subject,Status,Owner.name,parent.recordtype.name from Case where parent.id =: currentRecord.ID];
            if(!ChildCases.isEmpty())
            {
                for(Case caseRec : ChildCases)
                {
                    if(caseRec.recordtype.name == 'Adverse Event')
                    {
                        AECnt = AECnt+1;
                        AdverseEvent.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Medical Enquiry')
                    {
                        MIRCnt = MIRCnt+1;
                        MIR.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'General Inquiry')
                    {
                        GICnt = GICnt+1;
                        GI.add(caseRec);
                        ClosedGIRec = caseRec.Id;
                    }
                    else if(caseRec.recordtype.name == 'Product Quality Complaints')
                    {
                        PQCCnt = PQCCnt+1;
                        PQC.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Product Access Request')
                    {
                        PARCnt = PARCnt+1;
                        PAR.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Closed Adverse Event')
                    {
                        ClosedAECnt = ClosedAECnt+1;
                        ClosedAE.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Closed Medical Enquiry')
                    {
                        ClosedMIRCnt = ClosedMIRCnt+1;
                        ClosedMIR.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Closed General Inquiry')
                    {
                        
                        ClosedGICnt = ClosedGICnt+1;
                        ClosedGI.add(caseRec);
                        ClosedGIRec = caseRec.Id;
                    }
                    else if(caseRec.recordtype.name == 'Closed Product Quality Complaints')
                    {
                        ClosedPQCCnt = ClosedPQCCnt+1;
                        ClosedPQC.add(caseRec);
                    }
                    else if(caseRec.recordtype.name == 'Closed Product Access Request')
                    {
                        ClosedPARCnt = ClosedPARCnt+1;
                        ClosedPAR.add(caseRec);
                    }
                    
                }
            }
            }
                       
        }
        
       public void updateUser()
       {                    
        User u = new User();
        u.Id = UserInfo.getUserId();
        u.JJ_DefaultChildCase__c = CurrentTab;
        
        try
        {
            update u;
        }
        catch(DMLException de)
        {
        
        }
        catch(Exception e)
        {
        
        }
        
  

       }
        
       
                 
    }