/** Subject  : JJ_ANZ_CaseTriggerHandler

Description  : Class to handle all the calls from Case Trigger
Author       : Mahesh Somineni || msominen@its.jnj.com || 1/14/2016
             : Shiva Shanakar  || sshank10@its.jnj.com || 12/30/2016 || update code for MI to Case sync flag with respect to country
**/
    public class JJ_ANZ_VeevaMItoCaseSync
{
    Public static Map<string, string> MapMappingTable=new map<string,string>();
    Public Static String Checked = 'Yes';
    public static void syncToCase(List<Medical_Inquiry_vod__c> MIRecords)
    {
    //**Condition for Stopping creation of Case Records Flag*******************************************// 
                
    // get Custom Setting values
        Boolean miToCaseSync = JJ_MAFOPSSetting__c.getValues('MI to Case sync').JJ_MI_to_Case_Sync__c;
    
        if(miToCaseSync)
        {
        if(JJ_ANZ_RecursiveTriggers.firstRunforVeevaTrigger )
        {
        JJ_ANZ_RecursiveTriggers.firstRunforVeevaTrigger = false;        
        List<Case> InsertCases =  new List<Case>();
        List<RecordType> rtypes = [Select Name, Id From RecordType 
                  where sObjectType='Case' and isActive=true];
                  system.debug('hello');
        Map<String,String> caseRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes)
        caseRecordTypes.put(rt.Name,rt.Id);
        system.debug(caseRecordTypes);
        
            //Fetching the assignment rules on case
       AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            system.debug('AR'+AR);
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id; 
        MapMappingTable  = getAllMapping();
        for(Medical_Inquiry_vod__c m: MIRecords)
        {
            system.debug('case creation');
            Case tempCase = new Case();
            for(String caseField: MapMappingTable.keySet())
            {
                String  VeevaField = MapMappingTable.get(caseField);
                tempcase.put(caseField, m.get(VeevaField));
                system.debug('tempcase');
            }
            
            tempCase.AccountId=m.Account_vod__c;
            tempCase.JJ_ANZ_Medical_Inquiry_ID__c = m.ID;
            tempCase.recordTypeId =caseRecordTypes.get('Case Information');
            system.debug('Checking record type id' +caseRecordTypes.get('Case Information'));
            system.debug(tempCase);
            tempCase.JJ_ANZ_Follow_up_Consent__c='Consent Received';
            tempCase.status='New';
            tempCase.JJ_ANZ_MedicalEnquiryNumber__c = m.Name;
            tempCase.JJ_ANZ_JnJ_Notified_Date__c=m.JJ_ANZ_JnJ_Notified_Date__c;
            if(m.JJ_ANZ_Detail_Product__c!=null)
            {
                tempCase.JJ_ANZ_CaseProduct__c = m.JJ_ANZ_Detail_Product__c;
            }  
            if(m.JJ_ANZ_Product_Other__c!=null)
            {
                 tempCase.JJ_ANZ_CaseProductOther__c = m.JJ_ANZ_Product_Other__c;
            }
           if(m.JJ_ANZ_Product__c!=null)
            {
            if(m.JJ_ANZ_Product__c.contains('AU')) 
            {
                tempCase.JJ_ANZ_CaseProducts__c = m.JJ_ANZ_Product__c.replace(' AU','')+'®';
            }
            else if(m.JJ_ANZ_Product__c.contains('NZ')) 
            {
                tempCase.JJ_ANZ_CaseProducts__c = m.JJ_ANZ_Product__c.replace(' NZ','')+'®';
            }
            else
            {
                tempCase.JJ_ANZ_CaseProducts__c = m.JJ_ANZ_Product__c+'®';
            } 
            }
            tempCase.JJ_ANZ_EventDescription__c = m.JJ_ANZ_QuestionDetail__c;      
            tempCase.Origin='Veeva MI';
            tempCase.JJ_ANZ_Off_Label__c = m.JJ_ANZ_Offlabel__c;
            tempCase.JJ_ANZ_JNJ_Employee__c = m.CreatedbyId;
            tempCase.setOptions(dmlOpts);
            system.debug('tempcase'+tempcase);
            
           if(m.JJ_ANZ_AE__c)
            {
                 tempCase.JJ_ANZ_CreateAE__c = Checked ;
            }
            if(m.JJ_ANZ_GI__c)
            {
                tempCase.JJ_ANZ_CreateGI__c= Checked ;
                system.debug(tempCase);
            }
            
            if(m.JJ_ANZ_MIR__c)
            {
               tempCase.JJ_ANZ_CreateMIR__c= Checked ;
            }
            if(m.JJ_ANZ_PQC__c)
            {
               tempCase.JJ_ANZ_CreatePQC__c= Checked ;
               system.debug(tempCase);
            }
            
            
            //QuickFix
            tempCase.JJ_ANZ_JnJ_Employee__c = UserInfo.getUserId() ;
            
            InsertCases.add(tempCase);
            system.debug('Insert'+InsertCases);
        }
        

        if(!InsertCases.isEmpty())
        {
          try
          {
          
              insert InsertCases;
              system.debug('try');
          }
          catch(DMLException de)
          {
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
            Database.insert(newErrorRecord,false);
          }
          catch(Exception e)
          {
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
          }
          
        }
        
        }
        
        }
        }
     
    
    
    public static Map<string,string> getAllMapping()
    {
       
        try{
             for (VeevaToCaseMapping__c mappingTableRec : VeevaToCaseMapping__c.getall().Values())
             {
                if (mappingTableRec.Name != null && mappingTableRec.MIFieldAPIName__c != Null && mappingTableRec.JJ_ANZ_VeevaToCaseSync__c )
                {
                    MapMappingTable.put(mappingTableRec.Name , mappingTableRec.MIFieldAPIName__c);
                    
                }
             }
        }
        catch(exception ex)
        {
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = ex.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = ex.getTypeName();
            Database.insert(newErrorRecord,false);
        }
        return MapMappingTable;
    } 
}