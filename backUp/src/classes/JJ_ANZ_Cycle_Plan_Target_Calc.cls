/**
*   Created by: Hasseeb Mungroo (ICBWORKS)
*   Created Date: 18.12.2014
*   Last Date Modified: 14.05.2014
*   Email: hasseeb.mungroo@icbworks.com
*   
*   Description:  Calculate the Veeva Fields for Actual and Planned Calls based on the Cycle Plan Detail. 
*   This corrects the issue of the Gage data not appearing.
*   This needs to be run after the JJ_ANZ_Cycle_Plan_Calc batch is run.
**/
global class JJ_ANZ_Cycle_Plan_Target_Calc implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id, Actual_Calls_vod__c, JJ_ANZ_Actual_Calls__c, Planned_Calls_vod__c, JJ_ANZ_Planned_Calls__c FROM Cycle_Plan_Target_vod__c'; 
        // where Id=\'a1M900000019SRh\'
       
        return Database.getQueryLocator('SELECT Id, Actual_Calls_vod__c, JJ_ANZ_Actual_Calls__c, Planned_Calls_vod__c, JJ_ANZ_Planned_Calls__c FROM Cycle_Plan_Target_vod__c');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        
        List<Cycle_Plan_Target_vod__c> targets = (List<Cycle_Plan_Target_vod__c>) batch;
        Set<Id> targetIds = new Set<Id>();
        Map<String, Cycle_Plan_Target_vod__c> targetMap = new Map<String, Cycle_Plan_Target_vod__c>();
        for (Cycle_Plan_Target_vod__c target : targets) {
            target.Actual_Calls_vod__c = target.JJ_ANZ_Actual_Calls__c;
            target.Planned_Calls_vod__c = target.JJ_ANZ_Planned_Calls__c;
            targetIds.add(target.Id);
                         
        }
        
        //Get all Details based on the target
        Decimal actualDetails = 0;
        Decimal plannedDetails = 0;
        Map<String, Decimal> actualDetailsMap = new Map<String, Decimal>();
        Map<String, Decimal> plannedDetailsMap = new Map<String, Decimal>();
        for (Cycle_Plan_Detail_vod__c detail: [select id, Scheduled_Details_vod__c, Actual_Details_vod__c, Planned_Details_vod__c, Cycle_Plan_Target_vod__c from Cycle_Plan_Detail_vod__c where Cycle_Plan_Target_vod__c in :targetIds order by Cycle_Plan_Target_vod__c]) {
            
            //Calculate Actual Details
            if (actualDetailsMap.get(detail.Cycle_Plan_Target_vod__c) == null) {
                actualDetailsMap.put(detail.Cycle_Plan_Target_vod__c , (detail.Actual_Details_vod__c == null ? 0 : detail.Actual_Details_vod__c));
                
            } else {
                actualDetails = actualDetailsMap.get(detail.Cycle_Plan_Target_vod__c);
                actualDetails += (detail.Actual_Details_vod__c == null ? 0 : detail.Actual_Details_vod__c);
                actualDetailsMap.put(detail.Cycle_Plan_Target_vod__c , actualDetails);
                
            }
            
            
            //Calculate Actual Details
            if (plannedDetailsMap.get(detail.Cycle_Plan_Target_vod__c) == null) {
                plannedDetailsMap.put(detail.Cycle_Plan_Target_vod__c , (detail.Planned_Details_vod__c == null ? 0 : detail.Planned_Details_vod__c));
                
            } else {
                plannedDetails = plannedDetailsMap.get(detail.Cycle_Plan_Target_vod__c);
                plannedDetails += (detail.Planned_Details_vod__c == null ? 0 : detail.Planned_Details_vod__c);
                plannedDetailsMap.put(detail.Cycle_Plan_Target_vod__c , plannedDetails);
                
            }           
            
        }
        
        
         for (Cycle_Plan_Target_vod__c target : targets) {
            
            if (actualDetailsMap.get(target.Id) != null) {
                target.Actual_Calls_vod__c = actualDetailsMap.get(target.Id);
            }
            
            if (plannedDetailsMap.get(target.Id) != null) {
                target.Planned_Calls_vod__c = plannedDetailsMap.get(target.Id);
            }
                                
            
         }
                
        
        
        
        
        update targets;
        
    }
    
    global void finish(Database.BatchableContext BC) {
        // Access initialState here
        
        
    }   

}