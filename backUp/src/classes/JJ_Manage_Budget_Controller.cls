public with sharing class JJ_Manage_Budget_Controller {

    /**
    * Wraps the information about an event, adding the ability to check it and delete
    * it from the list in the VF component
    **/
    class JanssenEventBudgetWrapper{
        
        public Boolean selected {get; set;}
        
        public Boolean render=true;
        
        public Event_Budget__c value {get; set;}
        
        public JanssenEventBudgetWrapper(Event_Budget__c ev){ 
            selected=False; 
            value=ev;
        } 
        
    }
        
    
    /**
    *List of items in the budget
    **/ 
    public List<JanssenEventBudgetWrapper> items {get; set;}
    
    /**
    * The default budget Item
    **/
    public Event_Budget__c budgetItem {get; set;}
    public List<Event_Budget__c> deleted {get; set;}   
    public String EvId;
    public string rectype;
    public boolean hasValidError {get;set;}
    public Medical_Event_vod__c janevent {get;set;}
    public Speaker_Agreement__c speakagree {get;set;}   
    public JJ_ANZ_Grant__c grant {get;set;}   
    public map<string, boolean> readonlyfields{get;set;}
    public map<string, boolean> hiddenfields{get;set;}
    public map<String, String> mapProfilename;
    public string profilename{get;set;}
    public string recordtypename{get;set;}
    public string eventstatus{get;set;}
    public boolean actualamtreq{get;set;}
    public boolean isjanevent{get;set;}
    public boolean isffservice{get;set;}
    public boolean isgrant{get;set;}    
    public map<String, boolean> maptypevalues{get;set;}
    public string dummyjanevtid;
    public double amountactual{get;set;}
    public double amountplan{get;set;}
    
    public JJ_Manage_Budget_Controller(ApexPages.StandardController controller) {
        profilename = '';
        recordtypename = '';
        eventstatus = '';
        amountactual = 0;
        amountplan = 0;
        actualamtreq = false;
        
        maptypevalues = new map<String, boolean>();
        maptypevalues.put('', false);
        maptypevalues.put(null, false);
  
        map<String, Schema.SObjectField> fieldMap = Event_Budget__c.getSObjectType().getDescribe().fields.getMap(); 
        
        list<Schema.PicklistEntry> values = fieldMap.get('Type__c').getDescribe().getPickListValues();
        
        for (Schema.PicklistEntry a : values)
        {
         maptypevalues.put(a.getValue(), false);
        }
        
        //when type contains these values then make account required
        maptypevalues.put('Accommodation - Attendee', true);
        maptypevalues.put('Accommodation - Speaker', true);
        maptypevalues.put('Accommodation - Speaker/Consultant', true);
        maptypevalues.put('Consultancy', true);
        maptypevalues.put('Registration', true);
        maptypevalues.put('Speaker honorarium', true);
        maptypevalues.put('Travel - Attendee', true);
        maptypevalues.put('Travel - Speaker/Consultant', true);
        
        Id budgetid = ApexPages.currentPage().getParameters().get('Id');
        EvId = ApexPages.currentPage().getParameters().get('EventId');
                
        if(budgetid != null && EvId == null){
            Event_Budget__c tempbe = [select Janssen_Event__c, JJ_ANZ_Fee_For_Service__c, JJ_ANZ_Grant__c from Event_Budget__c where Id =:budgetid];            
            EvId = tempbe.JJ_ANZ_Fee_For_Service__c;
            EvId = (EvId == null)?tempbe.JJ_ANZ_Grant__c:EvId;
            EvId = (EvId == null)?tempbe.Janssen_Event__c:EvId;     
        }
        janevent = new Medical_Event_vod__c();
        isjanevent = (Id.valueOf(EvId).getSObjectType().getDescribe().getName() == 'Medical_Event_vod__c');
        isffservice = (Id.valueOf(EvId).getSObjectType().getDescribe().getName() == 'Speaker_Agreement__c');
        isgrant = (Id.valueOf(EvId).getSObjectType().getDescribe().getName() == 'JJ_ANZ_Grant__c');
                       
        string rectypename = '';
        if(isjanevent){
            rectypename = 'Event_Budget';
        }
        else if(isffservice){
            rectypename = 'JJ_ANZ_Fee_for_Service_Payment';
        }
        else if(isgrant){
            rectypename = 'JJ_ANZ_Grant_Payment';
        }
                
        speakagree = new Speaker_Agreement__c();
        grant = new JJ_ANZ_Grant__c();
        rectype = [Select Id From RecordType r where SobjectType='Event_Budget__c' and DeveloperName =:rectypename].Id;
        
        if(isjanevent){
            budgetItem = new Event_Budget__c(Janssen_Event__c = EvId, Type__c = '', JJ_Amount_Plan__c = 1, RecordTypeId = rectype);
        }
        else if(isffservice){           
            budgetItem = new Event_Budget__c(JJ_ANZ_Fee_For_Service__c = EvId, Type__c = '', JJ_Amount_Plan__c = 1, RecordTypeId = rectype);            
            speakagree = [select Id, Name, JJ_ANZ_Consulting_Group__c,JJ_Type__c,RecordType.DeveloperName, JJ_ANZ_Speaker_Status__c, JJ_Speaker__c from Speaker_Agreement__c where Id=:EvId];
            dummyjanevtid = [select JJ_ANZ_Dummy_Event_FFS_Budget__c from JJ_ANZ_Janssen_Events__c limit 1].JJ_ANZ_Dummy_Event_FFS_Budget__c;
        }
        else if(isgrant){           
            budgetItem = new Event_Budget__c(JJ_ANZ_Grant__c = EvId, Type__c = 'Grants', JJ_Amount_Plan__c = 1, RecordTypeId = rectype);
            grant = [select Id, Name, JJ_ANZ_Organisation__c,JJ_ANZ_Status__c,JJ_ANZ_Grant_Type__c,JJ_ANZ_Grants_Value__c,RecordType.DeveloperName, JJ_ANZ_Total_Amount_Plan__c, JJ_ANZ_Total_Amount_Actual__c from JJ_ANZ_Grant__c where Id=:EvId];
            dummyjanevtid = [select JJ_ANZ_Dummy_Event_FFS_Budget__c from JJ_ANZ_Janssen_Events__c limit 1].JJ_ANZ_Dummy_Event_FFS_Budget__c;
        }
        
        
        items = new List<JanssenEventBudgetWrapper>();
        deleted=new List<Event_Budget__c>();    
        hasValidError = false;
        mapProfilename = new map<String, String>();
        for(Profile p : [select Id, Name from Profile]){
            mapProfilename.put(p.Id, p.Name);
        }
        load();
    }
    
    
    /**
    * Adds and item to the editable list in the VF component
    **/
    public PageReference add(){
        if(isjanevent){
            items.add(new JanssenEventBudgetWrapper(new Event_Budget__c(Janssen_Event__c = EvId, RecordTypeId = rectype)));
        }
        else if(isffservice){           
            items.add(new JanssenEventBudgetWrapper(new Event_Budget__c(JJ_ANZ_Fee_For_Service__c = EvId, RecordTypeId = rectype, Account__c = speakagree.JJ_Speaker__c,Janssen_Event__c = dummyjanevtid)));
        }
        else if(isgrant){           
            items.add(new JanssenEventBudgetWrapper(new Event_Budget__c(JJ_ANZ_Grant__c = EvId, Type__c = 'Grants', RecordTypeId = rectype, Janssen_Event__c = dummyjanevtid)));
        }
        return null;
    }
    /**
    * Removes an item from the editable list in the VF component
    **/
    public PageReference remove(){
        Integer j = 0;
        while (j < items.size())
        {
          if(items.get(j).selected == true)
          { 
            if(items.get(j).value.Id != null){
                deleted.add(items.get(j).value);
            }
            items.remove(j);
          }else{
            j++;
          }
        }       

        return null;
    }
    
    /**
    * Marks all items from the editable list in the VF component
    **/
    public PageReference selectAll(){
        for(Integer i=0; i<items.size();i++){
            items.get(i).selected=true; 
        }
        
        return null;
    }   

    /**
    * Unmarks all items from the editable list in the VF component
    **/
    public PageReference unmarkAll(){
        for(Integer i=0; i<items.size();i++){
            items.get(i).selected=false; 
        }
        
        return null;
    }   
    public Pagereference saveandreturn(){
        mySave();
        if(hasValidError){
            return null;
        }
        else{
            return new PageReference('/' + EvId).setredirect(true);
        }
    }
        /**
    * Saves/updates/deletes the agenda items
    **/
    public void mySave(){ 
        Savepoint sp = Database.setSavepoint();
        if(EvId!=NULL){              
            try{            
                budgetItem.JJ_Hospitality_Total_Actual__c = 0;
                budgetItem.JJ_Hospitality_Total_Plan__c = 0;
                amountactual = 0;
                amountplan = 0;
                hasValidError = false;
                List<Event_Budget__c> saveitems = new List<Event_Budget__c>();
                set<string> hospitalset = new set<string>{'Food and beverage','Food and beverage – full-day package','Food and beverage – full-day package inc venue hire','Food and beverage – half-day package','Food and beverage – half-day package package inc venue hire'};
                for(Integer i=0;i<items.size();i++){ 
                    items.get(i).value.RecordTypeId = rectype;
                    if(isffservice){
                        items.get(i).value.JJ_ANZ_Consulting_Group__c = (speakagree.JJ_ANZ_Consulting_Group__c != null)?speakagree.JJ_ANZ_Consulting_Group__c:items.get(i).value.JJ_ANZ_Consulting_Group__c;
                        items.get(i).value.Account__c = speakagree.JJ_Speaker__c;
                    }
                    if(isgrant){
                        items.get(i).value.Type__c = 'Grants';
                        items.get(i).value.Account__c = grant.JJ_ANZ_Organisation__c;
                    }
                    
                    amountactual += (items.get(i).value.JJ_Amount_Actual__c == null)?0:items.get(i).value.JJ_Amount_Actual__c;
                    amountplan += (items.get(i).value.JJ_Amount_Plan__c == null)?0:items.get(i).value.JJ_Amount_Plan__c;
                
                    budgetItem.JJ_Hospitality_Total_Actual__c += (items.get(i).value.JJ_Amount_Actual__c != null && hospitalset.contains(items.get(i).value.Type__c))?items.get(i).value.JJ_Amount_Actual__c:0;
                    budgetItem.JJ_Hospitality_Total_Plan__c += (items.get(i).value.JJ_Amount_Plan__c != null && hospitalset.contains(items.get(i).value.Type__c))?items.get(i).value.JJ_Amount_Plan__c:0;
                    
                    saveitems.add(items.get(i).value);
                    if(items.get(i).value.Type__c == null && !readonlyfields.get('Type__c') && !hiddenfields.get('Type__c')){
                        hasValidError = true;
                        items.get(i).value.Type__c.addError('Error: You must enter a value');
                    }
                    if(items.get(i).value.JJ_Amount_Plan__c == null && (isjanevent || isgrant) && !readonlyfields.get('JJ_Amount_Plan__c') && !hiddenfields.get('JJ_Amount_Plan__c')){
                        hasValidError = true;
                        items.get(i).value.JJ_Amount_Plan__c.addError('Error: You must enter a value');
                    }
                    if(items.get(i).value.JJ_Amount_Actual__c == null && !readonlyfields.get('JJ_Amount_Actual__c') && !hiddenfields.get('JJ_Amount_Actual__c') && actualamtreq){
                        hasValidError = true;
                        items.get(i).value.JJ_Amount_Actual__c.addError('Error: You must enter a value');
                    }
                    if(items.get(i).value.Type__c == 'Other (You MUST provide details)' && items.get(i).value.JJ_Note__c == null && !readonlyfields.get('JJ_Note__c') && !hiddenfields.get('JJ_Note__c')){
                        hasValidError = true;
                        items.get(i).value.JJ_Note__c.addError('Error: You must enter a value');
                    }
                    if(items.get(i).value.JJ_Payment_Type__c == null && items.get(i).value.JJ_Amount_Actual__c != null && !readonlyfields.get('JJ_Payment_Type__c') && !hiddenfields.get('JJ_Payment_Type__c')){
                        hasValidError = true;
                        items.get(i).value.JJ_Payment_Type__c.addError('Error: You must enter a value');
                    }
                    if(maptypevalues.get(items.get(i).value.Type__c) && items.get(i).value.Account__c == null && !readonlyfields.get('Account__c') && !hiddenfields.get('Account__c') && isjanevent){
                        hasValidError = true;
                        items.get(i).value.Account__c.addError('Error: You must enter a value');
                    }
                                    
                    if(items.get(i).value.JJ_ANZ_Milestone__c == null && !readonlyfields.get('JJ_ANZ_Milestone__c') && !hiddenfields.get('JJ_ANZ_Milestone__c') && isgrant){
                        hasValidError = true;
                        items.get(i).value.JJ_ANZ_Milestone__c.addError('Error: You must enter a value');
                    }
                    if(items.get(i).value.JJ_ANZ_Milestone_Date__c == null && !readonlyfields.get('JJ_ANZ_Milestone_Date__c') && !hiddenfields.get('JJ_ANZ_Milestone_Date__c') && isgrant){
                        hasValidError = true;
                        items.get(i).value.JJ_ANZ_Milestone_Date__c.addError('Error: You must enter a value');
                    }
                }
                
                /*
                //grant value validation
                if(isgrant){
                    double grantvalue = (grant.JJ_ANZ_Grants_Value__c == null)?0:grant.JJ_ANZ_Grants_Value__c;
                    if(amountplan > grantvalue){
                        hasValidError = true;
                        grant.JJ_ANZ_Grants_Value__c.addError(System.Label.JJ_ANZ_GRANT_VALUE_EXCEEDED);
                    }
                }
                */
                
                if(hasValidError){
                    return;
                }
                for(Event_Budget__c eb:saveitems){                
                    eb.JJ_Hospitality_Total_Actual__c = budgetItem.JJ_Hospitality_Total_Actual__c;
                    eb.JJ_Hospitality_Total_Plan__c = budgetItem.JJ_Hospitality_Total_Plan__c;
                }                
                
                upsert saveitems;
                delete deleted;
                deleted=new List<Event_Budget__c>(); 
                if(isgrant){
                    sortmilestonenumber();                  
                }
                load();
            }catch(Exception e){
                Database.rollback(sp);
                deleted=new List<Event_Budget__c>();
                load();
                hasValidError = true;
                ApexPages.addMessages(e);
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage() + '*********' + e.getStackTraceString() + '***********' + string.valueOf(e.getLineNumber())));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.VALIDATION_AGENDA_SAVE_EVENT));
        }
    }
    
    public void sortmilestonenumber(){      
            List<Event_Budget__c> sortlist = [SELECT JJ_ANZ_Milestone_Number__c FROM Event_Budget__c WHERE JJ_ANZ_Grant__c =:EvId ORDER BY JJ_ANZ_Milestone_Date__c ASC];
            
            for(integer i = 0; i < sortlist.size(); i++){
                sortlist[i].JJ_ANZ_Milestone_Number__c = i+1;
            }
            update sortlist;
            
            grant.JJ_ANZ_Total_Amount_Plan__c = amountplan;
            grant.JJ_ANZ_Total_Amount_Actual__c =  amountactual;
            update grant;
    }
    
    /**
    * Reloads the agenda information
    **/
    public void load(){
        items.clear();
        if(EvId!=NULL){
        
            budgetItem.JJ_Hospitality_Total_Actual__c = 0;
            budgetItem.JJ_Hospitality_Total_Plan__c = 0;
            List<Event_Budget__c> budgetlist;
            if(isjanevent){
                budgetlist= [SELECT Id,Name,Account__c,JJ_Hospitality_Total_Actual__c,JJ_Hospitality_Total_Plan__c,JJ_Amount_Actual__c,JJ_Amount_Plan__c,JJ_ARIBA_Ref_No__c,Invoice_Received__c,Janssen_Event__c,JJ_Note__c,Type__c,JJ_Payment_Type__c,JJ_ANZ_Milestone__c, JJ_ANZ_Milestone_Date__c, JJ_ANZ_Milestone_Met__c, JJ_ANZ_Milestone_Number__c FROM Event_Budget__c
                                                WHERE Janssen_Event__c =:EvId ORDER BY Name ASC];
            }
            else if(isffservice){
                budgetlist= [SELECT Id,Name,Account__c,JJ_Hospitality_Total_Actual__c,JJ_Hospitality_Total_Plan__c,JJ_Amount_Actual__c,JJ_Amount_Plan__c,JJ_ARIBA_Ref_No__c,Invoice_Received__c,Janssen_Event__c,JJ_Note__c,Type__c,JJ_Payment_Type__c,JJ_ANZ_Milestone__c, JJ_ANZ_Milestone_Date__c, JJ_ANZ_Milestone_Met__c, JJ_ANZ_Milestone_Number__c FROM Event_Budget__c
                                                WHERE JJ_ANZ_Fee_For_Service__c =:EvId ORDER BY Name ASC];
            }
            else if(isgrant){
                budgetlist= [SELECT Id,Name,Account__c,JJ_Hospitality_Total_Actual__c,JJ_Hospitality_Total_Plan__c,JJ_Amount_Actual__c,JJ_Amount_Plan__c,JJ_ARIBA_Ref_No__c,Invoice_Received__c,Janssen_Event__c,JJ_Note__c,Type__c,JJ_Payment_Type__c,JJ_ANZ_Milestone__c, JJ_ANZ_Milestone_Date__c, JJ_ANZ_Milestone_Met__c, JJ_ANZ_Milestone_Number__c FROM Event_Budget__c
                                                WHERE JJ_ANZ_Grant__c =:EvId ORDER BY JJ_ANZ_Milestone_Number__c ASC];
            }
            
            amountactual = 0;
            amountplan = 0;
            for(Event_Budget__c value:budgetlist){                 
                items.add(new JanssenEventBudgetWrapper(value));
                budgetItem.JJ_Hospitality_Total_Actual__c = value.JJ_Hospitality_Total_Actual__c;
                budgetItem.JJ_Hospitality_Total_Plan__c = value.JJ_Hospitality_Total_Plan__c;
                
                amountactual += (value.JJ_Amount_Actual__c == null)?0:value.JJ_Amount_Actual__c;
                amountplan += (value.JJ_Amount_Plan__c == null)?0:value.JJ_Amount_Plan__c;
            }
            
            if(isjanevent){
                janevent = [select Id, RecordType.DeveloperName, JJ_ANZ_Event_Status__c, JJ_Total_Amount_Actual__c,JJ_Total_Amount_Plan__c,JJ_Organization__c from Medical_Event_vod__c where Id =:EvId];
                
                if(janevent.JJ_ANZ_Event_Status__c == 'Close Out' && janevent.RecordType.DeveloperName.contains('Close_Out')){
                    //actualamtreq = true; //FB206 - removed 
                }
                
                if(budgetlist.isEmpty()){
                    for(JanssenEventBudgetWrapper jebw:items){
                        jebw.value.Account__c = janevent.JJ_Organization__c;
                    }
                }
            }
            
            readonlyfields = new map<string, boolean>();
            hiddenfields = new map<string, boolean>();
            readonlyfields.put('Type__c',false);
            readonlyfields.put('JJ_Amount_Plan__c',false);
            readonlyfields.put('JJ_Amount_Actual__c',false);
            readonlyfields.put('JJ_Note__c',false);
            readonlyfields.put('JJ_Payment_Type__c',false);
            readonlyfields.put('JJ_ARIBA_Ref_No__c',false);
            readonlyfields.put('Invoice_Received__c',false);
            readonlyfields.put('Account__c',false);
            readonlyfields.put('Add_Button',false);
            readonlyfields.put('Delete_Button',false);
            readonlyfields.put('Save_Button',false);
            readonlyfields.put('JJ_ANZ_Milestone_Number__c',false);
            readonlyfields.put('JJ_ANZ_Milestone__c',false);
            readonlyfields.put('JJ_ANZ_Milestone_Date__c',false);
            readonlyfields.put('JJ_ANZ_Milestone_Met__c',false);
            
            hiddenfields.put('JJ_Amount_Plan__c',false);
            hiddenfields.put('JJ_Amount_Actual__c',false);
            hiddenfields.put('JJ_Note__c',false);
            hiddenfields.put('JJ_Payment_Type__c',false);
            hiddenfields.put('JJ_ARIBA_Ref_No__c',false);
            hiddenfields.put('Invoice_Received__c',false);
            hiddenfields.put('Type__c',isgrant);
            hiddenfields.put('Account__c',isgrant);         
            hiddenfields.put('JJ_ANZ_Milestone_Number__c',!isgrant);
            hiddenfields.put('JJ_ANZ_Milestone__c',!isgrant);
            hiddenfields.put('JJ_ANZ_Milestone_Date__c',!isgrant);
            hiddenfields.put('JJ_ANZ_Milestone_Met__c',!isgrant);
                    
            
            
                
            for(Event_Budget_Settings__c bset:[Select Id, Event_Record_Type__c,Event_Record_Type2__c,Event_Record_Type3__c, Budget_Read_Only_Fields__c,Budget_Read_Only_Fields2__c,Budget_Read_Only_Fields3__c, Event_Status__c,Event_Status2__c,Event_Status3__c, Hidden_Field__c, Hidden_Field2__c, Hidden_Field3__c, Profile_Name__c, Profile_Name2__c, Profile_Name3__c, Disable_Add_Button__c,Disable_Delete_Button__c,Disable_Save_Button__c FROM Event_Budget_Settings__c where Is_Grants__c =:isgrant and Is_Fee_For_Service__c =:isffservice and Is_Janssen_Event__c=:isjanevent and Active__c =true]){
                set<String> setprofile = new set<String>();
                if(bset.Profile_Name__c != null){
                    setprofile.addall(bset.Profile_Name__c.split(';'));
                }
                if(bset.Profile_Name2__c != null){
                    setprofile.addall(bset.Profile_Name2__c.split(';'));
                }
                if(bset.Profile_Name3__c != null){
                    setprofile.addall(bset.Profile_Name3__c.split(';'));
                }
                set<String> setEvtStatus =  new set<String>();
                if(bset.Event_Status__c != null){
                    setEvtStatus.addall(bset.Event_Status__c.split(';'));
                }
                if(bset.Event_Status2__c != null){
                    setEvtStatus.addall(bset.Event_Status2__c.split(';'));
                }
                if(bset.Event_Status3__c != null){
                    setEvtStatus.addall(bset.Event_Status3__c.split(';'));
                }
                set<String> setRecType =  new set<String>();                
                if(bset.Event_Record_Type__c != null){
                    setRecType.addall(bset.Event_Record_Type__c.split(';'));
                }        
                if(bset.Event_Record_Type2__c != null){
                    setRecType.addall(bset.Event_Record_Type2__c.split(';'));
                }        
                if(bset.Event_Record_Type3__c != null){
                    setRecType.addall(bset.Event_Record_Type3__c.split(';'));
                }
                
                profilename = mapProfilename.get(UserInfo.getProfileId());              
                if(isjanevent){
                recordtypename = janevent.RecordType.DeveloperName;
                eventstatus = janevent.JJ_ANZ_Event_Status__c;
                }
                else if(isffservice){ 
                recordtypename = speakagree.RecordType.DeveloperName;
                eventstatus = speakagree.JJ_ANZ_Speaker_Status__c;
                }
                else if(isgrant){
                recordtypename = grant.RecordType.DeveloperName;
                eventstatus = grant.JJ_ANZ_Status__c;
                }
                
                
                
                if((setprofile.contains(profilename) || (bset.Profile_Name__c == null && bset.Profile_Name2__c == null && bset.Profile_Name3__c == null))&&
                (setEvtStatus.contains(eventstatus) || (bset.Event_Status__c == null && bset.Event_Status2__c == null && bset.Event_Status3__c == null))&&
                (setRecType.contains(recordtypename) || (bset.Event_Record_Type__c == null && bset.Event_Record_Type2__c == null && bset.Event_Record_Type3__c == null))){
                    
                    if(bset.Disable_Add_Button__c){
                        readonlyfields.put('Add_Button',bset.Disable_Add_Button__c);
                    }
                    if(bset.Disable_Delete_Button__c){
                        readonlyfields.put('Delete_Button',bset.Disable_Delete_Button__c);
                    }
                    if(bset.Disable_Save_Button__c){
                        readonlyfields.put('Save_Button',bset.Disable_Save_Button__c);
                    }
                    
                    if(bset.Budget_Read_Only_Fields__c != null){
                        for(string s:bset.Budget_Read_Only_Fields__c.split(';')){
                            readonlyfields.put(s, true);                        
                        }
                    }
                    if(bset.Budget_Read_Only_Fields2__c != null){
                        for(string s:bset.Budget_Read_Only_Fields2__c.split(';')){
                            readonlyfields.put(s, true);                        
                        }
                    }
                    if(bset.Budget_Read_Only_Fields3__c != null){
                        for(string s:bset.Budget_Read_Only_Fields3__c.split(';')){
                            readonlyfields.put(s, true);                        
                        }
                    }
                    if(bset.Hidden_Field__c != null){
                        for(string s:bset.Hidden_Field__c.split(';')){
                            hiddenfields.put(s, true);
                        }
                    }                   
                    if(bset.Hidden_Field2__c != null){
                        for(string s:bset.Hidden_Field2__c.split(';')){
                            hiddenfields.put(s, true);
                        }
                    }
                    if(bset.Hidden_Field3__c != null){
                        for(string s:bset.Hidden_Field3__c.split(';')){
                            hiddenfields.put(s, true);
                        }
                    }
                
                }
            }    
                        
        }
    }
    
}