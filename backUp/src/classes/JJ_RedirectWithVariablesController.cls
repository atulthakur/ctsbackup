/**
*      @author Keerthana Thallam
*   
       @description    Contains the logic for redirecting to the case craetion page with parameters 
       

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------

**/



public with sharing class JJ_RedirectWithVariablesController {


    /*
     *  Method name:    redirectUser
     *  @description    Redirects the user to the given object + parameters
     *  @return         PageReference The page the user needs to be redirect to.
     */
    public PageReference redirectUser(){
        
        //Get object name
        String strObject = System.currentPageReference().getParameters().get('object');
        
        //Allow record type selection? if set to yes, then allow RTS
        String strEnableRTS = System.currentPageReference().getParameters().get('allowRTS');
        
        //check if recordType is passed
        string recType=System.currentPageReference().getParameters().get('RecordType');
        
        //If there is an object name
        if(strObject != ''){
            
            //Create a generic object based on the name
            Schema.Sobjecttype oGenericObj = Schema.getGlobalDescribe().get(strObject);     
            
            //If a real object has been given
            if(oGenericObj != null){
                
                PageReference pReference = null;
                
                //Check if we have to use record type selection or not
                if(strEnableRTS != '' && strEnableRTS == 'true'){
                    
                    //If we need RTS, the URL will have to point to the recordtypeselect.jsp
                    if(oGenericObj.getDescribe().custom){
                        pReference = new PageReference('/setup/ui/recordtypeselect.jsp?ent=' +oGenericObj.getDescribe().getKeyPrefix()+ '&save_new_url=/' + oGenericObj.getDescribe().getKeyPrefix() + '/e?');                   
                    }
                    else{
                        pReference = new PageReference('/setup/ui/recordtypeselect.jsp?ent=' + strObject + '&save_new_url=/' + oGenericObj.getDescribe().getKeyPrefix() + '/e?');                   
                    }
                        
                }
                else{
                
                    //Else create the page reference to the edit page of this object
                    pReference = new PageReference('/' +  oGenericObj.getDescribe().getKeyPrefix() + '/e');
                }
                
                //Also get a separate reference - we always need to get the parameters based on the edit page
                PageReference pEditReference;
                if(recType<>null)
                    pEditReference= new PageReference('/' +  oGenericObj.getDescribe().getKeyPrefix() + '/e?RecordType='+recType+'&nooverride=1');
                else
                    pEditReference= new PageReference('/' +  oGenericObj.getDescribe().getKeyPrefix() + '/e?nooverride=1');    
                //Get all current parameters - this could be either edit page or the record type selection.
                Map<String, String> m = pReference.getParameters();
                
                //Create the parameters for the URL (translates field to ID)             
                m.putAll(JJ_StaticFunctions.createLabelParameters(pEditReference,oGenericObj.newSObject()));
                     if(JJ_StaticConstants.CaseRecTypeMapByName.containsKey(JJ_StaticConstants.RTYPE_AE)
                     &&JJ_StaticConstants.CaseRecTypeMapByName.containsKey(JJ_StaticConstants.RTYPE_CANADA_AE)
                     &&JJ_StaticConstants.CaseRecTypeMapByName.containsKey(JJ_StaticConstants.RTYPE_Canada_PQC))
                     {
                         if(!((recType==JJ_StaticConstants.CaseRecTypeMapByName.get(JJ_StaticConstants.RTYPE_AE).getRecordTypeId())||
                        (recType==JJ_StaticConstants.CaseRecTypeMapByName.get(JJ_StaticConstants.RTYPE_CANADA_AE).getRecordTypeId())
                   ||recType==JJ_StaticConstants.CaseRecTypeMapByName.get(JJ_StaticConstants.RTYPE_Canada_PQC).getRecordTypeId()))
                    m.put('nooverride', '1');
                     
                     }
                     
                        
                    
                m.put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
                
                return pReference;
            }
        }
        
        //Return null if someone is playing with the URL
        return null;
    }
}