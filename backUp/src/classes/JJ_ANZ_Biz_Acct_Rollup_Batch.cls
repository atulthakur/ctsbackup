global class JJ_ANZ_Biz_Acct_Rollup_Batch implements Database.Batchable<sObject>{
    private final string initialstate;
    private datetime timenow;
    string query;
    global JJ_ANZ_Biz_Acct_Rollup_Batch(){
         timenow = system.now();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        Janssen_Settings__c vsc  = Janssen_Settings__c.getOrgDefaults();
        datetime timethen = vsc.JJ_ANZ_Last_Biz_Acct_Rollup_Batch_Run__c;
        query = 'select id ,' +
                '       JJ_ANZ_Janssen_Biz_Acct_Rules__c,' +
                '       JJ_ANZ_Teams__c,' +
                '       (select id ,' +
                '               child_account_vod__c,' +
                '               child_account_vod__r.jj_anz_janssen_territory_rule__c,' +
                '               child_account_vod__r.specialty_1_vod__c,' +
                '               child_account_vod__r.jj_core_professional_type__c,' +
                '               child_account_vod__r.jj_anz_janssen_biz_acct_rules__c,' +
                '               child_account_vod__r.jj_anz_teams__c' +
                '        from parent_account_vod__r)' +
                ' from account' +
                ' where id IN (select parent_account_vod__c' +
                '             from child_account_vod__c' +
                '             where child_account_vod__r.systemmodstamp >=' + 
                              timethen.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') +')';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC , list<sObject> batch){
        
    /*The query above is selecting all accounts who have child accounts that have been updated
    since the last time the code has ran. Any fields related to territory rule processing are queried
    for these related child accounts. Below we will process through the parent accounts and add all the values
    contained in the related children's jj_anz__teams__c field into same field for the business account in a
    semicolon delimited fashion. Since the rules being run off this field are contains, there is no need to control
    for duplicate values being entered in the field. To rollup additional fields to the business account,
    be sure to add the field to the query above, and include it in the processing which will be noted where to do so below*/
        JJ_ANZ_Process_Futures_Util.setnofutureprocess(true);
        list<account> updaccts = new list<account>();
        for (sObject obj : batch){
            string bigbucket = ';';
            account bizacct = (account)obj;
            list<child_account_vod__c> children = new list<child_account_vod__c>();
            for(child_account_vod__c addchild : bizacct.parent_account_vod__r){
                children.add(addchild);
                system.debug(addchild.child_account_vod__r.jj_anz_teams__c);
            }
                
            for (child_account_vod__c CA : children){
                //here is where additional fields can be added if they are queried above
                if(CA.child_account_vod__r.jj_anz_teams__c != null && CA.child_account_vod__r.jj_anz_teams__c.length() > 2){
                    if(!bigbucket.contains(CA.child_account_vod__r.jj_anz_teams__c)){
                        if(bigbucket.length() + CA.child_account_vod__r.jj_anz_teams__c.length() <= 254){
                        bigbucket += CA.child_account_vod__r.jj_anz_teams__c + ';';
                        }
                    }
                } 
            }
            bizacct.JJ_ANZ_teams__c = bigbucket;
            bizacct.JJ_ANZ_callout_trigger__c = true;
            updaccts.add(bizacct);
        }
        if(updaccts.size() > 0){
            update updaccts;
        }
    }
    global void finish(Database.BatchableContext BC) {
    Janssen_Settings__c vsc = Janssen_Settings__c.getOrgDefaults();
    vsc.JJ_ANZ_Last_Biz_Acct_Rollup_Batch_Run__c = timenow;    
        update vsc;
    JJ_ANZ_Process_Futures_Util.setnofutureprocess(false);
    }
}