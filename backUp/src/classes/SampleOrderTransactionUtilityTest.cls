@isTest
public class SampleOrderTransactionUtilityTest{
    static testmethod void testcreateSampleBRCTracking() {
        //Commented the creation of account as this will go to veeva server.....
        /*Account testAccount = new Account();
        testAccount.Name = 'TestCaseSOT';
        insert testAccount;*/
        
        Sample_Order_Transaction_vod__c sot = new Sample_Order_Transaction_vod__c();
        //sot.Account_vod__c = testAccount.Id;
        insert sot;
        
        List<Sample_BRC_Tracking__c> brcTrackings = [select id from Sample_BRC_Tracking__c where Transaction_Id__c= :sot.Id];
        
        System.assertEquals(1,brcTrackings.size());
    } 
}