public with sharing class JJ_CaseParent {
public Case currentRecord {get;set;}
public Id ParentId {get;set;}
public Case ChildCase{get;set;}
public List<Case> PrimaryCase{get;set;}
    public JJ_CaseParent(ApexPages.StandardController controller) {
        currentRecord  =(Case)controller.getRecord();
        ChildCase = [select id,parentid from case where id =:currentRecord.id];
        if(ChildCase!=null)
        {
            PrimaryCase = [select id from case where id =:ChildCase.parentid];
            if(!PrimaryCase.isEmpty())
            {
                ParentId = PrimaryCase[0].Id;
            }
        }
        
    }

}