global class JJ_ANZ_Sample_Quota_INV_Calc implements Database.Batchable<sObject>{
    //purpose of this class is to calculate the current inventory for any samples a rep has
    //this then allows reporting by SOSA to send out new orders to SAP for topping up the reps
    //inventory based on the allowed quota that is input by marketing
    private final string initialstate;
    
    global JJ_ANZ_Sample_Quota_Inv_Calc(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
    //grabbing all Sample Quota records which is equal to the number of reps * the number of sample products per rep
        String Query = 'Select id , JJ_ANZ_Current_Inventory__c , JJ_ANZ_Monthly_Quota__c , JJ_ANZ_Marketing_Approved__c ,'
                     + 'JJ_ANZ_New_Quota__c , JJ_ANZ_Representative__c , JJ_ANZ_Sample_Product__r.name ,'
                     + 'JJ_ANZ_Place_Shipment_On_Hold__c from JJ_ANZ_Sample_Quotas__c';
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC , List<sObject> batch) {
        
    //will iterate through all sample lots and if the owner and sample match the sample quota record
    //it will add the calculated quantity of the individual batch to the current inventory of the sample quota
        List<JJ_ANZ_Sample_Quotas__c> UpdQuota = new List<JJ_ANZ_Sample_Quotas__c>();
        For (sObject chunk : batch){
            JJ_ANZ_Sample_Quotas__c Quota = (JJ_ANZ_Sample_Quotas__c)chunk;
            For(Sample_Lot_vod__c SLot : [Select id , owner.id, sample_vod__c , Calculated_Quantity_vod__c
                                          from Sample_lot_vod__c where active_vod__c = true]){
                                              If(Slot.owner.id == Quota.jj_anz_representative__c && Slot.Sample_vod__c == Quota.jj_anz_sample_product__r.name){
                                                  Quota.jj_anz_current_inventory__c =+ Slot.calculated_quantity_vod__c;
                                              
                                          }
            
            }
            UpdQuota.add(Quota);
        }
        if(UpdQuota.size() > 0){
        update UpdQuota;
        }
    }
    global void finish(Database.BatchableContext BC) {
    }
}