/**
*      @author Atul Thakur
*   
       @description    Batch class for integrating Veeva Vault documents with Salesforce Knowledge Articles
       

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        Atul                        25/August/2017        Moving Vault from ASPAC to Global
        ------------------------------------------------------------------------------------

**/

global class JJ_ANZ_ArticlesIntegration implements Database.batchable<sObject>,Database.AllowsCallouts{

 public String authToken = JJ_StaticConstants.BLANK;
 public Boolean isDA=False;
 public String integrationName= System.Label.JJ_ANZ_integrationName;
 public map<String,String> specialCharMap = new map<String,String>{'"id"' =>'"id"','"name__v"' =>'"name"'};
 public JJ_Remote_Site_Settings__c vaultCredentials = JJ_Remote_Site_Settings__c.getInstance(integrationName);
 public String vaultBaseUrl  = vaultCredentials.JJ_Remote_Service_End_Point__c;
 public string QueryString = JJ_StaticConstants.BLANK;
 public List<JJ_ANZ_VeevaCountrySettings__c > Country=new List<JJ_ANZ_VeevaCountrySettings__c >();
 public List<JJ_ANZ_VeevaTypeSettings__c> Type=new List<JJ_ANZ_VeevaTypeSettings__c>();
 public List<JJ_ANZ_VeevaSubtypeSettings__c> SubType=new List<JJ_ANZ_VeevaSubtypeSettings__c>();
 public List<JJ_ANZ_VeevaClassificationSettings__c> Classification=new List<JJ_ANZ_VeevaClassificationSettings__c>();
 public List<JJ_ANZ_VeevaSizeSettings__c> Size=new List<JJ_ANZ_VeevaSizeSettings__c>();
 public List<JJ_ANZ_ArticlesIdentifier__c> articles;
 public String CountryFilter=JJ_StaticConstants.BLANK;
 public String SubTypeFilter=JJ_StaticConstants.BLANK;
 public String ClassificationFilter=JJ_StaticConstants.BLANK;
 public map<String,String> vaultCountryMap;
 public String countryQueryString=JJ_StaticConstants.BLANK;
 public List<JJ_ANZ_VeevastatusSettings__c > status=new List<JJ_ANZ_VeevastatusSettings__c >();
 public String statusFilter = JJ_StaticConstants.BLANK;    
        /*
        * @Class name: authenticationResponseWrapper
        * @description: contains the attributes that are returned in the response while an authentication call is made to the Veeva vault
        */   
    public class authenticationResponseWrapper {
        public String responseStatus;         //Contains 'Success/failure'  
        public String sessionId;             // SessionId - to be retrived an used as authorization token in further calls  
        public Integer userId; 
        public Integer vaultId; 
        
            /*default constructor 
            @TestVisible user becuase Using the TestVisible annotation to allow test methods to access private or 
            protected members of another class outside the test class */      
       
       @TestVisible authenticationResponseWrapper(){
            this.responseStatus = JJ_StaticConstants.BLANK;
            this.sessionId = JJ_StaticConstants.BLANK;
            this.userId = 0;
            this.vaultId = 0;
        }
    }
    
    public class  knowledgeResponseWrapper{
    public String Id{get;set;}
    public String name{get;set;}
    }
    
    public void setAuthToken(){
        
            authToken = JJ_StaticConstants.BLANK; //initalize authentication token
            JJ_HTTPService httpServiceUtility = new JJ_HTTPService();
            try {
            
            httpServiceUtility.setAuthenticationParamtersPrepareRequest(integrationName);
            //prepare the request to authenticate veeva vault
            HttpResponse response = httpServiceUtility.doCallout();
            system.debug('Response is ------'+response);
                  
         //parse the response and convert it into the wrapper
            authenticationResponseWrapper authResponseWrap = (authenticationResponseWrapper) JSON.deserialize(response.getBody(), authenticationResponseWrapper.class);
            if(authResponseWrap.responseStatus.equals(JJ_StaticConstants.SUCCESS) && authResponseWrap.sessionId <> JJ_StaticConstants.BLANK)
                authToken = authResponseWrap.sessionId; // store the authtoken in a variable
                system.debug('&&&& authToken'+authToken);
            //add the request to the logs
            
        }catch(Exception ex){
          if(ex.getTypeName().equals(JJ_StaticConstants.CALLOUT_EXCEPTION) && ex.getMessage().contains(JJ_StaticConstants.Error_Type_READ_TIMED_OUT)){
                system.debug('Error in setAuthToken');
                }
            throw ex;
        }
    }
    /*
    * Method name : replaceSpecialCharsInResponse()
    * @description: Replaces the strings with special chars with the strings as per the wrapper class attributes. 
                    a static map with the Key-value pair is maintained, the strings are replaced as per the values present in the map.
    * @param      : String responseBody : response body that contains special characters
    */ 
    public String replaceSpecialCharsInResponse(String responseBody){
        
        String replacedString = JJ_StaticConstants.BLANK;
        //Replace the special token with the supported Naming convention
        for(String key : specialCharMap.keyset()){
            if(responseBody.contains(key))
                responseBody = responseBody.replaceAll(key,specialCharMap.get(key));
        }
        return responseBody;
    }
    
    public class cls_responseDetails {
        public Integer limit1;  
        public Integer offset;  
        public Integer size;    
        public Integer total;
        //default constructor
        cls_responseDetails(){
            this.offset = 0;
            this.size = 0;
            this.total = 0;
            this.limit1 = 0;
        }   
    }
    
        public class VaultDetailsWrapper{
        public String responseStatus;   //SUCCESS
        public cls_responseDetails responseDetails;
        public List<knowledgeResponseWrapper> data;
      //  public list<cls_errors> errors;     // Contains error details in case if the response status contains 'Failure'
        VaultDetailsWrapper(){
            responseStatus=JJ_StaticConstants.BLANK;
            responseDetails = new cls_responseDetails();
            data = new List<knowledgeResponseWrapper>();
           // errors = new List<cls_errors>();
        }
    }
     global Database.QueryLocator start(Database.BatchableContext info){
   String publishStatus='Online';
   String language='en_US';
   String query='SELECT id,Title,KnowledgeArticleId from JJ_ANZ_MedInfo_Documents__kav WHERE PublishStatus =:publishStatus AND Language =:language';
   return Database.getQueryLocator(query);
  
   
   }    

   global void execute(Database.BatchableContext info, List<JJ_ANZ_MedInfo_Documents__kav> scope) {
     setAuthToken();
     VaultDetailsWrapper  DetailsFromVault = new VaultDetailsWrapper();
     JJ_HTTPService httpService = new JJ_HTTPService();
     HttpResponse resp = new HttpResponse();
     DateTime lasthour=Datetime.now().addHours(-1);
     String last=String.valueOfGmt(lasthour);
     String compareString=last.left(10)+'T'+last.right(8)+'.'+'000Z';
     articles=new List<JJ_ANZ_ArticlesIdentifier__c>();
     System.debug(compareString);
     // COMMENTED for checking
     QueryString='select+id+from+documents+where+((version_modified_date__v+>+'+ JJ_StaticConstants.SINGLE_QUOTE+compareString+'\')+OR+(version_modified_date__v+=+'+ JJ_StaticConstants.SINGLE_QUOTE+compareString+'\'))';
     //QueryString='select+id,name__v,version_modified_date__v+from+documents+where+(version_modified_date__v+>+'+JJ_StaticConstants.SINGLE_QUOTE+'2015-03-12T16:24:54.000Z'+'\')';
     //String testString='Testing Logic CTS 2 part';
     //QueryString='select+id,name__v,version_modified_date__v+from+documents+where+(status__v+!=+'+ JJ_StaticConstants.SINGLE_QUOTE+'Withdrawn'+'\')';
     //QueryString=QueryString+'+AND+(status__v+!=+'+ JJ_StaticConstants.SINGLE_QUOTE+'Draft'+'\')';
     //QueryString='select+id,name__v,version_modified_date__v+from+documents+where+(status__v+!=+'+ JJ_StaticConstants.SINGLE_QUOTE+'Draft'+'\')';
     Country=[select name from JJ_ANZ_VeevaCountrySettings__c];
     Type=[select name from JJ_ANZ_VeevaTypeSettings__c];
     SubType=[select name from JJ_ANZ_VeevaSubtypeSettings__c];
     Classification=[select name from JJ_ANZ_VeevaClassificationSettings__c];
     Size=[select name from JJ_ANZ_VeevaSizeSettings__c];
     status = [select name from JJ_ANZ_VeevastatusSettings__c];
     
     HttpResponse countryResp = new HttpResponse();
     VaultDetailsWrapper  countryDetailsFromVault = new VaultDetailsWrapper();
     vaultCountryMap=new map<String,String>();
     
          //Getting vault country data
     countryQueryString='select+id,name__v+from+country__v';
     httpService.addToRequestHeaders(isDA, authToken);
           httpService.prepareRequest(null,vaultBaseUrl+'query?q='+countryQueryString,JJ_StaticConstants.GET);
           countryResp = httpService.doCallout();
            if(countryResp <> null){
                    system.debug('**** response string product '+countryResp.getBody());
                    //de-serialize the response and replace the special characters in the response string
                    countryDetailsFromVault = (VaultDetailsWrapper) JSON.deserialize(replaceSpecialCharsInResponse(countryResp.getBody()), VaultDetailsWrapper.class);
                    system.debug('DetailsFromVault.data--'+countryDetailsFromVault .data);
                     if( countryDetailsFromVault.data<>null){
                     for(knowledgeResponseWrapper doc : countryDetailsFromVault.data){
                     vaultCountryMap.put(doc.name,doc.Id);
                     }
                     }
                    }
     //Adding status Filter this one already commented
 /* if(status<>null){
    statusFilter='status__v+CONTAINS+';
     for(Integer i=0;i<status.size();i++){
     JJ_ANZ_VeevastatusSettings__c obj=status[i];
     if((status.Size()>1) && (i==0))
        statusFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';
     if(status.Size()==1)
        statusFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
     if(status.Size()>1&& (i<>status.Size()-1)&&i<>0) 
        statusFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';  
     if((status.Size()>1) && (i==status.Size()-1))
       statusFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
          } 
        QueryString=QueryString+'+AND+'+statusFilter;     
  }*/
    
    //Commented by atul for checking  
        
 //Adding Country Filter          
  if(Country<>null){
    CountryFilter='country__v+CONTAINS+';
     for(Integer i=0;i<Country.size();i++){
     JJ_ANZ_VeevaCountrySettings__c obj=Country[i];
     if((Country.Size()>1) && (i==0))
        CountryFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+vaultCountryMap.get(obj.Name)+'\'';
     if(Country.Size()==1)
        CountryFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+vaultCountryMap.get(obj.Name)+'\')';
     if(Country.Size()>1&& (i<>Country.Size()-1)&&i<>0) 
        CountryFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+vaultCountryMap.get(obj.Name)+'\'';  
     if((Country.Size()>1) && (i==Country.Size()-1))
       CountryFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+vaultCountryMap.get(obj.Name)+'\')';
          } 
        QueryString=QueryString+'+AND+'+CountryFilter;     
          }
     
  //Adding Type Filter        
         if(Type<>null){
     String TypeFilter=JJ_StaticConstants.BLANK;
     for(Integer i=0;i<Type.size();i++){
     JJ_ANZ_VeevaTypeSettings__c  obj=Type[i];
     if((Type.Size()>1) && (i<>Type.Size()-1))
        TypeFilter+='(type__v+=+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')+OR+';  
     if((Type.Size()==1)||(i==Type.Size()-1))
          TypeFilter+='(type__v+=+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
     if((Type.Size()>1) && (i==Type.Size()-1))
          QueryString=QueryString+'+AND+'+'('+TypeFilter+')';
               }
     if(Type.Size()==1)
        QueryString=QueryString+'+AND+'+TypeFilter;
          }
          
    //Adding SubType Filter
    if(SubType<>null){
    SubTypeFilter='subtype__v+CONTAINS+';
     for(Integer i=0;i<SubType.size();i++){
     JJ_ANZ_VeevaSubTypeSettings__c  obj=SubType[i];
     if((SubType.Size()>1) && (i==0))
        SubTypeFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';
     if(SubType.Size()==1)
        SubTypeFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
     if(SubType.Size()>1&& (i<>SubType.Size()-1)&&i<>0) 
        SubTypeFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';  
     if((SubType.Size()>1) && (i==SubType.Size()-1))
       SubTypeFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
          } 
        QueryString=QueryString+'+AND+'+SubTypeFilter;     
          } 
          
  //Adding Classification Filter 
  /*        if(Classification<>null){
    ClassificationFilter='classification__v+CONTAINS+';
     for(Integer i=0;i<Classification.size();i++){
     JJ_ANZ_VeevaClassificationSettings__c  obj=Classification[i];
     if((Classification.Size()>1) && (i==0))
         ClassificationFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';
     if(Classification.Size()==1)
         ClassificationFilter+='('+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
     if(Classification.Size()>1&& (i<>Classification.Size()-1)&&i<>0) 
         ClassificationFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\'';  
     if((Classification.Size()>1) && (i==Classification.Size()-1))
        ClassificationFilter+='+,+'+JJ_StaticConstants.SINGLE_QUOTE+obj.Name+'\')';
          } 
        QueryString=QueryString+'+AND+'+ ClassificationFilter;     
          }  */
       
          /*
    //Adding Size Filter 
     if(Size<>null){
     String SizeFilter=JJ_StaticConstants.BLANK;
     for(Integer i=0;i<Size.size();i++){
     JJ_ANZ_VeevaSizeSettings__c  obj=Size[i];
     if((Size.Size()>1) && (i<>Size.Size()-1))
        SizeFilter+='(size__v+<'+obj.Name+')+OR+';  
     if((Size.Size()==1)||(i==Size.Size()-1))
          SizeFilter+='(size__v+<'+obj.Name+')';
     if((Size.Size()>1) && (i==Size.Size()-1))
          QueryString=QueryString+'+AND+'+'('+SizeFilter+')';
               }
     if(Size.Size()==1)
        QueryString=QueryString+'+AND+'+SizeFilter;
          } 
          
           */
           
           try{ 
           httpService.addToRequestHeaders(isDA, authToken);
            system.debug('QueryString'+QueryString);
            httpService.prepareRequest(null,vaultBaseUrl+'query?q='+QueryString,JJ_StaticConstants.GET);
           resp = httpService.doCallout();
           system.debug('Documents Response is ....'+resp);
           system.debug('**** response string product '+resp.getBody());
            if(resp <> null){
             DetailsFromVault = (VaultDetailsWrapper) JSON.deserialize(replaceSpecialCharsInResponse(resp.getBody()), VaultDetailsWrapper.class);
                    system.debug('DetailsFromVault.data--'+DetailsFromVault.data);
                    if(DetailsFromVault.data<>null){
                    for(knowledgeResponseWrapper doc : DetailsFromVault.data){
                    JJ_ANZ_ArticlesIdentifier__c newRec=new JJ_ANZ_ArticlesIdentifier__c();
                    newRec.JJ_ANZ_DocId__c=doc.Id;
                    articles.add(newRec);
                    }
                    
                    }
                    if(articles!=null)
                      upsert articles JJ_ANZ_DocId__c;
            
            }
           
           }
             catch(Exception ex){
          if(ex.getTypeName().equals(JJ_StaticConstants.CALLOUT_EXCEPTION) && ex.getMessage().contains(JJ_StaticConstants.Error_Type_READ_TIMED_OUT)){
                system.debug('Error in setAuthToken');
                }
            throw ex;
        }


   }  
    global void finish(Database.BatchableContext info){    
   
  }

}