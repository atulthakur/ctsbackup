/** Subject  : EmailMessageHandler

Description  : Method To update field 'New Email PAR' in parent object (Case) of EmailMessage to update PAR status.
Author       :Sunita & Shanmuga || schand63@its.jnj.com & SPriyan3@its.jnj.com ||30/11/2017

**/



public class EmailMessageHandler{

    /* Method to update field 'New Email' in parent object (Case) of EmailMessage */
    public static void UpdateCaseNewEmailwithEmailStatus(Set<Id> caseIds){
        
        Set<Id> validCaseIds = new Set<Id>();
         
        List<Case> listAllCases = [Select Id, JJ_ANZ_New_Email__c from Case where Id IN :caseIds];

        for(EmailMessage objEM : [SELECT Id, ParentId FROM EmailMessage WHERE Status = '0' AND ParentId IN :caseIds]){
             
            validCaseIds.add(objEM.ParentId);            
        }
         
        for(Case objCase : listAllCases){
             
            if(validCaseIds.contains(objCase.Id)){
                objCase.JJ_ANZ_New_Email__c = true;             
            }
            else{
                objCase.JJ_ANZ_New_Email__c = false;
            }
        }

        try{        
            update listAllCases;
        }
        catch(Exception e){
            System.debug(LoggingLevel.Info, 'Exception caught: ' + e);
        }
        
        
        

    }
    
    
     /* Method to update field 'New Email' in parent object (PAR Case) of EmailMessage */
    public static void UpdatePARCaseNewEmailwithEmailStatus(Set<Id> caseIds){
        
        Set<Id> validCaseIds = new Set<Id>();
         
        List<Case> listAllCases = [Select Id, JJ_New_Email_PAR__c,Status  from Case where Id IN :caseIds];

        for(EmailMessage objEM : [SELECT Id, ParentId FROM EmailMessage WHERE Status = '3' AND ParentId IN :caseIds]){
             
            validCaseIds.add(objEM.ParentId);            
        }
         
        for(Case objCase : listAllCases){
             
            if(validCaseIds.contains(objCase.Id)){
            
            if(objCase.status=='Approved'){
                objCase.JJ_New_Email_PAR__c= true; 
                //objCase.Status= Apporved;              
            }}
            else{
                objCase.JJ_New_Email_PAR__c = false;
            }
        }

        try{        
            update listAllCases;
        }
        catch(Exception e){
            System.debug(LoggingLevel.Info, 'Exception caught: ' + e);
        }

    }

}