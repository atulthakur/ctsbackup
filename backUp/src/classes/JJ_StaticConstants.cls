/**
 *      @author Keerthana Thallam
 *    
        @description    Class that stores all static constants used
 
        Function: Repository of all static constants.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        
        **/      

global class JJ_StaticConstants {
    
    //Case record type map
    public static map<string,Schema.RecordTypeInfo> CaseRecTypeMapByName=schema.Sobjecttype.Case.getRecordTypeInfosByName();
    
    //case rec type by id
    public static map<id,Schema.RecordTypeInfo> CaseRecTypeMapById=schema.Sobjecttype.Case.getRecordTypeInfosById();
    
     public static final String ACC_EMP_RECTYPE_NAME='Employee';
    
    //call record type
    public static map<string,Schema.RecordTypeInfo> CallRecTypeMapByName=schema.Sobjecttype.Call2_vod__c .getRecordTypeInfosByName();
    
    //Account Record type map
    public static map<string,Schema.RecordTypeInfo> AccountRecTypeMapByName=schema.Sobjecttype.Account.getRecordTypeInfosByName();
   
    public static map<string,Schema.RecordTypeInfo> MedicalInquiryRecTypeMapByName=schema.Sobjecttype.Medical_Inquiry_vod__c.getRecordTypeInfosByName();
    
    //MIR - No Signature CA  MI Record type
    public static final String RTYPE_MIR_CA_NO_SIGNATURE='MIR - No Signature CA';
    
    //MIR - No Signature CA  MI Record type
    public static final String RTYPE_MIR_CA_NO_SIGNATURE_DEVNAME='Medical_Inquiry_No_Signature_CA_JNJ';
    
    //MIR - No Signature CA - HCP  MI Record type
    public static final String RTYPE_MIR_CA_NO_SIGNATURE_HCP='MIR - No Signature CA - HCP';
    
     //MIR - No Signature CA - HCP  MI Record type
    public static final String RTYPE_MIR_CA_NO_SIGNATURE_HCP_DEVNAME='MIR_No_Signature_CA_HCP';
    
    //  MIR - Send To MIC MI rec type
    public static final String RTYPE_MIR_SEND_MIC='MIR - Send To MIC';
    
    //  MIR - Send To MIC MI rec type Dev Name
    public static final String RTYPE_MIR_SEND_MIC_DEVNAME='Medical_Inquiry_Send_To_MIC';
    
    //  MIR - Send To MIC - SR MI rec type
    public static final String RTYPE_MIR_SEND_MIC_SR='MIR - Send To MIC - SR';
    
    //  MIR - Send To MIC - SR MI rec type Dev Name
    public static final String RTYPE_MIR_SEND_MIC_SR_DEVNAME='MIR_Send_To_MIC_SR';
    
    //MIR - Self fulfilled - SR  MI Record type
    public static final String RTYPE_MIR_SELF_FULFILLED_SR='MIR - Self fulfilled - SR';
       
    //Record type of type Header for Case
    public static final String RTYPE_HEADER='Header Case';

    //Record type of type US Header for Case
    public static final String RTYPE_US_HEADER='US Header Case';
    
    //Record type of type Canada Header for Case
    public static final String RTYPE_CANADA_HEADER='Canada Header Case';    
    
    //Record type of type Cordis MIR for Case
    public static final String RTYPE_CORDIS_MIR='Cordis MIR';
    
    //Record type of type AE for Case
    public static final String RTYPE_AE='Adverse Event';
    
    //Record type of type Canada AE for Case
    public static final String RTYPE_CANADA_AE='Canada AE';
   
    //Record type of type Cordis PC for Case
    public static final String RTYPE_CORDIS_PC='Cordis PC';
    
    //Record type of type Cordis GI for Case
    public static final String RTYPE_CORDIS_GI='Cordis GI'; 
    
    //Record type of type GI for Case
    public static final String RTYPE_GI='General Inquiry'; 
    
      //Record type of type GI for Canada
    public static final String RTYPE_CANAdA_GI='Canada GI';   

    //Record type of type Monograph/Compendia for Canada
    public static final String RTYPE_Canada_MC = 'Monograph/Compendia';
    
    
    //Record type of type Canada MIR for Case
    public static final String RTYPE_Canada_MIR='Canada MIR';
    
    //Record type of type US MIR for Case
    public static final String RTYPE_US_MIR='US MIR';
    
    //Record type of type Canada PQC
    public static final String RTYPE_Canada_PQC='Canada PQC';
    
    //Record type of type Settlements for Case
    public static final String RTYPE_SETTLEMENTS='Settlements';
    
    //Record type of type Followup PQC for Case
    public static final String RTYPE_FollowUpPQC='PQC Follow Up';
     
     //Record type of type US PQC for Case
    public static final String RTYPE_USPQC='PQC';
    
    //Record type of type Business Account for Account
    public static final String RTYPE_BAccount='Business Account';
    
    //REcord type for call
    public static final String RTYPE_CALL='US_MSL_From_MIC';
    
    //Default status for Call
     public static final String CALL_TYPE='Planned_vod';
    
    //Employee record type developer name for account
    public static final String EMPLOYEE_DEVNAME='JJ_Employee';
    
    //Person account record type developer name for account
    public static final String PERSON_DEVNAME = 'PersonAccount';
    
    //Consumer record type developer name for account
    public static final String CONSUMER_DEVNAME = 'JJ_Consumer';
    
    //Default Interaction type for call discussion
    public static final String CALLDISCUSSION_INERACTION_TYPE='Unsolicited';
    
    //error log type -class
    public static final String TXT_CLASS='Class';
    public static final String TXT_RECORDTYPE ='RecordType';
    
    //Canada veeva case queue name
    public static final String CANADA_VEEVA_QUEUE='Canada Veeva Cases';
    
    //US veeva case queue name
    public static final String US_VEEVA_QUEUE='US Veeva Cases';
    
    // Determine Submit CHSS 
    public static Boolean mailSent = false;
    
    public static Boolean hasAlreadyMailSent(){
        return mailSent;
    }
    
    public static void setMailStatus(){
        mailSent = true;
    }
    
    /*** Iconnect US Canada Module Constants
            Deloitte Team ****/
    public final static string UserGroupAssociateAgent = 'Associate';
    public final static string UserGroupManagerDirector = 'Manager/Director';
    public final static String UserGroupAllProfile = 'N/A';
   
                
            //Status as closed  for Header and associate child cases @@@@
    public static final String STATUSCLOSED ='Closed';
    
     //Status as cancelled  for Header and associate child cases @@@@
    public static final String STATUSCANCELLED ='cancelled';
    
    
    
    //Sample method used for Static variable test class
     public void sampleMethod() {}
 
    // Merged on 0806 by Vinay from cccapollo1
    public static final String  SEMICOLON               = ';';
    public static final String  COLON                   = ':';
    public static final String  BLANK                   = '';
    public static final String  SPACE                   = ' ';
    public static final String  SINGLE_LINE_DELIM       = '\n';
    public static final String  COMMA                   = ',';
    public static final String  LEFT_BRACE        = '(';
    public static final String  RIGHT_BRACE        = ')';
    public static final String  SINGLE_QUOTE      = '\'';
    public static final String  VQL_SPACE        = '+';
    public static final String  DESC_LINE_DELIM         = SINGLE_LINE_DELIM + SINGLE_LINE_DELIM;
    public static final Integer TEXTFIELD_SIZE          = 255;
    public static final Integer LONGTEXTFIELD_SIZE      = 3000;
    public static final String  SystemLog_SobjectAPI    = 'Integration_Log__c';
    public static final String  SystemLog_ExceptionRT   = 'Apex_Exception';
    public static final String  SystemLog_RequestRT     = 'Integration_Request';
    public static final String  SystemLog_ResponseRT    = 'Integration_Response';
    public static final String   SUCCESS          = 'SUCCESS';
    public static final String  FAILURE          = 'FAILURE';
    public static final String  POST          = 'POST';
    public static final String  GET            = 'GET';
    public static final String  PRODUCT          = 'Product';
    public static final String  CATEGORY        = 'Category';
    public static final String APPROVED_FOR_USE_STATUS='Approved for use';
    /*@Cognizant:Added SUBCATEGORY constant variable*/
    public static final String  SUBCATEGORY        = 'SubCategory';
   
    public static final String  PRODUCT_KEY        = 'PRODUCT';
    public static final String  CATEGORY_KEY      = 'CATEGORY';
    public static final String  SUB_CATEGORY_KEY    = 'SUBCATEGORY';
    public static final String  TARGET_AUD_KEY      = 'TARGETAUDIENCE';
    public static final String  AND_CONNECTOR      = 'AND';
    public static final String  OR_CONNECTOR      = 'OR';
    public static final String WHERE_FILTER          = 'WHERE';
    public static final String  NONE          = '--None--';
    public static final String  TARGET_AUDIENCE_HCP    = 'HCP';
    public static final String  TARGET_AUDIENCE_CONSUMER= 'Consumer';
    public static final String  EMAIL          = 'Email';
    public static final String  FAX            = 'Fax';
    public static final String  VERBAL          = 'Verbal';
    public static final String  PRINT          = 'Print';
    public static final String  System_Administrator       = 'System Administrator';
    public static final String  Error_Type_INSUFFICIENT_ACCESS = 'INSUFFICIENT_ACCESS';
    public static final String  Error_Type_INVALID_SESSION_ID  = 'INVALID_SESSION_ID';
    public static final String  Error_Type_READ_TIMED_OUT     = 'Read Timed Out';
    public static final String  CALLOUT_EXCEPTION           = 'CalloutException';
    
    public static final String  SFDC_URL               = URL.getSalesforceBaseUrl().toExternalForm();   
}