global class JJ_ANZ_Biz_Acct_Rollup_Scheduling implements Schedulable{
global void execute(SchedulableContext sc) {
        JJ_ANZ_Biz_Acct_Rollup_Batch b = new JJ_ANZ_Biz_Acct_Rollup_Batch();
        database.executebatch(b,20);
}
}