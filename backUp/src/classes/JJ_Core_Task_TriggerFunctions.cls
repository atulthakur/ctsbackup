/**
 *      @author         Simon Roggeman
 *      @date           22/05/2015
        @name           JJ_Core_Task_TriggerFunctions
        @description    Class to handle all logic related to Task records 
 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer               Date                        Description
        ------------------------------------------------------------------------------------
        Simon Roggeman          22/05/2015                  Added logic to create campaign target records upon campaign response
 */
public with sharing class JJ_Core_Task_TriggerFunctions {
    
    //SRO, 22/05/2015: created function to handle incoming campaign responses
    public static void handleCampaignResponse(List<Task> campaignResponses) {

        //Init list to hold the valid campaign responses
        Map<Id,Task> validCampaignResponses = new Map<Id,Task>();

        /* 1. Validate */
        
        //Compose campaign Id set to validate if id's really exist
        Set<Id> campaignIds = new Set<Id>();

        //Loop responses - validation
        for(Task campaignResponse : campaignResponses) {
            
            try {
                
                //add campaign id's to set for validation (in proper 18 character version)
                Id campaignId = campaignResponse.JJ_Core_SYSTEM_Campaign_Id__c;
                campaignIds.add(campaignId);
                
                Task tempTask = campaignResponse.clone();
                
                tempTask.JJ_Core_SYSTEM_Campaign_Id__c = campaignId;
                validCampaignResponses.put(campaignResponse.Id, tempTask); //preserve id in map key (which is missing in the cloned object)
                 
            } catch (Exception ex) {
                //ignore
            }
        }
        
        //Get campaigns
        List<Campaign_vod__c> campaigns = [select Id from Campaign_vod__c where Id in :campaignIds];
        
        //Reset set
        campaignIds = new Set<Id>();
        
        //Build set for campaign id validation
        for(Campaign_vod__c campaign : campaigns) {
            campaignIds.add(campaign.Id);
        }
        
        //Clean up validCampaignResponses map - remove entries for invalid campaigns
        for(Task validCampaignResponse : validCampaignResponses.values().clone()) {
            if(!(campaignIds.contains(validCampaignResponse.JJ_Core_SYSTEM_Campaign_Id__c))) {
                validCampaignResponses.remove(validCampaignResponse.Id);
            }
        }
        
        /* 2. upsert campaign target records */
        Map<String, Campaign_Target_vod__c> campaignTargetsMap = new Map<String, Campaign_Target_vod__c>();
        
        //Loop responses - prepare campaign target upsert 
        for(Task campaignResponse : validCampaignResponses.values()) {
            String externalId = campaignResponse.JJ_Core_SYSTEM_Campaign_Id__c +'__' + campaignResponse.WhatId;
            
            Campaign_Target_vod__c campaignTarget = new Campaign_Target_vod__c();
            campaignTarget.External_Id_vod__c = externalId;
            campaignTarget.Target_Account_vod__c = campaignResponse.WhatId;
            campaignTarget.Campaign_vod__c = campaignResponse.JJ_Core_SYSTEM_Campaign_Id__c;
            campaignTarget.JJ_Core_Last_Response_Date__c = campaignResponse.JJ_Core_Activity_Date_Time__c;
            campaignTarget.JJ_Core_Last_Response_Type__c = campaignResponse.JJ_Core_Response_Type__c;
            
            campaignTargetsMap.put(campaignTarget.External_Id_vod__c, campaignTarget);
            
            System.debug('Campaign target record added for upsert: ' + campaignTarget.External_Id_vod__c);
        }
        
        //Force list ordering (cfr Java 8 change for unordered collections)
        List<Campaign_Target_vod__c> campaignTargetsForUpsert = campaignTargetsMap.values();
        System.debug(campaignTargetsForUpsert.size() + ' records staged for upsert');
        
        //upsert
        List<Database.UpsertResult> upsertResults = Database.upsert(campaignTargetsForUpsert, Campaign_Target_vod__c.External_Id_vod__c);
    }
}