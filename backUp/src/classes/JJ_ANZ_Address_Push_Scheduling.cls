global class JJ_ANZ_Address_Push_Scheduling implements Schedulable{
   global void execute(SchedulableContext sc) {
        VEEVA_BATCH_CHILD_ACCOUNT_PUSH b = new VEEVA_BATCH_CHILD_ACCOUNT_PUSH();
        database.executebatch(b,10);
    
        VEEVA_BATCH_CHILD_ACCOUNT_DELETE c = new VEEVA_BATCH_CHILD_ACCOUNT_DELETE();
        database.executebatch(c,25);
   

   }
}