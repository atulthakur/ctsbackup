global class JJ_ANZ_Cycle_Plan_Calc implements Database.Batchable<sObject> {
    private final String initialState;
    String query;
    global JJ_ANZ_Cycle_Plan_Calc() {
   
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // This is the base query that dirves the chunking.
        // We are grabbing all the plans are currently active and
        // have a start or end date that is currently in need of calculation.
        query = 'SELECT Id,' +
                '       Cycle_Plan_Target_vod__r.Id,' +
                '       Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Start_Date_vod__c, ' +
                '       Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.End_Date_vod__c,' +
                '       Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Territory_vod__c,' +
                '       Cycle_Plan_Target_vod__r.Cycle_Plan_Account_vod__c,' +
                '       JJ_ANZ_Over_Calls__c,' +
                '       Product_vod__c,' +
                '       Planned_Details_vod__c ' +
                ' FROM Cycle_Plan_Detail_vod__c ' +
                ' WHERE Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Start_Date_vod__c <= LAST_N_DAYS:1  '+
                ' AND  Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.End_Date_vod__c >= LAST_N_DAYS:1  ' +  
                ' AND  Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Active_vod__c = true ' ; 
       
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> batch) {
        // Loop through all the rows in the batch of Cycle Plan Details
        List <Cycle_Plan_Detail_vod__c> planDetailsToUpdate = new List <Cycle_Plan_Detail_vod__c> ();
        Set<String> setPlansInSet = new Set<String> ();
       
        for (sObject obj : batch) {
            Cycle_Plan_Detail_vod__c cycle = (Cycle_Plan_Detail_vod__c)obj;
           
            // Move some of the values from the object into easier to easier to read variables.
            String account = cycle.Cycle_Plan_Target_vod__r.Cycle_Plan_Account_vod__c;
            String product = cycle.Product_vod__c;
            String territory = cycle.Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Territory_vod__c;
            Date dateStart = cycle.Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.Start_Date_vod__c;
            Date dateEnd = cycle.Cycle_Plan_Target_vod__r.Cycle_Plan_vod__r.End_Date_vod__c;
            //janssen customization
            Decimal plandetailsdecimal;
            If(cycle.planned_details_vod__c == null){
                plandetailsdecimal = 0;
            }
            else {
                plandetailsdecimal = cycle.planned_details_vod__c;
            }
            integer plandetails = plandetailsdecimal.intvalue();
            // Loop through the Call2_Detail_vod__c results for what we need here.
           
            System.debug (account + '<->' + product + '<->' + territory + '<->'+dateStart +'<->' +dateEnd );
            Set <Id> haveCalls = new Set<Id> ();
           
            Integer countMyTerr = 0;
            Integer countAll = 0;
           
            Integer counterAllSched = 0;
            Integer counterMyTerrSched = 0;
            String counteddetail = ',';
            for (Call2_Detail_vod__c call_det :
                   [Select Call2_vod__r.Account_vod__c,
                           Call2_vod__r.Status_vod__c,
                           Call2_vod__r.Call_Date_vod__c,
                           Call2_vod__r.Territory_vod__c,
                           Product_vod__c,
                           Product_vod__r.Parent_product_vod__r.name,
                           Call2_vod__c,
                           Call2_vod__r.name,
                           Call2_vod__r.JJ_ANZ_Call_Category__c,
                           Call2_vod__r.JJ_ANZ_Account_is_person__c
                    FROM Call2_Detail_vod__c
                    WHERE Call2_vod__r.Account_vod__c = :account
                   
                    and Call2_vod__r.Call_Date_vod__c >= :dateStart
                    and Call2_vod__r.Call_Date_vod__c <= :dateEnd]) {
               
               
                    // if the lookup is null,  no work to do here. 
                    // Probably shouldnt happen but better safe
                if (call_det.Call2_vod__r == null)
                    continue;  
               
                // 26.01.2014 Ingrid Suprana
                // Added condition Professional and Contact calls 
                // to be counted for Cycle Plan actuals
                // 28.04.2015 Michael Helm
                // Added condition to count location calls for Lifescan Team
               
                string parprod = call_det.product_vod__r.parent_product_vod__c;
                system.debug(parprod);
                system.debug(product);
                if(call_det.product_vod__c == product ||
                   parprod == product){
                   if(!counteddetail.contains(call_det.call2_vod__r.name + '__' + product)){
                       counteddetail += call_det.call2_vod__r.name + '__' + product + ',';
                  
                if(call_det.Call2_vod__r.Territory_vod__c == territory) {
                    if (call_det.Call2_vod__r.Status_vod__c == 'Planned_vod' ||
                        (call_det.Call2_vod__r.Status_vod__c == 'Saved_vod' &&
                         call_det.Call2_vod__r.Call_Date_vod__c > System.now ())) {
                        counterMyTerrSched++;
                    } else {
                        if(call_det.call2_vod__r.status_vod__c == 'Submitted_vod' &&
                          ( call_det.Call2_vod__r.JJ_ANZ_Call_Category__c == 'Professional' ||
                            call_det.Call2_vod__r.JJ_ANZ_Call_Category__c == 'Short' ||
                            call_det.Call2_vod__r.JJ_ANZ_Account_is_person__c == false
                          )                       
                        ){
                        countMyTerr++;
                        }     
                    }
                }
               
                if (call_det.Call2_vod__r.Status_vod__c == 'Planned_vod' ||
                        (call_det.Call2_vod__r.Status_vod__c == 'Saved_vod' &&
                         call_det.Call2_vod__r.Call_Date_vod__c > System.now ())) {
                    counterAllSched++;     
                } else {
                    if(call_det.call2_vod__r.status_vod__c == 'Submitted_vod' &&
                          ( call_det.Call2_vod__r.JJ_ANZ_Call_Category__c == 'Professional' ||
                            call_det.Call2_vod__r.JJ_ANZ_Call_Category__c == 'Short' ||
                            call_det.Call2_vod__r.JJ_ANZ_Account_is_person__c == false
                          )                       
                    ){
                    countAll++;
                    }
                }
               }      
              }
            }
            integer overcalls;
            If( countmyterr > plandetails){ 
                overcalls = countmyterr - plandetails;
                countmyterr = plandetails;
            }
            Cycle_Plan_Detail_vod__c PlanDetail =
                   new Cycle_Plan_Detail_vod__c(ID = cycle.Id,
                                                Actual_Details_vod__c = countMyTerr,
                                                Scheduled_Details_vod__c = counterMyTerrSched,
                                                Total_Scheduled_Details_vod__c = counterAllSched,
                                                Total_Actual_Details_vod__c =  countAll,
                                                JJ_ANZ_Over_calls__c = overcalls);
                                               
            if (setPlansInSet.contains(cycle.Id) == false) {                                               
                    planDetailsToUpdate.add (PlanDetail);
                    setPlansInSet.add(cycle.Id);
            }
        }
       
        System.debug (planDetailsToUpdate);
        if (planDetailsToUpdate.size () > 0)
            update planDetailsToUpdate;
    }
    global void finish(Database.BatchableContext BC) {
        // Access initialState here
        // Access initialState here
        JJ_ANZ_Cycle_Plan_Target_Calc targetBatch = new JJ_ANZ_Cycle_Plan_Target_Calc();
        database.executebatch(targetBatch,1);        
    }
}