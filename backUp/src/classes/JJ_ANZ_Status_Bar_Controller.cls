public with sharing class JJ_ANZ_Status_Bar_Controller {
    
    public map<string, string> mapstatusfieldname;
    public map<string, string> mapstatusText {get;set;}
    public List<String> statusvalues{get;set;}
    public integer stsindx{get;set;}
    public map<string, Integer> mapStatusIndex{get;set;}
    
    public JJ_ANZ_Status_Bar_Controller(ApexPages.StandardController controller) {
        Id statusid = ApexPages.currentPage().getParameters().get('id');
        string objectname = statusid.getSobjectType().getDescribe().getName();
        
        mapstatusfieldname = new map<string, string>();
        mapstatusfieldname.put('Medical_Event_vod__c','JJ_ANZ_Event_Status__c');
        mapstatusfieldname.put('Janssen_Event_Venue__c','JJ_ANZ_Venue_Status__c');
        mapstatusfieldname.put('Meeting_Titles__c','JJ_Meeting_Title_Status__c');
        mapstatusfieldname.put('Speaker_Agreement__c','JJ_ANZ_Speaker_Status__c');
        
        set<string> cancelledset = new set<String>();
        if(objectname != 'Speaker_Agreement__c'){
            cancelledset.add('Draft');
            cancelledset.add('Planning');
        }
        cancelledset.add('Cancelled');
        
        set<string> rejectedset = new set<String>();
        rejectedset.add('Draft');
        rejectedset.add('Planning');
        rejectedset.add('Awaiting Approval');
        rejectedset.add('Rejected');
        
        Sobject statusobject = Database.Query('Select Recordtype.Name, ' + mapstatusfieldname.get(objectname) + ' From ' + objectname + ' Where Id =:statusid');
        string currentstatus = (String)statusobject.get(mapstatusfieldname.get(objectname));
        string rectypename = (statusobject.getSobject('Recordtype') != null)?(String)statusobject.getSobject('Recordtype').get('Name'):'';  
       
        
        mapstatusText = new map<string, string>();
        List<String> lstPickvals=new List<String>();
        Schema.sObjectType objType = statusid.getSobjectType(); 
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();     
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        list<Schema.PicklistEntry> values = fieldMap.get(mapstatusfieldname.get(objectname)).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values){
            lstPickvals.add(a.getValue());
            mapstatusText.put(a.getValue(), '');
        }
        
        set<String> updatedpicklist = new set<String>();        
        if(currentstatus == 'Cancelled'){
            updatedpicklist.addAll(cancelledset);
        }
        else if(currentstatus == 'Rejected'){        
            updatedpicklist.addAll(rejectedset);
        }
        else
        {
            updatedpicklist.addAll(lstPickvals);
            updatedpicklist.remove('Cancelled');
            updatedpicklist.remove('Rejected');
            
        }
        
        statusvalues = new List<String>();
        for(string pl1:lstPickvals){
            if(updatedpicklist.contains(pl1)){
                statusvalues.add(pl1);
            }        
        }
        
        mapStatusIndex = new map<string, Integer>();
        stsindx = 6;
        for(integer i = 0; i < lstPickvals.size(); i++){
            if(currentstatus == lstPickvals[i]){
                stsindx = (i + 1);
            }
            mapStatusIndex.put(lstPickvals[i], i+1);
        }
                
        string objnamequr = objectname + '_%';
        for(Message_vod__c vm : [select Name,Category_vod__c,Description_vod__c,Text_vod__c from Message_vod__c where Name like: objnamequr]){
            set<String> rectypeset = new set<string>();
            
            if(vm.Description_vod__c != null){
                rectypeset.addAll(vm.Description_vod__c.split(';'));
            }
            if(rectypeset.contains(rectypename) || vm.Description_vod__c == null){                
                string s = vm.name.remove(objectname + '_');
                mapstatusText.put(s, vm.Text_vod__c);
            }
        }
        
    }
    
}