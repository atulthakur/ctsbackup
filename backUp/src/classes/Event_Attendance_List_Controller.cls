/**
* Extension controller for the atendance list VF page
* uses without sharing to comply with requirements. We do not modifiy any info in the account,
* we just bypass the permissions to search for accounts and add them to our PDF list.
* If a user wants to modify the account, SFDC permissions are still enforced because the standard UI has to be used.
* Created: June 22 2013
* Modified July 25 2013
* Bluewolf
**/

/****************************************************************
Related requirements: DMP13, BETA REVIEW COMMENTS
*****************************************************************/
public without  sharing class Event_Attendance_List_Controller {
    public List<String> attendance {get;set;}
    
    public Medical_Event_vod__c event=new Medical_Event_vod__c();
        
    public Event_Attendance_List_Controller(){}
    
    public List<String> getList(){
        String evId=ApexPages.currentPage().getParameters().get('id');
        attendance=new List<String>();
        
        for(Account_Event_Association__c sp:[SELECT JJ_Account__r.Name FROM Account_Event_Association__c WHERE JJ_Janssen_Event__c=:evId]){
            attendance.add('* [Speaker] '+sp.JJ_Account__r.Name);
        }
        
        for(Event_Attendee_vod__c att:[SELECT Account_vod__r.Name/*Account_vod__r.FirstName,Account_vod__r.LastName*/ FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c=:evId
                                                                  AND Status_vod__c='Accepted' ORDER BY Account_vod__r.Name ASC]){           
            attendance.add(att.Account_vod__r.Name);//(att.Account_vod__r.LastName+','+att.Account_vod__r.FirstName);
        }       
        return attendance;
    }
    public Medical_Event_vod__c getEvent(){
        String evId=ApexPages.currentPage().getParameters().get('id');
        event=[SELECT Name,Start_Date_vod__c,JJ_Event_Name__c,JJ_Event_Venue__r.Name FROM Medical_Event_vod__c WHERE ID=:evId].get(0);
        return event;
    }
    public List<String> getEmptyLines(){
        attendance=new List<String>();
        for(Integer i=0;i<20;i++)
            attendance.add('');
        return attendance;
    }   
}