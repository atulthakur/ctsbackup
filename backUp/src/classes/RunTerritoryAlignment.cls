/**
*   Created by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 28.05.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description:  This controller Class is used as a custom link on Homepage to schedule the Territory alignment batch.
**/


public with sharing class RunTerritoryAlignment {
	
	
	public RunTerritoryAlignment(){
		
	}
	
	public void launchProcess(){
		List<Account_Territory_Alignment_Utilities__c> atlSetting = [select Id, Batch_Scheduled__c, Error__c From Account_Territory_Alignment_Utilities__c limit 1];
    		
		if(atlSetting.size() > 0){
			if(atlSetting[0].Batch_Scheduled__c == false){
				Datetime sysTime = System.now();
	            sysTime = sysTime.addMinutes(2);
	            
	            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
	            system.debug(chron_exp);
	            
	           	SchedulerBatchRunTerritoryAlignmentAcc newSched = new SchedulerBatchRunTerritoryAlignmentAcc(userInfo.getsessionid(), userInfo.getUserId());
	            //Schedule the next job, and give it the system time so name is unique
	            System.schedule('Run Territory Alignment Batch Job' + sysTime.getTime(), chron_exp, newSched);
	            
	            atlSetting[0].Batch_Scheduled__c = true;
	            atlSetting[0].Error__c = false;
	            
	            update atlSetting[0];
			}
		}
	}
	
	public pageReference backToHome(){
		pageReference pg = new pageReference('/home/home.jsp');
		
		return pg;
	}
}