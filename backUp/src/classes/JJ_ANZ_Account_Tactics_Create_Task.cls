global class JJ_ANZ_Account_Tactics_Create_Task implements Database.Batchable<sObject>,Schedulable  {


   /**************************************************************************
        Ingrid Suprana
        October 14, 2014
        Version 1.0
        
        Summary
        --------------
        This routine create Task assigned to Account Tactic owner 14 days before Due Date.
        FE-0000044
                    
        How to Run
        --------------
        Option 1. Run manually via the SFDC Developer Console:
        database.executebatch(new JJ_ANZ_Account_Tactics_Create_Task(),200); 
        
        Option 2. Schedule a nightly routine 
        Setup > Develop > Apex Classes > Schedule Apex
    **************************************************************************/
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Date d = system.today().addDays(14);
        String ds = String.valueOf(d);

        String query = 'SELECT Id, JJ_ANZ_End_Date__c, JJ_ANZ_Tactic_Due_Check__c, JJ_ANZ_PROW_Owner__c, Name ' + 
                    ' FROM Account_Tactic_vod__c ' +
                    ' WHERE JJ_ANZ_End_Date__c = ' + ds + 
                    ' and Complete_vod__c = false';
        //System.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext ctx) { 
        database.executebatch(new JJ_ANZ_Account_Tactics_Create_Task(),200); 
    } 
   
    global void execute(Database.BatchableContext BC, List<Account_Tactic_vod__c> scope) {

         for(Account_Tactic_vod__c a : scope)
         {

            Task u = new Task();    
            
            u.OwnerId=a.JJ_ANZ_PROW_Owner__c;    
            u.Subject='Account Tactic due in 14 days: '+a.name;    
            u.Description='Account Tactic due in 14 days: '+a.name;  
            u.Status='Not Started';    
            u.Priority='Normal'; 
            u.WhatId=a.Id;
            u.ActivityDate=a.JJ_ANZ_End_Date__c;
            u.ReminderDateTime=system.now();
            u.IsReminderSet=true;
           
            insert u;    
         }         
    }   
    
    global void finish(Database.BatchableContext BC) {
    }


}