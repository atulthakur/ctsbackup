/**
*   Created by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 12.05.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description:  Scheduler Class to schedule the Territory Alignment Batch Process. 
**/
 

global class SchedulerBatchRunTerritoryAlignmentAcc implements Schedulable {
	
	global String sessionId;
	global Id userId;
	
	global SchedulerBatchRunTerritoryAlignmentAcc(String sID, Id uId){
		sessionId = sID; 
		userId = uId;
	} 
    
    global void execute(SchedulableContext SC){
    	
    	if(sessionId != ''){
    		BatchRunTerritoryAlignmentOnAccounts newBatchJob = new BatchRunTerritoryAlignmentOnAccounts(sessionId, userId);
        
        	ID batchprocessid = Database.executeBatch(newBatchJob,500);
    	}else{
        
	        BatchRunTerritoryAlignmentOnAccounts newBatchJob = new BatchRunTerritoryAlignmentOnAccounts(userinfo.getsessionid(), userinfo.getUserId());
	        
	        ID batchprocessid = Database.executeBatch(newBatchJob,500);
    	}
    }
}