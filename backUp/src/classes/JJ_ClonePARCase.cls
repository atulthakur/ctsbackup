/** Subject  : JJ_ClonePARCase


    Description  : Class to Clone PAR and parent  Case 
    Author       : CTS|| schand63@its.jnj.com||1/16/2018

    **/
    global class JJ_ClonePARCase{

        /****************************************************
        Method:clonecopy 
        Description: web service method to clone the case.
        ***************************************************/
        webservice static String clonecopy(id caseId){  
            case eChildCase =new case(); // copy the existing child case values from SQL (to be cloned)    
            case rCase;  

            eChildCase = [SELECT id,CaseNumber,JJ_ANZ_ChildCaseNumber__c,JJ_ANZ_Non_Case__c,JJ_Initial_or_Resupply__c,JJ_SAS_request__c,
            JJ_PAR_Case_Product_Other__c,JJ_PAR_Case_Product__c,JJ_Indication_Other__c,JJ_Indication__c,JJ_Dose_Other__c,JJ_Dose__c,
            JJ_Dosing_regimen__c,JJ_Email_comment__c,Origin,OwnerId,Status,Subject,JJ_Resupply__c,JJ_SAP_reference__c,JJ_Reason_for_application__c,
            JJ_Cycle__c,Description,JJ_Address__c,JJ_Location_name__c,JJ_Email_1__c,JJ_Email_2__c,JJ_Email_3__c,JJ_Contact_Name__c,
            JJ_Contact_Phone__c,JJ_Product_Strength_1__c,JJ_Product_Strength_2__c,JJ_Quantity_1__c,JJ_Quantity_2__c,
            JJ_Units_1__c,JJ_Units_2__c,JJ_Country__c,JJ_State_or_City_NZ__c,JJ_Suburb_Town__c,JJ_Reason_comments__c,JJ_ReferenceId__c,
            JJ_Postcode__c,ParentId,Reason,RecordTypeId,JJ_ANZ_Patient_Height__c,JJ_ANZ_Patient_Initials__c,JJ_BSA__c,JJ_Patient_Weight__c,JJ_ANZ_Date_of_Birth__c
            FROM Case Where Id =:caseId];  
            
            string pId = eChildCase.JJ_ANZ_ChildCaseNumber__c; //pid is parent case number.
           
            case parentcase1 = new case();
            case parentcase = new case();
            if(String.isNotBlank(pid) || pid != null){ 
                pid = pid.split('-')[0];
                parentcase = [select Status,Subject,JJ_ANZ_Status__c,CaseNumber,Description,JJ_ANZ_Case_Narrative__c,Origin,JJ_ANZ_Deliver_Method__c,
                JJ_ANZ_CaseProductOther__c,JJ_ANZ_CaseProducts__c,JJ_ANZ_CaseProduct__c,JJ_ANZ_MSL_Contact__c,JJ_ANZ_JnJ_Program_Number__c,
                JJ_ANZ_CreatePAR__c,JJ_ANZ_Fax__c,JJ_ANZ_Initial_Only__c,JJ_Initial_Reporter_Name__c,JJ_ANZ_KOL__c,JJ_ANZ_Other__c,JJ_ANZ_Reporter_Initials__c,
                JJ_ANZ_Off_Label__c,JJ_ANZ_Linked_Case__c,JJ_ANZ_Is_highrisk__c,JJ_ANZ_Name__c,JJ_ANZ_Title__c,JJ_ANZ_Address__c,JJ_ANZ_Address1_Other__c,JJ_ANZ_Address2_Other__c,JJ_ANZ_Address_Line_1__c,JJ_ANZ_Address_Line_2__c,
                JJ_ANZ_Permission_to_contact_Reporter__c,JJ_ANZ_Follow_up_Consent__c,JJ_ANZ_Can_Contact_Physician__c,
                JJ_ANZ_Physician_aware_of_event__c,JJ_ANZ_Initial_Reporter_Type__c,JJ_ANZ_Initial_Reporter_Specialization__c,
                JJ_ANZ_Send_for_Verification__c,JJ_ANZ_Secondary_First_Name__c,JJ_ANZ_Secondary_Last_Name__c,
                JJ_ANZ_Email__c,JJ_ANZ_Fax_Number__c,JJ_ANZ_Phone__c,JJ_ANZ_Practice_Name__c,JJ_ANZ_Suburb_Town_Other__c,JJ_ANZ_Suburb_Town__c,
                JJ_ANZ_Postcode_Other__c,JJ_ANZ_Postcode__c,JJ_ANZ_State_Province__c,JJ_ANZ_State_Other__c,JJ_ANZ_Country1__c,JJ_ANZ_Other_Contact_Country__c,
                JJ_ANZ_Date_of_Birth__c,JJ_ANZ_Reporter_is_Patient__c,JJ_Patient_Weight__c,JJ_ANZ_Height_in__c,JJ_BSA__c,
                JJ_ANZ_Patient_Age__c,JJ_ANZ_Patient_Ethnicity__c,JJ_ANZ_Patient_Gender__c,JJ_ANZ_Patient_Height__c,JJ_ANZ_Patient_Initials__c,
                JJ_ANZ_Age_in__c,JJ_ANZ_Patient_Weight_On_AE__c,JJ_ANZ_Patient_Age_On_AE__c,JJ_ANZ_Aboriginal_Torres_Strait_Islander__c,
                JJ_ANZ_Weight_Measurement_Type__c,JJ_ANZ_Is_Pregnant__c,JJ_ANZ_Expected_Due_Date__c,
                JJ_ANZ_Last_Menstrual_Date__c,JJ_ANZ_Relationship_to_Initial_Reporter__c,JJ_ANZ_Can_Contact_Reporter_OC__c,
                JJ_ANZ_Other_Contact_Address__c,JJ_ANZ_Other_Contact_City__c,JJ_ANZ_Other_Contact_Email__c,JJ_ANZ_Other_Contact_Fax__c,
                JJ_ANZ_Other_Contact_FirstName__c,JJ_ANZ_Other_Contact_Last_Name__c,JJ_ANZ_Other_Contact_Phone__c,JJ_ANZ_Other_Contact_Title__c,JJ_ANZ_Other_Pharmacy_Name__c,
                JJ_ANZ_Other_Practice_Name__c,JJ_ANZ_Other_Product__c,JJ_ANZ_Other_2nd_Reporter_Type__c,JJ_ANZ_Reporter_Administered_the_Product__c,
                JJ_ANZ_Cause_Of_Death__c,JJ_ANZ_Autopsy_Result__c,JJ_ANZ_Date_Of_Death__c,JJ_ANZ_Autopsy__c,JJ_ANZ_Setting__c,
                JJ_ANZ_Is_Complaint_Related_To_Device__c,JJ_ANZ_When_Problem_Occur__c,JJ_ANZ_Is_Product_Instruction_Followed__c,
                JJ_ANZ_Describe_theDevice_Component__c,JJ_ANZ_Is_Patient_Experience_Harm__c,JJ_ANZ_Number_Of_Units__c,
                JJ_ANZ_Medical_History__c,JJ_ANZ_Drug_Abuse_Illicit_Drug_Use__c,JJ_ANZ_Drug_Abuse_Details__c,JJ_ANZ_Is_Smoking__c,
                JJ_ANZ_Smoking_Frequency__c,JJ_ANZ_Smoking_Frequency_No__c,JJ_ANZ_Concomitant_Medications__c,JJ_ANZ_Diagnostic_Data__c,
                JJ_ANZ_Allergies__c,JJ_ANZ_Allergy_Details__c,JJ_ANZ_Alcohol_Consumption__c,JJ_ANZ_Alcohol_Consumption_Frequency__c,
                JJ_ANZ_Alcohol_Frequency_No__c,JJ_ANZ_Vendor_Name__c,JJ_ANZ_Vendor_Number__c,
                JJ_ANZ_Product_Purchased_Place__c,JJ_ANZ_Product_Purchased_Contact_Number__c,JJ_ANZ_Product_Purchased_Place_Others__c,
                JJ_ANZ_Product_Administered_Place__c,JJ_ANZ_Product_Administered_By__c,JJ_ANZ_Controlled_Substances_Number__c,
                JJ_ANZ_Admission_Date__c,JJ_ANZ_Dose__c,JJ_ANZ_Initial_Submitter__c,JJ_ANZ_JNJ_Employee__c,
                JJ_ANZ_Product_Name__c,JJ_ANZ_Reporter_Type__c,JJ_ANZ_Report_to_Health_Authority__c,JJ_ANZ_Strength__c,
                JJ_ANZ_Wholesaler_Address__c,JJ_ANZ_Wholesaler_Name__c,JJ_CaseProductTemplate__c,
                RecordTypeId,Type from case where CaseNumber = :pid][0];

                parentcase1 = parentcase.clone(false,true);
                parentcase1.status ='Assessed'; // change the status which is required as per req
                parentcase1.JJ_ClonePAR__c  = true;
                parentcase1.JJ_ANZ_CreatePAR__c ='Yes';
                insert parentcase1;
            }

            rCase = eChildCase.clone(false,true);
            rCase.ParentId=parentcase1.id;

            case pCase = [select id,JJ_ANZ_CreatePAR__c,CaseNumber,JJ_ANZ_Name__c,JJ_ANZ_Date_of_Birth__c from case where id=:parentcase1.id];
            rCase.JJ_ANZ_ChildCaseNumber__c = pCase.CaseNumber +'-PAR'; 
            rCase.JJ_ANZ_Name__c=pCase.JJ_ANZ_Name__c;
            rCase.JJ_ANZ_Date_of_Birth__c=pCase.JJ_ANZ_Date_of_Birth__c;
            rCase.status ='New';
            insert rCase;
            return pCase.CaseNumber;
        } 
    }