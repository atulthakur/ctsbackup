public class HelpGizmoLoginController {
    public string md5String {get;set;}
    public string timeStamp {get;set;}
    public string redirect {get;set;}
    private string hgToken = '4d8ddb0ad8791e3c37063d950a5b2149';

    public HelpGizmoLoginController () {
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        timeStamp = String.valueof(DateTime.now().getTime() / 1000);
        redirect = ApexPages.CurrentPage().getParameters().get('r');
        String hash = UserInfo.getUserId() + this.hgToken + timeStamp;
        Blob keyblob = Blob.valueof(hash);
        Blob key = Crypto.generateDigest('MD5',keyblob);
        md5String = encodingUtil.convertToHex(key);
    }
}