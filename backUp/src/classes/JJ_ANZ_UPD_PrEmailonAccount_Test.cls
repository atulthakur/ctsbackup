/**
*      @author    Prabushanker Kumarasamy     
*      @date      25/11/2016  
       This class for covering the JJ_ANZ_UPD_Promo_Email_Consent_onAccount apex class 
**/       

@isTest
public class JJ_ANZ_UPD_PrEmailonAccount_Test
{
    private static testMethod void doTest() 
    {
    
        Test.startTest();
        RecordType pro = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName='Professional_vod' AND SobjectType='Account'];        
        Account testAccount = new Account(FirstName='Test', LastName='Atl', RecordTypeId=pro.Id);
        insert testAccount;
        Multichannel_Consent_vod__c mcc = new Multichannel_Consent_vod__c(Account_vod__c = testAccount.id,Capture_Datetime_vod__c = system.today(),Opt_Type_vod__c='Opt_In_vod');
        insert mcc;
        
        JJ_ANZ_UPD_Promo_Email_Consent_onAccount.ChannelPermissionUPD(New Id[]{mcc.Id});
        
        Test.stopTest();
        
    }    
     private static testMethod void doTest2() 
    {
    
        Test.startTest();
        RecordType pro = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName='Professional_vod' AND SobjectType='Account'];        
        Account testAccount = new Account(FirstName='Test', LastName='Atl', RecordTypeId=pro.Id);
        insert testAccount;
        Multichannel_Consent_vod__c mcc = new Multichannel_Consent_vod__c(Account_vod__c = testAccount.id,Capture_Datetime_vod__c = system.today(),Opt_Type_vod__c='Opt_Out_vod');
        insert mcc;
        JJ_Core_Channel_Permissions__c ccp = new JJ_Core_Channel_Permissions__c(JJ_Core_Account__c = testAccount.id,JJ_Core_Channel__c ='Email' ,JJ_Core_Permission__c='Opt-in');
        
        JJ_ANZ_UPD_Promo_Email_Consent_onAccount.ChannelPermissionUPD(New Id[]{mcc.Id});
        
        Test.stopTest();
        
    }  

}