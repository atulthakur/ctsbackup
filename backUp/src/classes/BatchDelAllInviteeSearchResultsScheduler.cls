/**
*	Created by: Khodabocus Zameer (ICBWORKS)
*	Last Date Modified: 20.03.2014
*	Email: zameer.khodabocus@icbworks.com
*	
*	Description: This class is the scheduler for the batch BatchDeleteAllInviteeSearchResults. 
**/
     
global with sharing class BatchDelAllInviteeSearchResultsScheduler implements Schedulable {
	
	global void execute(SchedulableContext SC){
        
        Id batchJob = database.executeBatch(new BatchDeleteAllInviteeSearchResults());
    } 
}