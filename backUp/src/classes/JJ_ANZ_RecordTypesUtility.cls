public class JJ_ANZ_RecordTypesUtility {
  /*
     @ Description -
     To fetch the RecordType Information of a certain Object.
     @Usage -
     Using this approach, we can avoid writing SOQL queries in apex class/test class to fetch the record type Id.
  */
 
  /*** recordTypeInfo Utility Method ***/
  //Below method will take sObject API as input.
  public static Map<string,schema.recordtypeinfo> recordTypeInfo(String objectApiName){
    
   Map<string, schema.sobjecttype> sObjectMap = Schema.getGlobalDescribe() ;
   Schema.SObjectType sObjType = sObjectMap.get(ObjectApiName) ;
   Schema.DescribeSObjectResult sObjTypeDescribe = sObjType.getDescribe() ;
    
   //returns all the record types info for a certain object
   return sObjTypeDescribe.getRecordTypeInfosByName();
    
  }
 }