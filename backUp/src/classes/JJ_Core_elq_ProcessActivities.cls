/**
 *      @author Simon Roggeman
 *      @date   13/11/2016
        @description    Class for processing Eloqua activities

        Modification Log:
        ------------------------------------------------------------------------------------------------------
        Developer                   Date                Description
        ------------------------------------------------------------------------------------------------------
        Simon Roggeman              13/11/2016          Original version
 */
global class JJ_Core_elq_ProcessActivities implements Database.Batchable<JJ_Core_elq_Activities__c>, Database.Stateful, Schedulable {

    //private members
    private String contactPrefix;
    private String campaignPrefix;
    private String sourceSystem;
    private Boolean ignoreInvalidCampaignId;
    
    private String notificationUserId;
    
    private Id mcEmailActivityRecordTypeId;
    private Id mcEmailBBActivityRecordTypeId;
    private Id mcFormActivityRecordTypeId;
    private Id mcEmailActivityLineRecordTypeId;
    
    private Integer errorCount;
    private Integer successCountActivityLines;

    //constructor
    global JJ_Core_elq_ProcessActivities() {
        
        //init counts
        errorCount = 0;
        successCountActivityLines = 0;
    
        //get settings
        JJ_Core_elq_Eloqua_Integration_Settings__c settings = JJ_Core_elq_Eloqua_Integration_Settings__c.getInstance();
        
        //set parameters
        this.sourceSystem = settings.JJ_Core_elq_Source_System_Id__c;
        this.notificationUserId = settings.JJ_Core_elq_Notification_User_Id__c;
        this.ignoreInvalidCampaignId = settings.JJ_Core_elq_Ignore_Invalid_Campaign_Id__c;        

        if(settings.JJ_Core_elq_Use_accounts__c) {
            this.contactPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
        } else {
            this.contactPrefix = Contact.sObjectType.getDescribe().getKeyPrefix();
        }

        this.campaignPrefix = Campaign_vod__c.sObjectType.getDescribe().getKeyPrefix();

        
        System.debug('Prefixes - Contact: ' + this.contactPrefix + ' / Campaign: ' + this.campaignPrefix);
        
        //get mcEmailActivityRecordTypeIds
        List<RecordType> recordTypes = [SELECT Id, DeveloperName, SobjectType FROM RecordType WHERE SobjectType in ('Multichannel_Activity_vod__c','Multichannel_Activity_Line_vod__c') and DeveloperName in ('JJ_Core_elq_Marketing_Email','JJ_Core_elq_Marketing_Form','JJ_Core_elq_Marketing_Email_Bounceback')];
        
        for(RecordType rt : recordTypes) {
            if(rt.SobjectType == 'Multichannel_Activity_vod__c') {
                if(rt.DeveloperName == 'JJ_Core_elq_Marketing_Email') {
                    this.mcEmailActivityRecordTypeId = rt.Id;               
                } else if(rt.DeveloperName == 'JJ_Core_elq_Marketing_Email_Bounceback') {
                    this.mcEmailBBActivityRecordTypeId = rt.Id;             
                } else {
                    this.mcFormActivityRecordTypeId = rt.Id;
                }
            } else {
                if(rt.DeveloperName == 'JJ_Core_elq_Marketing_Email') {
                    this.mcEmailActivityLineRecordTypeId = rt.Id;               
                }
            }
        }
        
        System.debug('Record types - EmailAct: ' + mcEmailActivityRecordTypeId);
        System.debug('Record types - FormAct: ' + mcFormActivityRecordTypeId);
        System.debug('Record types - EmailActLine: ' + mcEmailActivityLineRecordTypeId);
        System.debug('Record types - EmailBBActLine: ' + mcEmailBBActivityRecordTypeId);
    }

    //start - get all new activities from staging object
    global Iterable<JJ_Core_elq_Activities__c> start(Database.BatchableContext BC) {
        return [
            select 
                Id, 
                JJ_Core_elq_Activity_Date__c, 
                JJ_Core_elq_Activity_Type__c, 
                JJ_Core_elq_Asset_Name__c, 
                JJ_Core_elq_Campaign_Id__c, 
                JJ_Core_elq_Contact_Id__c, 
                JJ_Core_elq_Country__c,
                JJ_Core_elq_Contact_Email_Address__c,
                JJ_Core_elq_Error_Message__c, 
                JJ_Core_elq_Link__c, 
                JJ_Core_elq_Source_System__c, 
                JJ_Core_elq_Status__c, 
                JJ_Core_elq_URL__c 
            from JJ_Core_elq_Activities__c 
            where JJ_Core_elq_Status__c = 'New'
            order by JJ_Core_elq_Activity_Date__c];
    }

    global void execute(Database.BatchableContext BC, list<JJ_Core_elq_Activities__c> activities) {
        
        System.debug(activities.size() + ' records to process');
        
        list<JJ_Core_elq_Activities__c> activitiesForDelete = new list<JJ_Core_elq_Activities__c>();
        list<JJ_Core_elq_Activities__c> activitiesForUpdate = new list<JJ_Core_elq_Activities__c>();
        list<JJ_Core_elq_Activities__c> activitiesToProcess = new list<JJ_Core_elq_Activities__c>();
        
        Map<String, Multichannel_Activity_vod__c> mcActivities = new Map<String, Multichannel_Activity_vod__c>();
        Map<String, Multichannel_Activity_Line_vod__c> mcActivityLines = new Map<String, Multichannel_Activity_Line_vod__c>();
    
        //1. throw out records for other sfdc instances (source system based)
        for(JJ_Core_elq_Activities__c activity : activities) {
            if(activity.JJ_Core_elq_Source_System__c != this.sourceSystem) {
                activity.JJ_Core_elq_Error_Message__c = 'NO_OR_WRONG_SOURCE_SYSTEM'; //for testing purposes, record will anyway get deleted
                activitiesForDelete.add(activity);
            } else {
                activitiesToProcess.add(activity);
            }
        }
        
        System.debug(activitiesForDelete.size() + ' records to delete (wrong source system)');
        System.debug(activitiesToProcess.size() + ' records left to process');
        
        //2. validate records for current system
        for(JJ_Core_elq_Activities__c activity : activitiesToProcess) {
        
            String contactId = activity.JJ_Core_elq_Contact_Id__c;
            String campaignId = activity.JJ_Core_elq_Campaign_Id__c; 
            
            //valid contact id?
            if(contactId == null || !(contactId.startsWith(contactPrefix))) {
                activity.JJ_Core_elq_Status__c = 'Error';
                activity.JJ_Core_elq_Error_Message__c = 'MISSING_OR_INVALID_CONTACT_ID';
            }
            
            //valid campaign id?
            if(!ignoreInvalidCampaignId && (campaignId == null || !(campaignId.startsWith(campaignPrefix)))) {
                activity.JJ_Core_elq_Status__c = 'Error';
                activity.JJ_Core_elq_Error_Message__c = 'MISSING_OR_INVALID_CAMPAIGN_ID';
            }
            
            //valid activities
            if(activity.JJ_Core_elq_Status__c != 'Error') {
                
                //1. MC Activity
                //generate external id : group activities by email/contact combination
                String mcActivityExternalId;
                String displayOnTimeline;
                
                if(activity.JJ_Core_elq_Activity_Type__c == 'Bounceback') {
                    mcActivityExternalId = activity.JJ_Core_elq_Activity_Date__c + '-' + activity.JJ_Core_elq_Contact_Id__c;
                    displayOnTimeline = activity.JJ_Core_elq_Contact_Email_Address__c;
                } else if(activity.JJ_Core_elq_Activity_Type__c == 'Form Submit') {
                    mcActivityExternalId = activity.JJ_Core_elq_Activity_Date__c + '-' + activity.JJ_Core_elq_Asset_Name__c + '-' + activity.JJ_Core_elq_Contact_Id__c;
                    displayOnTimeline = activity.JJ_Core_elq_Asset_Name__c;
                } else {
                    mcActivityExternalId = activity.JJ_Core_elq_Asset_Name__c + '-' + activity.JJ_Core_elq_Contact_Id__c;
                    displayOnTimeline = activity.JJ_Core_elq_Asset_Name__c;
                }
                
                Multichannel_Activity_vod__c mcActivity = new Multichannel_Activity_vod__c();
                mcActivity.URL_vod__c = displayOnTimeline; //DISPLAYED ON TIMELINE!!!
                mcActivity.Account_vod__c = contactId;
                mcActivity.Color_vod__c = 'DarkRed';
                mcActivity.Icon_vod__c = 'Marketing_vod';
                mcActivity.JJ_Core_elq_Campaign__c = campaignId;
                mcActivity.JJ_Core_elq_Asset_Name__c = activity.JJ_Core_elq_Asset_Name__c;
                if(activity.JJ_Core_elq_Activity_Type__c == 'Email Send' || activity.JJ_Core_elq_Activity_Type__c == 'Form Submit' || activity.JJ_Core_elq_Activity_Type__c == 'Bounceback') {
                    mcActivity.Start_DateTime_vod__c = activity.JJ_Core_elq_Activity_Date__c;
                    mcActivity.JJ_Core_elq_Eloqua_Activity__c = activity.Id;
                }
                
                if(activity.JJ_Core_elq_Activity_Type__c.startsWith('Email') || activity.JJ_Core_elq_Activity_Type__c.startsWith('Click')) {
                    mcActivity.RecordTypeId = mcEmailActivityRecordTypeId;
                    mcActivityExternalId = 'MAIL-' + mcActivityExternalId;
                    System.debug('External id: ' + mcActivityExternalId);
                } else if(activity.JJ_Core_elq_Activity_Type__c == 'Form Submit') {
                    mcActivity.RecordTypeId = mcFormActivityRecordTypeId;
                    mcActivityExternalId = 'FORM-' + mcActivityExternalId;
                    System.debug('External id: ' + mcActivityExternalId);
                } else { //bounceback
                    mcActivity.RecordTypeId = mcEmailBBActivityRecordTypeId;
                    mcActivityExternalId = 'BOUNCE-' + mcActivityExternalId;
                    System.debug('External id: ' + mcActivityExternalId);
                }
                
                mcActivity.VExternal_Id_vod__c = mcActivityExternalId;

                //add to list for upsert (if no higher prio activity exists yet -- technically means don't overwrite records having JJ_Core_elq_Eloqua_Activity__c filled in, as those are true parent records, others could be temporary placeholders to make sure we can already create the activity line (no way to specify order of processing))
                if(mcActivities.get(mcActivityExternalId) == null || mcActivities.get(mcActivityExternalId).JJ_Core_elq_Eloqua_Activity__c == null) {
                    System.debug('Activity ' + mcActivityExternalId + ' added for processing');
                    mcActivities.put(mcActivityExternalId, mcActivity);                 
                } else {
                    System.debug('Activity ' + mcActivityExternalId + ' ignored');
                }


                //2. MC Activity Line
                //generate external id : group activities by email/contact combination
                String mcActivityLineExternalId;
                
                if(activity.JJ_Core_elq_Activity_Type__c == 'Email Send' || activity.JJ_Core_elq_Activity_Type__c == 'Email Open' || activity.JJ_Core_elq_Activity_Type__c == 'Click Through') {
                    mcActivityLineExternalId = activity.JJ_Core_elq_Activity_Type__c + '-' + activity.JJ_Core_elq_Asset_Name__c + '-' + activity.JJ_Core_elq_Activity_Date__c + '-' + activity.JJ_Core_elq_Contact_Id__c;

                    Multichannel_Activity_Line_vod__c mcActivityLine = new Multichannel_Activity_Line_vod__c(VExternal_Id_vod__c = mcActivityLineExternalId);
                    mcActivityLine.Multichannel_Activity_vod__r = new Multichannel_Activity_vod__c(VExternal_Id_vod__c = mcActivityExternalId);
                    mcActivityLine.DateTime_vod__c = activity.JJ_Core_elq_Activity_Date__c;
                    mcActivityLine.JJ_Core_elq_Eloqua_Activity__c = activity.Id;
                    mcActivityLine.RecordTypeId = mcEmailActivityLineRecordTypeId;
                    mcActivityLine.Event_Type_vod__c = activity.JJ_Core_elq_Activity_Type__c;
                    mcActivityLine.JJ_Core_elq_Full_URL__c = activity.JJ_Core_elq_URL__c;
                    mcActivityLine.JJ_Core_elq_Link_Clicked__c = activity.JJ_Core_elq_Link__c;
                    
                    //add to list for upsert
                    mcActivityLines.put(mcActivityLineExternalId, mcActivityLine);
                }
            } else {
                activitiesForUpdate.add(activity);
            }
        }
        
        System.debug(mcActivities.size() + ' activities to upsert');
        System.debug(mcActivityLines.size() + ' activity lines to upsert');
        System.debug(activitiesForUpdate.size() + ' staging records in error state');
        
        //3. create/update MC activities
        List<Database.UpsertResult> mcactResults = Database.upsert(mcActivities.values(), Multichannel_Activity_vod__c.VExternal_Id_vod__c, false);
        List<Database.UpsertResult> mcactLineResults = Database.upsert(mcActivityLines.values(), Multichannel_Activity_Line_vod__c.VExternal_Id_vod__c, false);
        
        //4. update corresponding records in staging object to error if needed
        Integer recCount = 0;
        
        //validate activity upsert
        for(Database.UpsertResult mcactResult : mcactResults) {
            
            //get id of staging activity
            Id elqActivityId = mcActivities.values().get(recCount).JJ_Core_elq_Eloqua_Activity__c;
            System.debug('Eval of staging rec: ' + elqActivityId);
            
            String activityType = mcActivities.values().get(recCount).RecordTypeId;
            System.debug('\tRecord type: ' + activityType);
            
            String externalId = mcActivities.values().get(recCount).VExternal_Id_vod__c;
            System.debug('\tExternal id: ' + externalId);
            
            if(mcactResult.isSuccess()) {
                //form submit or email bounced: no lines to be created, so staging rec can be deleted
                if(activityType == mcFormActivityRecordTypeId || activityType == mcEmailBBActivityRecordTypeId) {
                    
                    activitiesForUpdate.add(new JJ_Core_elq_Activities__c(Id=elqActivityId,JJ_Core_elq_Status__c='Processed'));
                    successCountActivityLines++;                    
                }
            } else {
                System.debug('Error upserting activity: ' + mcactResult.getErrors()[0].getMessage().left(5000));
            }
            
            recCount ++;
        }
        
        //reset count
        recCount = 0;
        
        //validate activity line upsert
        for(Database.UpsertResult mcactLineResult : mcactLineResults) {
            
            //get id of staging activity
            Id elqActivityId = mcActivityLines.values().get(recCount).JJ_Core_elq_Eloqua_Activity__c;
            
            System.debug('Eval act line: ' + elqActivityId);
            
            if(mcactLineResult.isSuccess()) {
                //processed succesfully - can be deleted (no need to check types, as form submit or bounceback don't even generate lines)
                System.debug(elqActivityId + ' ready for delete');
                activitiesForUpdate.add(new JJ_Core_elq_Activities__c(Id=elqActivityId,JJ_Core_elq_Status__c='Processed'));
                successCountActivityLines++;
            } else {
                System.debug(elqActivityId + ' ready for update (with error)');
                activitiesForUpdate.add(new JJ_Core_elq_Activities__c(Id=elqActivityId, JJ_Core_elq_Status__c = 'Error', JJ_Core_elq_Error_Message__c = mcactLineResult.getErrors()[0].getMessage().left(5000)));
            }
            
            recCount++;
        }
        
        //4. process staging updates and deletes
        System.debug(activitiesForUpdate.size() + ' staging records to be upserted to error state (incl. upsert errors)');
        System.debug(activitiesForDelete.size() + ' staging records ready to delete');
        update activitiesForUpdate;
        
        //SKIP FOR TESTING
        delete activitiesForDelete;
        //SKIP FOR TESTING
        
        //increment error count
        errorCount += activitiesForUpdate.size();
    }

    global void finish(Database.BatchableContext BC){
        System.debug('Total errors: ' + errorCount);
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { this.notificationUserId };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Eloqua outbound integration - summary';
        message.plainTextBody = 'Hi,\n\nresults of today\'s run:\n\t- activity lines created: ' + successCountActivityLines + '\n\t- staging records errored out: ' + errorCount + '\n\nKind regards,\nThe Eloqua integration team';
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
    
   global void execute(SchedulableContext sc) {
      JJ_Core_elq_ProcessActivities processActivitiesJob = new JJ_Core_elq_ProcessActivities(); 
      database.executebatch(processActivitiesJob);
   }
}