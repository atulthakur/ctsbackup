/**
* Extension controller to preview a dinner event agenda
* Created: July 9 2013
* Modified July 9 2013
* Bluewolf
**/

/*****************************
Related requirements: DMP13
*****************************/
public with sharing class AgendaPreviewController {
	public AgendaPreviewController(ApexPages.StandardController controller){}
	
	public List<Janssen_Event_Agenda__c> getAgendaList(){
		String evId=ApexPages.currentPage().getParameters().get('EventId');
		List<Janssen_Event_Agenda__c> agenda=new List<Janssen_Event_Agenda__c>();
		
		for(Janssen_Event_Agenda__c ag:[SELECT Description__c, Time__c, Time_Slot__c, JJ_ANZ_Educational_Content__c ,JJ_ANZ_Speaker_Name__c
									 FROM Janssen_Event_Agenda__c WHERE Janssen_Event__c=:evId ORDER BY Time__c ASC]){
			agenda.add(ag);
		}
	
		return agenda;
	}	
}