/**
*      @author Keerthana Thallam
*   
       @description    Contains the generic methods for sending out HTTP Requests 
       

        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------

**/
public with sharing class JJ_HTTPService {
    
  Http                          http;
  public HttpRequest            request;
  public HttpResponse           response;
  public Map<String,String>     mapRequestHeaders;
  public String                 baseUrl;

 /*
  * @name:  constructor
  * @description: initialize class parameters
  */
    public JJ_HTTPService(){

        http                = new Http();
        request             = new HttpRequest();
        mapRequestHeaders   = new Map<String,String>();
        
    }

 /*
  * Method name: setRequestHeaders
  * @description:  set request class property with header key-values defined in mapRequestHeaders
  */ 
  @TestVisible
  private void setRequestHeaders(){
    request = (request==null)? new HttpRequest() : request;
    for(String sKey : mapRequestHeaders.keySet()){
      request.setHeader(sKey, mapRequestHeaders.get(sKey));
    }
  }

 /*
  * Method name: addToRequestHeaders
  * @description:  add key-value to mapRequestHeaders class property
  * @param sIntegratioName     - Name of integration point
  * @param sIntegrationMethod  - Request method name like POST, GET, PUT 
  */
    public void addToRequestHeaders(String sKey, String sValue){

        mapRequestHeaders = (mapRequestHeaders==null)? new Map<String,String>():mapRequestHeaders;
        mapRequestHeaders.put(sKey,sValue);
    
    request = (request==null)? new HttpRequest() : request;
    request.setHeader(sKey, sValue);
    }

 /*
  * Method name: addToRequestHeaders
  * @description:  add key-value to mapRequestHeaders class property
  * @param delegatedAuth       - Boolean that checks if the request is for Delegated Authentication and sets the header acordingly 
  * @param authToken           - Authtoken, populated only when it's not a request for delegatedAuth
  */
    public void addToRequestHeaders(boolean delegatedAuth, String authToken){

        mapRequestHeaders = new Map<String,String>();
        mapRequestHeaders.put('Accept','application/json');
        if(delegatedAuth){
            mapRequestHeaders.put('X-Auth-Provider','sfdc');
            mapRequestHeaders.put('X-Auth-Host',URL.getSalesforceBaseUrl().toExternalForm());
            mapRequestHeaders.put('Authorization',UserInfo.getSessionId());
        }else if(!delegatedAuth && authToken<>'')
            mapRequestHeaders.put('Authorization',authToken);
        request = (request==null)? new HttpRequest() : request;
        //set the headers
        for(String sKey : mapRequestHeaders.keySet()){
            request.setHeader(sKey, mapRequestHeaders.get(sKey));
        }
        
    }

 /*
  * Method name: removeFromRequestHeaders
  * @description:  remove key value from mapRequestHeaders class property
  * @param sKey  - Key name of header that needs to be removed
  */
    public boolean removeFromRequestHeaders(String sKey){

        boolean bIsRemoved = false;
        if(mapRequestHeaders!=null && mapRequestHeaders.containsKey(sKey)){
            mapRequestHeaders.remove(sKey);
            bIsRemoved = true;
        }
        return bIsRemoved;
    }

 /*
  * Method name: setEndPoint
  * @description: query custom setting to set end point for request class property
  * @param sIntegratioName     - Name of integration point
  * @param sIntegrationMethod  - Request method name like POST, GET, PUT 
  */
  @TestVisible
  private void setEndPoint(String endPointUrl, String sIntegrationMethod){
        if(endPointUrl<>null)
            request.setEndpoint(endPointUrl);
        if(sIntegrationMethod<> null)
            request.setMethod(sIntegrationMethod);
  }
 
 /*
  * Method name: setAuthenticationParamtersPrepareRequest
  * @description: query custom setting to set user name, password and certificate for request class property 
                  and set the endpoint and Url 
  * @param sIntegratioName     - Name of integration point -- 'Veeva Vault'
  */
  public void setAuthenticationParamtersPrepareRequest(String sIntegratioName){

    //get the credentials from remote site setting 
    JJ_Remote_Site_Settings__c vaultCredentials = JJ_Remote_Site_Settings__c.getInstance(sIntegratioName);
    request = (request == null)? new HttpRequest():request;
    if(vaultCredentials<>null){
        if(vaultCredentials.JJ_User_Name__c<> null && vaultCredentials.JJ_User_Name__c <>'' &&
            vaultCredentials.JJ_Password__c <> null && vaultCredentials.JJ_Password__c <> ''){
            request.setBody('username='+vaultCredentials.JJ_User_Name__c+'&password='+vaultCredentials.JJ_Password__c);
            setEndpoint(vaultCredentials.JJ_Remote_Service_End_Point__c+'/auth',JJ_StaticConstants.POST);
            baseUrl = vaultCredentials.JJ_Remote_Service_End_Point__c;
            }
        }
    }

 /*
  * Method name: prepareRequest
  * @description: query custom setting to set end point for request class property
  * @param sBody               - 
  * @param sIntegratioName     - Name of integration point
  * @param sIntegrationMethod  - Request method name like POST, GET, PUT 
  */  
    public void prepareRequest(String sBody, String sIntegratioName, String sIntegrationMethod){

        setRequestHeaders();
        setEndPoint(sIntegratioName,sIntegrationMethod);
        if(mapRequestHeaders!=null){
            for(String sKey : mapRequestHeaders.keySet()){
                request.setHeader(skey,mapRequestHeaders.get(sKey));
            }
        }
        if(sBody!=null)
            request.setBody(sBody);
        if(sIntegrationMethod!=null)
            request.setMethod(sIntegrationMethod);
    }
     /*
    * @Method name: doCallout
    * @description: contains logic to make a callout, Log the requests and response body into the system Log object asynchronously
    */
   
    public HttpResponse doCallout(){
        
        try{
            response = http.send(request);
          //  system.debug('response-------------->'+response.getBody());
          //  system.debug('response-------------->'+JSON.serialize(response.getBody()));
        }catch(CalloutException ex){
            //Error Logging Logic
            throw ex;
        }
        return response;
    }
     /*
    * @Method name: getParsedResponse
    * @description: Retreives response, deserializes it when the HttpCallout is a success(Status code : 200)
    * @Param: responseObjectType : The fully qualified class name of the response wrapper class
    */
    public object getParsedResponse(String responseObjectType){

    object parsedResponse;
    if(response.getStatusCode() == 200){
      parsedResponse = JSON.deserialize(response.getBody(),Type.forName(responseObjectType));
    }
    return parsedResponse;

  }
}