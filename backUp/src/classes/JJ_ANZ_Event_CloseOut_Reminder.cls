global class JJ_ANZ_Event_CloseOut_Reminder implements Database.Batchable<sObject>,Schedulable  {

   /**************************************************************************
        Ingrid Suprana
        June 29, 2015
        Version 1.0
        
        Summary
        --------------
        This routine searches for: 
          Events in Close Out more than 14 Days after the Event Date
          Events in Reconciliation more than 28 Days after the Event Date
        and send email reminder to Event Owner to close out their event and/or follow up with invoices.
                    
        How to Run
        --------------
        Option 1. Run manually via the SFDC Developer Console:
        database.executebatch(new JJ_ANZ_Event_CloseOut_Reminder(),10); 
        
        Option 2. Schedule a nightly routine 
        Setup > Develop > Apex Classes > Schedule Apex
    **************************************************************************/
 
     global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT ID, Name, OwnerId, Start_Date_vod__c, JJ_ANZ_Event_Status__c, Days_to_Event__c, ' +
                       ' JJ_Event_Meeting_Title__c, JJ_Event_Venue__c, Event_Type__c ' +
                       ' FROM Medical_Event_vod__c ' +
                       ' WHERE (JJ_ANZ_Event_Status__c like \'%Close Out%\' and Days_to_Event__c < -14 )' +
                       ' or (JJ_ANZ_Event_Status__c like \'%Reconciliation%\' and Days_to_Event__c < -28 ) ';
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) { 
        database.executebatch(new JJ_ANZ_Event_CloseOut_Reminder(),10); 
    }     
      
    global void execute(Database.BatchableContext BC, List<Medical_Event_vod__c> scope) {
         List<Medical_Event_vod__c> eventToSendReminder = new List<Medical_Event_vod__c>();
         List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

         //EmailTemplate templateId = [Select id from EmailTemplate where DeveloperName = 'JJ_ANZ_Overdue_Event_Reminder_to_Close_Out'];

         for(Medical_Event_vod__c a : scope)
         {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          
            User[] contacts = [Select Email, Manager.Email from User where Id = :a.OwnerId];
                        
            String emailAddr = contacts[0].Email;     
            String MgremailAddr = contacts[0].Manager.Email;   
                 
            String[] toAddresses = new String[] {emailAddr};
            mail.setToAddresses(toAddresses);            

            String[] ccAddresses = new String[] {MgremailAddr};
            mail.setCcAddresses(ccAddresses);
            
            String[] BccAddresses = new String[] {'isuprana@its.jnj.com'};
            //mail.setBccAddresses(BccAddresses);

            //OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'iConnect'];
            //if ( owea.size() > 0 ) {
            //    mail.setOrgWideEmailAddressId(owea.get(0).Id);
            //}
            //mail.setSenderDisplayName('iConnect Reminder');
            
            String EventOwnerName = [select Name from User where Id = :a.OwnerId].Name;
            
            If (a.JJ_ANZ_Event_Status__c == 'Close Out') {
           
                mail.setSubject('Action Required: Close Out Event ' + a.Name);

                mail.setPlainTextBody(EventOwnerName + ', \n\n' +
                    'Our records indicate Event ' + a.Name+ ' (occured ' + (a.Days_to_Event__c.intValue() * -1)  + ' days ago) has not been closed out in iConnect. ' +
                    'Please complete as soon as possible. \n\n' +
                    'Event Name: ' + a.JJ_Event_Meeting_Title__c + '\n' +
                    'Event Date: ' + a.Start_Date_vod__c.format() + '\n' +
                    'Event Type: ' + a.Event_Type__c + '\n\n' +               
                    'To complete the ‘Close Out’ process: \n\n' +
                    'STEP 1: Open link ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + a.Id + ' \n\n' +
                    'STEP 2: Complete ‘Event Budget’ section with Actuals and Payment Type gXRS or ARIBA. \n\n' +
                    'STEP 3: Complete ‘Event Attendees’ section  \n\n' +
                    'STEP 4: Complete ‘Post Event Information‘ section: # HCP and JC attendees, confirm ‘All Information correct?’ and ‘Complete Close Out’.  \n\n' +
                    'Thank You, \n ' +
                    'iConnect Team'           
                );
            } else {

                mail.setSubject('Action Required: Follow Up Invoice for Event ' + a.Name);

                mail.setPlainTextBody(EventOwnerName + ', \n\n' +
                    'Our records indicate invoice(s) related to Event ' + a.Name+ ' (occured ' + (a.Days_to_Event__c.intValue() * -1)  + ' days ago) ' +
                    'have not been reconciled. \n\n' +
                    'Event Name: ' + a.JJ_Event_Meeting_Title__c + '\n' +
                    'Event Date: ' + a.Start_Date_vod__c.format() + '\n' +
                    'Event Type: ' + a.Event_Type__c + '\n\n' +               
                    'To complete the ‘Reconciliation’ process: \n\n' +
                    'STEP 1: Open link ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + a.Id + ' \n\n' +
                    'STEP 2: Review Event Budget section for items not finalised, and follow up with Customer for invoice if applicable. \n\n' +
                    'STEP 3: Forward invoice to Sales Support for processing. \n\n' +                    
                    'Thank You, \n ' +
                    'iConnect Team'  
                );
            }
           
            allmsg.add(mail);
         }         
         Messaging.sendEmail(allmsg,false);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }

}