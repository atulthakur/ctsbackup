@isTest
public class TestHelper{

public static List<Case> createCases(String recordTypeName,Integer numRecordsToCreate)
    {
          List<Case> caseList = new List<Case>();
          Case c;
          RecordType rt;
          if (recordTypeName != null)
            rt = [select Id, IsPersonType from RecordType where Name = :recordTypeName and SobjectType = 'Case' limit 1];
          for (Integer i=0; i<numRecordsToCreate; i++)
         {
            c= new Case();
            c.Subject = 'Test'+i;
            c.Origin = 'Phone';
            c.Status = 'New';
            c.RecordTypeId = rt.Id;
            if((i+1)/2==1)
            {
            c.JJ_ANZ_Create_AE__c = true;
            c.JJ_ANZ_Create_MIR__c = true;
            }
            else
            {
            c.JJ_ANZ_Create_GI__c = true;
            c.JJ_ANZ_Create_PQC__c = true;
            }
            caseList.add(c);
         }
         
         if(!caseList.isEmpty())
         {
             insert caseList;
         }
        return caseList;
    }
    
    public static List<Case> updateCases(List<Case> caseList)
    {
          List<Case> updateCases = new List<Case>();
           Case c;
          for (Case caseToUpdate : caseList)
         {
             System.debug('Cases to Update '+caseToUpdate);
            c.Id= caseToUpdate.Id;           
            if(!c.JJ_ANZ_Create_GI__c)
            {
                c.JJ_ANZ_Create_GI__c = true;
                c.JJ_ANZ_Create_PQC__c = true;
            }
            else if(!c.JJ_ANZ_Create_AE__c)
            {            
                c.JJ_ANZ_Create_AE__c = true;
                c.JJ_ANZ_Create_MIR__c = true;
            }
            updateCases.add(c);
         }
         
         if(!updateCases.isEmpty())
         {
             update updateCases;
         }
        return updateCases;
    }
    
}