/** Subject  : JJ_ANZ_RecursiveTriggers

Description  : Class to have static variables which stops recursive calling
Author       : Mahesh Somineni || msominen@its.jnj.com||1/14/2016

**/
public class JJ_ANZ_RecursiveTriggers { 
   public static boolean firstRunforUpdate = true;
   public static boolean firstRunforCaseTrigger = true;  
   public static boolean firstRunforVeevaTrigger = true;
   public static boolean firstRunforInsertCreateCases =  true;
   public static boolean firstRunforUpdateCreateCases =  true;
   public static boolean firstRunOnAdverseEvent = true;
   public static boolean firstRunOnCaseInformation = true;
   public static boolean firstRunOnClosePrimaryCase = true;
   public static boolean firstRunOnBusinessHours = true;
   public static boolean firstRunOnBeforeUpdate = true;
   public static boolean firstRunOnAfterUpdate = true;
   public static boolean firstRunforAfterUpdate = true;
   Public static boolean firstRunOnCaseStatusChange = true;
   public static boolean firstRunOnAssessPrimaryCases = true;
   Public static boolean AvoidFirstRunonCaseProduct = true;
   Public static boolean FirstRunonAfterInsertCaseProduct= true;
   Public static boolean FirstRunonBeforeInsertCaseProduct= true;
   Public static boolean FirstRunonBeforeUpdateCaseProduct= true;
   Public static boolean FirstRunonBeforeDeleteCaseProduct= true;
   Public static boolean FirstRunOnVeeva= true;
   Public static boolean firstEnterInUpdateMI=true;
   //RC1:added new static variable for INC000017924941
   Public static boolean firstRunUpdateOnAfterUpdate=true;
}