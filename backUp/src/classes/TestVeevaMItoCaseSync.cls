@isTest
public class TestVeevaMItoCaseSync
{
    public static testmethod void CreateMI()    
    {
        List<VeevaToCaseMapping__c> lsttestmapping = New List<VeevaToCaseMapping__c>();
        VeevaToCaseMapping__c testmap1 = New VeevaToCaseMapping__c();
        testmap1.Name ='Description';
        testmap1.JJ_ANZ_VeevaToCaseSync__c =true;
        testmap1.MIFieldAPIName__c ='JJ_ANZ_QuestionDetail__c';
        lsttestmapping.add(testmap1);
        VeevaToCaseMapping__c testmap2 = New VeevaToCaseMapping__c();
        testmap2.Name ='JJ_ANZ_Acknowledgement_Date__c';
        testmap2.JJ_ANZ_VeevaToCaseSync__c =true;
        testmap2.MIFieldAPIName__c ='JJ_Core_Acknowledge_Date__c';
        lsttestmapping.add(testmap2);
        Insert lsttestmapping;
        JJ_ANZ_CaseToMISync.getAllMapping();
        List<CasetoChaildCaseFieldMapping__c> lstcasemapping = New List<CasetoChaildCaseFieldMapping__c>();
        CasetoChaildCaseFieldMapping__c casemap1 = New CasetoChaildCaseFieldMapping__c();
        casemap1.Name ='JJ_ANZ_Active_Ingredient__c';
        casemap1.Chaild_Case_Field_API_Name_del__c ='JJ_ANZ_Active_Ingredient__c';
        lstcasemapping.add(casemap1);
        
        CasetoChaildCaseFieldMapping__c casemap2 = New CasetoChaildCaseFieldMapping__c();
        casemap2.Name ='JJ_ANZ_Address_Line_1__c';
        casemap2.Chaild_Case_Field_API_Name_del__c ='JJ_ANZ_Address_Line_1__c';
        lstcasemapping.add(casemap2);
        Insert lstcasemapping;
        JJ_ANZ_CaseToMISync.getCaseMapping();
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        
        insert acc;
        
        JJ_ANZ_MAFOPS_Product__c detailProduct = new JJ_ANZ_MAFOPS_Product__c();
        detailProduct.Name = 'DeadPool';
        
        insert detailProduct;
        
        RecordType rt = [Select Id,Name from RecordType where sObjectType='Medical_Inquiry_vod__c' and Name='iConnect Case'];
        Medical_Inquiry_vod__c MIrecord = new Medical_Inquiry_vod__c();
        MIrecord.RecordTypeId = rt.Id;
        MIrecord.Account_vod__c = acc.Id;
        MIrecord.JJ_ANZ_Subject__c = 'Testing';
        MIrecord.JJ_ANZ_QuestionDetail__c = 'Test class';
        MIrecord.I_Acknowledge_disclaimer__c = 'Yes, I acknowledge disclaimer.';
        MIrecord.JJ_ANZ_Detail_Product__c = detailProduct.Id;
        MIrecord.JJ_ANZ_Product_Other__c = 'test prod';
        MIrecord.JJ_ANZ_AE__c = true;
        MIrecord.JJ_ANZ_GI__c = true;
        MIrecord.JJ_ANZ_MIR__c = true;
        MIrecord.JJ_ANZ_PQC__c = true;
        MIrecord.Status_vod__c='Submitted_vod';
        
        insert MIrecord;

        
    }
    
    public static testmethod void secondCreateMI()    
    {
        List<VeevaToCaseMapping__c> lsttestmapping = New List<VeevaToCaseMapping__c>();
        VeevaToCaseMapping__c testmap1 = New VeevaToCaseMapping__c();
        testmap1.Name ='Description';
        testmap1.JJ_ANZ_VeevaToCaseSync__c =true;
        testmap1.MIFieldAPIName__c ='JJ_ANZ_QuestionDetail__c';
        lsttestmapping.add(testmap1);
        VeevaToCaseMapping__c testmap2 = New VeevaToCaseMapping__c();
        testmap2.Name ='JJ_ANZ_Acknowledgement_Date__c';
        testmap2.JJ_ANZ_VeevaToCaseSync__c =true;
        testmap2.MIFieldAPIName__c ='JJ_Core_Acknowledge_Date__c';
        lsttestmapping.add(testmap2);
        Insert lsttestmapping;
        
        TriggerDisabler__c testdisable = New TriggerDisabler__c();
        testdisable.JJ_ANZ_VOD_BEFORE_MED_INQ_UPD__c = true;
        Insert testdisable;
        Account acc = new Account();
        acc.Name = 'Test Account';
        
        insert acc;
        
        JJ_ANZ_MAFOPS_Product__c detailProduct = new JJ_ANZ_MAFOPS_Product__c();
        detailProduct.Name = 'DeadPool';
        
        insert detailProduct;
        
        RecordType rt = [Select Id,Name from RecordType where sObjectType='Medical_Inquiry_vod__c' and Name='iConnect Case'];
        Medical_Inquiry_vod__c MIrecord = new Medical_Inquiry_vod__c();
        MIrecord.RecordTypeId = rt.Id;
        MIrecord.Account_vod__c = acc.Id;
        MIrecord.JJ_ANZ_Subject__c = 'Testing';
        MIrecord.JJ_ANZ_QuestionDetail__c = 'Test class';
        MIrecord.I_Acknowledge_disclaimer__c = 'Yes, I acknowledge disclaimer.';
        MIrecord.JJ_ANZ_Detail_Product__c = detailProduct.Id;
        MIrecord.JJ_ANZ_Product_Other__c = 'test prod';
        MIrecord.JJ_ANZ_AE__c = true;
        MIrecord.JJ_ANZ_GI__c = true;
        MIrecord.JJ_ANZ_MIR__c = true;
        MIrecord.JJ_ANZ_PQC__c = true;
        MIrecord.Status_vod__c='Submitted_vod';
        MIrecord.JJ_ANZ_Case_Number__c = '0001-ch'; 
        insert MIrecord;
        MIrecord.JJ_ANZ_Initial_Reporter_Type__c = 'Physician';
        Update MIrecord;
        JJ_ANZ_CaseToMISync.getCaseMapping();
        List<Case> lstcase = New List<Case>();
        Case testcase = New Case();
        testcase.Subject = 'test subject';
        testcase.Status = 'New';
        testcase.JJ_ANZ_JnJ_Notified_Date__c = System.Today();
        testcase.JJ_ANZ_Permission_to_contact_Reporter__c = 'Yes';
        testcase.JJ_ANZ_ChildCaseNumber__c = '0001-ch';        
        Insert testcase;
        
        testcase.JJ_ANZ_Initial_Reporter_Type__c='Physician';
        Update testcase;
        lstcase.add(testcase);
        JJ_ANZ_CaseToMISync.updateMIRecords(lstcase);
        
    }
    
    public static testmethod void CaseToMISynctest()    
    {
        List<Case> lstcase = New List<Case>();
        Case testcase = New Case();
        testcase.Subject = 'test subject';
        testcase.Status = 'New';
        testcase.JJ_ANZ_JnJ_Notified_Date__c = System.Today();
        testcase.JJ_ANZ_Permission_to_contact_Reporter__c = 'Yes';
        testcase.JJ_ANZ_ChildCaseNumber__c = '0001-ch';        
        Insert testcase;
        
        testcase.JJ_ANZ_Initial_Reporter_Type__c='Physician';
        Update testcase;
        lstcase.add(testcase);
        Delete testcase;
        Undelete testcase;
    }
    public static testmethod void populateSeriousnessTest()    
    {
        Id merecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case Information').getRecordTypeId();
        List<Case> lstcase = New List<Case>();
        Case parenttestcase = New Case();
        parenttestcase.Subject = 'test subject';
        parenttestcase.Status = 'Closed';
        parenttestcase.JJ_ANZ_JnJ_Notified_Date__c = System.Today();
        parenttestcase.JJ_ANZ_Permission_to_contact_Reporter__c = 'Yes';
        parenttestcase.JJ_ANZ_ChildCaseNumber__c = '0001-ch';
        parenttestcase.RecordTypeId = merecordTypeId;    
        Insert parenttestcase; 
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Adverse Event').getRecordTypeId();
        Case testcase = New Case();
        testcase.ParentId = parenttestcase.Id;
        testcase.Subject = 'test subject';
        testcase.Status = 'New';
        testcase.JJ_ANZ_JnJ_Notified_Date__c = System.Today();
        testcase.JJ_ANZ_Permission_to_contact_Reporter__c = 'Yes';
        testcase.JJ_ANZ_ChildCaseNumber__c = '0001-ch';
        testcase.JJ_ANZ_Seriousness__c = 'Not Serious';        
        testcase.RecordTypeId = RecordTypeId;
        Insert testcase;
        testcase.JJ_ANZ_Serious_Criteria__c = 'Death';
        testcase.JJ_ANZ_Seriousness__c = 'Death/Life Threatening';
        Update testcase;
    }
}