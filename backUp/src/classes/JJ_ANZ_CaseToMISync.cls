/**
 *      @author Mahesh Somineni
 *      @date   March 18 2016
        @description    Class for syncing Cases and Medical Inquiries.  
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Mahesh Somineni                 18/03/2016         Original Version
        Shanmuga Priyan                 18/12/2017         Par case sync with MI
        */
public class JJ_ANZ_CaseToMISync {
    
    // Declare Static Variables
    public static String Checked = 'Yes';
    public static String MIRRecordType = 'Medical Inquiry Request';
    public static String GIRecordType = 'General Inquiry';
    public static String PCERecordType='Phone Case Entry';
    public static String AERecordType = 'Adverse Event';
    public static String PQCRecordType = 'Product Quality Complaints';
    public static String PARRecordType =  'Product Access Request';
    public static String PrimaryCaseRecordType = 'Case Information';
    Public static Map<string, string> MapMappingTable=new map<string,string>();
    Public static Map<string, string> CaseMappingTable=new map<string,string>();
    // Method to create child records in Medical Inquiry on Insert Call
    public static void CreateChildRecords(List<Case> listCases)
    {    
        //Get RecordTypes for Cases and Medical Inquiries
        System.debug('CreateChildRecords listcases');
        Map<String,String> caseRecordTypes = JJ_ANZ_CaseToMISync.getRecordTypes('Case');
        Map<String,String> MIRecordTypes = JJ_ANZ_CaseToMISync.getRecordTypes('Medical_Inquiry_vod__c');
        
        
        List<Medical_Inquiry_vod__c> InsertMIRecords=new List<Medical_Inquiry_vod__c>();
        List<Medical_Inquiry_vod__c> updateiConnectCases =  new List<Medical_Inquiry_vod__c>();
        List<Medical_Inquiry_vod__c> insertNewMIRecords=  new List<Medical_Inquiry_vod__c>();
        List<id> iConnectNumbers = new List<id>();
        
        //Get list of Medical Inquiry Ids.
        for(Case caseRec : listCases)
        {        
            if(caseRec.JJ_ANZ_Medical_Inquiry_ID__c!=null)
            {            
                iConnectNumbers.add(caseRec.JJ_ANZ_Medical_Inquiry_ID__c);
            }
        }
        Map<Id,Medical_Inquiry_vod__c> mapPrimaryRecords;
        
        //Query to get Medical Inquiries related to Cases
        if(!iConnectNumbers.isEmpty()){
        mapPrimaryRecords = new Map<Id,Medical_Inquiry_vod__c>([Select id,name from Medical_Inquiry_vod__c where id in:iConnectNumbers]);
        }
        
        
        System.debug('Line 49'+mapPrimaryRecords+'line '+listCases);
        for(Case caseRec : listCases)
        {
            if((!caseRecordTypes.isEmpty())&&((caseRecordTypes.containsKey(PrimaryCaseRecordType)&&caseRec.RecordTypeId==caseRecordTypes.get(PrimaryCaseRecordType))||(caseRecordTypes.containsKey(PCERecordType)&&caseRec.RecordTypeId==caseRecordTypes.get(PCERecordType))))
            {
              if((mapPrimaryRecords!=null)&&mapPrimaryRecords.size()>0)
              {
                if(caseRec.JJ_ANZ_Medical_Inquiry_ID__c!=null&&mapPrimaryRecords.containsKey(caseRec.JJ_ANZ_Medical_Inquiry_ID__c))
                {
                   // Updated iConnect Case into Case Information in Medical Inquiry
                     Medical_Inquiry_vod__c updateMI = new Medical_Inquiry_vod__c();
                    updateMI.Id = mapPrimaryRecords.get(caseRec.JJ_ANZ_Medical_Inquiry_ID__c).Id;
                    updateMI.JJ_ANZ_Case_Number__c = caseRec.casenumber;
                    updateMI.JJ_ANZ_CreateAE__c = caseRec.JJ_ANZ_CreateAE__c;
                    updateMI.JJ_ANZ_CreateGI__c = caseRec.JJ_ANZ_CreateGI__c;
                    updateMI.JJ_ANZ_CreateMIR__c = caseRec.JJ_ANZ_CreateMIR__c;
                    updateMI.JJ_ANZ_CreatePQC__c = caseRec.JJ_ANZ_CreatePQC__c; 
                    updateMI.JJ_ANZ_CreatePAR__c = caseRec.JJ_ANZ_CreatePAR__c; 
                    updateMI.JJ_ANZ_JnJ_Employee__c = caseRec.JJ_ANZ_JNJ_Employee__c;  
                    updateMI.Lock_vod__c = false;                 
                    updateMI.RecordTypeId = MIRecordTypes.get(PrimaryCaseRecordType);
                    if(caseRec.status=='New')
                    {
                        updateMI.Status_vod__c = 'New_vod';
                    }
                else
                    {
                    updateMI.Status_vod__c = caseRec.status;
                    
                    }
                    
                    updateiConnectCases.add(updateMI);
                }
                }
                else if(caseRec.JJ_ANZ_Name__c!=null)
                {
                System.debug('Welcome Atul Thakur');
                // Updated iConnect Case into Case Information in Medical Inquiry
                     Medical_Inquiry_vod__c newMI = new Medical_Inquiry_vod__c();
                    newMI.Account_vod__c=caseRec.JJ_ANZ_Name__c; 
                    if(caseRec.JJ_ANZ_Product_Name__c!=null){
                    System.debug(caseRec.JJ_ANZ_Product_Name__r.Name+'®');
                    newMI.JJ_ANZ_CaseProduct__c = caseRec.JJ_ANZ_Product_Name__r.Name+'®';
                    }
                    else
                    {
                     newMI.JJ_ANZ_CaseProduct__c = caseRec.JJ_ANZ_Other_Product__c+'®';
                    }
                    newMI.JJ_ANZ_Case_Number__c = caseRec.casenumber;
                    newMI.JJ_ANZ_CreateAE__c = caseRec.JJ_ANZ_CreateAE__c;
                    newMI.JJ_ANZ_CreateGI__c = caseRec.JJ_ANZ_CreateGI__c;
                    newMI.JJ_ANZ_CreateMIR__c = caseRec.JJ_ANZ_CreateMIR__c;
                    newMI.JJ_ANZ_CreatePQC__c = caseRec.JJ_ANZ_CreatePQC__c; 
                    newMI.JJ_ANZ_CreatePAR__c = caseRec.JJ_ANZ_CreatePAR__c; 
                    newMI.JJ_ANZ_JnJ_Employee__c = caseRec.JJ_ANZ_JNJ_Employee__c;  
                    newMI.JJ_ANZ_Subject__c = caseRec.Subject;
                    newMI.JJ_ANZ_Case_Origin__c = caseRec.Origin;
                    newMI.JJ_ANZ_CaseOwner__c= caseRec.OwnerId;
                    
                    newMI.JJ_ANZ_JnJ_Notified_Date__c=caseRec.JJ_ANZ_JnJ_Notified_Date__c;
                    newMI.JJ_ANZ_QuestionDetail__c= caseRec.JJ_ANZ_EventDescription__c ; 
                    newMI.JJ_ANZ_Offlabel__c = caseRec.JJ_ANZ_Off_Label__c;
        
                    newMI.Lock_vod__c = false;                 
                    newMI.RecordTypeId = MIRecordTypes.get(PrimaryCaseRecordType);
                    if(caseRec.status=='New')
                    {
                        newMI.Status_vod__c = 'New_vod';
                    }
                else
                    {
                    newMI.Status_vod__c = caseRec.status;
                    
                    }
                    
                    insertNewMIRecords.add(newMI);
                    
                }
            }
            
            //logic to create MIR if MIR is checked in Primary Case
            if(caseRec.JJ_ANZ_CreateMIR__c==Checked&&MIRecordTypes.containsKey(MIRRecordType)&&caseRec.JJ_ANZ_Name__c!=null)
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(MIRRecordType),'MIR');
                InsertMIRecords.add(veevaMedical);
            }
            //logic to create AE if AE is checked in Primary Case
            if(caseRec.JJ_ANZ_CreateAE__c==Checked&&MIRecordTypes.containsKey(AERecordType)&&caseRec.JJ_ANZ_Name__c!=null)
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(AERecordType),'AE');
                InsertMIRecords.add(veevaMedical);
            }
            //logic to create GI if GI is checked in Primary Case
            if(caseRec.JJ_ANZ_CreateGI__c==Checked&&MIRecordTypes.containsKey(GIRecordType)&&caseRec.JJ_ANZ_Name__c!=null)
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(GIRecordType),'GI');
                InsertMIRecords.add(veevaMedical);
            }
            //logic to create PQC if PQC is checked in Primary Case
            if(caseRec.JJ_ANZ_CreatePQC__c==Checked&&MIRecordTypes.containsKey(PQCRecordType)&&caseRec.JJ_ANZ_Name__c!=null)
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(PQCRecordType),'PQC');
                InsertMIRecords.add(veevaMedical);
            }
            
            //logic to create PAR if PAR is checked in Primary Case
            if(caseRec.JJ_ANZ_CreatePAR__c==Checked&&MIRecordTypes.containsKey(PARRecordType)&&caseRec.JJ_ANZ_Name__c!=null)
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(PARRecordType),'PAR');
                InsertMIRecords.add(veevaMedical);
            }
        }
        
        // Update Medical Inquiries
        if(!updateiConnectCases.isEmpty())
        {
            try
            {
                update updateiConnectCases;
            }catch(DMLException de)
            {
               
                // Log a DML Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);
            }catch(Exception e)
            {
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }
        
        //Insert MI Records
        if(!insertNewMIRecords.isEmpty())
        {
            try
            {
                insert insertNewMIRecords;
                System.debug('Inserted');
            }catch(DMLException de)
            {
               
                // Log a DML Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);
            }catch(Exception e)
            {
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }
        // Insert Child Records in Medical Inquiry
        if(!InsertMIRecords.isEmpty())
        {
            try
            {
                insert InsertMIRecords;
            }catch(DMLException de)
            {
                // Log a DML Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);
            }catch(Exception e)
            {
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }
    }
    
    // Method to create child records in Medical Inquiry on Update Call
    public static void CreateChildRecords(List<Case> listCases,Map<Id,Case> oldMapCases)
    { 
        //Get RecordTypes for Cases and Medical Inquiries
        System.debug('CreateChildRecords listcases and oldMapCases');
        Map<String,String> caseRecordTypes = JJ_ANZ_CaseToMISync.getRecordTypes('Case');
        Map<String,String> MIRecordTypes = JJ_ANZ_CaseToMISync.getRecordTypes('Medical_Inquiry_vod__c');
        
        List<Medical_Inquiry_vod__c> InsertMIRecords=new List<Medical_Inquiry_vod__c>();
        List<Case> ChildCases = [Select id,casenumber,parentid from case where parentid in:oldMapCases.keyset()];
        Map<Id,List<Case>> MapChildCases = new Map<Id,List<Case>>();
       
       //Put Child Records in a Map
       if(!ChildCases.isEmpty())
       {
       for(Case childCase : ChildCases)
        {
        if(MapChildCases.containsKey(childCase.ParentId))
        {
            MapChildCases.get(childCase.ParentId).add(childCase);
            
        }
        else
        {
            MapChildCases.put(childCase.ParentId, new  List <Case> { childCase });
        }
      }   
      } 
       
        //Get Mapping Fields for Case Object to populate Values from Parent to Child Case
        CaseMappingTable= JJ_ANZ_CreateChildCases.getAllMapping();
        
      List<Case> UpdateChildCases =  new List<Case>();
      
      // Loop to update Parent Case Values into Child Case
      for(Case caseRec : listCases)
      {
        if(MapChildCases.size()>0){
        if(MapChildCases.containsKey(caseRec.Id))
        {
            List<Case> listChildCases = MapChildCases.get(caseRec.Id);
            for(Case childCase : listChildCases)
            {
              Case updateCase = new Case();
              updateCase.Id=childCase.Id;
              if(CaseMappingTable.size()>0)
              {              
                 for(String newCaseField: CaseMappingTable.keySet())
                    {                        
                        String  caseField = CaseMappingTable.get(newCaseField);                       
                        updateCase.put(newCaseField, caseRec.get(caseField));
                    } 
              }                              
              UpdateChildCases.add(updateCase);
            }
        }
        }
        
        
      }
      
      // Loop to create Child Records in Medical Inquiry on Update call in Cases.
        for(Case caseRec : listCases)
        {
            Case oldCase = oldMapCases.get(caseRec.Id);
            if(caseRec.JJ_ANZ_CreateMIR__c==Checked&&oldCase.JJ_ANZ_CreateMIR__c!=Checked&&MIRecordTypes.containsKey(MIRRecordType)&&(caseRec.origin=='Veeva MI'||caseRec.origin!='Veeva MI'))
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(MIRRecordType),'MIR');
                InsertMIRecords.add(veevaMedical);
            }
            if(caseRec.JJ_ANZ_CreateAE__c==Checked&&oldCase.JJ_ANZ_CreateAE__c!=Checked&&MIRecordTypes.containsKey(AERecordType)&&(caseRec.origin=='Veeva MI'||caseRec.origin!='Veeva MI'))
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(AERecordType),'AE');
                InsertMIRecords.add(veevaMedical);
            }
            if(caseRec.JJ_ANZ_CreateGI__c==Checked&&oldCase.JJ_ANZ_CreateGI__c!=Checked&&MIRecordTypes.containsKey(GIRecordType)&&(caseRec.origin=='Veeva MI'||caseRec.origin!='Veeva MI'))
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(GIRecordType),'GI');
                InsertMIRecords.add(veevaMedical);
            }
            if(caseRec.JJ_ANZ_CreatePQC__c==Checked&&oldCase.JJ_ANZ_CreatePQC__c!=Checked&&MIRecordTypes.containsKey(PQCRecordType)&&(caseRec.origin=='Veeva MI'||caseRec.origin!='Veeva MI'))
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(PQCRecordType),'PQC');
                InsertMIRecords.add(veevaMedical);
            }
            if(caseRec.JJ_ANZ_CreatePAR__c==Checked&&oldCase.JJ_ANZ_CreatePAR__c!=Checked&&MIRecordTypes.containsKey(PARRecordType)&&(caseRec.origin=='Veeva MI'||caseRec.origin!='Veeva MI'))
            {
                Medical_Inquiry_vod__c veevaMedical = JJ_ANZ_CaseToMISync.createChild(caseRec,MIRecordTypes.get(PARRecordType),'PAR');
                InsertMIRecords.add(veevaMedical);
            }
        }
        
        // Insert child Records in Medical Inquiry
        if(!InsertMIRecords.isEmpty())
        {
            try
            {
                insert InsertMIRecords;
            }catch(DMLException de)
            {
                // Log a DML Exception if Insert fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);
            }catch(Exception e)
            {    
                // Log an Exception if Insert fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }
       
        //Update Child Cases with Parent Case Values.
        if(!UpdateChildCases.isEmpty())
        {
            try
            {
                update UpdateChildCases;
            }catch(DMLException de)
            {    
                // Log a DML Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);
            }catch(Exception e)
            {
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        } 
    }
    
    // Generic class to get RecordTypes for an Object.
    public static Map<String,String> getRecordTypes(String obj)
    {
    System.debug('getRecordTypes');
         List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType=:obj and isActive=true];
         Map<String,String> RecordTypes = new Map<String,String>();
         for(RecordType rt : rTypes)
         {
             RecordTypes.put(rt.Name, rt.id);
         }
        if(RecordTypes.size()>0)
        {
            return RecordTypes;
        }
        else
        {
            return null;
        }
    }
    
    //Re-usable method to create child Records in Medical Inquiry
    public static Medical_Inquiry_vod__c createChild(Case caseRec,Id RecordTypeId,String rType)
    {
    
        System.debug('createChildcaserec recordtypeid');
        Medical_Inquiry_vod__c veevaMedical=new Medical_Inquiry_vod__c();        
        veevaMedical.RecordTypeId = RecordTypeId;
        
        veevaMedical.Account_vod__c=caseRec.JJ_ANZ_NAME__c; 
         
        veevaMedical.JJ_ANZ_Medical_Enquiry__c = caseRec.JJ_ANZ_MedicalEnquiryNumber__c;
        veevaMedical.JJ_ANZ_Par_case_Product__c= caseRec.JJ_ANZ_CaseProducts__c;         
        veevaMedical.JJ_ANZ_CaseProduct__c = caseRec.JJ_ANZ_CaseProducts__c;                 
        veevaMedical.JJ_ANZ_Case_Number__c=caseRec.caseNumber+'-'+rType;
        veevaMedical.JJ_ANZ_Subject__c = caseRec.Subject;
        //veevaMedical.JJ_ANZ_Subject__c = caseRec.Subject;
        veevaMedical.Status_vod__c = caseRec.Status;
        if(caseRec.JJ_ANZ_Product_Name__c!=null){
          //ystem.debug(caseRec.JJ_ANZ_Product_Name__r.Name+'®');
                    veevaMedical.JJ_ANZ_CaseProduct__c = caseRec.JJ_ANZ_Product_Name__r.Name;
                    }
                    else
                    {
                     veevaMedical.JJ_ANZ_CaseProduct__c = caseRec.JJ_ANZ_Other_Product__c;
                    }
        //veevaMedical.JJ_ANZ_PAR_CaseProduct__c = caseRec.JJ_ANZ_CaseProducts__c;
        system.debug('veevaMedical');
        veevaMedical.JJ_ANZ_Case_Origin__c = caseRec.Origin;
        veevaMedical.JJ_ANZ_CaseOwner__c= caseRec.OwnerId;
        veevaMedical.JJ_ANZ_JnJ_Employee__c= caseRec.JJ_ANZ_JnJ_Employee__c;
        veevaMedical.Lock_vod__c = false;
        if(rType=='AE')
        {
            veevaMedical.JJ_ANZ_Case_Owner__c = 'Back Desk AE';
        }
        else if(rType=='PQC')
        {
            veevaMedical.JJ_ANZ_Case_Owner__c = 'Back Desk PQC';
        }
        else
        {
            veevaMedical.JJ_ANZ_Case_Owner__c = 'Front Desk';
        }
         if(rType=='PAR')
        {
            veevaMedical.JJ_ANZ_Case_Owner__c = 'Product Access Queue';
        }

        return veevaMedical;
    }
    // Method Update Medical Inquiry Records with the Updated Values in Case Records.
    public static Void updateMIRecords(List<Case> updateCases)
    {
        
        JJ_ANZ_RecursiveTriggers.firstEnterInUpdateMI=false;
        System.debug('In Update Mi Records'+updateCases);
        System.debug('updateMIRecords');
        MapMappingTable = getAllMapping();
        List<String> caseNumbers = new List<String>();
        List<Id> Userids = new List<Id>();
        List<Id> Queueids = new List<Id>();
        List<Medical_Inquiry_vod__c> UpdateMIRecords = new List<Medical_Inquiry_vod__c>();
        for(Case updateCase:updateCases)
        {
            caseNumbers.add(updateCase.JJ_ANZ_ChildCaseNumber__c);
                    System.debug('Update Case'+updateCase);
                    System.debug('Update Case number'+caseNumbers);
            if(String.Valueof(updateCase.OwnerId).startswith('005'))
            {
                Userids.add(updateCase.OwnerId);
            }
            else
            {
                Queueids.add(updateCase.OwnerId);
            }
        }
        
        // Get Medical Enquiries using Case Number
        List<Medical_Inquiry_vod__c> listMedicalRecords = new List<Medical_Inquiry_vod__c>();
         Map<Id,User> MapUsers = new Map<Id,User>();
         Map<Id,Group> MapQueues = new Map<Id,Group>();
        // if(JJ_ANZ_RecursiveTriggers.firstEnterInUpdateMI) this statement is commented out because Below If statements are not getting executed
    //   {
       if(!Userids.isEmpty())
       {
            MapUsers = new Map<Id,User>([Select id,Name from User where id in:Userids]);
       }
       if(!Queueids.isEmpty())
       {
           MapQueues = new Map<Id,Group>([select Id,Name from Group where Type = 'Queue' and id in : Queueids]);
       }
     //  }
       
        if(!caseNumbers.isEmpty())
        {
         listMedicalRecords = [SELECT Id,Name,Account_vod__c,RecordTypeId,JJ_ANZ_Case_Number__c FROM Medical_Inquiry_vod__c WHERE JJ_ANZ_Case_Number__c IN:caseNumbers and JJ_ANZ_Case_Number__c!=null];
        }
        System.debug('Medical Inquiries list'+listMedicalRecords);
        Map<String,Medical_Inquiry_vod__c> MapMedicalRecords = new Map<String,Medical_Inquiry_vod__c>();
        if(!listMedicalRecords.isEmpty())
        {
        for(Medical_Inquiry_vod__c MIRecord : listMedicalRecords)
        {
            MapMedicalRecords.put(MIRecord.JJ_ANZ_Case_Number__c, MIRecord);
        }
        }
        System.debug('Map Medical Records'+MapMedicalRecords);
        // Loop to populate values in Medical Inquiry from case
        for(Case caseRec : updateCases)
        {
          // Cases sync will happen only if Initial Reporter type is in any of the below.
          if((caseRec.JJ_ANZ_Name__c!=null) &&(caseRec.JJ_ANZ_JnJ_Program_Number__c==null||caseRec.JJ_ANZ_JnJ_Program_Number__c=='')){
          
            Medical_Inquiry_vod__c veevaMedical=new Medical_Inquiry_vod__c();
            if(MapMedicalRecords.size()>0)
            {
            if(MapMedicalRecords.containsKey(caseRec.JJ_ANZ_ChildCaseNumber__c))
            {                
                veevaMedical.Id = MapMedicalRecords.get(caseRec.JJ_ANZ_ChildCaseNumber__c).Id;
                if(caseRec.JJ_ANZ_ApprovalStatus__c!=null)
                {
                    veevaMedical.JJ_ANZ_ApprovalStatus__c = caseRec.JJ_ANZ_ApprovalStatus__c;
                }
                               
                    veevaMedical.JJ_ANZ_CaseNarrative__c = caseRec.JJ_ANZ_Case_Narrative__c;                                        
                
                if(caseRec.Status=='Closed')
                {
                    veevaMedical.JJ_Core_Status__c = 'Closed';
                }
                    veevaMedical.JJ_ANZ_CreateAE__c = caseRec.JJ_ANZ_CreateAE__c;
                    veevaMedical.JJ_ANZ_CreateGI__c = caseRec.JJ_ANZ_CreateGI__c;
                    veevaMedical.JJ_ANZ_CreateMIR__c = caseRec.JJ_ANZ_CreateMIR__c;
                    veevaMedical.JJ_ANZ_CreatePQC__c = caseRec.JJ_ANZ_CreatePQC__c;
                    veevaMedical.JJ_ANZ_CreatePAR__c = caseRec.JJ_ANZ_CreatePAR__c;
                if(String.Valueof(caseRec.OwnerId).startswith('005'))
                {
                    veevaMedical.JJ_ANZ_CaseOwner__c = caseRec.OwnerId;
                    if(MapUsers.containsKey(caseRec.OwnerId))
                    {
                      veevaMedical.JJ_ANZ_Case_Owner__c = MapUsers.get(caseRec.OwnerId).Name;
                    }
                }
                else
                {
                    if(MapQueues.containsKey(caseRec.OwnerId))
                    {
                      veevaMedical.JJ_ANZ_Case_Owner__c = MapQueues.get(caseRec.OwnerId).Name;
                    }
                }
                for(String VeevaField: MapMappingTable.keySet())
                    {
                       
                        String  caseField = MapMappingTable.get(VeevaField);
                        veevaMedical.put(VeevaField, caseRec.get(caseField));
                    }
                    veevaMedical.Status_vod__c = caseRec.Status;
                 UpdateMIRecords.add(veevaMedical);
            }
            }
            }
        }
        
        System.debug('Veeva Sync '+UpdateMIRecords);
        // Update Medical Inquiry Records with the Updated Values in Case Records.
        if(!UpdateMIRecords.isEmpty())
        {
            try
            {
                update UpdateMIRecords;
            }catch(DMLException de)
            {
                // Log a DML Exception if Update fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);  
            }catch(Exception e)
            {
                // Log a Exception if Update fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }
        
    }
        // Static Method to get Fields to be mapped between case and Medical Inquiry Objects
      public static Map<string,string> getAllMapping()
     {
     System.debug('getAllMapping');
       
        try{
             for (VeevaToCaseMapping__c mappingTableRec : VeevaToCaseMapping__c.getall().Values())
             {
                if (mappingTableRec.Name != null && mappingTableRec.MIFieldAPIName__c != Null && mappingTableRec.JJ_ANZ_CaseToMISync__c )
                {
                    MapMappingTable.put( mappingTableRec.MIFieldAPIName__c,mappingTableRec.Name );
                    
                }
             }
        }
        catch(exception ex)
        {
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =ex.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =ex.getTypeName();
                Database.insert(newErrorRecord,false);
        }
        return MapMappingTable;
    } 
    
    // Static Method to get Fields to be mapped in case Object
    public static Map<string,string> getCaseMapping()
    {
     System.debug('getCaseMapping');   
        try{
            for (CasetoChaildCaseFieldMapping__c mappingTableRec : CasetoChaildCaseFieldMapping__c.getall().Values())
            {
                if (mappingTableRec.Name != null &&mappingTableRec.Chaild_Case_Field_API_Name_del__c!=null )
                {
                    MapMappingTable.put(mappingTableRec.Chaild_Case_Field_API_Name_del__c,mappingTableRec.Name);
                    
                }
            }
        }
        catch(exception ex)
        {    
                // Log an Exception
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =ex.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =ex.getTypeName();
                Database.insert(newErrorRecord,false);
        }
        return MapMappingTable;
    }
    
}