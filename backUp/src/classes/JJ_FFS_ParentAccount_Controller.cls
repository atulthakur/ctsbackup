/**
* FB232 - ability to choose Parent Account of the HCP for FFS contract and address
*
* @author  Noel Lim
* @changehistory   
*  2017-08-03 initial, v1.0 
*/
public without sharing class JJ_FFS_ParentAccount_Controller {

    public final Speaker_Agreement__c feeForService {get; set;}   
    private ApexPages.StandardController sc; 
    Id ffsId;
    Id speakerAcctId;
    
    
    public JJ_FFS_ParentAccount_Controller(ApexPages.StandardController stdController) {
        this.sc = stdController;
        ffsId = stdController.getId();
        feeForService = (Speaker_Agreement__c) stdController.getRecord();
        List<Speaker_Agreement__c> listFFS = [Select JJ_Speaker__c FROM Speaker_Agreement__c WHERE Id = :ffsId];
        speakerAcctId = listFFS.get(0).JJ_Speaker__c;        

    }
    
    //list of Accounts that are Parents of the Speaker
    public SelectOption[] getParentAccountOptions() {  
        SelectOption[] parentAccountOptions = new SelectOption[]{}; 
        Set<Id> parentIds = new Set<Id>();
        Address_vod__c currAddress = null;
        
        List<Child_Account_vod__c> parentAccts = [SELECT Id, Parent_Account_vod__c, Parent_Account_vod__r.Name, Primary_vod__c 
                                            FROM Child_Account_vod__C 
                                            WHERE Child_Account_vod__c = :speakerAcctId 
                                            ORDER BY Primary_vod__c DESC];
                                            
                                            
        for(Child_Account_vod__c pAcct : parentAccts){
            parentIds.add(pAcct.Parent_Account_vod__c);
        }
        
        List<Address_vod__c> parentAddresses = [Select Account_vod__c, Name, Zip_vod__c, State_vod__c,Address_line_2_vod__c, Country_vod__c, City_vod__c, JJ_Core_Suburb__c 
                                FROM Address_vod__c 
                                WHERE Primary_vod__c = true AND Account_vod__c IN :parentIds];
         
        for (Child_Account_vod__c parentAcct: parentAccts) { 
            
            //get Parent's Address record,
            currAddress = null;
            for(Address_vod__c addr : parentAddresses){
                if(addr.Account_vod__c == parentAcct.Parent_Account_vod__c){
                    currAddress = addr;
                }
            }
            
            
            String optionText = (parentAcct.Primary_vod__c == 'Yes' ? '(PRIMARY) ' + parentAcct.Parent_Account_vod__r.Name : parentAcct.Parent_Account_vod__r.Name);
            if(currAddress != null){
                optionText = optionText + ' - ' + currAddress.Name + ', ' + currAddress.City_vod__c + ', ' + currAddress.State_vod__c + ', ' + currAddress.Zip_vod__c + ', ' + currAddress.Country_vod__c; 
            }
            else{
                optionText = optionText + ' (NO PRIMARY ADDRESS FOUND - please add an Address via Account Change Request)';
            }
            parentAccountOptions.add(new SelectOption(parentAcct.Parent_Account_vod__c, optionText ));  
        }  
        return parentAccountOptions;  
    }  
    
    //override std to prevent reloading within Page Section
    public PageReference save() {
        PageReference detailPage = sc.save();
        return null;
    }
    
}