/** Subject  : JJ_ANZ_UpdateCaseStatus 
jnj

Description  : Class to update Case status based on different Conditions
Author       : Mahesh Somineni || msominen@its.jnj.com||1/14/2016

 **/
public with sharing class JJ_ANZ_UpdateCaseStatus {



    //Static Variables
    Public static String CaseObject = 'Case';
    Public static String ParentCase = 'Case Information';
    Public static String PQCCase = 'Product Quality Complaints';
    Public static String AECase = 'Adverse Event';
    Public static String Checked = 'Yes';
    Public static String assessedCaseStatus = 'Assessed';
    Public static String MIRCase  ='Medical Enquiry';
    Public Static String GICase = 'General Inquiry';
    Public Static String ClosedMIRCase = 'Closed Medical Enquiry';
    Public Static String ClosedGICase = 'Closed General Inquiry';
    Public static Map<string, string> MapMappingTable=new map<string,string>();
    Public static String takePQCId;
    Public static String takeAEId;
    Public static String takePARId;



    // Method to update Case Information Status
    public static void UpdateCaseInformationStatus(List<Case> listCases)
    {

        for(Case updateCase : listCases)
        {
            updateCase.status =assessedCaseStatus;
        }
    }
   


    //Method to close or Reject primary Case based on Child Case status
    public static void ClosePrimaryCase(List<Case> UpdatedCases,Map<Id,Case> OldMapCases)
    {

        List<Case> PrimaryCases = new List<Case>();
        List<RecordType> rtypes = [SELECT Name, Id FROM RecordType WHERE sObjectType='Case' and isActive=true];
        Map<String,String> caseRecordTypes = new Map<String,String>{};
        List<Case> UpdateCases = new List<Case>();
        for(RecordType rt: rtypes)
        {

            caseRecordTypes.put(rt.Name,rt.Id);
        }  

        List<Id> primaryCaseIds = new List <Id>();
        for (Case caseRec : UpdatedCases)
        {
            Case oldCase = OldMapCases.get(caseRec.Id);
            if(caseRec.status=='Assigned'&&oldCase.status=='Closed'&&caseRec.RecordTypeId != JJ_ANZ_RecordTypesUtility.recordTypeInfo('Case').get('Case Information').getRecordTypeId())             
            {

                Case UpdateCase = new Case();
                UpdateCase.Id = caseRec.ParentId;
                UpdateCase.status = 'Assessed';
                UpdateCases.add(UpdateCase);
            }
        
        System.debug('Old case'+oldCase.IsClosed);
        System.debug('new case'+caseRec.IsClosed);
        System.debug('parent id'+caseRec.ParentId);


       if(caseRecordTypes.containsKey('Case Information')&&(oldCase.IsClosed == false)&& caseRec.ParentId==null)
        {

            Id PrimaryRecordTypeId = caseRecordTypes.get('Case Information');
            System.debug('Atul PRI'+PrimaryRecordTypeId);
            System.debug('Atul CUrrentPRI'+caseRec.RecordTypeId);
            if(caseRec.RecordTypeId!=PrimaryRecordTypeId)



            primaryCaseIds.add(caseRec.ParentId);

        } 








        if(oldCase.status!='Non-case'&&caseRec.status=='Non-case')
        {
            primaryCaseIds.add(caseRec.ParentId);
        }

    }








    List<Case> listPrimaryCases = [Select id,status,parentId,isClosed,JJ_ANZ_CreatePQC__c,JJ_ANZ_CreateAE__c from Case where parentId In:primaryCaseIds or Id In:primaryCaseIds limit 20000 ];
    System.debug('list of primary cases '+listPrimaryCases);




    Map<Id,List<Case>> mapPrimaryCases = new Map<Id,List<Case>>();



    for(Case c : listPrimaryCases)
    {
        if(mapPrimaryCases.containsKey(c.ParentId))
        {
            mapPrimaryCases.get(c.ParentId). add(c);



        }
        else
        {
            mapPrimaryCases.put(c.ParentId, new  List <Case> { c });
        }
    }

    List<Id> listPrimaryCasesToClose = new List<Id>();
    List<Id> listPrimaryCasesToReject = new List<Id>();
    List<Id> listPrimaryCasesToReOpen = new List<Id>();
    for(Case c :  UpdatedCases)
    {
        if(mapPrimaryCases.containsKey(c.ParentId))
        {
            Integer i = 0;
            Integer j = 0;






            List<Case> casesList = mapPrimaryCases.get(c.ParentId);
             System.debug('Mahesh: Child case size'+casesList.size());
            for(Case childcase : casesList)
            {    



                if(childcase.IsClosed)
                {
                   i++;
                }
                if(childcase.Status=='Non-case')
                {
                   j++;



                }



            }

if(casesList.size()==i)
            {
                listPrimaryCasesToClose.add(c.ParentId);
            }
            if(casesList.size()==j)
            {
                listPrimaryCasesToReject.add(c.ParentId);
            }

        }
    }

      if(!listPrimaryCasesToClose.isEmpty())
    {

        for(Id caseId: listPrimaryCasesToClose)
    {
               Case c = new Case();
          c.id =    caseId;  
        c.Status = 'Closed';
        UpdateCases.add(c);
    }
    }

    if(!listPrimaryCasesToReject.isEmpty())
    {
        for(Id caseRec : listPrimaryCasesToReject)
        {
         Case c = new Case();
         c.Id = caseRec;
        c.Status = 'Rejected';
        UpdateCases.add(c);
    }
    }
    if(!UpdateCases.isEmpty())
    {
        try
        {
            Update UpdateCases;    
        }
        catch(DMLException de)
        {

            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
            Database.insert(newErrorRecord,false);
        }
        catch(Exception e)
        {



            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
        }
        
    }
    }   






    // Method to update Seriousness in Parent Case from Child Case
    public static void SeriousnessCheck(List<Case> UpdatedCases,Map<Id,Case> OldMapCases)

    {

        List<Case> UpdateCases = new List<Case>();
        for (Case caseRec : UpdatedCases){

            Case oldCase = OldMapCases.get(caseRec.Id);
            Case UpdateCase = new Case();
        if(caseRec.ParentId !=null)
        {
        UpdateCase.Id = caseRec.ParentId;
        }
            if(caseRec.JJ_ANZ_Seriousness__c !=null)             
            {

                UpdateCase.JJ_ANZ_Serious_Criteria__c= caseRec.JJ_ANZ_Serious_Criteria__c;
                UpdateCase.JJ_ANZ_Admission_Date__c=caseRec.JJ_ANZ_Admission_Date__c;
                UpdateCase.JJ_ANZ_Discharge_Date__c=caseRec.JJ_ANZ_Discharge_Date__c;
                UpdateCase.JJ_ANZ_Choose_Item__c =caseRec.JJ_ANZ_Choose_Item__c;
                UpdateCase.JJ_ANZ_Is_Medical_Intervention_Required__c =caseRec.JJ_ANZ_Is_Medical_Intervention_Required__c;
                UpdateCase.JJ_ANZ_DSS_Ref_No__c=caseRec.JJ_ANZ_DSS_Ref_No__c;
                UpdateCase.JJ_ANZ_Report_Type__c=caseRec.JJ_ANZ_Report_Type__c;
                







            }
            if(caseRec.Id!=null)
            {
                takeAEId=caseRec.Id;
                UpdateCase.JJ_ANZ_AE_Case_ID__c=takeAEId.substring(0,15);
            }

            

            if(caseRec.JJ_ANZ_DSS_Ref_No__c !=null)
            {
                UpdateCase.JJ_ANZ_AE_DS_Ref_Number__c =true;
            }
            UpdateCases.add(UpdateCase);
        }

        if(!UpdateCases.isEmpty())


        {
            try
            {

                Update UpdateCases;    

            }
            catch(DMLException de)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }

        }

















    }







    public static void StampMIRCaseClosedBy(List<Case> listMIR)
    {



        List<Case> UpdateCases = new List<Case>();
        for(Case MIRlist: listMIR)
        {
            system.debug('Inside the MIR case closed loop');
            Case UpdateCase = new Case();
            if(null!=MIRList.ParentId)
            {
                UpdateCase.Id = MIRList.ParentId;
                UpdateCase.JJ_ANZ_MIR_Case_Closed_By_User__c=MIRList.JJ_ANZ_Current_User__c;
                UpdateCase.JJ_ANZ_MIR_Case_Closed_By_Team__c=MIRList.JJ_ANZ_Case_Closed_By__c;
            }
            UpdateCases.add(UpdateCase);





        }
        if(!UpdateCases.isEmpty())


        {
            try
            {

                Update UpdateCases;    

            }
            catch(DMLException de)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }

            catch(Exception e)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }










        }









    }

        
    
    
    public static void StampPQCWholeSaler(List<Case> listPQC)
    {

        List<Case> UpdateCases = new List<Case>();


        for(Case PQClist: listPQC)
        {
            Case UpdateCase = new Case();
            UpdateCase.Id = PQClist.ParentId;

            if(PQClist.id!=null)
            {
                takePQCId=PQClist.id;
                UpdateCase.JJ_ANZ_PQC_Case_ID__c=takePQCId.substring(0,15);
            }         

        

            if(PQClist.JJ_ANZ_Pharmacy__c !=null)
            {   
                UpdateCase.JJ_ANZ_Pharmacy__c=PQClist.JJ_ANZ_Pharmacy__c;
            }
            if(PQClist.JJ_ANZ_Other_Pharmacy_Name__c!=null){
                UpdateCase.JJ_ANZ_Other_Pharmacy_Name__c=PQClist.JJ_ANZ_Other_Pharmacy_Name__c;
                UpdateCase.JJ_ANZ_PhAlternate_Address__c=PQClist.JJ_ANZ_PhAlternate_Address__c;
            }

            if(PQClist.JJ_ANZ_Head_Pharmacy__c !=null){
                UpdateCase.JJ_ANZ_Head_Pharmacy__c=PQClist.JJ_ANZ_Head_Pharmacy__c;
            }
            if(PQClist.JJ_ANZ_PBS_Approval_Number__c !=null){
                UpdateCase.JJ_ANZ_PBS_Approval_Number__c=PQClist.JJ_ANZ_PBS_Approval_Number__c;
            }



            if(PQClist.JJ_ANZ_Wholesaler_Name__c !=null)
            {
                UpdateCase.JJ_ANZ_Wholesaler_Name__c=PQClist.JJ_ANZ_Wholesaler_Name__c;

            }
            if(PQClist.JJ_ANZ_PQC_Reference_Number__c !=null)
            {
                System.debug('inside pqc reference number');
                UpdateCase.JJ_ANZ_PQC_Ref_Number_Checkbox__c=true;
                UpdateCase.JJ_ANZ_PQC_Reference_Number__c=PQClist.JJ_ANZ_PQC_Reference_Number__c;
            }
            UpdateCases.add(UpdateCase);
        }
        if(!UpdateCases.isEmpty())


        {
            try
            {

                Update UpdateCases;    

            }
            catch(DMLException de)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }








        }









    }


    
    
    
   

   
    // this the method written for the korean functionality for merging child field to parent



    public static void childTOParent(List<Case> listMIR)
    {


        List<Case> UpdateCases = new List<Case>();
        System.debug('**listMIR ' +listMIR);
        for(Case childList : listMIR)














        {



            Case UpdateCase = new Case();
            UpdateCase.Id = childList.ParentId;


            if(childList.JJ_ANZ_Question_Detail__c !=null)
            {

                UpdateCase.JJ_ANZ_Question_Detail__c=childList.JJ_ANZ_Question_Detail__c;
            }
            UpdateCases.add(UpdateCase);











        }
        if(!UpdateCases.isEmpty())


        {
            try
            {

                Update UpdateCases;    

            }
            catch(DMLException de)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {

                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }






        }

















    }







    public static void CongaCheckBox(List<Case> listMIR)
    {


        List<Case> UpdateCases = new List<Case>();
        System.debug('**listMIR ' +listMIR);
        for(Case congaList : listMIR)
        {


            //  Case UpdateCase = new Case();
            //    UpdateCase.Id = congaList.ParentId;



            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Off Label Disclaimer'))
            {


                congaList.JJ_ANZ_Off_Label_Disclaimer_Check__c=true;
            }else
            {




                congaList.JJ_ANZ_Off_Label_Disclaimer_Check__c=false;
            }

            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Medical Information Summary'))
            {
                congaList.JJ_ANZ_Medical_Information_Summary_Check__c=true;
            }
            else
            {

                congaList.JJ_ANZ_Medical_Information_Summary_Check__c=false;
            }
            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Literature Search'))
            {
                congaList.JJ_ANZ_Published_Information__c=true;
            }
            else
            {
                congaList.JJ_ANZ_Published_Information__c=false;
            }
            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Global Summary of data'))
            {
                congaList.JJ_ANZ_Global_MIR__c=true;
            }
            else
            {
                congaList.JJ_ANZ_Global_MIR__c=false;
            }
            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Possibility of an AE'))
            {
                congaList.JJ_ANZ_Possibility_of_an_AE__c=true;
            }
            else
            {
                congaList.JJ_ANZ_Possibility_of_an_AE__c=false;
            }
            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Drug Interaction Inquiries'))
            {
                congaList.JJ_ANZ_Drug_Interaction_Inquiries_Check__c=true;
            }
            else
            {
                congaList.JJ_ANZ_Drug_Interaction_Inquiries_Check__c=false;
            }
            if(congaList.JJ_ANZ_Consumer_Cover_Letter__c !=null && congaList.JJ_ANZ_Consumer_Cover_Letter__c.contains('Unapproved Medical Inquiries'))
            {
                congaList.JJ_ANZ_Unapproved_Medical_Inquiries__c=true;
            }
            else
            {
                congaList.JJ_ANZ_Unapproved_Medical_Inquiries__c=false;
            }

        }
        /* if(!UpdateCases.isEmpty())
    {




































































































        try
        {

         //  Update UpdateCases;    



        }
        catch(DMLException de)
        {
            System.debug('On Line 299');
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
            Database.insert(newErrorRecord,false);
        }
        catch(Exception e)
        {
            System.debug('On Line 306');
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
            Database.insert(newErrorRecord,false);
        }



    }   */                 
    }



    Public Static void CloseParentCases(List<Case> ParentCases)
    {


        List<Id> ParentIds = new List<Id>();
        List<Case> CloseCases = new List<Case>();
        for(Case caseRec : ParentCases)
        {

            if(caseRec.JJ_ANZ_CreateAE__c == 'No' && caseRec.JJ_ANZ_CreatePQC__c=='No'&&caseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo('Case').get('Case Information').getRecordTypeId())   
            {

                ParentIds.add(caseRec.Id);
            }    





        }
        List<Case> ChildCases = new List<Case>();
        Map<Id,List<Case>> mapPrimaryCases = new Map<Id,List<Case>>();
        if(!ParentIds.isEmpty())
        {
            ChildCases = [Select id,RecordTypeId,Status,ParentId from Case where ParentId IN: ParentIds and Status='Closed'];    
        }
        if(!ChildCases.isEmpty())
        {
            for(Case c : ChildCases )
            {


                if(mapPrimaryCases.containsKey(c.ParentId))
                {


                    mapPrimaryCases.get(c.ParentId). add(c);

                }
                else
                {
                    mapPrimaryCases.put(c.ParentId, new  List <Case> { c });
                }
            }


        }















        for(Case caseRec : ParentCases)
        {
            List<Case> CaseList = new List<Case>();



            if(mapPrimaryCases.Containskey(caseRec.Id))






            {
                CaseList = mapPrimaryCases.get(caseRec.Id);
                if(caseRec.JJ_ANZ_CreateGI__c == 'Yes' &&  caseRec.JJ_ANZ_CreateMIR__c=='Yes'&&CaseList.size()==2)
                {
                    caseRec.Status = 'Closed';
                    CloseCases.add(caseRec);
                }
                else if(caseRec.JJ_ANZ_CreateMIR__c=='Yes' && caseRec.JJ_ANZ_CreateGI__c == 'No' &&CaseList.size()==1)
                {
                    caseRec.Status = 'Closed';
                    CloseCases.add(caseRec);
                }
                else if(caseRec.JJ_ANZ_CreateGI__c=='Yes'&& caseRec.JJ_ANZ_CreateMIR__c=='No' && CaseList.size()==1)
                {
                    caseRec.Status = 'Closed';
                    CloseCases.add(caseRec);
                }
            }





















        }




        if(!CloseCases.isEmpty())
        {
            try{
                // Update CloseCases;    
            }
            catch(DMLException de)
            {
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }



        }
    }



    Public Static void CloseFrontDeskCase(List<Case> ChildCases)
    {

        List<Id> ParentIds = new List<Id>();
        List<Case> CaseRecords  = new List<Case>();
        List<Case> ClosedChildCases = new List<case>();
        List<case> ParentCases = new List<Case>();
        List<Case> UpdateParentCases = new List<Case>();
        for(Case caseRec : ChildCases)
        {
            ParentIds.add(caseRec.ParentId);
        }
        System.debug('Front Desk Cases in Handler 333'+ParentIds);
        if(!ParentIds.isEmpty())
        {
            CaseRecords = [Select id,RecordTypeId,JJ_ANZ_Open_Case_for_Front_Desk__c,ParentId,JJ_ANZ_CreateGI__c,JJ_ANZ_CreateMIR__c from Case where (ParentId IN :ParentIds and Status='Closed' and (RecordType.Name = :MIRCase or RecordType.Name=:ClosedMIRCase or RecordType.Name = :GICase or RecordType.Name=:ClosedGICase)) or Id IN:ParentIds];
        }
        System.debug('Front Desk Cases in Handler 337'+CaseRecords);
        if(!caseRecords.isEmpty())
        {
            for(Case caseRec : CaseRecords)
            {
                if(CaseRec.RecordTypeId == JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(ParentCase).getRecordTypeId())
                {
                    ParentCases.add(CaseRec);
                }
                else
                {
                    ClosedChildCases.add(CaseRec);
                }



            }
        }
        System.debug('Front Desk Cases in Handler 353'+ParentCases);
        System.debug('Front Desk Cases in Handler 354'+ClosedChildCases);
        Map<Id,List<Case>> mapPrimaryCases = new Map<Id,List<Case>>();        
        for(Case c : ClosedChildCases )
        {
            if(mapPrimaryCases.containsKey(c.ParentId))
            {


                mapPrimaryCases.get(c.ParentId). add(c);

            }
            else
            {




                mapPrimaryCases.put(c.ParentId, new  List <Case> { c });
            }


        }





        System.debug('Front Desk Cases in Handler 369'+mapPrimaryCases);
        for(Case caseRec : ParentCases)








        {
            List<Case> CaseList = new List<Case>();

            if(mapPrimaryCases.Containskey(caseRec.Id))
            {
                CaseList = mapPrimaryCases.get(caseRec.Id);
                System.debug('Front Desk Cases in Handler 376'+CaseList.size());
                if(CaseList.Size()==2 && caseRec.JJ_ANZ_CreateGI__c==Checked&&caseRec.JJ_ANZ_CreateMIR__c==Checked)
                {


                    System.debug('hi');
                    caseRec.JJ_ANZ_Open_Case_for_Front_Desk__c  = false; 
                }


                else if(CaseList.size()==1&&(caseRec.JJ_ANZ_CreateGI__c!=Checked||caseRec.JJ_ANZ_CreateMIR__c!=Checked))
                {


                    System.debug('hello'); 
                    caseRec.JJ_ANZ_Open_Case_for_Front_Desk__c  = false;           
                }


                System.debug(' Case Rec '+CaseRec);
                System.debug(' Case Rec UpdateParentCases'+UpdateParentCases);
                UpdateParentCases.add(CaseRec);
            }
        } 




        System.debug(' Case Rec UpdateParentCases'+UpdateParentCases); 
        if(!UpdateParentCases.isEmpty())












        {
            try
            {
                Update UpdateParentCases;
            }
            catch(DMLException de)
            {
                System.debug('On Line 396');
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {
                System.debug('On Line 404');
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            }
        }


















    }  



    Public static void ParentToChildCaseSync(List<Case> ParentCases)
    {

        List<Id> ParentIds = new List<Id>();
        List<Case> CloseCases = new List<Case>();
        List<Case> UpdateChildCases = new List<Case>();
        for(Case caseRec : ParentCases)
        {
            ParentIds.add(caseRec.Id);



        }
        MapMappingTable = getAllMapping();
        List<Case> ChildCases = new List<Case>();
        Map<Id,List<Case>> mapPrimaryCases = new Map<Id,List<Case>>();
        System.debug('Mahesh ParentIds'+ParentIds);
        if(!ParentIds.isEmpty())
        {
            ChildCases = [Select id,RecordTypeId,Status,ParentId from Case where ParentId IN: ParentIds];    
        }
        System.debug('Mahesh ChildCases'+ChildCases);
        if(!ChildCases.isEmpty())
        {
            for(Case c : ChildCases )
            {


                if(mapPrimaryCases.containsKey(c.ParentId))
                {


                    mapPrimaryCases.get(c.ParentId). add(c);

                }
                else
                {
                    mapPrimaryCases.put(c.ParentId, new  List <Case> { c });
                }
            }


        }















        for(Case caseRec : ParentCases)
        {
            List<Case> CaseList = new List<Case>();



            if(mapPrimaryCases.Containskey(caseRec.Id))
            {


                CaseList = mapPrimaryCases.get(caseRec.Id);
                for(Case childCaseRec:CaseList)
                {


                    Case ChildCase =  new Case();
                    ChildCase.Id = childCaseRec.Id;
                    ChildCase.RecordTypeId= childcaseRec.RecordTypeId;
                    if(childcaseRec.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(AECase).getRecordTypeId())
                    {


                        ChildCase.JJ_ANZ_AE_Initial_Review_turn_around__c = caseRec.JJ_ANZ_AE_Initial_Review_turn_around__c;
                        ChildCase.JJ_ANZ_Initial_review_date__c = caseRec.JJ_ANZ_Initial_review_date__c;


                    }






                    else if(childcaseRec.RecordTypeId==JJ_ANZ_RecordTypesUtility.recordTypeInfo(CaseObject).get(PQCCase).getRecordTypeId())
                    {


                        ChildCase.JJ_ANZ_PQCInitialReviewturnaround__c = caseRec.JJ_ANZ_PQCInitialReviewturnaround__c;
                        ChildCase.JJ_ANZ_PQCInitialReviewDate__c = caseRec.JJ_ANZ_PQCInitialReviewDate__c;
                    }


                    if(caseRec.JJ_ANZ_Follow_up_Consent__c == 'Consent Declined')
                    {


                        ChildCase.JJ_ANZ_Follow_Up_Date_1__c= null;
                        ChildCase.JJ_ANZ_Follow_Up_Date_2__c =null;
                        ChildCase.JJ_ANZ_Follow_Up_Completion_Date_1__c = null;
                        ChildCase.JJ_ANZ_Follow_Up_Completion_Date_2__c = null;
                        ChildCase.Status = 'Assigned';
                    }


                    for(String newCaseField: MapMappingTable.keySet())
                    {
                        String  caseField = MapMappingTable.get(newCaseField);
                        ChildCase.put(newCaseField, caseRec.get(caseField));
                    }
                    System.debug('mahesh '+ChildCase);
                    UpdateChildCases.add(ChildCase);
                }
            }


        }


        System.debug('Mahesh UpdateChildCases'+UpdateChildCases);
        if(!UpdateChildCases.isEmpty())


















        {
            try
            {
                // Database.Update(UpdateChildCases,false);
                System.debug('Mahesh Update Call before'+UpdateChildCases);
                Update UpdateChildCases;
                System.debug('Mahesh Update Call after'+UpdateChildCases);
            }
            catch(DMLException de)
            {
                System.debug('On Line 396');


                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }
            catch(Exception e)
            {
                System.debug('On Line 404');
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            } 
        }





















    }



    //Assess Case Information if Child Case is Assigned
    Public static void AssessPrimaryCase(List<Id> PrimaryCaseIds)
    {

        List<Case> updateCases = new List<Case>();       
        if(!PrimaryCaseIds.isEmpty())
        {
            for(Id caseRecId : PrimaryCaseIds)
            {
                Case updateCase =  new Case();
                updateCase.Id = caseRecId;
                updateCase.Status = 'Assessed';
                updateCases.add(updateCase);
            }
        }
        if(!updateCases.isEmpty())
        {
            try{
                Database.update(updateCases);
            }
            catch(DMLException de)
            {


                System.debug('On Line 396');
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = de.getTypeName();
                Database.insert(newErrorRecord,false);
            }


            catch(Exception e)
            {


                System.debug('On Line 404');
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c = e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c = e.getTypeName();
                Database.insert(newErrorRecord,false);
            } 


        }
    }
    // Get Map of Field to populate Parent Values to Child Cases
    public static Map<string,string> getAllMapping()
    {




        try{
            for (CasetoChaildCaseFieldMapping__c mappingTableRec : CasetoChaildCaseFieldMapping__c.getall().Values())
            {
                if (mappingTableRec.Name != null  )
                {
                    MapMappingTable.put(mappingTableRec.Chaild_Case_Field_API_Name_del__c,mappingTableRec.Name);



                }
            }
        }
        catch(exception ex)
        {
            // Log an Exception
            JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
            newErrorRecord.JJ_ANZ_Error_Description__c = ex.getMessage();
            newErrorRecord.JJ_ANZ_Exception_Type__c = ex.getTypeName();
            Database.insert(newErrorRecord,false);
        }
        return MapMappingTable;
    }
}