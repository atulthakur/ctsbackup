global class JJ_ANZ_DeleteActivityHistory implements schedulable
{
    global void execute(SchedulableContext sc)
    {
    List<Task> TaskActivityList= new List<Task>([SELECT Id FROM Task WHERE Account.Name ='Janssen Medical']);
      
   for(Task TaskList : TaskActivityList) 
   {
   if(TaskList!=null)
   {
         delete TaskList;
   }
   }
    }
}