/**
* Builds the data needed Matrix diagram in the Stakeholders Matrix VF page. 
* JS data expected as a JS array.
* The VF page is used in Account Plans - in Page Layout Section. 
* Google Charts is the JS library used to present the Matrix diagram
*
* @author  Noel Lim
* @changehistory   
*  2016-03-30 initial, v1.0 
*/
public without sharing class JJ_ANZ_StakeholderMatrixController {

    public final Account_Plan_vod__c accountPlan {get; set;}
    public boolean hasStakeholders{ get; set; }
    public List<List<String>> listChartDataTable {get; set;}
    Id acctPlanId;
    
    private final String nullInfluenceValue = '1';
    private final String nullInterestValue = '1';
    

    public JJ_ANZ_StakeholderMatrixController(ApexPages.StandardController stdController) {
        acctPlanId = stdController.getId();
        accountPlan = [Select Id, Name, Account_vod__c, Account_vod__r.Name From Account_Plan_vod__c Where Id = :acctPlanId]; 
        hasStakeholders = false;

        
        listChartDataTable = buildAccountPlanData(this.accountPlan);
    }
    
    private List<List<String>> buildAccountPlanData(Account_Plan_vod__c accountPlan){
        
        List<List<String>> jaggedListTable = new List<List<String>>();
        jaggedListTable.add(new List<String>{'\'ID\'', '\'Interest\'', '\'Influence\'', '\'Affiliations\'','\'Calls\'', '\'AccountId\''}); //must escape strings in single quotes for Javacript
        
        //Query and build Data maps/lists
        Map<Id, Integer> mapAcctAfflCount = new Map<Id, Integer>();
               
        List<Key_Stakeholder_vod__c> keyStakeholders = [SELECT Id, Key_Stakeholder_vod__c, Key_Stakeholder_vod__r.Name, JJ_ANZ_Interest__c, JJ_ANZ_Influence__c, JJ_ANZ_YTD_Calls__c 
                                        FROM Key_Stakeholder_vod__c WHERE Account_Plan_vod__c =:acctPlanId AND Key_Stakeholder_vod__c <> NULL];
        
        if(keyStakeholders.size() > 0)
            hasStakeholders = true;
        
        List<Id> stkAcctIds = new List<Id>();                                
        for(Key_Stakeholder_vod__c tempKS : keyStakeholders){
            stkAcctIds.add(tempKS.Key_Stakeholder_vod__c);
        }
                                
        List<AggregateResult> aggResults = [Select From_Account_vod__c, COUNT(Id) afflCount FROM Affiliation_vod__c WHERE From_Account_vod__c IN :stkAcctIds GROUP BY From_Account_vod__c];
        for(AggregateResult ar : aggResults){
            mapAcctAfflCount.put((Id)ar.get('From_Account_vod__c'), (Integer)ar.get('afflCount'));
        }
        for( Id tempAcctId : stkAcctIds){
            if(!mapAcctAfflCount.containsKey(tempAcctId)){
              mapAcctAfflCount.put(tempAcctId,0);
            }
        }
                
        
        //Build 2-level List String
        String stakeholderName;
        for(Key_Stakeholder_vod__c tempKS : keyStakeholders){
            stakeholderName = tempKS.Key_Stakeholder_vod__r.Name.replace('\'', '');
            
            List<String> tempLstString = new List<String>();
            tempLstString.add('\'' + stakeholderName + '\'');
            tempLstString.add(tempKS.JJ_ANZ_Interest__c != NULL ? tempKS.JJ_ANZ_Interest__c : nullInterestValue);
            tempLstString.add(tempKS.JJ_ANZ_Influence__c != NULL ? tempKS.JJ_ANZ_Influence__c : nullInfluenceValue);
            String strAfflCount = String.valueOf(mapAcctAfflCount.get(tempKS.Key_Stakeholder_vod__c));
            tempLstString.add(strAfflCount);
            tempLstString.add(String.valueOf(tempKS.JJ_ANZ_YTD_Calls__c)); 
            tempLstString.add('\'' + tempKS.Key_Stakeholder_vod__c + '\'');          
            
            jaggedListTable.add(tempLstString);
        }
        
        return jaggedListTable;
    }
   
}