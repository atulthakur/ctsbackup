/** Subject  : JJ_ANZ_CaseProductTriggerHandler

Description  : Class to handle all the calls from Case Product Trigger
Author       : Atul Thakur || athaku11@its.jnj.com||15 May 2016

**/

public with sharing class JJ_ANZ_CaseProductTriggerHandler {

    Public static String CaseObject = 'Case';
    Public static String ParentCase = 'Case Information';
    Public static String PQCCase = 'Product Quality Complaints';
    Public static String AECase = 'Adverse Event';
    Public static String MIRCase = 'Medical Enquiry';

    public void OnBeforeInsert(List<JJ_Case_Product__c> newCaseProduct)
    {
        // EXECUTE BEFORE INSERT LOGIC
        if(JJ_ANZ_RecursiveTriggers.FirstRunonBeforeInsertCaseProduct)
        {
            JJ_ANZ_RecursiveTriggers.FirstRunonBeforeInsertCaseProduct=false;
            List<JJ_Case_Product__c> OnBeforeInsertList = new List<JJ_Case_Product__c>();

            for(JJ_Case_Product__c caseProduct : newCaseProduct)
            {
                if(caseProduct.JJ_ANZ_Sup_Case__c!=null || caseProduct.JJ_ANZ_Detail_Product__c !=null || caseProduct.JJ_ANZ_Other_Product__c !=null)
                {
                    OnBeforeInsertList.add(caseProduct);
                }
            }
            if(OnBeforeInsertList !=null && !OnBeforeInsertList.isEmpty())
            {
                JJ_CaseProduct_Updates.removeCountry(OnBeforeInsertList);     //this method is call to remove the country codes from the Product
                JJ_CaseProduct_Updates.stampChildCaseID(OnBeforeInsertList);  //this method is call to stamp child case Id's to Case product Object
            }


        }
    }

    public void OnAfterInsert(List<JJ_Case_Product__c> newCaseProduct)
    {   
        if(JJ_ANZ_RecursiveTriggers.FirstRunonAfterInsertCaseProduct)
        {
            JJ_ANZ_RecursiveTriggers.FirstRunonAfterInsertCaseProduct=false;
            List<JJ_Case_Product__c> OnAfterInsertList = new List<JJ_Case_Product__c>();
            
            for(JJ_Case_Product__c  cp : newCaseProduct)
            {
                if(cp.JJ_ANZ_Detail_Product__c!=null || cp.JJ_ANZ_Other_Product__c !=null)
                {   
                    OnAfterInsertList.add(cp);
                }   
            }
            
            if(OnAfterInsertList !=null && !OnAfterInsertList.isEmpty())
            {
                JJ_CaseProduct_Updates.stampingDataToParentCase(OnAfterInsertList); //this method is call to fetch data from Product Records and stamp to case 
            }


        }
    }
    public void OnBeforeUpdate(List<JJ_Case_Product__c> oldCaseProduct,List<JJ_Case_Product__c> updateCaseProduct,Map<id,JJ_Case_Product__c> oldMapCaseProduct)
    {
        List<JJ_Case_Product__c> onBeforeUpdateList= new List<JJ_Case_Product__c>();
        if(JJ_ANZ_RecursiveTriggers.FirstRunonBeforeUpdateCaseProduct)
        {
            JJ_ANZ_RecursiveTriggers.FirstRunonBeforeUpdateCaseProduct= false;
            for(JJ_Case_Product__c caseProduct : updateCaseProduct)
            {

                if(caseProduct.JJ_ANZ_Sup_Case__c!=null || caseProduct.JJ_ANZ_Detail_Product__c !=null || caseProduct.JJ_ANZ_Other_Product__c !=null)
                {
                    onBeforeUpdateList.add(caseProduct);
                }

            }
            if(onBeforeUpdateList!=null && !onBeforeUpdateList.isEmpty())
            {
                JJ_CaseProduct_Updates.removeCountry(onBeforeUpdateList); //this method is call to remove the country codes from the Product
            } 

        }




    }


    public void OnBeforeDelete(List<JJ_Case_Product__c> oldCaseProduct,Map<id,JJ_Case_Product__c> oldMapCaseProduct)
    {
        if(JJ_ANZ_RecursiveTriggers.FirstRunonBeforeDeleteCaseProduct)
        {
            JJ_ANZ_RecursiveTriggers.FirstRunonBeforeDeleteCaseProduct=false;
            List<JJ_Case_Product__c> onBeforeDeleteList= new List<JJ_Case_Product__c>();

            for(JJ_Case_Product__c  cp : oldCaseProduct)
            {
                if(cp.JJ_ANZ_Sup_Case__c !=null)
                {
                    onBeforeDeleteList.add(cp);
                }                          
            }
            if(onBeforeDeleteList!=null && !onBeforeDeleteList.isEmpty())
            {
                JJ_CaseProduct_Updates.removeProductFromCase(onBeforeDeleteList); //this method is call to remove the product from the Case Object
            } 

        }


    }
}