/**
* Builds the data to populate the Network diagram in the Stakeholders Network VF page. 
* JS data is expected to be JSON.
* The VF page is used in Account Plans - in Page Layout Section. * vis.js is the JS library used to present the Network diagram
* 
* Inner classes are used to define Nodes and Edges, flexible and easily converted to JSON 
* Some of the Node/Edge's attributes from vis.js are Apex reserved keywords, so they are manually 'text replaced' after conversion to JSON.
*
*
* @author  Noel Lim
* @changehistory   
*  2016-03-30 initial, v1.0 
*
*/
 
public without sharing class JJ_ANZ_StakeholderNetworkController {

    public final Account_Plan_vod__c accountPlan {get; set;}
    Id acctPlanId;      
    public String jsonNodes {get; set;}
    public String jsonEdges {get; set;}
    public boolean hasStakeholders{ get; set; }
    
    private boolean acctPlanTargetIsPerson = false;   
    private Id acctPlanTargetId;
    private List<Key_Stakeholder_vod__c> keyStakeholders;
    private Set<Id> stkAcctIds = new Set<Id>();
    private Set<Id> affliatesAcctIds = new Set<Id>();
    private List<Affiliation_vod__c> afflFromStakeholder;
    private List<Affiliation_vod__c> afflBetweenAffiliates = new List<Affiliation_vod__c>();

    public JJ_ANZ_StakeholderNetworkController(ApexPages.StandardController stdController) {
        acctPlanId = stdController.getId();
        accountPlan = [Select Id, Name, Account_vod__c, Account_vod__r.Name, Account_vod__r.IsPersonAccount From Account_Plan_vod__c Where Id = :acctPlanId]; 
        hasStakeholders = false;
        acctPlanTargetIsPerson = accountPlan.Account_vod__r.IsPersonAccount;
        
        //set up Lists from Stakeholders              
        this.keyStakeholders = [SELECT Id, Key_Stakeholder_vod__c, Key_Stakeholder_vod__r.Name, Key_Stakeholder_vod__r.YTD_Calls__c FROM Key_Stakeholder_vod__c WHERE Account_Plan_vod__c =:acctPlanId AND Key_Stakeholder_vod__c <> NULL];
        
        if(keyStakeholders.size() > 0){
            
            hasStakeholders = true;
            

            for(Integer j = 0; j < this.keyStakeholders.size(); j++){
                Id tempAcctId = this.keyStakeholders.get(j).Key_Stakeholder_vod__c;
                if(!stkAcctIds.contains(tempAcctId)){
                    stkAcctIds.add(tempAcctId);
                }
                else{
                    this.keyStakeholders.remove(j);
                }
            }    
                    
            this.afflFromStakeholder = [SELECT From_Account_vod__c, To_Account_vod__c, To_Account_vod__r.Name, To_Account_vod__r.IsPersonAccount, To_Account_vod__r.YTD_Calls__c, Influence_vod__c, Comments_vod__c, Relationship_Strength_vod__c
                                         FROM Affiliation_vod__c WHERE From_Account_vod__c IN :this.stkAcctIds];
            
            
            //build Network data tables
            List<NetworkNode> listNodesDataTable = buildNetworkNodes(this.accountPlan);
            List<NetworkEdge> listRelationshipsDataTable = buildNetworkRelationships(this.accountPlan);
            
            //serailise List of objects to JSON
            jsonNodes = json.serialize(listNodesDataTable);
            jsonEdges = json.serialize(listRelationshipsDataTable);
            
            //replace APEX reserved into the vis.js correct optionName
            jsonNodes = jsonNodes.replace('groupStyle', 'group');            
            jsonEdges = jsonEdges.replace('fromNode', 'from');
            jsonEdges = jsonEdges.replace('edgeValue', 'value');
            
            jsonNodes = jsonNodes.replace('null', 'undefined'); 
            jsonEdges = jsonEdges.replace('null', 'undefined'); 
        }
    }
    
    private List<NetworkNode> buildNetworkNodes(Account_Plan_vod__c accountPlan){
        List<NetworkNode> nodeListTable = new List<NetworkNode>();
        Set<Id> nodesAddedIds = new Set<Id>();  //ensures no double Nodes of the same Account
        
        //manually add Account Plan and Main Account        
        NetworkNode accountPlanNN = new NetworkNode(accountPlan.Id, accountPlan.Name, 'account_plan');
        nodeListTable.add(accountPlanNN);
        nodesAddedIds.add(accountPlan.Id);        
        if(!acctPlanTargetIsPerson){
            NetworkNode mainAccountNN = new NetworkNode(accountPlan.Account_vod__r.Id, accountPlan.Account_vod__r.Name, 'main_account'); 
            nodeListTable.add(mainAccountNN);
            nodesAddedIds.add(accountPlan.Account_vod__r.Id);
        }
        
        
        //Stakeholders
        for(Key_Stakeholder_vod__c tempKS : this.keyStakeholders){
            NetworkNode tempNN = new NetworkNode(tempKS.Key_Stakeholder_vod__r.Id, 
                tempKS.Key_Stakeholder_vod__r.Name + ' ('+ tempKS.Key_Stakeholder_vod__r.YTD_Calls__c +')', 'stakeholder');           
            nodeListTable.add(tempNN);
            nodesAddedIds.add(tempKS.Key_Stakeholder_vod__r.Id);
        }
        
        //Affiliates
        for(Affiliation_vod__c affl : this.afflFromStakeholder){
            if(!nodesAddedIds.contains(affl.To_Account_vod__c) )
            {
                nodesAddedIds.add(affl.To_Account_vod__c);
                affliatesAcctIds.add(affl.To_Account_vod__c);
                
                String nodeType = affl.To_Account_vod__r.IsPersonAccount?'affl_hcp':'affl_hco';
                NetworkNode tempNN = new NetworkNode(affl.To_Account_vod__c, 
                    affl.To_Account_vod__r.Name + ' ('+ affl.To_Account_vod__r.YTD_Calls__c +')', nodeType);           
                nodeListTable.add(tempNN);
                
            }
        }
        
        /*
        Retrieve YTD calls
        for(Account tempAcct : [SELECT Id, YTD_Calls__c FROM Account WHERE Id IN :nodesAddedIds]){
            for(NetworkNode tempNode : nodeListTable){
                if(tempNode.Id == tempAcct.Id){
                    tempNode.label = tempNode.label + ' ('+ tempAcct.YTD_Calls__c +')';
                }
                    
            }
        }
        */
        
        
        return nodeListTable;
        
    }
    
    private List<NetworkEdge> buildNetworkRelationships(Account_Plan_vod__c accountPlan){
        List<NetworkEdge> edgeListTable = new List<NetworkEdge>();
        Set<String> edgeListIds = new Set<String>();
        
        //get list of Affiliations between Affiliates
        this.afflBetweenAffiliates = [Select From_Account_vod__c, To_Account_vod__c, To_Account_vod__r.Name, To_Account_vod__r.IsPersonAccount, Influence_vod__c, Comments_vod__c, Relationship_Strength_vod__c 
                                      FROM Affiliation_vod__c 
                                      WHERE (From_Account_vod__c IN :this.affliatesAcctIds AND To_Account_vod__c IN :this.affliatesAcctIds)];
        
        //manually add relationships between the Account Plan, Main Account, and Stakeholders
        if(!acctPlanTargetIsPerson){
            NetworkEdge acctPlan2MainAcct = new NetworkEdge(accountPlan.Id, accountPlan.Account_vod__r.Id);
            edgeListTable.add(acctPlan2MainAcct);     
        }   
                
        for(Key_Stakeholder_vod__c tempKS : this.keyStakeholders){
            NetworkEdge tempNE = new NetworkEdge(accountPlan.Id, tempKS.Key_Stakeholder_vod__r.Id);           
            edgeListTable.add(tempNE);
        }
        
        
        for(Affiliation_vod__c affl : this.afflFromStakeholder){
            if(!edgeListIds.contains(affl.To_Account_vod__c + '_' + affl.From_Account_vod__c)){
                String afflDirection;
                if(affl.Influence_vod__c == 'Has influence')
                    afflDirection = 'to';
                else if(affl.Influence_vod__c == 'Is influenced')
                    afflDirection = 'from';
                else
                    afflDirection = '';
                
                //Add both combinations of From and To Ids
                edgeListIds.add(affl.From_Account_vod__c + '_' + affl.To_Account_vod__c);
                edgeListIds.add(affl.To_Account_vod__c + '_' + affl.From_Account_vod__c);
                
                NetworkEdge tempNE = new NetworkEdge(affl.From_Account_vod__c, affl.To_Account_vod__c, afflDirection, affl.Comments_vod__c, affl.Relationship_Strength_vod__c);           
                edgeListTable.add(tempNE);
            }
        }
        
        for(Affiliation_vod__c affl : this.afflBetweenAffiliates){
            if(!edgeListIds.contains(affl.To_Account_vod__c + '_' + affl.From_Account_vod__c)){
                String afflDirection;
                if(affl.Influence_vod__c == 'Has influence')
                    afflDirection = 'to';
                else if(affl.Influence_vod__c == 'Is influenced')
                    afflDirection = 'from';
                else
                    afflDirection = '';
                
                //Add both combinations of From and To Ids
                edgeListIds.add(affl.From_Account_vod__c + '_' + affl.To_Account_vod__c);
                edgeListIds.add(affl.To_Account_vod__c + '_' + affl.From_Account_vod__c);
                
                NetworkEdge tempNE = new NetworkEdge(affl.From_Account_vod__c, affl.To_Account_vod__c, afflDirection, affl.Comments_vod__c, affl.Relationship_Strength_vod__c);           
                edgeListTable.add(tempNE);
            }
        }
         
         
        return edgeListTable;
    }   
    
    private class NetworkNode{  
        
        
        private String id {get; set;}
        private String label {get; set;}
        private String groupStyle {get; set;} //should be group, an APEX reserved keyword. Post processing at VF JS side is the workaround
        
        public NetworkNode(){
        
        }
        
        public NetworkNode(String id, String label, String groupStyle){
            this.id = id;
            this.label = label;
            this.groupStyle = groupStyle;
        }
    }
    
    private class NetworkEdge{  
        
        private final String stakeholderDefaultStrength = '0';
        
        private String fromNode {get; set;} //should be from - but is a reserved keyword. Post processing for workaround
        private String to {get; set;}
        private String arrows {get; set;}
        private String label {get; set;} 
        private boolean dashes {get; set;}
        private String title {get; 
          set{
            title = value;
            if(!String.isBlank(value)){
              label = '[note]';
            }
          }
        }
        private String edgeValue {get; 
          set{ 
            if(value == '0'){
               edgeValue = '1';
            }
            else if(value == '2'){
               edgeValue = '5';
            }
            else{
                edgeValue = '3';
            } 
          }
        }
    
        public NetworkEdge(){
        
        }
        
        //Set All Members
        public NetworkEdge(String fromNode, String to, String arrows, String title, String width){
            this.fromNode = fromNode;
            this.to = to;
            this.arrows = arrows;
            this.title = title;
            this.edgeValue = width;
            this.dashes = false;
        }
        
        //Plan-Stakeholders. No Title, Default relationship Strength, Dashes
        public NetworkEdge(String fromNode, String to){
            this.fromNode = fromNode;
            this.to = to;
            this.arrows = '';
            this.title = null;
            this.edgeValue = stakeholderDefaultStrength;
            this.dashes = false;
        }

    }
}