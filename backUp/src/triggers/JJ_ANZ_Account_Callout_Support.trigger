/**
*   Modified by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 14.05.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description:  When specific fields are changed on Account, the territory alignment batch gets scheduled 
*				  and jj_anz_callout_trigger__c checkbox is set to true.
**/

trigger JJ_ANZ_Account_Callout_Support on Account (before insert, before update) {
    
    //ensuring this does not run if kicked off from a batch process
    system.debug(JJ_ANZ_Process_Futures_Util.getnofutureprocess());
    
    If(!JJ_ANZ_Process_Futures_Util.getnofutureprocess()){
        boolean scheduleBatch = false;
        
        if(trigger.isUpdate){
            
            for(Account acct : Trigger.new){
                //comparing the fields that are relevant to territory alignment 
                //to see if there was an update change. territory_vod__c should be included
                //the rest are customer specific territory filters
                string proftype = trigger.oldmap.get(acct.id).jj_core_professional_type__c;
                string spec1 = trigger.oldmap.get(acct.id).specialty_1_vod__c;
                string spec2 = trigger.oldmap.get(acct.id).specialty_2_vod__c;
                string terrrule = trigger.oldmap.get(acct.id).jj_anz_janssen_territory_rule__c;
                string bizacctrule = trigger.oldmap.get(acct.id).jj_anz_teams__c;
                string terrvod = trigger.oldmap.get(acct.id).territory_vod__c;
                boolean persontype = trigger.oldmap.get(acct.id).JJ_ANZ_Person_Account__c;
                string coreposition = trigger.oldmap.get(acct.id).JJ_Core_Position__c;
                string acctname = trigger.oldmap.get(acct.id).Name;
                boolean inactive = trigger.oldmap.get(acct.id).JJ_ANZ_Inactive__c;
                string rectype = trigger.oldmap.get(acct.id).RecordTypeId;
                 
                If(acct.jj_core_professional_type__c != proftype || acct.specialty_1_vod__c != spec1 || acct.specialty_2_vod__c != spec2 ||
                   acct.jj_anz_janssen_territory_rule__c != terrrule || acct.jj_anz_teams__c != bizacctrule || acct.territory_vod__c != terrvod || 
                   acct.JJ_ANZ_Person_Account__c != persontype || acct.JJ_Core_Position__c != coreposition || acct.Name != acctname || 
                   acct.JJ_ANZ_Inactive__c != inactive || acct.RecordTypeId != rectype ){
    
                    acct.JJ_ANZ_Callout_Trigger__c = true;
                    scheduleBatch = true;
                }
            }
        }else{
            for(Account acct : Trigger.new){
                acct.JJ_ANZ_Callout_Trigger__c = true;
                scheduleBatch = true;
            }
        }
        
        if(scheduleBatch == true){
            
            List<Account_Territory_Alignment_Utilities__c> atlSetting = [select Id, Batch_Scheduled__c, Error__c From Account_Territory_Alignment_Utilities__c limit 1];
            
            if(atlSetting.size() > 0){
                if(atlSetting[0].Batch_Scheduled__c == false){
                    Datetime sysTime = System.now();
                    sysTime = sysTime.addHours(1);
                    //sysTime = sysTime.addMinutes(2);
                    
                    String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
                    system.debug(chron_exp);

                    SchedulerBatchRunTerritoryAlignmentAcc newSched = new SchedulerBatchRunTerritoryAlignmentAcc(userinfo.getsessionid(), userInfo.getUserId());
                    //Schedule the next job, and give it the system time so name is unique
                    System.schedule('Run Territory Alignment Batch Job' + sysTime.getTime(), chron_exp, newSched);
                    
                    atlSetting[0].Batch_Scheduled__c = true;
                    atlSetting[0].Error__c = false;
                    
                    update atlSetting[0];
                }
            }
            
        }
    }
}

/*trigger JJ_ANZ_Account_Callout_Support on Account (after insert, after update) {
    
    //ensuring this does not run if kicked off from a batch process
    system.debug(JJ_ANZ_Process_Futures_Util.getnofutureprocess());
    If(!JJ_ANZ_Process_Futures_Util.getnofutureprocess()){
        
    List <String> acctIdList = new List <String> () ;
        for (Account acct : Trigger.new) {
            If(trigger.isinsert){
                acctIdList.add (acct.Id) ;
            }
            
            If(trigger.isupdate){
       //comparing the fields that are relevant to territory alignment 
       //to see if there was an update change. territory_vod__c should be included
       //the rest are customer specific territory filters
            string proftype = trigger.oldmap.get(acct.id).jj_core_professional_type__c;
            string spec1 = trigger.oldmap.get(acct.id).specialty_1_vod__c;
            string spec2 = trigger.oldmap.get(acct.id).specialty_2_vod__c;
            string terrrule = trigger.oldmap.get(acct.id).jj_anz_janssen_territory_rule__c;
            boolean calloutbool = trigger.oldmap.get(acct.id).jj_anz_callout_trigger__c;
            string bizacctrule = trigger.oldmap.get(acct.id).jj_anz_teams__c;
            string terrvod = trigger.oldmap.get(acct.id).territory_vod__c;
            If(acct.jj_core_professional_type__c != proftype ||
               acct.specialty_1_vod__c != spec1 ||
               acct.specialty_2_vod__c != spec2 ||
               acct.jj_anz_janssen_territory_rule__c != terrrule ||
               (acct.jj_anz_callout_trigger__c != calloutbool && acct.jj_anz_callout_trigger__c == false) ||
               acct.jj_anz_teams__c != bizacctrule || 
               acct.territory_vod__c != terrvod){
                   acctIdList.add(acct.id);
               }
            }
                
            
        }

System.debug ('hk: acctIdList is ' + acctIdList) ;

        // For all found Accounts, check the Territory_vod__c field
        String acctIds = '' ;
        /* for (Account a : [select Id, Territory_vod__c from Account where Id in :acctIdList and (Territory_vod__c = '' or Territory_vod__c = null)]) {
            if (acctIds != '') {
                acctIds += ',' ;
            }
            acctIds += a.Id ;   
        } *
        for (String acId : acctIdList) {
            if (acctIds != '') {
                acctIds += ',' ;
            }
            acctIds += acId ;   
        }

System.debug ('hk: acctIds is ' + acctIds) ;
        
        // Call the future method which does the aligment for all found accounts
        if (acctIds != '' && !System.isFuture()) {
            JJ_ANZ_Account_Alignment_Callout.calloutAssignTerritories(userinfo.getsessionid(), acctIds) ;
        }
    }
}*/