/*************************************************************************************
 *
 *  Developer: ICBWORKS
 *  Date: 18.02.2014
 *  Email: zameer.khodabocus@icbworks.com / dhanjay.ujoodha@icbworks.com
 *  
 *  Description: This trigger contains all the triggers developed by ICBWORKS and Bluewolf on the Janssen Event (Medical_Event_vod__c) Object.
 *  
 *  1) Trigger name: JanssenEventDisplayErrorMessageIfNoDocumentsUploaded
 *     Date completed: 01.08.2013
 *     Date Modified: 22.08.2013
 *     Decription: Trigger to prevent User for setting checkboxes Request letter/Prospectus? and Agenda Uploaded? to TRUE if no documents have been uploaded.
 *  
 *  2) Trigger name: CreateDeleteSharingRuleForRepDelegateAfterUpsertJanssenEvent
 *     Description: This trigger is used to share visibility of Janssen Events with the Rep Delegate 2 of the Event and 
 *                  to share visibility of accounts (JJ_Account__c) with:
 *                  1) Janssen Event Owner on the Janssen Event,
 *                  2) Rep Delegate 2 on the Janssen Event,
 *                  
 *                  when a new account (Speaker) is associated with an Event.   
 *  
 *  3) Trigger name: populateSigneeDetails
 *     Description: Trigger to fill Signee Details when create Janssen Event
 *
 *  4) Trigger name: UpdateCongaJanssenEventFieldsBeforeUpdateJanssenEvent
 *
 *  5) Trigger name: updatePrintAgreement
 *     Description: trigger to update print agreement on Speaker Agreement once Event is approved.
 *
 *  6) Trigger name: JanssenEventTrigger (developed by Bluewolf)
 *     Date created: July 8 2013
 *     Date modified: July 8 2013
 *     Description: Trigger to set the email address for the owners manager. Used by a worlflow because manager field is not availabe via default SFDC UI for email notifications.
 *                  Related requirements: DMP1 - Fetches the manager email for workflow notifications
 *
 *  7) Trigger name: JJ_ANZ_Dummy_Event_for_Budget
 *     Date completed: 08.10.2015
 *     Date Modified: 08.10.2015
 *     Decription: Trigger to prevent deletion of the Dummy Event for Budgets, used for non-Janssen Event Budget items (Fee for Service, Grant). 
        The Dummy Event Id is stored in Custom Settings
        
    8) Trigger name: Consulting_Group_stamp
       Date completed: 02.03.2016
       Description: Trigger to stamp values from Consulting Group, as Janssen Event is at the limit of 10 cross-referenced objects. 
       Only for RType - Consulting Group Formal Meeting. 
       
    9) Added logic Before Delete - cannot delete record if Event Status is not Planning
       2016-12-07 @author Noel Lim
**************************************************************************************/

trigger JanssenEventTriggers on Medical_Event_vod__c (after insert, after update, before insert, 
before update, before delete) {

    if(trigger.isBefore && trigger.isInsert){
        
        /**
            JanssenEventTrigger trigger
        **/
        List<ID> users=new List<ID>();
        
        for(Medical_Event_vod__c ev:Trigger.new){
            users.add(ev.OwnerId);      
        }
        for(user u:[SELECT Id,Manager.Email FROM User WHERE Id in:users]){
            for(Medical_Event_vod__c ev:Trigger.new){
                if(ev.OwnerId==u.Id){
                    ev.JJ_Manager_Email__c=u.Manager.Email;
                }
            }
        }
        
        /**
            JanssenEventDisplayErrorMessageIfNoDocumentsUploaded trigger
        **/
        for(Medical_Event_vod__c je : trigger.new){
            /*if(je.JJ_Sponsorship_Agreement_Signed__c == true){
                je.JJ_Sponsorship_Agreement_Signed__c.addError(Label.JE_No_Sponsorship_Document_Error);
            }*/
            if(je.JJ_Prospectus_Uploaded__c == true){
                je.JJ_Prospectus_Uploaded__c.addError(Label.JE_No_Request_letter_Prospectus_Error);
            }
            if(je.JJ_Agenda_Uploaded__c == true){
                je.JJ_Agenda_Uploaded__c.addError(Label.JE_No_Agenda_Uploaded_Error);
            }
        }
        
        /**
            populateSigneeDetails trigger
        **/
        map<Id, Account> mapOrg = new map<Id, Account>();
    
        for(Medical_Event_vod__c medEVt:trigger.new){
            if(medEVt.JJ_Signee__c){
                mapOrg.put(medEVt.JJ_Organization__c,null);
            }
        }
        
        for(Account acc:[select Id, Name, (select Name, Zip_vod__c, State_vod__c, Address_line_2_vod__c, City_vod__c, JJ_Core_Suburb__c from Address_vod__r limit 1) from Account where Id in:mapOrg.keySet()]){
            mapOrg.put(acc.Id,acc);
        }
        
         for(Medical_Event_vod__c medEVt:trigger.new){
            if(medEVt.JJ_Signee__c){
                Account acc1 = mapOrg.get(medEVt.JJ_Organization__c);
                medEVt.JJ_Signee_Full_Name__c = medEVt.JJ_Contact_Job_Title__c + ' ' + medEVt.JJ_Contact_First_name__c + ' ' + medEVt.JJ_Contact_Last_Name__c;
                if(!acc1.Address_vod__r.isEmpty()){
                    medEVt.JJ_Signee_Address__c = acc1.Address_vod__r[0].Name;
                    medEVt.JJ_Signee_Postcode__c = (acc1.Address_vod__r[0].Zip_vod__c == null) ? '':acc1.Address_vod__r[0].Zip_vod__c;
                    medEVt.JJ_Signee_State__c = (acc1.Address_vod__r[0].State_vod__c == null) ? '':acc1.Address_vod__r[0].State_vod__c;
                    medEVt.JJ_Signee_Street_2__c = (acc1.Address_vod__r[0].Address_line_2_vod__c == null) ? '':acc1.Address_vod__r[0].Address_line_2_vod__c;
                    medEVt.JJ_Signee_Suburb__c = (acc1.Address_vod__r[0].City_vod__c == null) ? '':acc1.Address_vod__r[0].City_vod__c;//JJ_Core_Suburb__c
                }
            }
        }
        
        /**
            Consulting_Group_Stamp
        **/
        Set<Id> cgIds = new Set<Id>();
        List<Medical_Event_vod__c> cgJEvents = new List<Medical_Event_vod__c>();
        for(Medical_Event_vod__c je : trigger.new){
            if(je.JJ_ANZ_Consulting_Group__c != null){
                cgIds.add(je.JJ_ANZ_Consulting_Group__c);
                cgJEvents.add(je);
            }
        }
        
        if(cgIds.size() > 0){
        
            Map<Id,JJ_ANZ_Consulting_Group__c> cGroups = new Map<Id,JJ_ANZ_Consulting_Group__c>([SELECT Id, JJ_ANZ_Type__c FROM JJ_ANZ_Consulting_Group__c WHERE Id IN :cgIds]);
            
            for(Medical_Event_vod__c je : cgJEvents){
                if(je.JJ_ANZ_Consulting_Group__c != null){
                    Id cgId = je.JJ_ANZ_Consulting_Group__c;
                    je.JJ_ANZ_Consulting_Group_Type__c = cGroups.get(cgId).JJ_ANZ_Type__c;          
                }
            }
        }
        
     }else if(trigger.isBefore && trigger.isDelete){
        /**
            JJ_ANZ_Dummy_Event_for_Budget
        **/

        //Get Dummy Event Id in Janssen Event Custom Settings
        JJ_ANZ_Janssen_Events__c jeCS = JJ_ANZ_Janssen_Events__c.getInstance();
        

        for(Medical_Event_vod__c ev:Trigger.old){
            if(ev.Id == jeCS.JJ_ANZ_Dummy_Event_FFS_Budget__c){
                ev.addError('This Event cannot be deleted, as it is used as a Dummy Event for Event Budgets. See Janssen Event in the Custom Settings');
            }
            else if(ev.JJ_ANZ_Event_Status__c != 'Planning'){
                ev.addError('Events can only be deleted while in Planning status');
            }
        }
    }else if(trigger.isBefore && trigger.isUpdate){
        /**
            JanssenEventTrigger trigger
        **/
        List<ID> users=new List<ID>();
        
        for(Medical_Event_vod__c ev:Trigger.new){
            users.add(ev.OwnerId);      
        }
        
        List<User> usersList = [SELECT Id,Manager.Email FROM User WHERE Id in:users];
        
        for(user u : usersList){
            for(Medical_Event_vod__c ev:Trigger.new){
                if(ev.OwnerId==u.Id){
                    ev.JJ_Manager_Email__c=u.Manager.Email;
                }
            }
        }
        
        /**
            CreateDeleteSharingRuleForRepDelegateAfterUpsertJanssenEvent trigger
        **/
        Map<Id, Medical_Event_vod__c> previousRepDelegateMap = new Map<Id, Medical_Event_vod__c>();
        Set<Id> oldRepDelegatesId = new Set<Id>();
        List<Id> eventsId = new List<Id>();
        List<Medical_Event_vod__Share> newEventShareList = new List<Medical_Event_vod__Share>();
        List<AccountShare> newAccountsShareList = new List<AccountShare>();
        Map<Id, String> medicalEventsOwnerAccessMap = new Map<Id, String>();
        Set<Id> oldOwnersId = new Set<Id>();
        Id ownerId = null;
        Set<Id> accountsId = new Set<Id>();
        
        for(Medical_Event_vod__c medEVt : trigger.new){
            //Set of the Janssen Events Id
            eventsId.add(medEVt.Id);
            //Id of the Janssen Event's Owner
            ownerId = medEVt.OwnerId;
        }
        
        if(eventsId.size() > 0 && ownerId != null){
            //Getting the access permissions to Janssen Events of the Janssen Event Owner. This is used to give the Rep Delegate 2 the same permissions.
            List<UserRecordAccess> ownerRecordAccessRules = [Select RecordId, MaxAccessLevel, HasTransferAccess, HasReadAccess, HasEditAccess, 
                                                              HasDeleteAccess, HasAllAccess 
                                                              From UserRecordAccess
                                                              where RecordId in :eventsId AND UserId = :ownerId];
                                                              
            for(UserRecordAccess recAccess : ownerRecordAccessRules){
                //Map of the permissions of the Owner
                medicalEventsOwnerAccessMap.put(recAccess.RecordId, recAccess.MaxAccessLevel);
            }
        }
        
        system.debug('==================medicalEventsOwnerAccessMap========================' + medicalEventsOwnerAccessMap);
        
        for(Medical_Event_vod__c medEVt : trigger.old){
            //Map of Janssen Event Record before applying the new changes to it.
            previousRepDelegateMap.put(medEVt.Id, medEVt);
        }
        
        if(eventsId.size() > 0){
            //Getting all the Account Associations related to this Event.
            List<Account_Event_Association__c> accEvAssociations = [select Id, JJ_Janssen_Event__c, JJ_Account__c, JJ_Speaker_Agreement__c
                                                                    from Account_Event_Association__c
                                                                    where JJ_Janssen_Event__c in :eventsId];
            
            for(Account_Event_Association__c aea : accEvAssociations){
                //Set of the Accounts Id related to this Event
                accountsId.add(aea.JJ_Account__c);
            }
            
            for(Medical_Event_vod__c medEVt : trigger.new){
                //Getting the old record of the Janssen Event before changes are saved
                Medical_Event_vod__c jEventOldRecord = previousRepDelegateMap.get(medEVt.Id);
                
                if(jEventOldRecord != null){
                    if(medEVt.Rep_Delegate_2__c == medEVt.OwnerId){
                        return; 
                    }
                    //Creating new shares only if previous value of Rep Delegate 2 was not equal to new value
                    if(jEventOldRecord.Rep_Delegate_2__c != medEVt.Rep_Delegate_2__c){
                        if(jEventOldRecord.Rep_Delegate_2__c != null){
                            //Set of the old Rep Delegates 2 Id. This will be used to get and delete all previous Janssen Event & Account shares
                            //for the old Rep Delegates. 
                            if(jEventOldRecord.Rep_Delegate_2__c != medEVt.OwnerId){
                                oldRepDelegatesId.add(jEventOldRecord.Rep_Delegate_2__c);
                            }
                        }
                        if(medEVt.Rep_Delegate_2__c != null){
                            //New Janssen Event share with the new Rep Delegate 2
                            Medical_Event_vod__Share newMedShare = new Medical_Event_vod__Share();
                    
                            newMedShare.ParentId = medEVt.Id;
                            newMedShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                            
                            //Applying the same access permissions (if any) to the Events for the Rep Delegate 2
                            if(medicalEventsOwnerAccessMap != null && medicalEventsOwnerAccessMap.size() > 0){
                                String accessLevl = medicalEventsOwnerAccessMap.get(medEVt.Id);
                                
                                system.debug('==================accessLevl========================' + accessLevl);
                                
                                if(accessLevl != ''){
                                    if(accessLevl == 'All'){
                                        newMedShare.AccessLevel = 'Edit';
                                    }else if(accessLevl == 'Read'){
                                        newMedShare.AccessLevel = 'Read';
                                    }else if(accessLevl == 'Delete'){
                                        newMedShare.AccessLevel = 'Edit';
                                    }
                                }
                            }else{
                                //If no existing permissions for the Owner of the Event, then give only Read access to the Rep Delegate 2
                                newMedShare.AccessLevel = 'Read';
                            }
                            
                            newEventShareList.add(newMedShare);
                            
                            //Creating new Account shares for the Rep Delegate 2. This will allow the Rep Delegate 2 to view the 
                            //Accounts related to this Event
                            if(accEvAssociations.size() > 0){
                                for(Account_Event_Association__c aea : accEvAssociations){
                                    if(aea.JJ_Janssen_Event__c == medEVt.Id){
                                        //New Account share with the new Rep Delegate 2
                                        AccountShare newAccShare = new AccountShare();
                            
                                        newAccShare.AccountId = aea.JJ_Account__c;
                                        newAccShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                                        newAccShare.OpportunityAccessLevel = 'None';
                                        
                                        newAccShare.AccountAccessLevel = 'Edit';
                                        
                                        newAccountsShareList.add(newAccShare);
                                    }
                                }
                            }
                        }
                    }
                    
                    if(jEventOldRecord.OwnerId != medEVt.OwnerId){
                        //Set of the old Owners Id. This will be used to get and delete all previous Account shares
                        //for the old Owners.
                        oldOwnersId.add(jEventOldRecord.OwnerId);
                        
                        if(accEvAssociations.size() > 0){
                            for(Account_Event_Association__c aea : accEvAssociations){
                                if(aea.JJ_Janssen_Event__c == medEVt.Id){
                                    //New Account share with the Owner of the Janssen Event
                                    AccountShare newAccShare = new AccountShare();
                        
                                    newAccShare.AccountId = aea.JJ_Account__c;
                                    newAccShare.UserOrGroupId = medEVt.OwnerId;
                                    newAccShare.OpportunityAccessLevel = 'None';
                                    
                                    newAccShare.AccountAccessLevel = 'Edit';
                                    
                                    newAccountsShareList.add(newAccShare);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //Getting the old Janssen Events Shares for the old Rep Delegate 2 to be deleted
        if(oldRepDelegatesId.size() > 0 && eventsId.size() > 0){
            List<Medical_Event_vod__Share> oldSharingRules = [select Id
                                                              From Medical_Event_vod__Share
                                                              where ParentId in :eventsId AND UserOrGroupId in :oldRepDelegatesId];
            
            if(oldSharingRules.size() > 0){ 
                //Deleting the old Janssen Events Shares for the Rep Delegate 2                                     
                delete oldSharingRules;
            }
            //Getting the old Accounts Shares for the old Rep Delegate 2 to be deleted
            if(accountsId.size() > 0){
                List<AccountShare> oldAccSharingRules = [select Id From AccountShare where AccountId in :accountsId 
                                                         AND UserOrGroupId in :oldRepDelegatesId];
                
                if(oldAccSharingRules.size() > 0){  
                    //Deleting the old Accounts Shares for the Rep Delegate 2                                       
                    delete oldAccSharingRules;
                }
            }
        }
        //Getting the old Accounts Shares for the old Owners to be deleted
        if(oldOwnersId.size() > 0 && accountsId.size() > 0){
            List<AccountShare> oldAccSharingRules = [select Id
                                                     From AccountShare
                                                     where AccountId in :accountsId AND UserOrGroupId in :oldOwnersId
                                                     AND RowCause = 'Manual'];
            
            if(oldAccSharingRules.size() > 0){  
                //Deleting the old Accounts Shares for the Owners                                   
                delete oldAccSharingRules;
            }
        }

        if(newEventShareList.size() > 0){
            //Inserting the new Janssen Event shares for the new Rep Delegates 2
            insert newEventShareList;
        }
        if(newAccountsShareList.size() > 0){
            //Inserting the new Account shares for the new Rep Delegates 2 and Owners
            insert newAccountsShareList;
        }
        
        /**
            populateSigneeDetails trigger
        **/
        map<Id, Account> mapOrg = new map<Id, Account>();
    
        for(Medical_Event_vod__c medEVt:trigger.new){
            if(medEVt.JJ_Signee__c){
                mapOrg.put(medEVt.JJ_Organization__c,null);
            }
        }
        
        for(Account acc:[select Id, Name, (select Name, Zip_vod__c, State_vod__c, Address_line_2_vod__c, City_vod__c, JJ_Core_Suburb__c from Address_vod__r limit 1) from Account where Id in:mapOrg.keySet()]){
            mapOrg.put(acc.Id,acc);
        }
        
         for(Medical_Event_vod__c medEVt:trigger.new){
            if(medEVt.JJ_Signee__c){
                Account acc1 = mapOrg.get(medEVt.JJ_Organization__c);
                medEVt.JJ_Signee_Full_Name__c = medEVt.JJ_Contact_Job_Title__c + ' ' + medEVt.JJ_Contact_First_name__c + ' ' + medEVt.JJ_Contact_Last_Name__c;
                if(!acc1.Address_vod__r.isEmpty()){
                    medEVt.JJ_Signee_Address__c = acc1.Address_vod__r[0].Name;
                    medEVt.JJ_Signee_Postcode__c = (acc1.Address_vod__r[0].Zip_vod__c == null) ? '':acc1.Address_vod__r[0].Zip_vod__c;
                    medEVt.JJ_Signee_State__c = (acc1.Address_vod__r[0].State_vod__c == null) ? '':acc1.Address_vod__r[0].State_vod__c;
                    medEVt.JJ_Signee_Street_2__c = (acc1.Address_vod__r[0].Address_line_2_vod__c == null) ? '':acc1.Address_vod__r[0].Address_line_2_vod__c;
                    medEVt.JJ_Signee_Suburb__c = (acc1.Address_vod__r[0].City_vod__c == null) ? '':acc1.Address_vod__r[0].City_vod__c;//JJ_Core_Suburb__c
                }
            }
        }
        
        /**
            UpdateCongaJanssenEventFieldsBeforeUpdateJanssenEvent trigger
        **/
        Set<Id> meetingEventsId = new Set<Id>();
        Map<Id, List<Janssen_Event_Agenda__c>> meetingEventAgendaMap = new Map<Id, List<Janssen_Event_Agenda__c>>();
        List<Janssen_Event_Agenda__c> agendaList = new List<Janssen_Event_Agenda__c>();
        integer counter = 0;
        Id previousMeetingEventId = null;
        accountsId = new Set<Id>();
        Set<Id> speakerAgsId = new Set<Id>();
        Map<Id, Account> accountsMap = new Map<Id, Account>();
        Map<Id, Speaker_Agreement__c> speakerAgreementsMap = new Map<Id, Speaker_Agreement__c>();
        Map<Id, List<Account_Event_Association__c>> acctEventAssociationsMap = new Map<Id, List<Account_Event_Association__c>>();
        List<Account_Event_Association__c> acctEventAssociationsList = new List<Account_Event_Association__c>();
        Set<Id> rsvpContacts1Id = new Set<Id>();
        Set<Id> rsvpContacts2Id = new Set<Id>();
        Set<Id> rsvpContacts3Id = new Set<Id>();
        Map<Id, User> rsvpContacts1Map = new Map<Id, User>();
        Map<Id, User> rsvpContacts2Map = new Map<Id, User>();
        Map<Id, User> rsvpContacts3Map = new Map<Id, User>();
        Set<Id> orgsId = new Set<Id>();
        Map<Id, Address_vod__c> orgAddressMap = new Map<Id, Address_vod__c>();
        
        for(Medical_Event_vod__c me : trigger.new){
            if(me.Process_Status__c == 'Approved'){
                meetingEventsId.add(me.Id);
                rsvpContacts1Id.add(me.RSVP_Contact_1__c);
                rsvpContacts2Id.add(me.RSVP_Contact_2__c);
                rsvpContacts3Id.add(me.JJ_RSVP_Contact_3__c);
                orgsId.add(me.JJ_Organization__c);
            }
        }
        
        if(meetingEventsId.size() > 0){
            List<Janssen_Event_Agenda__c> eventAgenda = [select Id, Time__c, Janssen_Event__c, Description__c 
                                                          From Janssen_Event_Agenda__c
                                                          where Janssen_Event__c in :meetingEventsId order by Janssen_Event__c,Time__c];
                                                          
            for(Janssen_Event_Agenda__c evAgenda : eventAgenda){
                if(counter == 0){
                    previousMeetingEventId = evAgenda.Janssen_Event__c;
                }
                if(previousMeetingEventId == evAgenda.Janssen_Event__c){
                    agendaList.add(evAgenda);
                }else{
                    meetingEventAgendaMap.put(previousMeetingEventId, agendaList);
                    
                    previousMeetingEventId = evAgenda.Janssen_Event__c;
                    agendaList = new List<Janssen_Event_Agenda__c>();
                    agendaList.add(evAgenda);
                }
                if(counter == eventAgenda.size() - 1){
                    meetingEventAgendaMap.put(previousMeetingEventId, agendaList);
                }
                counter++;
            }
            
            counter = 0;
            previousMeetingEventId = null;
            
            List<Account_Event_Association__c> accountEventAssociations = [select Id, JJ_Speaker_Agreement__c, JJ_Janssen_Event__c, JJ_Account__c
                                                                           From Account_Event_Association__c
                                                                           where JJ_Janssen_Event__c in :meetingEventsId 
                                                                           order by JJ_Janssen_Event__c];
                                                                           
            for(Account_Event_Association__c aea : accountEventAssociations){
                accountsId.add(aea.JJ_Account__c);
                speakerAgsId.add(aea.JJ_Speaker_Agreement__c);
            }
            
            if(accountsId.size() > 0){
                List<Account> accounts = [select Id, LastName, FirstName, Salutation from Account where Id in :accountsId];
                
                for(Account acc : accounts){
                    accountsMap.put(acc.Id, acc);
                }
            }
            
            if(speakerAgsId.size() > 0){
                List<Speaker_Agreement__c> speakerAgreements = [select Id, JJ_Speaker_Credentials__c, JJ_Bio__c, JJ_Bio_Approved__c, JJ_Speaker__c 
                                                                From Speaker_Agreement__c
                                                                where Id in :speakerAgsId order by JJ_Start_Date__c];
                                                                
                for(Speaker_Agreement__c spa : speakerAgreements){
                    speakerAgreementsMap.put(spa.Id, spa);
                }
            }
            
            for(Account_Event_Association__c aea : accountEventAssociations){
                if(counter == 0){
                    previousMeetingEventId = aea.JJ_Janssen_Event__c;
                }
                if(previousMeetingEventId == aea.JJ_Janssen_Event__c){
                    acctEventAssociationsList.add(aea);
                }else{
                    acctEventAssociationsMap.put(previousMeetingEventId, acctEventAssociationsList);
                    
                    previousMeetingEventId = aea.JJ_Janssen_Event__c;
                    acctEventAssociationsList = new List<Account_Event_Association__c>();
                    
                    acctEventAssociationsList.add(aea);
                }
                if(counter == accountEventAssociations.size() - 1){
                    acctEventAssociationsMap.put(previousMeetingEventId, acctEventAssociationsList);
                }
                counter++;
            }
            
            if(rsvpContacts1Id.size() > 0){
                List<User> rsvp1Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts1Id];
                
                for(User user : rsvp1Contacts){
                    rsvpContacts1Map.put(user.Id, user);
                }
            }
            
            if(rsvpContacts2Id.size() > 0){
                List<User> rsvp2Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts2Id];
                
                for(User user : rsvp2Contacts){
                    rsvpContacts2Map.put(user.Id, user);
                }
            }
            
            if(rsvpContacts3Id.size() > 0){
                List<User> rsvp3Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts3Id];
                
                for(User user : rsvp3Contacts){
                    rsvpContacts3Map.put(user.Id, user);
                }
            }
            
            if(orgsId.size() > 0){
                List<Address_vod__c> organisationAddresses = [select Id, Account_vod__c, Zip_vod__c, State_vod__c, Primary_vod__c, Name, City_vod__c, Address_line_2_vod__c 
                                                              From Address_vod__c
                                                              where Account_vod__c in :orgsId AND Primary_vod__c = true
                                                              order by Account_vod__c];
                                                              
                for(Address_vod__c add : organisationAddresses){
                    orgAddressMap.put(add.Account_vod__c, add);
                }
            }
            
            agendaList = new List<Janssen_Event_Agenda__c>();
            acctEventAssociationsList = new List<Account_Event_Association__c>();
            
            for(Medical_Event_vod__c me : trigger.new){
                if(meetingEventAgendaMap != null){
                    agendaList = meetingEventAgendaMap.get(me.Id);
                    
                    if(agendaList != null && agendaList.size() > 0){
                        integer countAgenda = 1;
                        
                        for(Janssen_Event_Agenda__c ev : agendaList){
                            if(countAgenda == 1){
                                me.JJ_Agenda01__c = ev.Description__c;
                                me.JJ_Agenda01_Time__c = ev.Time__c;
                            }else if(countAgenda == 2){
                                me.JJ_Agenda02__c = ev.Description__c;
                                me.JJ_Agenda02_Time__c = ev.Time__c;
                            }else if(countAgenda == 3){
                                me.JJ_Agenda03__c = ev.Description__c;
                                me.JJ_Agenda03_Time__c = ev.Time__c;
                            }else if(countAgenda == 4){
                                me.JJ_Agenda04__c = ev.Description__c;
                                me.JJ_Agenda04_Time__c = ev.Time__c;
                            }else if(countAgenda == 5){
                                me.JJ_Agenda05__c = ev.Description__c;
                                me.JJ_Agenda05_Time__c = ev.Time__c;
                            }else if(countAgenda == 6){
                                me.JJ_Agenda06__c = ev.Description__c;
                                me.JJ_Agenda06_Time__c = ev.Time__c;
                            }else if(countAgenda == 7){
                                me.JJ_Agenda07__c = ev.Description__c;
                                me.JJ_Agenda07_Time__c = ev.Time__c;
                            }else if(countAgenda == 8){
                                me.JJ_Agenda08__c = ev.Description__c;
                                me.JJ_Agenda08_Time__c = ev.Time__c;
                            }else if(countAgenda == 9){
                                me.JJ_Agenda09__c = ev.Description__c;
                                me.JJ_Agenda09_Time__c = ev.Time__c;
                            }else if(countAgenda == 10){
                                me.JJ_Agenda10__c = ev.Description__c;
                                me.JJ_Agenda10_Time__c = ev.Time__c;
                            }
                            countAgenda++;
                        }
                    }
                }
                
                if(acctEventAssociationsMap != null && acctEventAssociationsMap.size() > 0){
                    acctEventAssociationsList = acctEventAssociationsMap.get(me.Id);
                    
                    if(acctEventAssociationsList != null && acctEventAssociationsList.size() > 0){
                        integer countAEA = 1;
                        
                        for(Account_Event_Association__c aea : acctEventAssociationsList){
                            Speaker_Agreement__c speaker = speakerAgreementsMap.get(aea.JJ_Speaker_Agreement__c);
                            
                            if(speaker != null){
                                Account acc = accountsMap.get(speaker.JJ_Speaker__c);
                                
                                if(acc != null){
                                    if(countAEA == 1){
                                        me.JJ_Speaker1_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                        me.JJ_Speaker1_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                        me.JJ_Speaker1_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                        me.JJ_Speaker1_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                    }else if(countAEA == 2){
                                        me.JJ_Speaker2_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                        me.JJ_Speaker2_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                        me.JJ_Speaker2_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                        me.JJ_Speaker2_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                    }else if(countAEA == 3){
                                        me.JJ_Speaker3_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                        me.JJ_Speaker3_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                        me.JJ_Speaker3_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                        me.JJ_Speaker3_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                    }
                                    countAEA++;
                                }
                            }
                        }
                    }
                }
                
                if(rsvpContacts1Map.size() > 0){
                    User rsvpContact1 = rsvpContacts1Map.get(me.RSVP_Contact_1__c);
                    
                    if(rsvpContact1 != null){
                        me.JJ_RSVP1_Name__c = rsvpContact1.Name;
                        me.JJ_RSVP1_Email__c = rsvpContact1.Email;
                        me.JJ_RSVP1_Mobile__c = rsvpContact1.MobilePhone;
                    }
                }
                
                if(rsvpContacts2Map.size() > 0){
                    User rsvpContact2 = rsvpContacts2Map.get(me.RSVP_Contact_2__c);
                    
                    if(rsvpContact2 != null){
                        me.JJ_RSVP2_Name__c = rsvpContact2.Name;
                        me.JJ_RSVP2_Email__c = rsvpContact2.Email;
                        me.JJ_RSVP2_Mobile__c = rsvpContact2.MobilePhone;
                    }
                }
                
                if(rsvpContacts3Map.size() > 0){
                    User rsvpContact3 = rsvpContacts3Map.get(me.JJ_RSVP_Contact_3__c);
                    
                    if(rsvpContact3 != null){
                        me.JJ_RSVP3_Name__c = rsvpContact3.Name;
                        me.JJ_RSVP3_Email__c = rsvpContact3.Email;
                        me.JJ_RSVP3_Mobile__c = rsvpContact3.MobilePhone;
                    }
                }
                
                if(orgAddressMap != null){
                    if(me.JJ_Organization__c != null){
                        Address_vod__c add = orgAddressMap.get(me.JJ_Organization__c);
                        
                        if(add != null){
                            me.JJ_Organisation_Street__c = add.Name;
                            me.JJ_Organisation_Street2__c = add.Address_line_2_vod__c;
                            me.JJ_Organisation_Suburb__c = add.City_vod__c;
                            me.JJ_Organisation_State__c = add.State_vod__c;
                            me.JJ_Organisation_Postcode__c = add.Zip_vod__c;
                        }
                    }
                }
            }
        }
        
        /**
            Consulting_Group_Stamp
        **/
        Set<Id> cgIds = new Set<Id>();
        List<Medical_Event_vod__c> cgJEvents = new List<Medical_Event_vod__c>();
        for(Medical_Event_vod__c je : trigger.new){
            if(je.JJ_ANZ_Consulting_Group__c != null){
                cgIds.add(je.JJ_ANZ_Consulting_Group__c);
                cgJEvents.add(je);
            }
        }
        
        if(cgIds.size() > 0){
        
            Map<Id,JJ_ANZ_Consulting_Group__c> cGroups = new Map<Id,JJ_ANZ_Consulting_Group__c>([SELECT Id, JJ_ANZ_Type__c FROM JJ_ANZ_Consulting_Group__c WHERE Id IN :cgIds]);
            
            for(Medical_Event_vod__c je : cgJEvents){
                if(je.JJ_ANZ_Consulting_Group__c != null){
                    Id cgId = je.JJ_ANZ_Consulting_Group__c;
                    je.JJ_ANZ_Consulting_Group_Type__c = cGroups.get(cgId).JJ_ANZ_Type__c;          
                }
            }
        }
        
    }else if(trigger.isAfter && trigger.isInsert){
        /**
            CreateDeleteSharingRuleForRepDelegateAfterUpsertJanssenEvent trigger
        **/
        List<Id> eventsId = new List<Id>();
        List<Medical_Event_vod__Share> newEventShareList = new List<Medical_Event_vod__Share>();
        Map<Id, String> medicalEventsOwnerAccessMap = new Map<Id, String>();
        Id ownerId = null;
        
        for(Medical_Event_vod__c medEVt : trigger.new){
            //Set of the Janssen Events Id
            eventsId.add(medEVt.Id);
            //Id of the Janssen Event's Owner
            ownerId = medEVt.OwnerId;
        }
        
        if(eventsId.size() > 0 && ownerId != null){
            //Getting the access permissions to Janssen Events of the Janssen Event Owner. This is used to give the Rep Delegate 2 the same permissions.
            List<UserRecordAccess> ownerRecordAccessRules = [Select RecordId, MaxAccessLevel, HasTransferAccess, HasReadAccess, HasEditAccess, 
                                                              HasDeleteAccess, HasAllAccess 
                                                              From UserRecordAccess
                                                              where RecordId in :eventsId AND UserId = :ownerId];
                                                              
            for(UserRecordAccess recAccess : ownerRecordAccessRules){
                //Map of the permissions of the Owner
                medicalEventsOwnerAccessMap.put(recAccess.RecordId, recAccess.MaxAccessLevel);
            }
        }
    
        for(Medical_Event_vod__c medEVt : trigger.new){
            if(medEVt.Rep_Delegate_2__c != null){
                
                //New Janssen Event share with the new Rep Delegate 2
                Medical_Event_vod__Share newMedShare = new Medical_Event_vod__Share();
                
                newMedShare.ParentId = medEVt.Id;
                newMedShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                
                //Applying the same access permissions (if any) to the Events for the Rep Delegate 2
                if(medicalEventsOwnerAccessMap != null && medicalEventsOwnerAccessMap.size() > 0){
                    String accessLevl = medicalEventsOwnerAccessMap.get(medEVt.Id);
                    
                    if(accessLevl != ''){
                        if(accessLevl == 'All'){
                            newMedShare.AccessLevel = 'Edit';
                        }else if(accessLevl == 'Read'){
                            newMedShare.AccessLevel = 'Read';
                        }else if(accessLevl == 'Delete'){
                            newMedShare.AccessLevel = 'Edit';
                        }
                    }
                }else{
                    //If no existing permissions for the Owner of the Event, then give only Read access to the Rep Delegate 2
                    newMedShare.AccessLevel = 'Read';
                }
                
                newEventShareList.add(newMedShare);
            }
        }
        
        if(newEventShareList.size() > 0){
            //Inserting the new Janssen Event shares for the new Rep Delegates 2
            insert newEventShareList;
        }
    }else if(trigger.isAfter && trigger.isUpdate){
        /**
            updatePrintAgreement trigger
        **/
        set<Id> medIds = new set<Id>();
    
        for(Medical_Event_vod__c m:trigger.new){
            if(m.Process_Status__c == 'Approved' && trigger.oldmap.get(m.Id).Process_Status__c != 'Approved'){
                medIds.add(m.Id);
            }
        }
        
        List<Speaker_Agreement__c> updSpeakerAgrLst = new List<Speaker_Agreement__c>();
        
        for(Account_Event_Association__c a:[Select JJ_Speaker_Agreement__c From Account_Event_Association__c where JJ_Speaker_Agreement__r.JJ_Print_Agreement__c =:false and JJ_Speaker_Agreement__r.JJ_Process_Status__c =:'Approved' and JJ_Janssen_Event__c in: medIds]){
            updSpeakerAgrLst.add(new Speaker_Agreement__c(Id = a.JJ_Speaker_Agreement__c, JJ_Print_Agreement__c = true));    
        }
        
        update updSpeakerAgrLst;
    }
}