trigger populateSigneeDetails on Medical_Event_vod__c (before insert, before update) {
    
    /*ICBWORKS 
    Trigger to fill Signee Details when create Janssen Event
    */
    
    map<Id, Account> mapOrg = new map<Id, Account>();
    
    for(Medical_Event_vod__c medEVt:trigger.new){
        if(medEVt.JJ_Signee__c){
            mapOrg.put(medEVt.JJ_Organization__c,null);
        }
    }
    
    for(Account acc:[select Id, Name, (select Name, Zip_vod__c, State_vod__c, Address_line_2_vod__c, City_vod__c, JJ_Core_Suburb__c from Address_vod__r limit 1) from Account where Id in:mapOrg.keySet()]){
        mapOrg.put(acc.Id,acc);
    }
    
     for(Medical_Event_vod__c medEVt:trigger.new){
        if(medEVt.JJ_Signee__c){
            Account acc1 = mapOrg.get(medEVt.JJ_Organization__c);
            medEVt.JJ_Signee_Full_Name__c = medEVt.JJ_Contact_Job_Title__c + ' ' + medEVt.JJ_Contact_First_name__c + ' ' + medEVt.JJ_Contact_Last_Name__c;
            if(!acc1.Address_vod__r.isEmpty()){
                medEVt.JJ_Signee_Address__c = acc1.Address_vod__r[0].Name;
                medEVt.JJ_Signee_Postcode__c = (acc1.Address_vod__r[0].Zip_vod__c == null) ? '':acc1.Address_vod__r[0].Zip_vod__c;
                medEVt.JJ_Signee_State__c = (acc1.Address_vod__r[0].State_vod__c == null) ? '':acc1.Address_vod__r[0].State_vod__c;
                medEVt.JJ_Signee_Street_2__c = (acc1.Address_vod__r[0].Address_line_2_vod__c == null) ? '':acc1.Address_vod__r[0].Address_line_2_vod__c;
                medEVt.JJ_Signee_Suburb__c = (acc1.Address_vod__r[0].City_vod__c == null) ? '':acc1.Address_vod__r[0].City_vod__c;//JJ_Core_Suburb__c
            }
        }
    }
}