/**
 *      @author    RAVITEJA CHERUKURI     
 *      @date      11/06/2015      
        @name           
        @description This trigger is used when the record is inserted or updated in Multichannel Consent object and does the following tasks:
                     - Updates the value of Promotional Email Consent to 'Yes' or 'No' , Marketing Contact to true or false for the opt in and opt out scenario respectively.
                     - Updates the value of Non Promotional Email Consent to 'Yes' if Promotional Email consent is 'Yes'
                     - Updates the Channel Permission object fields - JJ_Core_Account__c, JJ_Core_Channel__c, JJ_Core_Permission__c
                        
        
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
         Raviteja CH                  11/06/2015         This trigger is used when the record is inserted or updated in Multichannel Consent object and does the following tasks:
                                                         - Updates the value of Promotional Email Consent to 'Yes' or 'No' , Marketing Contact to true or false for the opt in and opt out scenario respectively.
                                                         - Updates the value of Non Promotional Email Consent to 'Yes' if Promotional Email consent is 'Yes'
                                                         - Updates the Channel Permission object fields - JJ_Core_Account__c, JJ_Core_Channel__c, JJ_Core_Permission__c
 
*/
trigger JJ_ANZ_UPD_Promo_Email_Consent_onAccount on Multichannel_Consent_vod__c(After insert, After update){
      //Get the list of Accounts to be updated
    List<ID> AccIDs = new List<ID>();
    map<id,list<JJ_Core_Channel_Permissions__c>> channelpermissions=new map<id,list<JJ_Core_Channel_Permissions__c>>();
    map<id,list<Multichannel_Consent_vod__c>> AccountMCMap = new map<id,list<Multichannel_Consent_vod__c>>();
    list<JJ_Core_Channel_Permissions__c> ChpList=new list<JJ_Core_Channel_Permissions__c>();
    list<JJ_Core_Channel_Permissions__c> ChpToInsert=new list<JJ_Core_Channel_Permissions__c>();
    //list<JJ_Core_Channel_Permissions__c> ChpToUpdate=new list<JJ_Core_Channel_Permissions__c>();
    //Set<JJ_Core_Channel_Permissions__c> ChpToUpdate=new Set<JJ_Core_Channel_Permissions__c>();
    Map<Id,JJ_Core_Channel_Permissions__c> ChpToUpdate=new Map<Id,JJ_Core_Channel_Permissions__c>();
    for (Multichannel_Consent_vod__c oMcc: trigger.new){
        AccIDs.add(oMcc.Account_vod__c);
    }
    
    
    for(Multichannel_Consent_vod__c Mcc: [select id, Account_vod__c, CreatedDate, Capture_Datetime_vod__c From Multichannel_Consent_vod__c Where Account_vod__c in: AccIDs order by Capture_Datetime_vod__c desc]){
       if(!AccountMCMap.containsKey(Mcc.Account_vod__c)){  
            AccountMCMap.put(Mcc.Account_vod__c, new list<Multichannel_Consent_vod__c>{Mcc} );   
        } else {
            AccountMCMap.get(Mcc.Account_vod__c). add(Mcc);            
        }   
    }
    
    for(JJ_Core_Channel_Permissions__c chp:[select Id,JJ_Core_Account__c,JJ_Core_Channel__c,JJ_Core_Permission__c,CreatedDate from JJ_Core_Channel_Permissions__c where JJ_Core_Account__c in:AccIDs and JJ_Core_Channel__c='Email' order by CreatedDate desc]){
       if(!channelpermissions.containsKey(chp.JJ_Core_Account__c)){  
            channelpermissions.put(chp.JJ_Core_Account__c, new list<JJ_Core_Channel_Permissions__c>{chp} );   
        } else {
            channelpermissions.get(chp.JJ_Core_Account__c). add(chp);            
        }   
    }
    
    //Get a list of all the records for Accounts that contain the above IDs
    List<Account> Accs = new List<Account>([select id, PersonEmail,JJ_ANZ_NonPromo_Email_Consent__c,JJ_ANZ_Promo_Email_Consent__c,JJ_ANZ_NonPromotional_Consent_Modified__c from Account where id in: AccIDs]);
    //List<Account> AccsToUpdate = new List<Account>();
    //Set<Account> AccsToUpdate = new Set<Account>();
    Map<Id,Account> AccsToUpdate=new Map<Id,Account>();

    for (Multichannel_Consent_vod__c oMcc: trigger.new){   
        if(Trigger.isInsert){
            for (integer i = 0; i < Accs.size(); i++){  
                list<Multichannel_Consent_vod__c> Mcs = new list<Multichannel_Consent_vod__c>();
                Mcs = AccountMCMap.get(Accs[i].id); 
                if(oMcc.id == Mcs[0].id){               
                if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'Yes';
                    
                    Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;
                    if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c == null){
                          Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes';
                         
                    }else{
                          
                          if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c < Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c){
                              
                              Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes';
                                                            
                          }
                    }
                    
                      
                    AccsToUpdate.put(Accs[i].id, Accs[i]);
                }else if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'No'; 
                    
                    Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;
                                         
                    AccsToUpdate.put(Accs[i].id, Accs[i]);
                }  
                }               
            }
            ChpList=channelpermissions.get(oMcc.Account_vod__c);
            if(ChpList==null)
            {
            JJ_Core_Channel_Permissions__c newchp=new JJ_Core_Channel_Permissions__c();
            newchp.JJ_Core_Account__c=oMcc.Account_vod__c;
            newchp.JJ_Core_Channel__c='Email';
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-in';
                }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-out';
                }
            ChpToInsert.add(newchp);
            }
            else
            {
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    ChpList[0].JJ_Core_Permission__c= 'Opt-in';
                }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    ChpList[0].JJ_Core_Permission__c= 'Opt-out';
                }         
             ChpToUpdate.put(ChpList[0].Id,ChpList[0]);
             System.debug('Jaya isinsert'+ChpToUpdate.size()+' '+ChpToUpdate);
            }
           
            
        } else if (Trigger.isUpdate){
            for (integer i = 0; i < Accs.size(); i++){  
                list<Multichannel_Consent_vod__c> Mcs = new list<Multichannel_Consent_vod__c>();
                Mcs = AccountMCMap.get(Accs[i].id);
                if(oMcc.id == Mcs[0].id){
                    if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                        Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'Yes';
                        Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;
                        if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c == null){
                          Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes';
                         
                        }else{
                          
                          if(Accs[i].JJ_ANZ_NonPromotional_Consent_Modified__c < Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c){
                              
                              Accs[i].JJ_ANZ_NonPromo_Email_Consent__c = 'Yes'; 
                             
                          }
                        }
                        
                        AccsToUpdate.put(Accs[i].id, Accs[i]);
                    }else if (oMcc.Account_vod__c== Accs[i].id && oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    Accs[i].JJ_ANZ_Promo_Email_Consent__c = 'No'; 
                    
                    Accs[i].JJ_ANZ_Promotional_Consent_Last_Modified__c = oMcc.Capture_Datetime_vod__c;           
                                     
                    AccsToUpdate.put(Accs[i].id, Accs[i]);
                    }   
                }           
             }
             
            ChpList=channelpermissions.get(oMcc.Account_vod__c);
            if(ChpList==null)
            {
            JJ_Core_Channel_Permissions__c newchp=new JJ_Core_Channel_Permissions__c();
            newchp.JJ_Core_Account__c=oMcc.Account_vod__c;
            newchp.JJ_Core_Channel__c='Email';
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-in';
                }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    newchp.JJ_Core_Permission__c= 'Opt-out';
                }
            ChpToInsert.add(newchp);
            }
            else
            {
            if(oMcc.Opt_Type_vod__c == 'Opt_In_vod'){
                    ChpList[0].JJ_Core_Permission__c= 'Opt-in';
            }else if(oMcc.Opt_Type_vod__c == 'Opt_Out_vod'){
                    ChpList[0].JJ_Core_Permission__c= 'Opt-out';
            }
            ChpToUpdate.put(ChpList[0].Id,ChpList[0]);
            System.debug('Jaya isupdate'+ChpToUpdate.size()+' '+ChpToUpdate);
            }
        }
        
     
   }
     
    //update (new List<Account>(AccsToUpdate));
    if(!AccsToUpdate.isEmpty()){
        update AccsToUpdate.Values();
        system.debug('@@@1'+AccsToUpdate);
    }
    if(!ChpToInsert.isEmpty()){
        insert ChpToInsert;
        system.debug('@@@2'+ChpToInsert);
    }
    //update (new List<JJ_Core_Channel_Permissions__c>(ChpToUpdate)); 
   
    if(!ChpToUpdate.isEmpty()){
        update ChpToUpdate.Values();
        system.debug('@@@3'+ChpToUpdate);
   }
}