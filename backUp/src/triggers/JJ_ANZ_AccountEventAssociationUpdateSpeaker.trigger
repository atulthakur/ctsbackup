//developped by ICBWORKS
//trigger to update event field when adding new speaker and delete
trigger JJ_ANZ_AccountEventAssociationUpdateSpeaker on Account_Event_Association__c (before delete, after insert) {
    
    //select the recordtype to query in the custom setting
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select Speaker_Agreement_Event_RT_update_4__c,Speaker_Agreement_Event_RT_update_3__c,Speaker_Agreement_Event_RT_update_2__c, Speaker_Agreement_Event_RT_update_1__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.Speaker_Agreement_Event_RT_update_1__c == null)?'':je.Speaker_Agreement_Event_RT_update_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.Speaker_Agreement_Event_RT_update_2__c == null)?'':je.Speaker_Agreement_Event_RT_update_2__c;
        jerectype.addAll(s.split(';'));
        s = (je.Speaker_Agreement_Event_RT_update_3__c == null)?'':je.Speaker_Agreement_Event_RT_update_3__c;
        jerectype.addAll(s.split(';'));
        s = (je.Speaker_Agreement_Event_RT_update_4__c == null)?'':je.Speaker_Agreement_Event_RT_update_4__c;
        jerectype.addAll(s.split(';'));
    }
    
    set<Id> janevtIds = new set<Id>();
    set<Id> apkagrIds = new set<Id>();
    
    //save the event and speaker id from the junction object account event association on delete or create
    if(trigger.isDelete){
        for(Account_Event_Association__c aea:trigger.old){
            janevtIds.add(aea.JJ_Janssen_Event__c);
            apkagrIds.add(aea.JJ_Speaker_Agreement__c);
        }
    }
    else if(trigger.isInsert){      
        for(Account_Event_Association__c aea:trigger.new){
            janevtIds.add(aea.JJ_Janssen_Event__c);
            apkagrIds.add(aea.JJ_Speaker_Agreement__c);
        }
    }
    
    //select the event and speaker fields from the above id
    map<Id, Medical_Event_vod__c> mapjanevt = new map<Id, Medical_Event_vod__c>([select Id, RecordType.DeveloperName, JJ_ANZ_Event_Status__c, JJ_ANZ_Speak_Agree_Statuses__c from Medical_Event_vod__c where RecordType.DeveloperName in:jerectype and (JJ_ANZ_Event_Status__c = 'Rejected' OR JJ_ANZ_Event_Status__c = 'Planning') and Id in:janevtIds]);
    
    map<Id, Speaker_Agreement__c> mapspkagr = new map<Id, Speaker_Agreement__c>([Select JJ_ANZ_Speaker_Status__c, Name, Id From Speaker_Agreement__c where Id in:apkagrIds]);
    
    Medical_Event_vod__c tempjanEvt = new Medical_Event_vod__c();
    Speaker_Agreement__c tempspkagr = new Speaker_Agreement__c();
    List<Medical_Event_vod__c> updmedEvnt = new List<Medical_Event_vod__c>();
    string statuslst = '';
    //on create add the speaker name and status to the field on the event and remove the speaker on delete
    if(trigger.isDelete){
        for(Account_Event_Association__c aea:trigger.old){
            tempjanEvt = mapjanevt.get(aea.JJ_Janssen_Event__c);
            tempspkagr = mapspkagr.get(aea.JJ_Speaker_Agreement__c);
            if(tempjanEvt != null && tempspkagr != null){
                statuslst = (tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == null)?'':tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c;
                tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c = '';
                for(string s:statuslst.split(',')){
                    if(s.split(':')[0] != tempspkagr.Name){
                        tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c += (s == '' || tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == '')?s:',' + s;
                    }
                }
                updmedEvnt.add(tempjanEvt);
            }
        }
    }
    else if(trigger.isInsert){
        for(Account_Event_Association__c aea:trigger.new){
            tempjanEvt = mapjanevt.get(aea.JJ_Janssen_Event__c);
            tempspkagr = mapspkagr.get(aea.JJ_Speaker_Agreement__c);
            if(tempjanEvt != null && tempspkagr != null){
                tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c = (tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == null)?'':tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c;
                tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c += (tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == '')?tempspkagr.Name + ':' + tempspkagr.JJ_ANZ_Speaker_Status__c:','+tempspkagr.Name + ':' + tempspkagr.JJ_ANZ_Speaker_Status__c;
                updmedEvnt.add(tempjanEvt);
            }
        }
    }
    update updmedEvnt;
}