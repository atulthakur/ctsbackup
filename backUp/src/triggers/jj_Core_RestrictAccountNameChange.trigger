trigger jj_Core_RestrictAccountNameChange on Account (before update) {

 /*
     * Author:  Raj Bhattacharjee
     * Date:    6/04/2012
     * Summary: Revert changes if a user without "PermissionsModifyAllData"
     *          on the Account object modifies the following fields:
     *              - firstname
     *              - lastname
     *              - Middle_vod__c
     *              - salutation
     *              - name (for business accounts)
     *
     * 09.02.2015 Allow name changes for RT Virtual Account  
     *
 */
     Boolean bChangeExists = false;
     Boolean bModAllData = false;

     for (integer i=0;i<Trigger.new.size();i++) {
         if (Trigger.new[i].isPersonAccount == true) {

             if ((Trigger.new[i].FirstName != Trigger.old[i].FirstName) || (Trigger.new[i].LastName != Trigger.old[i].LastName) || (Trigger.new[i].Middle_vod__c != Trigger.old[i].Middle_vod__c)) {
                 bChangeExists = true;
             }
             if (Trigger.new[i].Salutation != Trigger.old[i].Salutation ) {
                 bChangeExists = true;
             }
         }
         else {
             if ((Trigger.new[i].Name != Trigger.old[i].Name)) {
                 bChangeExists = true;
             }
         }
     }

     /* Prevent update if name change happened and non-admin user */
     if (bChangeExists == true) {

         String ProfileId = UserInfo.getProfileId();

         Profile pr = [Select Id, PermissionsModifyAllData From Profile where Id = :ProfileId];

         if (pr != null && pr.PermissionsModifyAllData) {
             bModAllData = true;
         }


         /* 06.02.2015 Allow update if name change happened to Virtual Account record type */
         //Query for the Account record types
         List <RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='Account' and isActive=true] ;
       
         //Create a map between the Record Type Name and Id for easy retrieval
         Map <String, String> accountRecordTypes = new Map< String,String >{};
         for(RecordType rt: rtypes) {
            accountRecordTypes.put(rt.Name, rt.Id);
         }
       
         for (integer i=0;i<Trigger.new.size();i++) {
                 if (Trigger.old[i].RecordTypeId == accountRecordTypes.get('Virtual Account') ) {
                      bModAllData = true;
                 }
         }


         if (bModAllData == false) {
             for (integer i=0;i<Trigger.new.size();i++) {
                 if (Trigger.new[i].isPersonAccount == true) {

                     if ((Trigger.new[i].FirstName != Trigger.old[i].FirstName) || (Trigger.new[i].LastName != Trigger.old[i].LastName) || (Trigger.new[i].Middle_vod__c != Trigger.old[i].Middle_vod__c)) {

                         Trigger.new[i].FirstName = Trigger.old[i].FirstName;

                         Trigger.new[i].LastName = Trigger.old[i].LastName;

                         Trigger.new[i].Middle_vod__c = Trigger.old[i].Middle_vod__c;
                     }

                     if (Trigger.new[i].Salutation != Trigger.old[i].Salutation) {

                         Trigger.new[i].Salutation = Trigger.old[i].Salutation;
                     }
                 }
                 else {
                     if ((Trigger.new[i].Name != Trigger.old[i].Name)) {
                         Trigger.new[i].Name = Trigger.old[i].Name;
                     }
                 }
             }
         }
     }

}