trigger RemovePreviousApproval on Meeting_Titles__c (before insert, before update, after insert, after update) {


                Meeting_Titles__c subMeet = new Meeting_Titles__c();
                
                for(Meeting_Titles__c mt: [select Id, Name, CreatedById from Meeting_Titles__c where Id =: trigger.new[0].Replaced_Title_Id__c]){
                    subMeet = mt;
                }
                
                
                if(subMeet.Id != null && ![Select Id From ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:subMeet.Id].isEmpty()){
                    
                            Approval.ProcessWorkitemRequest PWR = new Approval.ProcessWorkitemRequest();
                            PWR.setComments('Replaced by new request');
                            PWR.setAction('Removed');
                            PWR.setWorkitemId([Select Id From ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:subMeet.Id].Id);
                            Approval.ProcessResult result1 =  Approval.process(PWR);
                                
                            if(result1.isSuccess()){ 
                                List<Messaging.SingleEmailMessage> mailLst = new List<Messaging.SingleEmailMessage>();
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                mail.setSubject(subMeet.Name + ' has been replaced');
                                mail.setHtmlBody('The Meeting Title '  + subMeet.Name + ' was cancelled and replaced.');
                                mail.setToAddresses(new String[]{[select Email from User where Id =: subMeet.CreatedById].Email});
                                mailLst.add(mail);
                                if(!mailLst.isEmpty()){        
                                  Messaging.sendEmail(mailLst);
                                }
                            }
                    
             }
                    

}