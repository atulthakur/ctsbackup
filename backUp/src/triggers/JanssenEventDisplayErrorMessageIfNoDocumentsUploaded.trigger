/*************************************************************************************
 *
 *  Developer: Khodabocus Zameer (ICBWORKS)
 *  Date Completed: 01.08.2013
 *  Date Modified: 22.08.2013
 *
 *  Trigger to prevent User for setting checkboxes Request letter/Prospectus? and
 *  Agenda Uploaded? to TRUE if no documents have been uploaded.
 *
**************************************************************************************/

trigger JanssenEventDisplayErrorMessageIfNoDocumentsUploaded on Medical_Event_vod__c (before insert) {
    
    for(Medical_Event_vod__c je : trigger.new){
        /*if(je.JJ_Sponsorship_Agreement_Signed__c == true){
            je.JJ_Sponsorship_Agreement_Signed__c.addError(Label.JE_No_Sponsorship_Document_Error);
        }*/
        if(je.JJ_Prospectus_Uploaded__c == true){
            je.JJ_Prospectus_Uploaded__c.addError(Label.JE_No_Request_letter_Prospectus_Error);
        }
        if(je.JJ_Agenda_Uploaded__c == true){
            je.JJ_Agenda_Uploaded__c.addError(Label.JE_No_Agenda_Uploaded_Error);
        }
    }
}