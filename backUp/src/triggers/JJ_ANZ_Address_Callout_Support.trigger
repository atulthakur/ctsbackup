/**
*   Modified by: Khodabocus Zameer (ICBWORKS)
*   Last Date Modified: 14.05.2014
*   Email: zameer.khodabocus@icbworks.com
*   
*   Description:  When brick is changed or new address is linked with an Account, the territory alignment batch gets scheduled 
*				  and jj_anz_callout_trigger__c checkbox is set to true on linked Account.
**/


trigger JJ_ANZ_Address_Callout_Support on Address_vod__c (after insert, after update, before delete) {   
    list<account> acctupdates = new list<account>();
    string acctfilterids = ';';
    if(trigger.isUpdate || trigger.isInsert){
        for (Address_vod__c addr : Trigger.new) {
            
            if ((trigger.isUpdate && (
                addr.brick_vod__c != trigger.oldMap.get(addr.Id).brick_vod__c)) || (
                trigger.isInsert)){
                    
                    account acct = new account(id = addr.account_vod__c , jj_anz_callout_trigger__c = true);
                    
                    if(!acctfilterids.contains(acct.id)){
                    	acctupdates.add(acct);
                    	acctfilterids += acct.id + ';';
                    }
            }
        }
    }
    if (trigger.isDelete){
        for (Address_vod__c deladdr : Trigger.old) {
        	account acct = new account(id = deladdr.account_vod__c , jj_anz_callout_trigger__c = true);
        	
        	if(!acctfilterids.contains(acct.id)){
            	acctupdates.add(acct);
            	acctfilterids += acct.id + ';';
            }
        }
    }
    if (acctupdates.size() > 0){
    	update acctupdates;
    	
    	List<Account_Territory_Alignment_Utilities__c> atlSetting = [select Id, Batch_Scheduled__c, Error__c From Account_Territory_Alignment_Utilities__c limit 1];
    		
		if(atlSetting.size() > 0){
			if(atlSetting[0].Batch_Scheduled__c == false){
				Datetime sysTime = System.now();
	            sysTime = sysTime.addHours(1);
	            
	            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
	            system.debug(chron_exp);
	            
	           	SchedulerBatchRunTerritoryAlignmentAcc newSched = new SchedulerBatchRunTerritoryAlignmentAcc(userInfo.getsessionid(), userInfo.getUserId());
	            //Schedule the next job, and give it the system time so name is unique
	            System.schedule('Run Territory Alignment Batch Job' + sysTime.getTime(), chron_exp, newSched);
	            
	            atlSetting[0].Batch_Scheduled__c = true;
	            atlSetting[0].Error__c = false;
	            
	            update atlSetting[0];
			}
		}
    }
    
}