trigger JJ_ANZ_VeevaMItoCaseSync on Medical_Inquiry_vod__c(after insert, after update, before insert,before update) {

 if (TriggerDisabler__c.getInstance().JJ_ANZ_VeevaMItoCaseSync__c) {return;}   
  
 List < Medical_Inquiry_vod__c > listMIRecords = new List < Medical_Inquiry_vod__c > ();
 Map < String, String > MIRecordTypes = new Map < String, String > {};
 List < RecordType > MIrtypes = [Select Name, Id From RecordType
  where sObjectType = 'Medical_Inquiry_vod__c'
  and isActive = true];
 //Put RecordTypes in a Map
 for (RecordType MIrt: MIrtypes)
  MIRecordTypes.put(MIrt.Name, MIrt.Id);
  
 //Populate ANZ JNJ Employee with the Owner of MI Record
 if (Trigger.isBefore) {
 List<Id> ProductIds = new List<Id>();
 
 for(Medical_Inquiry_vod__c MIRec: Trigger.new)
 {
     
     if(MIRec.JJ_ANZ_Detail_Product__c!=null&&MIRec.recordtypeId == MIRecordTypes.get('iConnect Case'))
     {
         ProductIds.add(MIRec.JJ_ANZ_Detail_Product__c);
     }

     
 }
 
 Map<Id,JJ_ANZ_MAFOPS_Product__c> MAFOPSProducts = new Map<Id,JJ_ANZ_MAFOPS_Product__c>();
 if(!ProductIds.isEmpty())
 {
    MAFOPSProducts = new Map<Id,JJ_ANZ_MAFOPS_Product__c>([Select id,name from JJ_ANZ_MAFOPS_Product__c where id in:ProductIds limit 2000]);
 }
 

for(Medical_Inquiry_vod__c MIRec: Trigger.new)
{
    if(MAFOPSProducts.containsKey(MIRec.JJ_ANZ_Detail_Product__c))
    {
        JJ_ANZ_MAFOPS_Product__c detailProduct = MAFOPSProducts.get(MIRec.JJ_ANZ_Detail_Product__c);
        MIRec.JJ_ANZ_CaseProduct__c = detailProduct.Name;
    }
    if(MIRec.JJ_ANZ_Product_Other__c!=null)
     {
         if(MIRec.JJ_ANZ_Detail_Product__c!=null)
         {
             MIRec.JJ_ANZ_CaseProduct__c = MIRec.JJ_ANZ_CaseProduct__c+ '||' +MIRec.JJ_ANZ_Product_Other__c;
         }
         else
         {
             MIRec.JJ_ANZ_CaseProduct__c = MIRec.JJ_ANZ_Product_Other__c;
         }
         
     }
} 
  for (Medical_Inquiry_vod__c MIRec: Trigger.new) {
   MIRec.JJ_ANZ_JnJ_Employee__c = MIRec.CreatedByID;
    if (MIRec.recordtypeId == MIRecordTypes.get('iConnect Case') && MIRec.Status_vod__c == 'Submitted_vod') {
       // MIRec.Status_vod__c = 'Submitted';
      if(MIRec.JJ_ANZ_JnJ_Notified_Date__c==null)
      if(MIRec.JJ_ANZ_AE__c==false && MIRec.JJ_ANZ_PQC__c==false)
      {
        MIRec.JJ_ANZ_JnJ_Notified_Date__c = System.today();
    }
    }

  }
 }
 


 if(Trigger.isAfter)
 {
    for (Medical_Inquiry_vod__c MIRec: Trigger.new) {
  if (MIRec.recordtypeId == MIRecordTypes.get('iConnect Case') && MIRec.Status_vod__c == 'Submitted_vod') {
   listMIRecords.add(MIRec);
  }
 }

 if (!listMIRecords.isEmpty()) {
  JJ_ANZ_VeevaMItoCaseSync.syncToCase(listMIRecords);
 }
 }
 

}