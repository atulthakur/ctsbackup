/**************************ICBWORKS*********************************************/
//trigger to update print agreement on Speaker Agreement once Event is approved.

trigger updatePrintAgreement on Medical_Event_vod__c (after update) {
    
    set<Id> medIds = new set<Id>();
    
    for(Medical_Event_vod__c m:trigger.new){
        if(m.Process_Status__c == 'Approved' && trigger.oldmap.get(m.Id).Process_Status__c != 'Approved'){
            medIds.add(m.Id);
        }
    }
    
    List<Speaker_Agreement__c> updSpeakerAgrLst = new List<Speaker_Agreement__c>();
    
    for(Account_Event_Association__c a:[Select JJ_Speaker_Agreement__c From Account_Event_Association__c where JJ_Speaker_Agreement__r.JJ_Print_Agreement__c =:false and JJ_Speaker_Agreement__r.JJ_Process_Status__c =:'Approved' and JJ_Janssen_Event__c in: medIds]){
        updSpeakerAgrLst.add(new Speaker_Agreement__c(Id = a.JJ_Speaker_Agreement__c, JJ_Print_Agreement__c = true));    
    }
    
    update updSpeakerAgrLst;
}