/**
* @author Noel Lim, Veeva Systems
* @date 2/03/2017
* @description 
*   1) Update the Progress % on linked Account Objective and Account Objective's linked Strategic Objective
*
*/
trigger JJ_ANZ_AccountTacticTrigger on Account_Tactic_vod__c (after update, after insert, after delete) {
    
    Set<Id> acctObjIds = new Set<Id>();
    
    if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
        //==  1) Progress % update ===
        for( Id acctTacticId : Trigger.newMap.keySet() )
        {
          Boolean newComplete = Trigger.newMap.get( acctTacticId ).Complete_vod__c;
          Id AcctObjId = Trigger.newMap.get( acctTacticId ).JJ_Objective__c;

            if(trigger.isUpdate){
                if( Trigger.oldMap.get( acctTacticId ).Complete_vod__c != newComplete ) //Was marked as Complete
                {               
                  acctObjIds.add(AcctObjId);
                }
            }          
          
            if(trigger.isInsert){
                acctObjIds.add(AcctObjId);
            }
        }

    }

    if(trigger.isAfter && trigger.isDelete){
        for(Id acctTacticId : Trigger.oldMap.keySet()){
            acctObjIds.add(Trigger.oldMap.get(acctTacticId).JJ_Objective__c);
        }
    }


    
    //1)
    if(acctObjIds.size() > 0){                  
          List<Account_Objectives__c> lstAcctObjs = 
                          [SELECT Id, JJ_Strategy_Objective__c, JJ_Progress_Percent__c 
                           FROM Account_Objectives__c 
                           WHERE Id IN :acctObjIds];  
                           
          List<Account_Tactic_vod__c> lstAllAccountTactics = [SELECT Id, JJ_Objective__c, Complete_vod__c 
                                                              FROM Account_Tactic_vod__c 
                                                              WHERE JJ_Objective__c IN :acctObjIds];
          Set<Id> stratObjIds = new Set<Id>();                                                   
          for(  Account_Objectives__c acctObj : lstAcctObjs){
              stratObjIds.add(acctObj.JJ_Strategy_Objective__c);
          }
                                                              
          List<JJ_Brand_Objective__c> lstStrategyObjs = [SELECT Id, JJ_Progress_Percent__c
                                                                FROM JJ_Brand_Objective__c
                                                                WHERE Id IN :stratObjIds ];
                                                                
          
          for(JJ_Brand_Objective__c stratObj : lstStrategyObjs){
              
              integer countAcctObj = 0;
              double sumAcctObjProgress = 0;
              for(Account_Objectives__c acctObj : lstAcctObjs){
                  countAcctObj++;
              
                  integer countAcctTactic = 0;
                  integer countCompletedTactics = 0;
                  for(Account_Tactic_vod__c acctTactic : lstAllAccountTactics){
                      countAcctTactic++;
                      if(acctTactic.Complete_vod__c)
                          countCompletedTactics++;
                  }
                  
                  if(countAcctTactic > 0){
                      acctObj.JJ_Progress_Percent__c = (countCompletedTactics*100)/countAcctTactic;
                      sumAcctObjProgress += acctObj.JJ_Progress_Percent__c;
                  }
              }
              
              if(countAcctObj > 0){
                  stratObj.JJ_Progress_Percent__c = (sumAcctObjProgress/countAcctObj);
              }
          }
          
          update lstAcctObjs;
          update lstStrategyObjs;
    }
       
    
}