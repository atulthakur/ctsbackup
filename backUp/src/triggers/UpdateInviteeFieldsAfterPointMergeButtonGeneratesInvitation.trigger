trigger UpdateInviteeFieldsAfterPointMergeButtonGeneratesInvitation on FeedItem (after insert) {
    
    Set<Id> medicalEventId = new Set<Id>();
    List<Event_Attendee_vod__c> evAttUpdateList = new List<Event_Attendee_vod__c>();
    String invMethod = '';
    
    for(FeedItem att : trigger.new){
        if(att.Title != null && att.Title.contains('Personal invitations') && att.ParentId != null){
            medicalEventId.add(att.ParentId);
            if(att.Title.contains('hand delivery')){
                invMethod = 'Hand Delivered';
            }
            else if(att.Title.contains('SOSA')){
                invMethod = 'Mail by SOSA';
            }
        }
    }
    
    if(medicalEventId.size() > 0){
        List<Event_Attendee_vod__c> invitees = [select Id, JJ_Invite_Generated__c, JJ_Invite_Generated_Date__c, 
                                                JJ_Invite_Method__c, Medical_Event_vod__c
                                                from Event_Attendee_vod__c 
                                                where Medical_Event_vod__c in :medicalEventId AND JJ_Invite_Method__c=:invMethod 
                                                AND JJ_Invite_Generated__c = false];
                                                
        for(Event_Attendee_vod__c evAtt : invitees){
            evAtt.JJ_Invite_Generated__c = true;
            evAtt.JJ_Invite_Generated_Date__c = date.today();
            
            evAttUpdateList.add(evAtt);
        }
        
        if(evAttUpdateList.size() > 0){
            update evAttUpdateList;
        }
    }
}