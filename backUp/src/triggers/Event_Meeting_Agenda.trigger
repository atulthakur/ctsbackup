/**
* Trigger for the event agenda object
* Created: June 13 2013
* Modified July 9 2013
* Bluewolf 
**/

/************************************************************
Related requirements: DMP1 - Validations to the event agenda
*************************************************************/
trigger Event_Meeting_Agenda on Janssen_Event_Agenda__c (after insert,after update) {   
    Double eduTime=0.0;
    ID evId=Trigger.new.get(0).Janssen_Event__c;
    
    String[] tmp=null;          
    Datetime myDateTime=null;
    
    //Indicates if dinner was found for a dinner event
    Boolean dinnerFound=False;
    Boolean inProcess=True;
    Boolean foundReg=False;
    
    //get a ref to the event    
    Medical_Event_vod__c ev=(Medical_Event_vod__c)[SELECT Id, Educational_Content_Duration__c,Process_Status__c,
                                                   Cost_Per_Person__c FROM Medical_Event_vod__c WHERE ID=:evId];
    
    if(ev.Process_Status__c!='In Process'){
        inProcess=False;    
    }
    
    //Check if it is the insertion of items other than the registration
    if(Trigger.isInsert==True && Trigger.new.size()==1){        
        if(Trigger.new.get(0).Description__c.indexOf('Registration')>-1){
            foundReg=True;
        }       
    }
    
    for(Janssen_Event_Agenda__c item:Trigger.new){      
        //get date to validate if breakfast, lunch or dinner
        if(item.Description__c.indexOf('Registration')>-1){
            tmp=item.Time__c.split(':');
            myDateTime=Datetime.newInstance(2008, 12, 1, Integer.valueOf(tmp[0]),Integer.valueOf(tmp[1]), 0);   
        }   
        //If it is not the insertion of the default registration item, query the object for an existing registration already in the 'DB'
        if(foundReg==False){
            String timer=[SELECT Time__c FROM Janssen_Event_Agenda__c WHERE Janssen_Event__c=:ev.Id AND Description__c LIKE 'Registration%'].get(0).Time__c;
            tmp=timer.split(':');
            myDateTime=Datetime.newInstance(2008, 12, 1, Integer.valueOf(tmp[0]),Integer.valueOf(tmp[1]), 0);
        }
        //Dinner
        if(myDateTime.hour()>=17){
            if(item.Description__c=='Dinner'&&(item.Time_Slot__c==NULL)){
                item.addError('A minimum of 15 minutes should be allowed for dinner');
            }
        }       
        //Lunch & Breakfast     
        if(myDateTime.hour()<17){
            if(item.Description__c=='Dessert'){
                item.addError('Breakfast and lunch agendas should not contain dessert elements');
            }
            if(item.Description__c=='Dinner'){
                item.addError('Breakfast and lunch agendas should not contain dinner elements');
            }
            if(item.Description__c=='Entrée served'){
                item.addError('Breakfast and lunch agendas should not contain entrée elements');
            }
        }
    }
    
    //Check for cost per person depending on type of event (after 5pm dinner, otherwise Breakfast or lunch)
    Double costPP=ev.Cost_Per_Person__c;
    
    if(costPP!=NULL){
        if(myDateTime.hour()>=17&&costPP>100.0){        
            Trigger.new.get(0).addError('Cost per person exceed the maximum amount of $100');
        }
        else{
            if(myDateTime.hour()<17&&costPP>45.0){
                Trigger.new.get(0).addError('Cost per person exceed the maximum amount of $45');
            }
        }
    }
    
    //Update the edu time of the event and check dinner
    
    for(Janssen_Event_Agenda__c elem:[SELECT Description__c,Time_Slot__c FROM Janssen_Event_Agenda__c WHERE Janssen_Event__c=:evId AND Description__c IN ('Dinner')]){      
        if(elem.Description__c=='Dinner'){
            dinnerFound=True;
        }
    }   

    if(myDateTime.hour()>=17&&dinnerFound==False&&inProcess==False){
        Trigger.new.get(0).addError('Dinner missing in dinner event');
    }
        
}