trigger EmailMessageTrigger on EmailMessage (after insert, after update, after delete, after undelete) {

    Set<Id> caseIds = new Set<Id>();
    
    if(Trigger.isDelete) {
        
        for(EmailMessage eMsg : Trigger.Old) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdateCaseNewEmailwithEmailStatus(caseIds);  
    }
    else if(Trigger.isUpdate) {
        
        for(EmailMessage eMsg : Trigger.New) {
            caseIds.add(eMsg.ParentId);  
        }

        for(EmailMessage eMsg : Trigger.Old) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdateCaseNewEmailwithEmailStatus(caseIds);  
    }
    else{
        
        for(EmailMessage eMsg : Trigger.New) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdateCaseNewEmailwithEmailStatus(caseIds);
    }
    
}