/*
* This trigger creates BRC Tracking whenever a Sample Order Transaction is being created.
* Author: Hasseeb Mungroo
* Date Created: 20/03/2014
*/
trigger AutoCreateSampleBRCTracking on Sample_Order_Transaction_vod__c (after insert) {
	
    SampleOrderTransactionUtility.createSampleBRCTracking(Trigger.New);
}