trigger JJ_ANZ_CallDiscussion_Complete on Call2_Discussion_vod__c (after insert, after update) {
 for(Call2_Discussion_vod__c v_CallDisc: Trigger.new){
           if(v_CallDisc.JJ_ANZ_Completed__c == true ){
                //system.debug('CPM 14 vCall2.Status_vod__c ' + vCall2.Status_vod__c);
                //vId = v_CallDisc.Id;
                //boolean vAccTacticComp = false;
                List<Account_Tactic_vod__c> cLookupAccTactic = new List<Account_Tactic_vod__c>();
                    cLookupAccTactic = [SELECT     Id, Complete_vod__c 
                                        FROM    Account_Tactic_vod__c 
                                        WHERE   Id = :v_CallDisc.Account_Tactic_vod__c];
                If (cLookupAccTactic.size() > 0){                                  
                    //vAccTacticComp = cLookupAccTactic[0].Complete_vod__c;
                    cLookupAccTactic[0].Complete_vod__c = true;
                    update cLookupAccTactic;}
                 }
            } 
}