/*
* This trigger fills up the practice name on the Sample Order Transaction
* Author: Hasseeb Mungroo
* Date Created: 20/03/2014
* Change:
* Added class call to copy Sample delivery details on to the SOT
* Author: Paul Hosking 26/06/2014
*/
trigger JJ_ANZ_SOTGetAdditionalCallDetails on Sample_Order_Transaction_vod__c (before insert) {
    SampleOrderTransactionUtility.setPracticeName(Trigger.New);
    SampleOrderTransactionUtility.setSampleDeliveryDetails(Trigger.New);
}