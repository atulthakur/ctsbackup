/*****
***     Developed by: Khodabocus Zameer (ICBWORKS)
***     Description: This trigger is used to share visibility of accounts (JJ_Account__c) with:
***                  1) Janssen Event Owner on the Janssen Event,
***                  2) Rep Delegate 2 on the Janssen Event,
***                  3) Speaker Liason on Speaker Agreement,
***                  
***                  when a new account is associated with an Event.
*****/


trigger ShareAccountsAfterUpsertAccountEventAssociation on Account_Event_Association__c (after insert) {
    
    Set<Id> jEventsId = new Set<Id>();
    Set<Id> speakersId = new Set<Id>();
    Map<Id, Medical_Event_vod__c> medicalEventsMap = new Map<Id, Medical_Event_vod__c>();
    Map<Id, Speaker_Agreement__c> speakerAgreementsMap = new Map<Id, Speaker_Agreement__c>();
    List<AccountShare> newAccountsShareList = new List<AccountShare>();
    
    if(trigger.isInsert){
        for(Account_Event_Association__c aea : trigger.new){
            //set of Janssen Events Id
            jEventsId.add(aea.JJ_Janssen_Event__c);
            //set of Speaker Agreements Id
            speakersId.add(aea.JJ_Speaker_Agreement__c);
        }
        
        if(jEventsId.size() > 0){
            //Getting the Owner and Rep Delegate 2 of the related Janssen Events
            List<Medical_Event_vod__c> janssenEvents = [select Id, OwnerId, Rep_Delegate_2__c 
                                                        from Medical_Event_vod__c
                                                        where Id in :jEventsId];
                                                        
            for(Medical_Event_vod__c me : janssenEvents){
                //Map of the Janssen Events
                medicalEventsMap.put(me.Id, me);
            }
        }
        
        if(speakersId.size() > 0){
            //Getting the Speaker Liason of the related Speaker Agreements
            List<Speaker_Agreement__c> speakerAgreements = [select Id, JJ_Speaker__c, JJ_Speaker_Liason__c 
                                                            From Speaker_Agreement__c
                                                            where Id in :speakersId];
                                                            
            for(Speaker_Agreement__c spa : speakerAgreements){
                //map of the Speaker Agreements
                speakerAgreementsMap.put(spa.Id, spa);
            }
        }
        
        for(Account_Event_Association__c aea : trigger.new){
            if(aea.JJ_Account__c != null && aea.JJ_Janssen_Event__c != null){
                if(medicalEventsMap.size() > 0){
                    //Getting the Janssen Event record related to this new Account Event Association using the Map created above
                    Medical_Event_vod__c jEvent = medicalEventsMap.get(aea.JJ_Janssen_Event__c);
                    
                    if(jEvent != null){
                        //New Account share with the Owner of the Janssen Event
                        AccountShare newOwnerAccShare = new AccountShare();
                            
                        newOwnerAccShare.AccountId = aea.JJ_Account__c;
                        newOwnerAccShare.UserOrGroupId = jEvent.OwnerId;
                        newOwnerAccShare.OpportunityAccessLevel = 'None';
                        
                        newOwnerAccShare.AccountAccessLevel = 'Edit';
                        
                        newAccountsShareList.add(newOwnerAccShare);
                        
                        if(jEvent.Rep_Delegate_2__c != null){
                            //New Account share with the Rep Delegate 2 of the Janssen Event
                            AccountShare newRepDelegate2AccShare = new AccountShare();
                                
                            newRepDelegate2AccShare.AccountId = aea.JJ_Account__c;
                            newRepDelegate2AccShare.UserOrGroupId = jEvent.Rep_Delegate_2__c;
                            newRepDelegate2AccShare.OpportunityAccessLevel = 'None';
                            
                            newRepDelegate2AccShare.AccountAccessLevel = 'Edit';
                            
                            newAccountsShareList.add(newRepDelegate2AccShare);
                        }
                    }
                }
                
                if(speakerAgreementsMap.size() > 0){
                    if(aea.JJ_Speaker_Agreement__c != null){
                        //Getting the Speaker Agreement record related to this new Account Event Association using the Map created above
                        Speaker_Agreement__c speakerAgreement = speakerAgreementsMap.get(aea.JJ_Speaker_Agreement__c);
                        
                        if(speakerAgreement != null && speakerAgreement.JJ_Speaker_Liason__c != null){
                            //New Account share with the Speaker Liason of the Speaker Agreement
                            AccountShare newSpLiasonAccShare = new AccountShare();
                            
                            newSpLiasonAccShare.AccountId = aea.JJ_Account__c;
                            newSpLiasonAccShare.UserOrGroupId = speakerAgreement.JJ_Speaker_Liason__c;
                            newSpLiasonAccShare.OpportunityAccessLevel = 'None';
                            
                            newSpLiasonAccShare.AccountAccessLevel = 'Edit';
                            
                            newAccountsShareList.add(newSpLiasonAccShare);
                        }
                    }
                }
            }
        }
        
        if(newAccountsShareList.size() > 0){
            //insertng the new account shares
            insert newAccountsShareList;
        }
    }
}