/**
* Trigger for the predefined title templates
* Creates all the possible combinations for the new template, for all the esxixting P, PG, BU, DS, and SP in the 
* predefined meeting titles osetup object
* Created: July 15 2013
* Modified July 16 2013
* Bluewolf 
**/

/************************************************************
Related requirements: DMP1A (Item 63)
*************************************************************/
trigger Title_Template_Trigger on Title_Templates__c (after insert, after update) {
	List<AggregateResult> existingInsert= new List<AggregateResult>();
	List<Meeting_Titles_Config__c> newCombinedTitles= new List<Meeting_Titles_Config__c>();
	List<Meeting_Titles_Config__c> updatedCombinedTitles= new List<Meeting_Titles_Config__c>();
	String aux='';
	Meeting_Titles_Config__c mt=new Meeting_Titles_Config__c();
	
	if(Trigger.isInsert){
		existingInsert=[SELECT JJ_Business_Unit__c,JJ_Product__c,JJ_Disease_State__c,JJ_Product_Group__c, JJ_Specialty_Area__c
			 FROM Meeting_Titles_Config__c  GROUP BY JJ_Business_Unit__c,JJ_Product__c,JJ_Disease_State__c,JJ_Product_Group__c,JJ_Specialty_Area__c ];
	}
	
	if(Trigger.isUpdate){
		updatedCombinedTitles=[SELECT Id,JJ_Title_Template__c,JJ_Business_Unit__c,JJ_Product__c,JJ_Disease_State__c,JJ_Product_Group__c, JJ_Specialty_Area__c
			 FROM Meeting_Titles_Config__c  WHERE JJ_Title_Template__c IN:Trigger.new];
	}
			 
	for(Title_Templates__c tt:Trigger.new){
		if(Trigger.isInsert){
			for(AggregateResult agg:existingInsert){
				mt=new Meeting_Titles_Config__c();
				mt.JJ_Title_Template__c=tt.Id;
				mt.JJ_Disease_State__c=(String)agg.get('JJ_Disease_State__c');
				mt.JJ_Product_Group__c=(String)agg.get('JJ_Product_Group__c');
				mt.JJ_Specialty_Area__c=(String)agg.get('JJ_Specialty_Area__c');
				mt.JJ_Business_Unit__c=(String)agg.get('JJ_Business_Unit__c');
				mt.JJ_Product__c=(String)agg.get('JJ_Product__c');
				aux=tt.JJ_Template__c.replace('[DS]',mt.JJ_Disease_State__c).replace('[PG]',mt.JJ_Product_Group__c).
					replace('[P]',mt.JJ_Product__c).replace('[SP]',mt.JJ_Specialty_Area__c);
				mt.JJ_Combined_Title__c=aux;
				mt.JJ_Cloned__c=True;//avoid llops and generate only the combinations for the new title
				newCombinedTitles.add(mt);
			}
		}
		if(Trigger.isUpdate){
			for(Meeting_Titles_Config__c mt1:updatedCombinedTitles){
				if(mt1.JJ_Title_Template__c==tt.Id){
					aux=tt.JJ_Template__c.replace('[DS]',mt1.JJ_Disease_State__c).replace('[PG]',mt1.JJ_Product_Group__c).
						replace('[P]',mt1.JJ_Product__c).replace('[SP]',mt1.JJ_Specialty_Area__c);
					mt1.JJ_Combined_Title__c=aux;		
				}
			}
		}
	}
	try{
		insert newCombinedTitles;
		update updatedCombinedTitles;
	}catch(Exception e){
		System.debug(e);
	}
}