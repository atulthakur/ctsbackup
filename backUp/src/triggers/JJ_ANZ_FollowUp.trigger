trigger JJ_ANZ_FollowUp on JJ_ANZ_Follow_Up_Version__c (before update,before delete) {

JJ_ANZ_FollowUpTriggerHandler handler = new JJ_ANZ_FollowUpTriggerHandler();

    /* Before Insert
    if(Trigger.isInsert && Trigger.isBefore){
     
        handler.OnBeforeInsert(Trigger.new);
    } */
    
    /* After Insert
    else if(Trigger.isInsert && Trigger.isAfter){
     
        handler.OnAfterInsert(Trigger.new);
    } */
    
   /* Before Update */
    
    if(Trigger.isUpdate && Trigger.isBefore)
    {    
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    }
    /* Before Delete*/
    else if(trigger.isBefore && trigger.isDelete)
    {     
        handler.OnBeforeDelete(Trigger.old,Trigger.oldMap);
    }

    }