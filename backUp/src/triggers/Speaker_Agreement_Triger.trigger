/**
* Trigger to update events related to the agreement and calculate the end date for a 12 month agreement
* Created: June 22 2013
* Modified July 25 2013
* Bluewolf
**/
trigger Speaker_Agreement_Triger on Speaker_Agreement__c (after insert, before update) {
    List<Medical_Event_vod__c> events=new List<Medical_Event_vod__c>();
    List<Account_Event_Association__c> evLinks=new List<Account_Event_Association__c>();
    Set<ID> evId=new Set<ID>();
    Set<ID> updateAccountId=new Set<ID>();
    List<Medical_Event_vod__c> updates=new List<Medical_Event_vod__c>();
    
    for(Account_Event_Association__c asso :[SELECT JJ_Speaker_Agreement__c,JJ_Janssen_Event__c FROM Account_Event_Association__c
            WHERE JJ_Speaker_Agreement__c IN:Trigger.new]){
        evLinks.add(asso);
        evId.add(asso.JJ_Janssen_Event__c);
    }
    events=[SELECT Id,JJ_Speaker_Signature_Obtained__c,Start_Date_vod__c FROM Medical_Event_vod__c WHERE ID IN:evId];

    for(Speaker_Agreement__c ag:Trigger.new){
        //If signature received
        if(Trigger.new.get(0).JJ_Pending_Signature__c==True){           
            for(Account_Event_Association__c asso:evLinks){
                if(ag.Id==asso.JJ_Speaker_Agreement__c){
                    for(Medical_Event_vod__c ev:events){                            
                        if(asso.JJ_Janssen_Event__c==ev.Id){
                            ev.JJ_Speaker_Signature_Obtained__c=True;
                            updates.add(ev);
                        }                           
                    }
                }
            }
        }
        
        //If approved   -date update
        if(ag.JJ_Process_Status__c=='Approved'&&Trigger.isBefore){      
            //Update account later
            if(ag.JJ_Speaker_HCC_Status__c==False){
                updateAccountId.add(ag.JJ_Speaker__c);
            }   
            if(ag.JJ_HCC_Grace_Valid__c==False){
                updateAccountId.add(ag.JJ_Speaker__c);
            }
            //If 12 month
            if(ag.JJ_Type__c=='12 Month'){
                ag.JJ_Start_Date__c=Date.today();
                ag.JJ_End_Date__c=Trigger.new.get(0).JJ_Start_Date__c.addDays(364);
            }           
            //If one-Off set start and end to event date
            if(Trigger.new.get(0).JJ_Type__c=='One-Off'){
                try{
                    for(Account_Event_Association__c asso:evLinks){
                        if(ag.Id==asso.JJ_Speaker_Agreement__c){
                            for(Medical_Event_vod__c ev:events){                            
                                if(asso.JJ_Janssen_Event__c==ev.Id){
                                    ag.JJ_Start_Date__c=ev.Start_Date_vod__c;
                                    ag.JJ_End_Date__c=ev.Start_Date_vod__c;
                                }                           
                            }
                        }
                    }
                }catch(Exception e){
                    System.debug(e);
                }
            }   
        }   
    }               
    //Mass update events
    try{
        update updates;
    }catch(Exception e){
        System.debug(e);
    }
    //mass update accounts for HCC approval=True if agreement was approved
    List<Account> updateAcc=[SELECT Id, JJ_HCC_Approved__c,JJ_HCC_Expiration__c,JJ_HCC_Expiration_Grace__c FROM Account WHERE Id IN:updateAccountId];
    //try{
        if(updateAcc!=NULL){
            for(Account ac:updateAcc){
                if(ac.JJ_HCC_Approved__c=='Approved'){
                    ac.JJ_HCC_Expiration__c=Date.today().addMonths(36);
                    ac.JJ_HCC_Expiration_Grace__c=Date.today().addMonths(37);
                }
                else{
                    ac.JJ_HCC_Approved__c='Approved';
                }
            }   
            update updateAcc;
        }
    //}catch(Exception e){
    //  System.debug(e);
    //}
}