/**
 *      @author    RAVITEJA CHERUKURI     
 *      @date      11/06/2015      
        @name           
        @description This trigger is used to create a multichannel consent record, if the Account.Email is changed and
                     the existing Account.Email and latest multichannel consent record's 'Channel Value' is same.   
        
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
         Raviteja CH                  11/08/2015         This trigger is used to create a multichannel consent record, if the Account.Email is changed and
                                                         the existing Account.Email and latest multichannel consent record's 'Channel Value' is same. 
 
*/
trigger JJ_ANZ_Create_MultiChannel_Consent on Account (after update) {
   
    list<Multichannel_Consent_vod__c> MccsToCreate = new list<Multichannel_Consent_vod__c>();
    map<id,list<Multichannel_Consent_vod__c>> accountmccmap = new map<id,list<Multichannel_Consent_vod__c>>();
    set<id> accids = new set<id>();
    for(account acc:Trigger.New){
       accids.add(acc.id); 
    }
    
     List<Account> listAcc = new List<Account>([select ID, PersonEmail, JJ_Core_Marketing_Contact__c from Account where id in: accids]);
    List<Account> AccToUpdate = new List<Account>();
    for (integer i = 0; i < listAcc.size(); i++){ 
       if(listAcc[i].PersonEmail != null){
           if(listAcc[i].JJ_Core_Marketing_Contact__c != true){
                Account acc = new Account();
                acc.ID = listAcc[i].ID;
                acc.JJ_Core_Marketing_Contact__c = true;
                AccToUpdate.add(acc);
           }
       }
    }
    
    
   
    list<Multichannel_Consent_vod__c> mccs = new list<Multichannel_Consent_vod__c>([select Account_vod__c, Channel_Value_vod__c,Product_vod__c, Detail_Group_vod__c, Signature_Datetime_vod__c, Opt_Type_vod__c, Optout_Event_Type_vod__c, Signature_ID_vod__c,Capture_Datetime_vod__c,Last_Device_vod__c,JJ_ANZ_Portals__c,JJ_ANZ_OtherSource__c,JJ_ANZ_Event__c,JJ_ANZ_Comments__c from Multichannel_Consent_vod__c Where Account_vod__c in: accids Order by Capture_Datetime_vod__c desc]);
    
    
    for(Multichannel_Consent_vod__c mc : mccs){
        if(!accountmccmap.containsKey(mc.Account_vod__c)) {
            accountmccmap.put(mc.Account_vod__c,new list<Multichannel_Consent_vod__c>{mc});
        } else {
           accountmccmap.get(mc.Account_vod__c).add(mc); 
        }  
    }
    
    
    for( Id accountId : Trigger.newMap.keySet())
    {
        String OldAccEmail = Trigger.oldMap.get( accountId ).PersonEmail;
        String NewAccEmail = Trigger.newMap.get( accountId ).PersonEmail;
                
            if(NewAccEmail != null && OldAccEmail != NewAccEmail && accountmccmap.size() > 0)
         { 
            Multichannel_Consent_vod__c mcinfo = accountmccmap.get(accountid)[0];          
            if(OldAccEmail == mcinfo.Channel_Value_vod__c || (OldAccEmail== Null && NewAccEmail != mcinfo.Channel_Value_vod__c))              
            {
            Multichannel_Consent_vod__c mcc = new Multichannel_Consent_vod__c();
            mcc.Account_vod__c = accountid;
            mcc.Channel_Value_vod__c = Trigger.newMap.get( accountId ).PersonEmail;
            mcc.Product_vod__c = mcinfo.Product_vod__c;
            mcc.Detail_Group_vod__c = mcinfo.Detail_Group_vod__c;
            mcc.Opt_Type_vod__c = mcinfo.Opt_Type_vod__c;
            mcc.Signature_Datetime_vod__c = mcinfo.Signature_Datetime_vod__c;
            mcc.Optout_Event_Type_vod__c = mcinfo.Optout_Event_Type_vod__c;
            mcc.Signature_ID_vod__c = mcinfo.Signature_ID_vod__c;                       
            mcc.Capture_Datetime_vod__c = mcinfo.Capture_Datetime_vod__c + decimal.valueof(Label.Capture_Date_Time);
            mcc.Last_Device_vod__c  = mcinfo.Last_Device_vod__c;
            mcc.JJ_ANZ_Portals__c = mcinfo.JJ_ANZ_Portals__c;
            mcc.JJ_ANZ_OtherSource__c = mcinfo.JJ_ANZ_OtherSource__c;
            mcc.JJ_ANZ_Event__c= mcinfo.JJ_ANZ_Event__c;
            mcc.JJ_ANZ_Comments__c = mcinfo.JJ_ANZ_Comments__c;
            MccsToCreate.add(mcc);            
            }       
        }       
    }
    update AccToUpdate;
    insert MccsToCreate; 
}