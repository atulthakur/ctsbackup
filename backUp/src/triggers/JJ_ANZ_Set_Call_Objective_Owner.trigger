trigger JJ_ANZ_Set_Call_Objective_Owner on Call_Objective_vod__c (before insert, before update) {

    // If Call Objective is assigned to another user, change OwnerID

    for(Call_Objective_vod__c ao:Trigger.new) {
   
      if(ao.OwnerId != ao.JJ_ANZ_Call_Objective_Owner__c && ao.JJ_ANZ_Call_Objective_Owner__c != null) {
        ao.OwnerId= ao.JJ_ANZ_Call_Objective_Owner__c;               
      }
    }
}