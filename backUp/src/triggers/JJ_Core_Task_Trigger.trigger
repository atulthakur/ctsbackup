/**
 *      @author         Simon Roggeman
 *      @date           22/05/2015
        @name           JJ_Core_Task_Trigger
        @description    Trigger on Task
 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer               Date                        Description
        ------------------------------------------------------------------------------------
        Simon Roggeman          22/05/2015                  Added logic to create campaign target records upon campaign response
 */
trigger JJ_Core_Task_Trigger on Task (after insert, after update) {
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            
            // list containing all marketing activities
            List<Task> marketingActivities = new List<Task>();
            
            for (Task task : Trigger.new) {
                //checking if marketing activity and if campaign id properly filled out
                if(task.Type != null && task.JJ_Core_SYSTEM_Campaign_Id__c != null && task.WhatId != null) {
                    
                    //Save as string to check type (by object prefix)
                    String campaignId = task.JJ_Core_SYSTEM_Campaign_Id__c;
                    String accountId = task.WhatId;
                    
                    if(task.Type.startsWith('Marketing') 
                        && campaignId.startsWith(Campaign_vod__c.sObjectType.getDescribe().getKeyPrefix())
                        && accountId.startsWith(Account.sObjectType.getDescribe().getKeyPrefix()))
                    {
                        marketingActivities.add(task);
                    }
                }               
            }
            
            //Handle responses
            JJ_Core_Task_TriggerFunctions.handleCampaignResponse(marketingActivities);
        }
    }
}