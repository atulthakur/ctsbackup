/**
 * 1) R-8028 - assign the 1 current KOL Account Plan to the KOL Account. To achieve >> from Account Plan Key Stakeholders, display a link to the active KOL plan if it exists
 * 
 * @author noel.lim@veeva.com 2017-05-11
 */
trigger JJ_AccountPlan_Triggers on Account_Plan_vod__c (after insert, after update) {
    
    
    Set<Id> acctIds = new Set<Id>();
    Set<Id> personAcctIds = new Set<Id>();
    
    for(Account_Plan_vod__c ap: Trigger.new){
        acctIds.add(ap.Account_vod__c);
    }
    
    List<Account> personAccounts = [SELECT Id, JJ_Current_KOL_Plan__c FROM Account WHERE Id IN :acctIds AND IsPersonAccount = true];
    
    for(Account personAcct: personAccounts){
        personAcctIds.add(personAcct.Id);
    } 
    
    List<Account_Plan_vod__c> allAccountPlans = [SELECT Id, Account_vod__c, CreatedDate FROM Account_Plan_vod__c 
                                                WHERE Account_vod__c IN :personAcctIds AND Active_vod__c = TRUE ORDER BY CreatedDate DESC];
    
    //System.debug('AP count - ' + allAccountPlans.size());
    
     // 1)                                           
    for(Account personAcct: personAccounts){
        Boolean acctPlanAssigned = false;
        for(Account_Plan_vod__c acctPlan : allAccountPlans){
            if(acctPlan.Account_vod__c == personAcct.Id && !acctPlanAssigned){
                personAcct.JJ_Current_KOL_Plan__c = acctPlan.Id;
                acctPlanAssigned = true;
            }
        }  
        if(!acctPlanAssigned){
            personAcct.JJ_Current_KOL_Plan__c = null;
        }
    } 
    
    update personAccounts;
                                           
}