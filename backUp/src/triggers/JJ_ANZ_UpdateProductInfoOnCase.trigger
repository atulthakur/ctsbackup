trigger JJ_ANZ_UpdateProductInfoOnCase on JJ_Case_Product__c (after insert,before insert,before update,before delete) {

JJ_ANZ_CaseProductTriggerHandler handler = new JJ_ANZ_CaseProductTriggerHandler();

/* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
     
        handler.OnBeforeInsert(Trigger.new);
    }
    /* After Insert */
    else if(Trigger.isInsert && Trigger.isAfter){
     
        System.debug('After Insert Call Product');
        handler.OnAfterInsert(Trigger.new);
    }
    /* Before Update */
    else if(Trigger.isUpdate && Trigger.isBefore){
    
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    }
    /* Before Delete*/
    else if(trigger.isBefore && trigger.isDelete){
     
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }

    }