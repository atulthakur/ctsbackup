//developped by Icbworks
//trigger to prevent user edit/delete agenda when event status is not Planning or Rejected
trigger JJ_ANZ_Event_Agenda_ValidationError on Janssen_Event_Agenda__c (before delete, before update) {
    
    string errormsgstr = '';
    for(Message_vod__c m:[Select Text_vod__c From Message_vod__c where Name=:'Event Agenda Cannot Modify' and Category_vod__c =:'Events' and Active_vod__c =:true]){
        errormsgstr = m.Text_vod__c;
    }
    
    //get all events related to the agenda
    set<Id> eventids = new set<Id>();    
    if(trigger.isUpdate){
        for(Janssen_Event_Agenda__c jea:trigger.new){
            eventids.add(jea.Janssen_Event__c);
        }
    }
    else if(trigger.isDelete){    
        for(Janssen_Event_Agenda__c jea:trigger.old){
            eventids.add(jea.Janssen_Event__c);     
        }
    }
    
    //event record types which cannot modify the agenda
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Events_Agenda_Lock_1__c, JJ_ANZ_Events_Agenda_Lock_2__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.JJ_ANZ_Events_Agenda_Lock_1__c == null)?'':je.JJ_ANZ_Events_Agenda_Lock_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Events_Agenda_Lock_2__c == null)?'':je.JJ_ANZ_Events_Agenda_Lock_2__c;
        jerectype.addAll(s.split(';'));
    }
    
    //event status which cannot modify the agenda
    set<string> setstatus = new set<String>(); 
    setstatus.add('Awaiting Approval');
    setstatus.add('Approved');
    setstatus.add('Close Out');
    setstatus.add('Reconciliation');
    setstatus.add('Completed'); 
    
    //check the event status and record type to see if there should be a validation error
    map<Id, boolean> mapvalerror = new map<Id, boolean>();
    for(Medical_Event_vod__c e:[select Id from Medical_Event_vod__c where Id in:eventids and RecordType.DeveloperName in:jerectype and JJ_ANZ_Event_Status__c in:setstatus]){
        mapvalerror.put(e.Id, true);
    }
        
    //display error in case there is validation error
    if(trigger.isUpdate){
        for(Janssen_Event_Agenda__c jea:trigger.new){
            if(mapvalerror.get(jea.Janssen_Event__c) != null){
                jea.addError(errormsgstr);
            }
        }
    }
    else if(trigger.isDelete){    
        for(Janssen_Event_Agenda__c jea:trigger.old){
            if(mapvalerror.get(jea.Janssen_Event__c) != null){
                jea.addError(errormsgstr);
            }
        }
    }
    
    
}