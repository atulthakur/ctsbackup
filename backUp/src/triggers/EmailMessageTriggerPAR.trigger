/** Subject  : EmailMessageTriggerPAR 

Description  : Method To update field 'New Email PAR' in parent object (Case) of EmailMessage to update PAR status.
Author       :Sunita & Shanmuga || schand63@its.jnj.com & SPriyan3@its.jnj.com ||30/11/2017

**/


trigger EmailMessageTriggerPAR on EmailMessage (after insert, after update, after delete, after undelete) {

    Set<Id> caseIds = new Set<Id>();
    
    if(Trigger.isDelete) {
        
        for(EmailMessage eMsg : Trigger.Old) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdatePARCaseNewEmailwithEmailStatus(caseIds);  
    }
    else if(Trigger.isUpdate) {
        
        for(EmailMessage eMsg : Trigger.New) {
            caseIds.add(eMsg.ParentId);  
        }

        for(EmailMessage eMsg : Trigger.Old) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdatePARCaseNewEmailwithEmailStatus(caseIds);  
    }
    else{
        
        for(EmailMessage eMsg : Trigger.New) {
            caseIds.add(eMsg.ParentId);  
        }
        EmailMessageHandler.UpdatePARCaseNewEmailwithEmailStatus(caseIds);
    }
    
}