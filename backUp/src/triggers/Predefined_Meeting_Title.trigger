/**
* Trigger for the predefined title configuration
* Creates all possible title onfigurations on creation of a record
* Created: June 24 2013
* Modified July 16 2013
* Bluewolf 
**/


/************************************************************
Related requirements: DMP1A (Item 63)
*************************************************************/
trigger Predefined_Meeting_Title on Meeting_Titles_Config__c (before insert,before update) {
		//Read schema and get all possible title list			
		List<Title_Templates__c> titles = new List<Title_Templates__c>();
		titles=[SELECT ID, 	JJ_Template__c FROM Title_Templates__c ORDER BY JJ_Template__c ASC];
				  
		Meeting_Titles_Config__c tmp, item;   
		String aux='';
		List<Meeting_Titles_Config__c> replacement=new List<Meeting_Titles_Config__c>();
		
		//Setup one title for the inserted elements, then clone and add the remining//..
		for(Integer i=0;i<Trigger.new.size();i++){
			item=Trigger.new.get(i);
			if(Trigger.isInsert&&titles.size()>0){						
				if(item.JJ_Single_Title_Generation__c==False){
					//Avoid cycles and multiple DML statements
					if(item.JJ_Cloned__c==False){									
						if(titles.size()>=1){		
							item.JJ_Title_Template__c=titles.get(0).Id;										
							aux=titles.get(0).JJ_Template__c.replace('[DS]',item.JJ_Disease_State__c).replace('[PG]',item.JJ_Product_Group__c).
								replace('[P]',item.JJ_Product__c).replace('[SP]',item.JJ_Specialty_Area__c);
							item.JJ_Combined_Title__c=aux;
							
							for(Integer j=1;j<titles.size(); j++){					
								tmp=item.clone();	
								tmp.JJ_Title_Template__c=titles.get(j).Id;			
								aux=titles.get(j).JJ_Template__c.replace('[DS]',item.JJ_Disease_State__c).replace('[PG]',item.JJ_Product_Group__c).
									replace('[P]',item.JJ_Product__c).replace('[SP]',item.JJ_Specialty_Area__c);
								tmp.JJ_Combined_Title__c=aux;
								tmp.JJ_Cloned__c=True;
								replacement.add(tmp);
							}									
						}						
					}					
				}
				else{		
					if(item.JJ_Title_Template__c!=NULL){
					   for(Title_Templates__c tt:titles){	
							if(tt.Id==item.JJ_Title_Template__c){
								item.JJ_Combined_Title__c=tt.JJ_Template__c.replace('[DS]',item.JJ_Disease_State__c).replace('[PG]',item.JJ_Product_Group__c).
									replace('[P]',item.JJ_Product__c).replace('[SP]',item.JJ_Specialty_Area__c);
							}
						}
					}				
				}	
			}	
			if(Trigger.isUpdate&&titles.size()>0){	
				for(Title_Templates__c tt:titles){	
					if(tt.Id==item.JJ_Title_Template__c){								
						aux=tt.JJ_Template__c.replace('[DS]',item.JJ_Disease_State__c).replace('[PG]',item.JJ_Product_Group__c).
									replace('[P]',item.JJ_Product__c).replace('[SP]',item.JJ_Specialty_Area__c);
						item.JJ_Combined_Title__c=aux;		
					}
				}		
			}	
		}				
		try{
			insert replacement;
		}catch(Exception e){
			System.debug(e);
		}
}