/**
* @author Noel Lim, Veeva Systems
* @date 16-11-2017
* @description 
* Func1) update Attendee Status based on Sent Email, or create new Attendee. Its possible 1 Sent email and it's Fragments can create Attendee to 2+ events.
*    - For Sent Email where its Approved Document is linked to an Event. 
*    - For each Sent Document child record (Fragments) of a Sent Email - where its Approved Document is linked to an Event
* 
* @history
* - 30-05-2018 - Add/Update Attendee based on Sent Email Fragments (Sent Documents of a Sent Email)
*/
trigger JJ_SentEmail_Triggers on Sent_Email_vod__c (after insert, after update) {
     
     List<Sent_Email_vod__c> listEmailsToInvitee =  new List<Sent_Email_vod__c>();
     List<Sent_Fragment_vod__c> listEmailFragmentsToInvitee = new List<Sent_Fragment_vod__c>();
     Set<Id> janssenEventIds = new Set<Id>();
     Set<Id> inviteeAcctIds = new Set<Id>();
     Set<Id> deliveredSentEmailIds = new Set<Id>();
     
     public static FINAL String inviteeStartingStatus = 'Added';
     public static FINAL String invitedStatus = 'Invited';
     public static FINAL String inviteMethodAE = 'Email (Approved Email)';
     public static FINAL String actionAECreatedInvitee = 'Created'; 
     public static FINAL String actionAEUpdatedInvitee = 'Updated'; 
     
    List<Sent_Email_vod__c> listEmailsToProcess = [SELECT Id, Status_vod__c, Account_vod__c, Approved_Email_Template_vod__r.JJ_Janssen_Event__c, OwnerId, Approved_Email_Template_vod__r.JJ_Janssen_Event__r.OwnerId 
                                                     FROM Sent_Email_vod__c
                                                     WHERE Id IN :Trigger.newMap.keySet()];
                                                      
     
     
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
        
        // Func1 Build lists and maps
        for( Sent_Email_vod__c email : listEmailsToProcess){
            if(email.Status_vod__c == 'Delivered_vod' 
                && !String.IsBlank(email.Account_vod__c)  ){                              
                deliveredSentEmailIds.add(email.Id);
            }
            if(!String.IsBlank(email.Approved_Email_Template_vod__r.JJ_Janssen_Event__c) ){
                listEmailsToInvitee.add(email);  
                inviteeAcctIds.add(email.Account_vod__c);                
            }
        }
        
        List<Sent_Fragment_vod__c> listEmailFragmentsToProcess = [SELECT Id, Sent_Email_vod__r.Status_vod__c, Account_vod__c, Sent_Fragment_vod__r.JJ_Janssen_Event__c, Sent_Email_vod__r.OwnerId, Sent_Fragment_vod__r.JJ_Janssen_Event__r.OwnerId 
                                                     FROM Sent_Fragment_vod__c
                                                     WHERE Sent_Email_vod__r.Id IN :deliveredSentEmailIds];
        
        for( Sent_Fragment_vod__c emailFragment : listEmailFragmentsToProcess){
            if(!String.IsBlank(emailFragment.Account_vod__c) 
                && !String.IsBlank(emailFragment.Sent_Fragment_vod__r.JJ_Janssen_Event__c) ){
                listEmailFragmentsToInvitee.add(emailFragment);                
                inviteeAcctIds.add(emailFragment.Account_vod__c);
            }
        }
        
        
        // Func1 Setup Lists and Maps
        List<Event_Attendee_vod__c> listAttendees = [SELECT Id, Medical_Event_vod__c, Status_vod__c, Account_vod__c, JJ_Invite_Method__c, JJ_Invite_Generated__c, JJ_Invite_Generated_Date__c, JJ_Actions_Approved_Email__c, JJ_Approved_Sent_Email__c    
                                                        FROM Event_Attendee_vod__c 
                                                        WHERE Account_vod__c IN :inviteeAcctIds];
        
        List<Event_Attendee_vod__c> listAttendeesToUpdate = new List<Event_Attendee_vod__c>();
        List<Sent_Email_vod__c>     listSentEmailforAttendeeCreate = new List<Sent_Email_vod__c>();
        List<Sent_Fragment_vod__c>  listSentDocumentforAttendeeCreate = new List<Sent_Fragment_vod__c>();
        Set<Id>                     setAcctIdAttendeeCreate =  new Set<Id>();
        Map<Id,Id>   mapAttendeeToSentEmail = new Map<Id,Id>();
        Map<Id,Id>   mapAttendeeToSentDocument = new Map<Id,Id>();
        Boolean eventInviteeFound = false;        
               
        //Func1 Determine whether to insert or update an Event Invitee      
        for(Sent_Email_vod__c inviteeEmail : listEmailsToInvitee){
            eventInviteeFound = false;
            for(Event_Attendee_vod__c ea : listAttendees){
                
                //System.debug('Sent Email Acct - ' + inviteeEmail.Account_vod__c + ', Event Id - ' + inviteeEmail.Approved_Email_Template_vod__r.JJ_Janssen_Event__c);
                //System.debug('Invitee Account - ' + ea.Account_vod__c + ', Event Id - ' + ea.Medical_Event_vod__c);
                
                if(ea.Account_vod__c == inviteeEmail.Account_vod__c && 
                   ea.Medical_Event_vod__c == inviteeEmail.Approved_Email_Template_vod__r.JJ_Janssen_Event__c){                    
                    eventInviteeFound = true;  
                    //System.debug('Invitee matched - Invitee Account - ' + ea.Account_vod__c + ', Event Id - ' + ea.Medical_Event_vod__c);
                    
                    //Only update Invitees where it's Status is the default starting status before 'Invited'
                    if(ea.Status_vod__c == inviteeStartingStatus){
                        listAttendeesToUpdate.add(ea);
                        mapAttendeeToSentEmail.put(ea.Id, inviteeEmail.Id);     
                    }          
                }                    
            }
            
            if(!eventInviteeFound){
                listSentEmailforAttendeeCreate.add(inviteeEmail);
                setAcctIdAttendeeCreate.add(inviteeEmail.Account_vod__c);
            }
        }
        
        for(Sent_Fragment_vod__c inviteeEmailFragment : listEmailFragmentsToInvitee){
            eventInviteeFound = false;
            for(Event_Attendee_vod__c ea : listAttendees){
                
                if(ea.Account_vod__c == inviteeEmailFragment.Account_vod__c 
                    && ea.Medical_Event_vod__c == inviteeEmailFragment.Sent_Fragment_vod__r.JJ_Janssen_Event__c){                    
                    eventInviteeFound = true;  
                    
                    //Only update Invitees where it's Status is the default starting status before 'Invited'
                    if(ea.Status_vod__c == inviteeStartingStatus){
                        listAttendeesToUpdate.add(ea);
                        mapAttendeeToSentDocument.put(ea.Id, inviteeEmailFragment.Id);     
                    }          
                }                    
            }
            
            if(!eventInviteeFound){
                System.debug('InviteeEmailFragment added - ID ' + inviteeEmailFragment.Id);
                listSentDocumentforAttendeeCreate.add(inviteeEmailFragment);
                setAcctIdAttendeeCreate.add(inviteeEmailFragment.Account_vod__c);
            }
        }
        
        
        
        
        //Func1 execute UPDATES on Event Invitee records        
        for(Event_Attendee_vod__c ea : listAttendeesToUpdate){
            ea.Status_vod__c = invitedStatus;
            ea.JJ_Invite_Method__c = inviteMethodAE;
            ea.JJ_Invite_Generated__c = true;
            ea.JJ_Invite_Generated_Date__c = Date.today();
            ea.JJ_Actions_Approved_Email__c = actionAEUpdatedInvitee;
            ea.JJ_Approved_Sent_Email__c = mapAttendeeToSentEmail.get(ea.Id);
            ea.JJ_Approved_Sent_Document__c = mapAttendeeToSentDocument.get(ea.Id);
        }
        update listAttendeesToUpdate;
        
        
        
        
        //Func1 execute INSERTS on Event Invitee records  
        List<EventInviteeToCreate> listInviteCreates = new List<EventInviteeToCreate>();        
        List<Address_vod__c> listAttendeesToInsertAddresses = [SELECT Id, Account_vod__c, Primary_vod__c FROM Address_vod__c WHERE Account_vod__c IN :setAcctIdAttendeeCreate AND Primary_vod__c = true];
        
        for(Sent_Email_vod__c inviteeEmail : listSentEmailforAttendeeCreate){
            
            EventInviteeToCreate inviteeCreate = new EventInviteeToCreate(inviteeEmail.Account_vod__c, inviteeEmail.Approved_Email_Template_vod__r.JJ_Janssen_Event__c);  
     
            inviteeCreate.inviteeOwnerId = inviteeEmail.OwnerId;            
            inviteeCreate.sentEmailId = inviteeEmail.Id;
            inviteeCreate.medEventOwnerId = inviteeEmail.Approved_Email_Template_vod__r.JJ_Janssen_Event__r.OwnerId;
            
            for(Address_vod__c addr : listAttendeesToInsertAddresses){
                if(addr.Account_vod__c == inviteeEmail.Account_vod__c){
                    inviteeCreate.addressId = addr.Id;
                }
            }                       
            
            listInviteCreates.add(inviteeCreate);
        }
        
        for(Sent_Fragment_vod__c inviteeEmailFragment : listSentDocumentforAttendeeCreate){
            
            System.debug('Creating InviteeToCreate - Fragment ID ' + inviteeEmailFragment.Id);
            
            EventInviteeToCreate inviteeCreate = new EventInviteeToCreate(inviteeEmailFragment.Account_vod__c, inviteeEmailFragment.Sent_Fragment_vod__r.JJ_Janssen_Event__c);  
     
            inviteeCreate.inviteeOwnerId = inviteeEmailFragment.Sent_Email_vod__r.OwnerId;          
            inviteeCreate.sentDocumentId = inviteeEmailFragment.Id;
            inviteeCreate.medEventOwnerId = inviteeEmailFragment.Sent_Fragment_vod__r.JJ_Janssen_Event__r.OwnerId;          
            
            for(Address_vod__c addr : listAttendeesToInsertAddresses){
                if(addr.Account_vod__c == inviteeEmailFragment.Account_vod__c){
                    inviteeCreate.addressId = addr.Id;
                }
            }                      
            
            listInviteCreates.add(inviteeCreate);   
            
        }
        
        
        List<Event_Attendee_vod__c> listAttendeesToInsert = new List<Event_Attendee_vod__c>();
        Event_Attendee_vod__c ea;
        
        for(EventInviteeToCreate inviteeCreate : listInviteCreates){
            
            System.debug('::Creating EventInvitee:: Invitee Account - ' + inviteeCreate.acctId + ', Event Id - ' + inviteeCreate.medEventId);
            
            ea = new Event_Attendee_vod__c();   
            
            ea.Account_vod__c = inviteeCreate.acctId;
            ea.Medical_Event_vod__c = inviteeCreate.medEventId;
            ea.JJ_ANZ_Invitee_Owner__c = inviteeCreate.inviteeOwnerId;
            
            ea.Status_vod__c = inviteeCreate.status;
            ea.JJ_Invite_Method__c = inviteeCreate.inviteMethod;
            ea.JJ_Invite_Generated__c = inviteeCreate.inviteGenerated;
            ea.JJ_Invite_Generated_Date__c = inviteeCreate.inviteGeneratedDate;
            
            ea.JJ_Actions_Approved_Email__c = inviteeCreate.actionsApprovedEmail;            
            ea.JJ_ANZ_Event_Owner__c = inviteeCreate.medEventOwnerId;
            
            ea.JJ_Approved_Sent_Email__c = inviteeCreate.sentEmailId;
            ea.JJ_Approved_Sent_Document__c = inviteeCreate.sentDocumentId;
            ea.JJ_Address__c = inviteeCreate.addressId;                           
            
            listAttendeesToInsert.add(ea);
        }
        
        
        insert listAttendeesToInsert;
        
    }
    
    class EventInviteeToCreate
    {
 
        public Id acctId {get; set;} 
        public Id medEventId {get; set;}
        public Id medEventOwnerId {get; set;}
        public Id inviteeOwnerId {get; set;}   
        public Id sentEmailId {get; set;}
        public Id sentDocumentId {get; set;}  
        public Id addressId {get; set;}              
        
        public String status {get; set;}
        public String inviteMethod {get; set;}
        public Boolean inviteGenerated {get; set;}
        public Date inviteGeneratedDate {get; set;}
        public String actionsApprovedEmail {get; set;}
        
        public EventInviteeToCreate(Id acctId, Id medEventId){
            
            this.acctId = acctId;
            this.medEventId = medEventId;
            this.status = invitedStatus;
            this.inviteMethod = inviteMethodAE;
            this.inviteGenerated = true;
            this.inviteGeneratedDate = Date.today();
            this.actionsApprovedEmail = actionAECreatedInvitee;
        }        

   }
}