/**
* @author Noel Lim, Veeva Systems
* @date 06/12/2016
* @description 
* Prevent deletion of Event Invitee records for all Profiles unless it is in Draft status AND the Invitee is Sponsored
*/
trigger JJ_ANZ_EventAttendeeTriggers on Event_Attendee_vod__c (before delete) {
    if(trigger.isBefore && trigger.isDelete){
        for( Event_Attendee_vod__c ea :Trigger.old){
            if(ea.JJ_ANZ_Sponsorship_Approval_Status__c != 'Draft' &&
               ea.JJ_ANZ_Nature_of_Sponsorship__c != ''){
                ea.addError('Event Attendees with Sponsorship can only be deleted while in Draft status');
            }
        }
    }
}