//developped by Icbworks
//trigger to prevent user delete budget when event status is not Planning or Rejected
trigger JJ_ANZ_Event_Budget_ValidationError on Event_Budget__c (before delete) {
    
    string errormsgstr = '';
    for(Message_vod__c m:[Select Text_vod__c From Message_vod__c where Name=:'Event Budget Cannot Modify' and Category_vod__c =:'Events' and Active_vod__c =:true]){
        errormsgstr = m.Text_vod__c;
    }
    
    //get all events related to the budget
    set<Id> eventids = new set<Id>();    
    if(trigger.isDelete){    
        for(Event_Budget__c eb:trigger.old){
            eventids.add(eb.Janssen_Event__c);      
        }
    }
    
    //event record types which cannot modify the budget
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Events_Budget_Lock_1__c, JJ_ANZ_Events_Budget_Lock_2__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.JJ_ANZ_Events_Budget_Lock_1__c == null)?'':je.JJ_ANZ_Events_Budget_Lock_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Events_Budget_Lock_2__c == null)?'':je.JJ_ANZ_Events_Budget_Lock_2__c;
        jerectype.addAll(s.split(';'));
    }
    
    //event status which cannot modify the budget
    set<string> setstatus = new set<String>(); 
    setstatus.add('Awaiting Approval');
    setstatus.add('Approved');
    setstatus.add('Close Out');
    setstatus.add('Reconciliation');
    setstatus.add('Completed'); 
    
    //check the event status and record type to see if there should be a validation error
    map<Id, boolean> mapvalerror = new map<Id, boolean>();
    for(Medical_Event_vod__c e:[select Id from Medical_Event_vod__c where Id in:eventids and RecordType.DeveloperName in:jerectype and JJ_ANZ_Event_Status__c in:setstatus]){
        mapvalerror.put(e.Id, true);
    }
        
    //display error in case there is validation error
    if(trigger.isDelete){    
        for(Event_Budget__c eb:trigger.old){
            if(mapvalerror.get(eb.Janssen_Event__c) != null){
                eb.addError(errormsgstr);
            }
        }
    }
    
    
}