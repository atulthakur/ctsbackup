/**
* @author Noel Lim, Veeva Systems
* @date 29/02/2016
* @description 
*   1) Stamp Consulting Group Type - on linked Formal Meetings (Janssen Event object). The Janssen Event object has reached the limit of 10 cross-referenced objects
*
*/
trigger JJ_ANZ_ConsultingGroupTrigger on JJ_ANZ_Consulting_Group__c (after update) {
    
    if(trigger.isAfter && trigger.isUpdate){
        /**
        1) Stamp Consulting Group Type
        **/
        for( Id consGroupId : Trigger.newMap.keySet() )
        {
          String newCGType = Trigger.newMap.get( consGroupId ).JJ_ANZ_Type__c;
          
          if( Trigger.oldMap.get( consGroupId ).JJ_ANZ_Type__c != newCGType ) //CG Type has changed
          {               
              List<Medical_Event_vod__c> cgFormalMeetings = [SELECT Id, JJ_ANZ_Consulting_Group__c, JJ_ANZ_Consulting_Group_Type__c FROM Medical_Event_vod__c WHERE JJ_ANZ_Consulting_Group__c = :consGroupId];
              
              if(cgFormalMeetings.size() > 0){                  
                  update cgFormalMeetings; //A Trigger from Janssen Event will pull the Type itself when updated
              }
          }
        }

    }
}