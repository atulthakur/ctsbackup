/**
Developped by ICBWORKS
Trigger to Request Approval Process for each Event related to the Venue
*/
trigger JJ_ANZ_VenueUpdateEvent on Janssen_Event_Venue__c (after update) {
    
    //save old venue values to be compared to new values
    map<Id,Janssen_Event_Venue__c> mapoldvenue = trigger.oldmap;
    
    //save the venue Ids whose Venue Status changes from Pending Approval to Approved OR Rejected OR Exception
    set<Id> venueIds = new set<Id>();    
    for(Janssen_Event_Venue__c venue:trigger.new){
        if(venue.JJ_ANZ_Venue_Status__c != mapoldvenue.get(venue.Id).JJ_ANZ_Venue_Status__c){
            venueIds.add(venue.Id);        
        }
    }
    
    //query the custom setting to know which janssen event record types to query for approval submission
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Venue_Approval_Event_RT_update_4__c, JJ_ANZ_Venue_Approval_Event_RT_update_3__c, JJ_ANZ_Venue_Approval_Event_RT_update_2__c, JJ_ANZ_Venue_Approval_Event_RT_update_1__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.JJ_ANZ_Venue_Approval_Event_RT_update_1__c == null)?'':je.JJ_ANZ_Venue_Approval_Event_RT_update_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Venue_Approval_Event_RT_update_2__c == null)?'':je.JJ_ANZ_Venue_Approval_Event_RT_update_2__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Venue_Approval_Event_RT_update_3__c == null)?'':je.JJ_ANZ_Venue_Approval_Event_RT_update_3__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Venue_Approval_Event_RT_update_4__c == null)?'':je.JJ_ANZ_Venue_Approval_Event_RT_update_4__c;
        jerectype.addAll(s.split(';'));
    }
 
    map<Id, Id> medevtIds = new map<Id, Id>();
    for(Medical_Event_vod__c janevent:[Select Id, OwnerId From Medical_Event_vod__c where RecordType.DeveloperName in:jerectype and (JJ_ANZ_Event_Status__c='Planning' OR JJ_ANZ_Event_Status__c='Awaiting Approval' OR JJ_ANZ_Event_Status__c='Approved')  and JJ_Event_Venue__c in:venueIds]){
        medevtIds.put(janevent.Id, janevent.OwnerId);
    }
    
    List<Approval.ProcessWorkitemRequest> rejectReqList = new List<Approval.ProcessWorkitemRequest>();
    set<Id> rejmedevtIds = new set<Id>();
    for(ProcessInstanceWorkitem workItem:[Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId in:medevtIds.keySet()]){
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Venue status changed.');
        req.setAction('Approve');
        req.setWorkitemId(workItem.Id);
        rejectReqList.add(req);
        rejmedevtIds.add(workItem.ProcessInstance.TargetObjectId);
       system.debug('***************Id**************' + workItem.Id);
       system.debug('***************ProcessInstance.TargetObjectId**************' + workItem.ProcessInstance.TargetObjectId);
    }   
    List<Approval.ProcessResult> resultrejList = Approval.process(rejectReqList);
    
    List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();    
    //select events related to the venue + with a Status of Pending Approval + RT equal to a list of RT’s of Custom Setting called "Venue Approval-Event RT’s to update"
    for(Id i:rejmedevtIds){
       // create the new approval request to submit    
       Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
       req.setComments('Event system Approval Processing');
       req.setObjectId(i);
       req.setSubmitterId(medevtIds.get(i));
       approvalReqList.add(req);
       system.debug('***************setObjectId**************' + i);
    }
    
    system.debug('***************approvalReqList**************' + approvalReqList.size());

    // submit the approval request for processing        
    List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);
    
}