trigger SPEAKER_AGREEMENT_AFTER_UPSERT on Speaker_Agreement__c (after insert, after update) {
    // get new Speaker Agreement Data  
    Speaker_Agreement__c [] triggerSpeakerAgreements =  Trigger.new;
    
    for (Speaker_Agreement__c speakerAgreement : triggerSpeakerAgreements) {
        //get Event Association ID and Speaker_Status_Text__c
        Account_Event_Association__c [] eventAssociationList = [select Id, JJ_ANZ_Speaker_Status_Text__c from Account_Event_Association__c where JJ_Speaker_Agreement__c = :speakerAgreement.Id];
        if (eventAssociationList.size()>0){
            for (Account_Event_Association__c eventAssociation : eventAssociationList){
                eventAssociation.JJ_ANZ_Speaker_Status_Text__c = speakerAgreement.JJ_Process_Status__c;
            }
            Update eventAssociationList;
        }
    }   
}