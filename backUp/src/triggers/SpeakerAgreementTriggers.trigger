/*************************************************************************************
 *
 *  Developer: ICBWORKS
 *  Date: 18.02.2014
 *  Email: zameer.khodabocus@icbworks.com / dhanjay.ujoodha@icbworks.com
 *  
 *  Description: This trigger contains all the triggers developed by ICBWORKS and Bluewolf on the Speaker Agreement (Speaker_Agreement__c) Object.
 *  
 *  1) Trigger name: PopulateSpeakerAgreementAddress
 *     Decription: Speaker Agreement Address Trigger. We need the following fields to be placed on the Speaker Agreement object from the linked 
 *                 relevant Speaker's "Address object" Fields via JJ_Speaker__c on the Speaker agreement to go to are - Street_c, Street_2__c, Suburb__c, Postcode__c, State__c, Country__c
 *  
 *  2) Trigger name: ShareAccountsAfterUpsertSpeakerAgreements
 *     Description: This trigger is used to share visibility of accounts (JJ_Speaker__c) with Speaker Liason on Speaker Agreement whenever
 *                  the Speaker Liaison is updated. 
 *  
 *  3) Trigger name: Speaker_Agreement_Triger (developed by Bluewolf)
 *     Date created: June 22 2013
 *     Date modified: July 25 2013
 *     Description: Trigger to update events related to the agreement and calculate the end date for a 12 month agreement
 *
 *  4) Added logic Before Delete - cannot delete record if Speaker Status is not Draft
       2016-12-07 @author Noel Lim
 *  
 *  5) Validation - Selected Parent Account must have an affiliation (Child Account record) to the Speaker. If Parent Account empty, default to Speaker Primary Parent
 *     2017-07-06 @author Noel Lim    
**************************************************************************************/

trigger SpeakerAgreementTriggers on Speaker_Agreement__c (before insert, before update, after insert) {
    
    
    // 5) Validation on Parent Account
    Set<Id> validateParentAcctIds = new Set<Id>();
    Set<Id> validateParentChildAcctIds = new Set<Id>();
    List<Speaker_Agreement__c> listSpeakerAgreementParentAccount = new List<Speaker_Agreement__c>();
    List<Speaker_Agreement__c> listSpeakerAgreementParentAccountToDefault = new List<Speaker_Agreement__c>();
    
    
    set<Id> accIds = new set<Id>();
    set<Id> parentAccIds = new set<Id>();
    
    
    if(trigger.isBefore && trigger.isUpdate){      
        
        for(Speaker_Agreement__c sp:trigger.new){
            accIds.add(sp.JJ_Speaker__c);
            parentAccIds.add(sp.JJ_Parent_Account__c);
            
            // 5) Validate Parent Account
            if(sp.JJ_Parent_Account__c != null){
                validateParentAcctIds.add(sp.JJ_Parent_Account__c);
                validateParentChildAcctIds.add(sp.JJ_Speaker__c);
                listSpeakerAgreementParentAccount.add(sp);
            }
            else{               
                listSpeakerAgreementParentAccountToDefault.add(sp);
            }
            if(sp.JJ_Speaker__c != Trigger.oldMap.get(sp.Id).JJ_Speaker__c){
                listSpeakerAgreementParentAccountToDefault.add(sp);
            }
        }
        
                
        /**
            Speaker_Agreement_Triger trigger
        **/
        List<Medical_Event_vod__c> events=new List<Medical_Event_vod__c>();
        List<Account_Event_Association__c> evLinks=new List<Account_Event_Association__c>();
        Set<ID> evId=new Set<ID>();
        Set<ID> updateAccountId=new Set<ID>();
        List<Medical_Event_vod__c> updates=new List<Medical_Event_vod__c>();
        
        for(Account_Event_Association__c asso :[SELECT JJ_Speaker_Agreement__c,JJ_Janssen_Event__c FROM Account_Event_Association__c
                WHERE JJ_Speaker_Agreement__c IN:Trigger.new]){
            evLinks.add(asso);
            evId.add(asso.JJ_Janssen_Event__c);
        }
        events=[SELECT Id,JJ_Speaker_Signature_Obtained__c,Start_Date_vod__c FROM Medical_Event_vod__c WHERE ID IN:evId];
    
        for(Speaker_Agreement__c ag:Trigger.new){
            //If signature received
            if(Trigger.new.get(0).JJ_Pending_Signature__c==True){           
                for(Account_Event_Association__c asso:evLinks){
                    if(ag.Id==asso.JJ_Speaker_Agreement__c){
                        for(Medical_Event_vod__c ev:events){                            
                            if(asso.JJ_Janssen_Event__c==ev.Id){
                                ev.JJ_Speaker_Signature_Obtained__c=True;
                                updates.add(ev);
                            }                           
                        }
                    }
                }
            }
            
            //If approved   -date update
            if(ag.JJ_Process_Status__c=='Approved'&&Trigger.isBefore){      
                //Update account later
                if(ag.JJ_Speaker_HCC_Status__c==False){
                    updateAccountId.add(ag.JJ_Speaker__c);
                }   
                if(ag.JJ_HCC_Grace_Valid__c==False){
                    updateAccountId.add(ag.JJ_Speaker__c);
                }
                //If 12 month
                if(ag.JJ_Type__c=='12 Month'){
                    /** Modified by ICB
                    *   Date: 10.04.2014
                    **/
                    if(ag.JJ_Start_Date__c == null){
                        ag.JJ_Start_Date__c=Date.today();
                    }
                    
                    ag.JJ_End_Date__c=Trigger.new.get(0).JJ_Start_Date__c.addDays(364);
                }           
                //If one-Off set start and end to event date
                if(Trigger.new.get(0).JJ_Type__c=='One-Off'){
                    try{
                        for(Account_Event_Association__c asso:evLinks){
                            if(ag.Id==asso.JJ_Speaker_Agreement__c){
                                for(Medical_Event_vod__c ev:events){                            
                                    if(asso.JJ_Janssen_Event__c==ev.Id){
                                        
                                        /** Modified by ICB
                                        *   Date: 10.04.2014
                                        **/
                                        if(ag.JJ_Start_Date__c == null){
                                            ag.JJ_Start_Date__c=ev.Start_Date_vod__c;
                                        }
                                        
                                        ag.JJ_End_Date__c=ev.Start_Date_vod__c;
                                    }                           
                                }
                            }
                        }
                    }catch(Exception e){
                        System.debug(e);
                    }
                }   
            }   
        }               
        //Mass update events
        try{
            update updates;
        }catch(Exception e){
            System.debug(e);
        }
        //mass update accounts for HCC approval=True if agreement was approved
        List<Account> updateAcc=[SELECT Id, JJ_HCC_Approved__c,JJ_HCC_Expiration__c,JJ_HCC_Expiration_Grace__c FROM Account WHERE Id IN:updateAccountId];
        //try{
            if(updateAcc!=NULL){
                for(Account ac:updateAcc){
                    if(ac.JJ_HCC_Approved__c=='Approved'){
                        ac.JJ_HCC_Expiration__c=Date.today().addMonths(36);
                        ac.JJ_HCC_Expiration_Grace__c=Date.today().addMonths(37);
                    }
                    else{
                        ac.JJ_HCC_Approved__c='Approved';
                    }
                }   
                update updateAcc;
            }
        //}catch(Exception e){
        //  System.debug(e);
        //}
        
        /**
            ShareAccountsAfterUpsertSpeakerAgreements trigger
        **/
        Map<Id, Speaker_Agreement__c> previousSpeakerLiasonMap = new Map<Id, Speaker_Agreement__c>();
        Map<Id, Id> speakersAccountsMap = new Map<Id, Id>();
        Set<Id> oldSpLaisionsId = new Set<Id>();
        List<AccountShare> newAccountsShareList = new List<AccountShare>();
        
        for(Speaker_Agreement__c sa : trigger.new){
            //map of all Speakers (Accounts) for the Speaker Agreement being updated
            speakersAccountsMap.put(sa.Id, sa.JJ_Speaker__c);
        }
        
        for(Speaker_Agreement__c sa : trigger.old){
            if(sa.JJ_Speaker__c != null){
                //map of Speaker Agreements before the new changes are saved
                previousSpeakerLiasonMap.put(sa.Id, sa);
            }
        }
        
        for(Speaker_Agreement__c sa : trigger.new){
            if(previousSpeakerLiasonMap.size() > 0){
                Speaker_Agreement__c speakerAggOld = previousSpeakerLiasonMap.get(sa.Id);
                
                if(speakerAggOld != null){
                    //Compare the value of old Speaker Liason with the new one being saved.
                    //If they are not same, share the Speaker Account with the new Liason and delete the previous share with the Old Liason
                    if(speakerAggOld.JJ_Speaker_Liason__c != sa.JJ_Speaker_Liason__c){
                        if(speakerAggOld.JJ_Speaker_Liason__c != null){
                            //set of the old Speaker Liason Ids to be used to delete previous share
                            oldSpLaisionsId.add(speakerAggOld.JJ_Speaker_Liason__c);
                        }
                        //New Account share with the new Speaker Liason
                        AccountShare newAccShare = new AccountShare();
                            
                        newAccShare.AccountId = speakersAccountsMap.get(sa.Id);
                        newAccShare.UserOrGroupId = sa.JJ_Speaker_Liason__c;
                        newAccShare.OpportunityAccessLevel = 'None';
                        
                        newAccShare.AccountAccessLevel = 'Edit';
                        
                        newAccountsShareList.add(newAccShare);
                    }
                }
            }
        }
        
        //Getting the list of old account shared with the old Speaker Liason 
        if(oldSpLaisionsId.size() > 0 && speakersAccountsMap.size() > 0){
            List<AccountShare> oldAccSharingRules = [select Id
                                                     From AccountShare
                                                     where AccountId in :speakersAccountsMap.values() AND UserOrGroupId in :oldSpLaisionsId
                                                     AND RowCause = 'Manual'];
                                                     
            if(oldAccSharingRules.size() > 0){
                //deleting visibility of the Speaker accounts for the old Speaker Liason
                delete oldAccSharingRules;
            }
        }
        
        if(newAccountsShareList.size() > 0){
            //insertng the new account shares for the new Speaker Liason
            insert newAccountsShareList;
        }
        
        
    }else if(trigger.isBefore && trigger.isInsert){
        
        for(Speaker_Agreement__c sp:trigger.new){
            accIds.add(sp.JJ_Speaker__c);
            parentAccIds.add(sp.JJ_Parent_Account__c);
            
            // 5) Validate Parent Account
            listSpeakerAgreementParentAccountToDefault.add(sp);           
            /** VALIDATION Disabled - not required anymore with the VF that only displays actual Parent Accounts
            if(sp.JJ_Parent_Account__c != null){
                validateParentAcctIds.add(sp.JJ_Parent_Account__c);
                validateParentChildAcctIds.add(sp.JJ_Speaker__c);
                listSpeakerAgreementParentAccount.add(sp);
            }
            else{
                listSpeakerAgreementParentAccountToDefault.add(sp);
            }*/
        }
        
       
    }else if(trigger.isAfter && trigger.isInsert){
        /**
            Speaker_Agreement_Triger trigger
        **/
        List<Medical_Event_vod__c> events=new List<Medical_Event_vod__c>();
        List<Account_Event_Association__c> evLinks=new List<Account_Event_Association__c>();
        Set<ID> evId=new Set<ID>();
        Set<ID> updateAccountId=new Set<ID>();
        List<Medical_Event_vod__c> updates=new List<Medical_Event_vod__c>();
        
        for(Account_Event_Association__c asso :[SELECT JJ_Speaker_Agreement__c,JJ_Janssen_Event__c FROM Account_Event_Association__c
                WHERE JJ_Speaker_Agreement__c IN:Trigger.new]){
            evLinks.add(asso);
            evId.add(asso.JJ_Janssen_Event__c);
        }
        events=[SELECT Id,JJ_Speaker_Signature_Obtained__c,Start_Date_vod__c FROM Medical_Event_vod__c WHERE ID IN:evId];
    
        for(Speaker_Agreement__c ag:Trigger.new){
            //If signature received
            if(Trigger.new.get(0).JJ_Pending_Signature__c==True){           
                for(Account_Event_Association__c asso:evLinks){
                    if(ag.Id==asso.JJ_Speaker_Agreement__c){
                        for(Medical_Event_vod__c ev:events){                            
                            if(asso.JJ_Janssen_Event__c==ev.Id){
                                ev.JJ_Speaker_Signature_Obtained__c=True;
                                updates.add(ev);
                            }                           
                        }
                    }
                }
            }
            
            //If approved   -date update
            if(ag.JJ_Process_Status__c=='Approved'&&Trigger.isBefore){      
                //Update account later
                if(ag.JJ_Speaker_HCC_Status__c==False){
                    updateAccountId.add(ag.JJ_Speaker__c);
                }   
                if(ag.JJ_HCC_Grace_Valid__c==False){
                    updateAccountId.add(ag.JJ_Speaker__c);
                }
                //If 12 month
                if(ag.JJ_Type__c=='12 Month'){
                    /** Modified by ICB
                    *   Date: 10.04.2014
                    **/
                    if(ag.JJ_Start_Date__c == null){
                        ag.JJ_Start_Date__c=Date.today();
                    }
                    
                    ag.JJ_End_Date__c=Trigger.new.get(0).JJ_Start_Date__c.addDays(364);
                }           
                //If one-Off set start and end to event date
                if(Trigger.new.get(0).JJ_Type__c=='One-Off'){
                    try{
                        for(Account_Event_Association__c asso:evLinks){
                            if(ag.Id==asso.JJ_Speaker_Agreement__c){
                                for(Medical_Event_vod__c ev:events){                            
                                    if(asso.JJ_Janssen_Event__c==ev.Id){
                                        /** Modified by ICB
                                        *   Date: 10.04.2014
                                        **/
                                        if(ag.JJ_Start_Date__c == null){
                                            ag.JJ_Start_Date__c=ev.Start_Date_vod__c;
                                        }
                                        
                                        ag.JJ_End_Date__c=ev.Start_Date_vod__c;
                                    }                           
                                }
                            }
                        }
                    }catch(Exception e){
                        System.debug(e);
                    }
                }   
            }   
        }               
        //Mass update events
        try{
            update updates;
        }catch(Exception e){
            System.debug(e);
        }
        //mass update accounts for HCC approval=True if agreement was approved
        List<Account> updateAcc=[SELECT Id, JJ_HCC_Approved__c,JJ_HCC_Expiration__c,JJ_HCC_Expiration_Grace__c FROM Account WHERE Id IN:updateAccountId];
        //try{
            if(updateAcc!=NULL){
                for(Account ac:updateAcc){
                    if(ac.JJ_HCC_Approved__c=='Approved'){
                        ac.JJ_HCC_Expiration__c=Date.today().addMonths(36);
                        ac.JJ_HCC_Expiration_Grace__c=Date.today().addMonths(37);
                    }
                    else{
                        ac.JJ_HCC_Approved__c='Approved';
                    }
                }   
                update updateAcc;
            }
        //}catch(Exception e){
        //  System.debug(e);
        //}
    }
    if(trigger.isBefore && trigger.isDelete){
        for(Speaker_Agreement__c sa :Trigger.old){
            if(sa.JJ_ANZ_Speaker_Status__c != 'Draft'){
                sa.addError('Fee for Service records can only be deleted while in Draft status');
            }
        }
    }
    
    if(trigger.isBefore){
    
         //5) Default Parent account to the Speaker Primary as a default if it is not provided OR Speaker is updated        
        for(Speaker_Agreement__c sp : listSpeakerAgreementParentAccountToDefault){
            sp.JJ_Parent_Account__c = sp.JJ_Speaker_Primary_Account_Id__c;
            parentAccIds.add(sp.JJ_Parent_Account__c);
        }
        
        /*5) Validate Parent Account - VALIDATION DISABLED with addition of VF page
        if(listSpeakerAgreementParentAccount.size() > 0){   
            
            List<Child_Account_vod__c> listChildAccounts = [SELECT Id, Child_Account_vod__c, Parent_Account_vod__c, Parent_Account_vod__r.Name,  Child_Account_vod__r.Name 
                                                            FROM Child_Account_vod__c
                                                            WHERE Child_Account_vod__c IN :validateParentChildAcctIds OR 
                                                            Parent_Account_vod__c IN :validateParentAcctIds];
            
                                                                     
            Boolean childAccountRelationshipFound;
            
            for(Speaker_Agreement__c sp : listSpeakerAgreementParentAccount){
                Id childId = sp.JJ_Speaker__c;
                Id parentId = sp.JJ_Parent_Account__c;
                
                childAccountRelationshipFound = false;
                
                for(Child_Account_vod__c childAcct : listChildAccounts){
                    //System.debug('Parent Name in Affilation - ' + childAcct.Parent_Account_vod__r.Name + ');
                    //System.debug(childId + ', ' + childAcct.Child_Account_vod__c +'::' + parentId +', '+ childAcct.Parent_Account_vod__c);
                    
                    if(childId == childAcct.Child_Account_vod__c &&
                        parentId == childAcct.Parent_Account_vod__c){   
                                         
                        childAccountRelationshipFound = true;
                        //System.debug('Affilation found');
                    }
                } 
                
                
                if(!childAccountRelationshipFound){                
                    sp.JJ_Parent_Account__c.addError('Please select a Parent Account that the Speaker is a member of (e.g. it is their workplace)');                              
                }   
                     
            }
            
            
            
        } 
        */   
       
        
        /**
            PopulateSpeakerAgreementAddress trigger
        **/
        // 5) Updated to use Parent Account
        map<Id, Address_vod__c> mapaddress = new map<Id, Address_vod__c>();
        for(Account acc:[Select ID, (Select Name, Zip_vod__c, State_vod__c,Address_line_2_vod__c, Country_vod__c, City_vod__c, JJ_Core_Suburb__c From Address_vod__r Where Primary_vod__c = true limit 1) From Account where Id in :parentAccIds]){
            for(Address_vod__c addr:acc.Address_vod__r){
                mapaddress.put(acc.Id, addr);
            }
        }
        for(Speaker_Agreement__c sp:trigger.new){
            sp.Street__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).Name;
            sp.Street2__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).Address_line_2_vod__c;
            sp.Suburb__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).City_vod__c;//JJ_Core_Suburb__c
            sp.Postcode__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).Zip_vod__c;
            sp.State__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).State_vod__c;
            sp.Country__c = (mapaddress.get(sp.JJ_Parent_Account__c) == null)?null:mapaddress.get(sp.JJ_Parent_Account__c).Country_vod__c;
        }
    }
    
}