trigger EmailMessageAfterUpdate on EmailMessage (before insert,after Insert) {
system.debug('......before');
    
    if(Trigger.isAfter)
    {
    Emailmessage em = Trigger.new[0];
    
     Map<Id,CaseArticle> mapArticles = new Map<Id,CaseArticle>([Select KnowledgeArticleId,caseid from CaseArticle where caseid=:em.ParentId]);
            System.debug('Mahesh Map Articles '+mapArticles);
            List<Id> listIds = new List<Id>();
            for(CaseArticle c : mapArticles.values())
            {
              System.debug('First before '+c.KnowledgeArticleId);
              listIds.add(c.KnowledgeArticleId);
              System.debug('First after '+listIds);
            }
    
    List<JJ_ANZ_MedInfo_Documents__kav> te =[select KnowledgeArticleId, JJ_ANZ_MedInfo_Docs__ContentType__s ,JJ_ANZ_MedInfo_Docs__Name__s,JJ_ANZ_MedInfo_Docs__Body__s
         from JJ_ANZ_MedInfo_Documents__kav  where PublishStatus = 'Online' and JJ_ANZ_Article_Subtype__c='Written Response' and KnowledgeArticleId IN:listIds];
    
    system.debug('....listArticles'  + te);
    List<Attachment> listAttachments = new List<Attachment>();
    for(JJ_ANZ_MedInfo_Documents__kav j : [select KnowledgeArticleId, JJ_ANZ_MedInfo_Docs__ContentType__s ,JJ_ANZ_MedInfo_Docs__Name__s,JJ_ANZ_MedInfo_Docs__Body__s
         from JJ_ANZ_MedInfo_Documents__kav  where PublishStatus = 'Online' and JJ_ANZ_Article_Subtype__c='Written Response' and KnowledgeArticleId IN:listIds])
    {
       
        Attachment a=New Attachment();
        a.parentId = em.Id;
        a.Body=j.JJ_ANZ_MedInfo_Docs__Body__s;
        a.Name = j.JJ_ANZ_MedInfo_Docs__Name__s;
        
        if(j.JJ_ANZ_MedInfo_Docs__ContentType__s!=null){
       if(j.JJ_ANZ_MedInfo_Docs__ContentType__s.contains('doc'))
        {
            a.ContentType='doc';
        }
        else
        {
            a.ContentType='application/pdf';
        }
        }
        system.debug('....Attachments'  + a);
        listAttachments.add(a);
        

    }
    system.debug('....listAttachments'  + listAttachments);
    try{
    if(!listAttachments.isEmpty())
    {
        Insert listAttachments;
    }
    }catch(DMLException de)
    {
    
    }catch(Exception e)
    {
    }
        
    }
    
    /*if(trigger.isbefore){
        List<EmailMessage> delem=[Select Id,Subject from EmailMessage where Subject like 'Basic template to send email'];
        if(!delem.isEmpty())
        {
            Database.delete(delem,false);
        }
        
    }*/ 

}