/* Loganathan Sekar 30/04/18
     The purpose of this trigger is to change the last modified date on account plan if any new notes added in to system */

trigger JJ_ANZ_Account_Plan_Note on Note (after insert, after update) {
    list<Note> note = new list<Note>();
    Set<ID> Accplids = new Set<ID>();
    
    for (Note nt : Trigger.new) {    
         note.add(nt);                  
         Accplids.add(nt.ParentID);     
        
    }
    
     List<Account_Plan_vod__c> Accid = [Select ID,LastModifiedById,LastModifiedDate,JJ_ANZ_Last_update_on_Note__c from Account_Plan_vod__c where ID IN: Accplids];
       
       for(note n : note){
           for(Account_Plan_vod__c Accpid : Accid){
               if(accpid.id == n.parentid){
            
                  Accpid.JJ_ANZ_Last_update_on_Note__c = n.LastModifiedDate;}
                
                 update Accpid;
                 } 
     
}

}