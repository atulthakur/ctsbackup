/**
* Trigger for the event attendee object object Validates that a person has 
* not been invited twice, and that the speaker is included in the list of
* invited people
* Crested: June 17 2013
* Modified July 22 2013
* Bluewolf 
* Modified by: Khodabocus Zameer
* Modified Date: 25.03.2014
* Description: Trigger has been disabled and the logic has been put in the EV_Attendee_Comp_Controller Class
**/

trigger Event_Attendees_Validation on Event_Attendee_vod__c (before insert) {
	//Validate duplicates
/*	ID eventId=((Event_Attendee_vod__c)Trigger.new.get(0)).	Medical_Event_vod__c;
	//List of accounts to be updated
	Set<ID> attAccount=new Set<ID>();
	
	for(Event_Attendee_vod__c item:Trigger.new){
		attAccount.add(item.Account_vod__c);
	}
	
	Integer countRepeated=[SELECT COUNT() FROM Event_Attendee_vod__c WHERE Medical_Event_vod__c=:eventId AND Account_vod__c IN:attAccount];
	
	
	if(Trigger.isInsert){			
		if(countRepeated>0){
			Trigger.new.get(0).addError('Duplicate invitee');			
		}
		else{
			//Update event for pending invite printing
			Medical_Event_vod__c ev=[SELECT ID,JJ_Needs_Invite_Printing__c FROM Medical_Event_vod__c WHERE Id=:eventId].get(0);
			ev.JJ_Needs_Invite_Printing__c=True; 
			try{
				update ev;
			}catch(Exception e){
				System.debug(e);
			}
		}
	}*/
}