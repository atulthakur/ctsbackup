/**
* This Trigger prevents to delete Attachments when a record is in Completed or readonly stage. 
* Objects are: Janssen Events, Fee for Service, Event Invitees, Grants
*
* @author noel.lim@veeva.com
*/  
trigger JJ_AttachmentiConnect_Triggers on Attachment (before delete) {

    private String janssenEventStatusCompletedVal = 'Completed';
    private String speakerStatusCompletedVal1 = 'Approved';
    private String speakerStatusCompletedVal2 = 'Expired';
    private String grantStatusCompletedVal = 'Approved';
    private String cgStatusCompletedVal1 = 'Expired';
    private String cgStatusCompletedVal2 = 'Approved';
    
    if(Trigger.isBefore  && Trigger.isDelete){       
        
        //Map at first the Id of the attachment with the Id of the Parent record    
        //key: parentID, value: Attachment Id
        Map<Id, Id> parentAttachmentMap = new Map<Id, Id>();
        for(Attachment attachment:Trigger.old){
            parentAttachmentMap.put(attachment.ParentId, attachment.Id);
        }   
        
        if(!parentAttachmentMap.isEmpty()){
            //====Janssen Event that is Completed===
            List<Medical_Event_vod__c> jEventAttachments = [Select je.Id, (Select ParentId, Name From Attachments) 
                                                            From Medical_Event_vod__c je 
                                                            WHERE Id IN:parentAttachmentMap.keySet() AND JJ_ANZ_Event_Status__c = :janssenEventStatusCompletedVal];            
            
            //if Janssen event attachments was deleted, add an error message for the appropriate attachments 
            if(!jEventAttachments.isEmpty()){
                for(Medical_Event_vod__c jEvent :jEventAttachments){
                    Id triggerElementId = parentAttachmentMap.get(jEvent.Id);
                        if(triggerElementId != null){
                            Map<Id, Attachment> triggerElementsMap = Trigger.oldMap;                        
                            triggerElementsMap.get(triggerElementId).Id.addError('Attachments to completed Events cannot be deleted');                      
                        }
                }               
            } 
            
            //====Event Invitee - Event is completed ===
            List<Event_Attendee_vod__c > eventAttendeeAttachments = [Select ea.Id, (Select ParentId, Name From Attachments) 
                                                            From Event_Attendee_vod__c  ea 
                                                            WHERE Id IN:parentAttachmentMap.keySet() AND Event_Status__c  = :janssenEventStatusCompletedVal];            
            
            //if attachments was deleted, add an error message for the appropriate attachments 
            if(!eventAttendeeAttachments.isEmpty()){
                for(Event_Attendee_vod__c eAttendee : eventAttendeeAttachments){
                    Id triggerElementId = parentAttachmentMap.get(eAttendee.Id);
                        if(triggerElementId != null){
                            Map<Id, Attachment> triggerElementsMap = Trigger.oldMap;                        
                            triggerElementsMap.get(triggerElementId).Id.addError('Attachments to Event Attendees of completed Events cannot be deleted');                       
                        }
                }               
            }  
            
            //====Fee For Service - Approved or Expired ===
            List<Speaker_Agreement__c  > feeServiceAttachments = [Select ea.Id, (Select ParentId, Name From Attachments) 
                                                            From Speaker_Agreement__c   ea 
                                                            WHERE Id IN:parentAttachmentMap.keySet() 
                                                            AND (JJ_ANZ_Speaker_Status__c = :speakerStatusCompletedVal1 OR
                                                                 JJ_ANZ_Speaker_Status__c = :speakerStatusCompletedVal2 )];            
            
            //if attachments was deleted, add an error message for the appropriate attachments 
            if(!feeServiceAttachments.isEmpty()){
                for(Speaker_Agreement__c  feeService : feeServiceAttachments){
                    Id triggerElementId = parentAttachmentMap.get(feeService.Id);
                        if(triggerElementId != null){
                            Map<Id, Attachment> triggerElementsMap = Trigger.oldMap;                        
                            triggerElementsMap.get(triggerElementId).Id.addError('Attachments to Fee for Service agreements that are Approved or Expired cannot be deleted');                       
                        }
                }               
            }
            
            //====Grants - Approved ===
            List<JJ_ANZ_Grant__c> grantsAttachments = [Select ea.Id, (Select ParentId, Name From Attachments) 
                                                            From JJ_ANZ_Grant__c    ea 
                                                            WHERE Id IN:parentAttachmentMap.keySet() 
                                                            AND JJ_ANZ_Status__c = :grantStatusCompletedVal];            
            
            //if attachments was deleted, add an error message for the appropriate attachments 
            if(!grantsAttachments.isEmpty()){
                for(JJ_ANZ_Grant__c  grant : grantsAttachments){
                    Id triggerElementId = parentAttachmentMap.get(grant.Id);
                        if(triggerElementId != null){
                            Map<Id, Attachment> triggerElementsMap = Trigger.oldMap;                        
                            triggerElementsMap.get(triggerElementId).Id.addError('Attachments to Grants that are Approved cannot be deleted');                      
                        }
                }               
            }
            
            //====Consulting Group - Approved or Expired ===
            List<JJ_ANZ_Consulting_Group__c> cgAttachments = [Select ea.Id, (Select ParentId, Name From Attachments) 
                                                            From JJ_ANZ_Consulting_Group__c    ea 
                                                            WHERE Id IN:parentAttachmentMap.keySet() 
                                                            AND (JJ_ANZ_Consulting_Group_Status__c = :cgStatusCompletedVal1 OR
                                                                 JJ_ANZ_Consulting_Group_Status__c = :cgStatusCompletedVal2 )];            
            
            //if attachments was deleted, add an error message for the appropriate attachments 
            if(!cgAttachments.isEmpty()){
                for(JJ_ANZ_Consulting_Group__c  cg : cgAttachments){
                    Id triggerElementId = parentAttachmentMap.get(cg.Id);
                        if(triggerElementId != null){
                            Map<Id, Attachment> triggerElementsMap = Trigger.oldMap;                        
                            triggerElementsMap.get(triggerElementId).Id.addError('Attachments to Consulting Groups that are Approved or Expired cannot be deleted');                      
                        }
                }               
            }
            
        }       
    
    }
}