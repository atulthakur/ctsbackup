//developeed by Icbworks
//when creating an attendee, add the account to the owner's territory
trigger AttendeeAccountAlignment on Event_Attendee_vod__c (after insert, after update) {

    List <Account_Territory_Loader_vod__c> atlupsertList = new List <Account_Territory_Loader_vod__c>();
    set<id> accids = new set<id>();
    set<id> ownerids = new set<id>();
    boolean skipatl = false;
    
    List<Event_Attendee_vod__c> eventattendeeList = [Select Medical_Event_vod__r.OwnerId, Account_vod__c, Account_vod__r.Name From Event_Attendee_vod__c where Id in:trigger.newmap.keySet()];

    for(Event_Attendee_vod__c att:eventattendeeList){
        system.debug('*************************Account_vod__r.Name**************************' + att.Account_vod__r.Name);
        accids.add(att.Account_vod__c);
        ownerids.add(att.Medical_Event_vod__r.OwnerId);
    }
    
    map<Id,List<Account_Territory_Loader_vod__c>> mapatl = new map<Id,List<Account_Territory_Loader_vod__c>>(); 
    for(Account_Territory_Loader_vod__c atl:[select Id, Territory_vod__c,Account_vod__c from Account_Territory_Loader_vod__c where Account_vod__c in:accids]){
        List<Account_Territory_Loader_vod__c> tempatl= (mapatl.get(atl.Account_vod__c) == null)?new List<Account_Territory_Loader_vod__c>():mapatl.get(atl.Account_vod__c);
        tempatl.add(atl);
        mapatl.put(atl.Account_vod__c, tempatl);
    }
    
    map<Id, List<UserTerritory>> mapuserterritory = new map<Id, List<UserTerritory>>(); 
    set<id> territoryId = new set<id>();   
    for (UserTerritory ut :[select UserId, TerritoryId from userterritory where UserId in:ownerids]){
        List<UserTerritory> temput= (mapuserterritory.get(ut.UserId) == null)?new List<UserTerritory>():mapuserterritory.get(ut.UserId);
        temput.add(ut);
        mapuserterritory.put(ut.UserId, temput);
        territoryId.add(ut.TerritoryId);
    }
    
    map<id, string> mapterritoryName = new map<id, string>();
    for (Territory t : [SELECT Id, Name FROM Territory WHERE ID IN :territoryId]) {
        mapterritoryName.put(t.Id, t.Name);
    }
    
    map<Id, List<AccountShare>> mapAccShare = new map<Id, List<AccountShare>>();  
    set<id> userOrGroupId = new set<id>();     
    for(AccountShare accs:[select AccountId,UserOrGroupId, RowCause from AccountShare where AccountId in:accids]){
        List<AccountShare> temaccs= (mapAccShare.get(accs.AccountId) == null)?new List<AccountShare>():mapAccShare.get(accs.AccountId);
        temaccs.add(accs);
        mapAccShare.put(accs.AccountId, temaccs);
        userOrGroupId.add(accs.UserOrGroupId);
    }
           
            
    map<Id, List<Group>> mapGroup = new map<Id, List<Group>>();    
    set<id> relatedId = new set<id>();     
    for(Group gp:[select Id, RelatedId from group where Id in:UserOrGroupId]){
        List<Group> temgp= (mapGroup.get(gp.Id) == null)?new List<Group>():mapGroup.get(gp.Id);
        temgp.add(gp);
        mapGroup.put(gp.Id, temgp);
        relatedId.add(gp.RelatedId) ;
    }
    

    map<Id, List<Territory>> mapTerritory = new map<Id, List<Territory>>(); 
    for (Territory t : [SELECT Id, Name FROM Territory WHERE ID IN :relatedId]) {
        List<Territory> temTerritory= (mapTerritory.get(t.Id) == null)?new List<Territory>():mapTerritory.get(t.Id);
        temTerritory.add(t);
        mapTerritory.put(t.Id, temTerritory);
    }
    
    
    for(Event_Attendee_vod__c att:eventattendeeList){
        skipatl = false;
        // Get the territory for the user
        //build list of multiple territories and territory Ids
        List<UserTerritory> temput= (mapuserterritory.get(att.Medical_Event_vod__r.OwnerId) == null)?new List<UserTerritory>():mapuserterritory.get(att.Medical_Event_vod__r.OwnerId);
        if (temput.size() == 0) {
            skipatl = true;
        }
        // Get the name of the Territory
        List<String> userTerrNames = new List<String>();
        String terrString = '';
        for (UserTerritory ut :temput) {
            userTerrNames.add(mapterritoryName.get(ut.TerritoryId));
            terrString += (terrString == '')?mapterritoryName.get(ut.TerritoryId): ';' + mapterritoryName.get(ut.TerritoryId);
        }
        
        // Now check for existing territories for the Account
        List <AccountShare> asList = (mapAccShare.get(att.Account_vod__c)==null)?new List<AccountShare>():mapAccShare.get(att.Account_vod__c);
        
        // Get all the UserOrGroupIds, and store in a List
        List<Id> idlist1 = new List<Id> () ;
        Set<Id> manuallyAssignedGrpIDs = new Set<ID>();//capture what's manually assigned
        Set<Id> manuallyAssignedTerrIDs = new Set<ID>();//capture what's manually assigned
        List<Group> groupList = new List<Group>();
        for (AccountShare ash : asList) {
            idlist1.add(ash.UserOrGroupId) ;
            if(ash.RowCause=='TerritoryManual'){
                manuallyAssignedGrpIDs.add(ash.UserOrGroupId);   //capture what's manually assigned              
            }
            List<Group> temgp = (mapGroup.get(ash.UserOrGroupId) == null)?new List<Group>():mapGroup.get(ash.UserOrGroupId);
            groupList.addAll(temgp);
        }
        
        // Now let's get the IDs of the Territories
        List<Id> idlist2 = new List<Id> () ;
        List <Territory> terrList = new List <Territory>();
        for (Group g : groupList) {
            idlist2.add(g.RelatedId) ;
            if(manuallyAssignedGrpIDs.contains(g.Id)){
                manuallyAssignedTerrIDs.add(g.RelatedId); //capture what's manually assigned
            }
            List<Territory> temTerritory= (mapTerritory.get(g.RelatedId) == null)?new List<Territory>():mapTerritory.get(g.RelatedId);
            terrList.addAll(temTerritory);
        }
        
        
        // Finally, get the names
        for (Territory t : terrList) {
            // If the Territory is already there, output a warning and exit
            for (String utn : userTerrNames) {
                if (t.Name == utn) {
                    skipatl = true; 
                }
            }
            if(manuallyAssignedTerrIDs.contains(t.Id)){ //only add to the terrString if this is Manually already assigned
                terrString = (terrString == null || terrString =='null' || terrString =='')?t.Name :terrString + ';' + t.Name ;                    
            }
        }
        if(terrString == null || terrString =='null' || terrString ==''){
            skipatl = true; 
        }
        
        if(!skipatl){
            List <Account_Territory_Loader_vod__c> atlList = (mapatl.get(att.Account_vod__c) == null)?new List <Account_Territory_Loader_vod__c>():mapatl.get(att.Account_vod__c);
            Account_Territory_Loader_vod__c atl;
            if (atlList.size() > 0) {
                atl = atlList[0] ;
                atl.Territory_vod__c = terrString ;
            }
            else {
                atl = new Account_Territory_Loader_vod__c(Account_vod__c=att.Account_vod__c,External_ID_vod__c=att.Account_vod__c,Territory_vod__c=terrString);
            }
            atlupsertList.add(atl);
        }
    }
    
    //20180620 - Additional logic to combine ATLs for the same Account -and the Account does not have an ATL yet
    List <Account_Territory_Loader_vod__c> newATLList = new List <Account_Territory_Loader_vod__c>();
    if(atlupsertList.size() > 0){                
        for(Account_Territory_Loader_vod__c atl : atlupsertList){
            boolean found = false;
            for(Account_Territory_Loader_vod__c newATL : newATLList){
                if(atl.Account_vod__c == newATL.Account_vod__c){
                    found = true;                    
                    newATL.Territory_vod__c = newATL.Territory_vod__c + ';' + atl.Territory_vod__c;
                }
            }
            if(!found){
                newATLList.add(atl);
            }
        }
    
    }
    
    
    
    system.debug('*************************atlupsertList**************************' + atlupsertList);
    upsert newATLList;
    
}