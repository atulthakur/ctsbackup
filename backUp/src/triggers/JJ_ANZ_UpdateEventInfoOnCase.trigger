trigger JJ_ANZ_UpdateEventInfoOnCase  on JJ_Compalint_Event_Description__c (after insert) {

  List<Case> UpdateCases = new List<Case>();
  List<Id> CaseIds= new List<Id>();
  for(JJ_Compalint_Event_Description__c  EventInfo : Trigger.new)
  {
       if(EventInfo.JJ_ANZ_Description__c!=null & EventInfo.JJ_ANZ_Case__c!=null)
      {
          CaseIds.add(EventInfo.JJ_ANZ_Case__c);
      } 
  }
  Map<Id,Case> MapCases = new Map<Id,Case>([Select id,JJ_ANZ_EventDescription__c from Case where id in:CaseIds]);
  for(JJ_Compalint_Event_Description__c  EventInfo : Trigger.new)
  {
      if(EventInfo.JJ_ANZ_Description__c!=null & EventInfo.JJ_ANZ_Case__c!=null)
      {
           Case updateCase = MapCases.get(EventInfo.JJ_ANZ_Case__c);    
          if(updateCase.JJ_ANZ_EventDescription__c!=null)
          {
              updateCase.JJ_ANZ_EventDescription__c = updateCase.JJ_ANZ_EventDescription__c + ';'+EventInfo.JJ_ANZ_Description__c;
          }
          else
          {
              updateCase.JJ_ANZ_EventDescription__c = EventInfo.JJ_ANZ_Description__c;
          }
          UpdateCases.add(updateCase);
      }
  }
  
  if(!updateCases.isEmpty())
    {
      try
      {
          update updateCases;
      }
      catch(DMLException de)
      {
          System.debug('DMLException occurred '+de);
      }
      catch(Exception e)
      {
          System.debug('Exception occurred '+e);
      }
     }

}