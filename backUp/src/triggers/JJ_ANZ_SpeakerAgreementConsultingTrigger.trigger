/**
* @author Noel Lim, Veeva Systems
* @date 05/05/2015
* @description Trigger to support the 'Consulting Agreement' record type and its business logic.
* 1) Validate a Consulting Agreement of a HCP (Account) that it does not overlap with a HCP's existing Agreement for the same Group.
*    - Factors considered are for the same Account, same Consulting Group, date overlaps, and the Status of the other Agreement/s is valid.
*    - Trigger will cover the following scenarios:
*       i) Allow:: Create/Update a Speaker Agreement that's not RType CA, within different date range, different CG or Account, Status that is not covered
*       ii) Error:: Create/Update a Speaker Agreement of RType CA, with same Account/CG, and date range overlaps. Changes to any of those values will trigger error, e.g. change in RT
*
*/
trigger JJ_ANZ_SpeakerAgreementConsultingTrigger on Speaker_Agreement__c (before insert, before update) {
       
    
    if (trigger.isBefore){

        //Query the custom setting to know which Speaker Agreement record types to query for deduplication
        set<string> consAgreementDedupeRecTypes = new set<string>();
        for(JJ_ANZ_Janssen_Events__c je:[Select Consulting_Agreement_Overlap_RecTypes1__c, Consulting_Agreement_Overlap_RecTypes2__c From JJ_ANZ_Janssen_Events__c limit 1]){
            string s = (je.Consulting_Agreement_Overlap_RecTypes1__c == null)?'':je.Consulting_Agreement_Overlap_RecTypes1__c;
            consAgreementDedupeRecTypes.addAll(s.split(';'));
            s = (je.Consulting_Agreement_Overlap_RecTypes2__c == null)?'':je.Consulting_Agreement_Overlap_RecTypes2__c;
            consAgreementDedupeRecTypes.addAll(s.split(';'));
        }
        map<Id, String> mapRtDevname = new map<Id, String>();
        for(RecordType rt:[select Id, DeveloperName from RecordType where SobjectType=:'Speaker_Agreement__c' and DeveloperName in:consAgreementDedupeRecTypes]){
            mapRtDevname.put(rt.Id, rt.DeveloperName);
        }    

        //Query the custom setting to know which Speaker Agreement record types to query for deduplication
        set<string> consAgreementDedupeStatus = new set<string>();
        for(JJ_ANZ_Janssen_Events__c je:[Select Consulting_Agreement_Overlap_Status__c From JJ_ANZ_Janssen_Events__c limit 1]){
            string s = (je.Consulting_Agreement_Overlap_Status__c == null)?'':je.Consulting_Agreement_Overlap_Status__c;
            consAgreementDedupeStatus.addAll(s.split(';'));
        }

        //Check for Consulting Agreement RTs and Status in Trigger
        set<Id> consAgreementIds = new set<Id>(); 
        map<Id, Speaker_Agreement__c> mapConsAgreement = new map<Id, Speaker_Agreement__c>();   
        for(Speaker_Agreement__c consAgreement:trigger.new){
            if( mapRtDevname.containsKey(consAgreement.RecordTypeId) && consAgreementDedupeStatus.contains(consAgreement.JJ_ANZ_Speaker_Status__c)){
                consAgreementIds.add(consAgreement.Id);        
                mapConsAgreement.put(consAgreement.Id, consAgreement);
            }
        }

        if(consAgreementIds.size() > 0)
        {
            /** Run Overlap Validation */


            //Get a start/end date range to limit query
            string errorMsgPrepend = System.Label.JJ_ANZ_CONS_AGR_OVERLAP_ERROR;
            string errorMsgAppend = '';  
            Date earliestStartDate = null;
            Date latestEndDate = null;
            set<Id> consultantIds = new set<Id>();
            set<Id> consultingGroupIds = new set<Id>();         
            for(Speaker_Agreement__c consAgreement : mapConsAgreement.values()){      
                consultantIds.add(consAgreement.JJ_Speaker__c);
                consultingGroupIds.add(consAgreement.JJ_ANZ_Consulting_Group__c);
                Date caStartDate = consAgreement.JJ_Start_Date__c;
                Date caEndDate = consAgreement.JJ_End_Date__c;

                earliestStartDate = (earliestStartDate == null || earliestStartDate > caStartDate)? caStartDate : earliestStartDate;
                latestEndDate = (latestEndDate == null || latestEndDate < caEndDate)? caEndDate : latestEndDate;
            }

            System.debug('Earliest and Latest Date = ' + earliestStartDate + ' ' + latestEndDate);

            //Bulkified query to collect all Speaker Agreements. Note filter for RType and Status
            Speaker_Agreement__c consAgreementError = new Speaker_Agreement__c();            
            List<Speaker_Agreement__c> existingConsAgreements = [select Name, JJ_Speaker__c, JJ_ANZ_Consulting_Group__c, JJ_ANZ_Speaker_Status__c, JJ_Start_Date__c, JJ_End_Date__c,  RecordType.DeveloperName 
                                                            from Speaker_Agreement__c 
                                                            where 
                                                                RecordTypeId in: mapRtDevname.keySet() and 
                                                                JJ_ANZ_Speaker_Status__c in: consAgreementDedupeStatus and
                                                                JJ_Speaker__c in:consultantIds and 
                                                                JJ_ANZ_Consulting_Group__c in:consultingGroupIds and                                                                 
                                                                ((JJ_Start_Date__c >=: earliestStartDate and JJ_Start_Date__c <=: latestEndDate) 
                                                                    OR (JJ_End_Date__c >=: earliestStartDate and JJ_End_Date__c <=: latestEndDate) 
                                                                    OR (JJ_Start_Date__c >=: earliestStartDate and JJ_End_Date__c <=: latestEndDate) 
                                                                    OR (JJ_Start_Date__c <=: earliestStartDate and JJ_End_Date__c >=: latestEndDate)
                                                                ) and
                                                                Id not in:consAgreementIds];
                                                                
            //OVERLAP VALIDATION LOGIC
            for(Speaker_Agreement__c trigCA : mapConsAgreement.values()){
                for(Speaker_Agreement__c existingCA:existingConsAgreements){
                    if(existingCA.JJ_Speaker__c == trigCA.JJ_Speaker__c && 
                        existingCA.JJ_ANZ_Consulting_Group__c == trigCA.JJ_ANZ_Consulting_Group__c && 
                        ((existingCA.JJ_Start_Date__c >= trigCA.JJ_Start_Date__c && existingCA.JJ_Start_Date__c <= trigCA.JJ_End_Date__c) || (existingCA.JJ_End_Date__c >= trigCA.JJ_Start_Date__c && existingCA.JJ_End_Date__c <= trigCA.JJ_End_Date__c) || 
                            (existingCA.JJ_Start_Date__c >= trigCA.JJ_Start_Date__c && existingCA.JJ_End_Date__c <= trigCA.JJ_End_Date__c) || (existingCA.JJ_Start_Date__c <= trigCA.JJ_Start_Date__c && existingCA.JJ_End_Date__c >= trigCA.JJ_End_Date__c))
                    ){
                        errorMsgAppend += (errorMsgAppend == '')?existingCA.Name : ',' + existingCA.Name;
                        consAgreementError = trigCA;
                    }
                }
                //display error in case of date overlaping
                if(errorMsgAppend != null){      
                    consAgreementError.adderror(errorMsgPrepend +' ' + errorMsgAppend);
                }
            }

        }
    }

}