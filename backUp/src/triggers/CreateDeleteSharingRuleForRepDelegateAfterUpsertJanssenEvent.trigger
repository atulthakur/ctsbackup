/*****
***     Developed by: Khodabocus Zameer (ICBWORKS)
***     Description: This trigger is used to share visibility of Janssen Events with the Rep Delegate 2 of the Event and 
***                  to share visibility of accounts (JJ_Account__c) with:
***                  1) Janssen Event Owner on the Janssen Event,
***                  2) Rep Delegate 2 on the Janssen Event,
***                  
***                  when a new account (Speaker) is associated with an Event.
*****/


trigger CreateDeleteSharingRuleForRepDelegateAfterUpsertJanssenEvent on Medical_Event_vod__c (after insert, before update) {
    
    Map<Id, Medical_Event_vod__c> previousRepDelegateMap = new Map<Id, Medical_Event_vod__c>();
    Set<Id> oldRepDelegatesId = new Set<Id>();
    Set<Id> eventsId = new Set<Id>();
    List<Medical_Event_vod__Share> newEventShareList = new List<Medical_Event_vod__Share>();
    List<AccountShare> newAccountsShareList = new List<AccountShare>();
    Map<Id, String> medicalEventsOwnerAccessMap = new Map<Id, String>();
    Set<Id> oldOwnersId = new Set<Id>();
    Id ownerId = null;
    Set<Id> accountsId = new Set<Id>();
    
    for(Medical_Event_vod__c medEVt : trigger.new){
        //Set of the Janssen Events Id
        eventsId.add(medEVt.Id);
        //Id of the Janssen Event's Owner
        ownerId = medEVt.OwnerId;
    }
    
    if(eventsId.size() > 0 && ownerId != null){
        //Getting the access permissions to Janssen Events of the Janssen Event Owner. This is used to give the Rep Delegate 2 the same permissions.
        List<UserRecordAccess> ownerRecordAccessRules = [Select RecordId, MaxAccessLevel, HasTransferAccess, HasReadAccess, HasEditAccess, 
                                                          HasDeleteAccess, HasAllAccess 
                                                          From UserRecordAccess
                                                          where RecordId in :eventsId AND UserId = :ownerId];
                                                          
        for(UserRecordAccess recAccess : ownerRecordAccessRules){
            //Map of the permissions of the Owner
            medicalEventsOwnerAccessMap.put(recAccess.RecordId, recAccess.MaxAccessLevel);
        }
    }
    
    //This part is run on Update of Janssen Events only.    
    if(trigger.isUpdate){
        for(Medical_Event_vod__c medEVt : trigger.old){
            //Map of Janssen Event Record before applying the new changes to it.
            previousRepDelegateMap.put(medEVt.Id, medEVt);
        }
        
        if(eventsId.size() > 0){
            //Getting all the Account Associations related to this Event.
            List<Account_Event_Association__c> accEvAssociations = [select Id, JJ_Janssen_Event__c, JJ_Account__c, JJ_Speaker_Agreement__c
                                                                    from Account_Event_Association__c
                                                                    where JJ_Janssen_Event__c in :eventsId];
            
            for(Account_Event_Association__c aea : accEvAssociations){
                //Set of the Accounts Id related to this Event
                accountsId.add(aea.JJ_Account__c);
            }
            
            for(Medical_Event_vod__c medEVt : trigger.new){
                //Getting the old record of the Janssen Event before changes are saved
                Medical_Event_vod__c jEventOldRecord = previousRepDelegateMap.get(medEVt.Id);
                
                if(jEventOldRecord != null){
                    //Creating new shares only if previous value of Rep Delegate 2 was not equal to new value
                    if(jEventOldRecord.Rep_Delegate_2__c != medEVt.Rep_Delegate_2__c){
                        if(jEventOldRecord.Rep_Delegate_2__c != null){
                            //Set of the old Rep Delegates 2 Id. This will be used to get and delete all previous Janssen Event & Account shares
                            //for the old Rep Delegates. 
                            oldRepDelegatesId.add(jEventOldRecord.Rep_Delegate_2__c);
                        }
                        if(medEVt.Rep_Delegate_2__c != null){
                            //New Janssen Event share with the new Rep Delegate 2
                            Medical_Event_vod__Share newMedShare = new Medical_Event_vod__Share();
                    
                            newMedShare.ParentId = medEVt.Id;
                            newMedShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                            
                            //Applying the same access permissions (if any) to the Events for the Rep Delegate 2
                            if(medicalEventsOwnerAccessMap != null && medicalEventsOwnerAccessMap.size() > 0){
                                String accessLevl = medicalEventsOwnerAccessMap.get(medEVt.Id);
                                
                                if(accessLevl != ''){
                                    if(accessLevl == 'All'){
                                        newMedShare.AccessLevel = 'Edit';
                                    }else if(accessLevl == 'Read'){
                                        newMedShare.AccessLevel = 'Read';
                                    }else if(accessLevl == 'Delete'){
                                        newMedShare.AccessLevel = 'Edit';
                                    }
                                }
                            }else{
                                //If no existing permissions for the Owner of the Event, then give only Read access to the Rep Delegate 2
                                newMedShare.AccessLevel = 'Read';
                            }
                            
                            newEventShareList.add(newMedShare);
                            
                            //Creating new Account shares for the Rep Delegate 2. This will allow the Rep Delegate 2 to view the 
                            //Accounts related to this Event
                            if(accEvAssociations.size() > 0){
                                for(Account_Event_Association__c aea : accEvAssociations){
                                    if(aea.JJ_Janssen_Event__c == medEVt.Id){
                                        //New Account share with the new Rep Delegate 2
                                        AccountShare newAccShare = new AccountShare();
                            
                                        newAccShare.AccountId = aea.JJ_Account__c;
                                        newAccShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                                        newAccShare.OpportunityAccessLevel = 'None';
                                        
                                        newAccShare.AccountAccessLevel = 'Edit';
                                        
                                        newAccountsShareList.add(newAccShare);
                                    }
                                }
                            }
                        }
                    }
                    
                    if(jEventOldRecord.OwnerId != medEVt.OwnerId){
                        //Set of the old Owners Id. This will be used to get and delete all previous Account shares
                        //for the old Owners.
                        oldOwnersId.add(jEventOldRecord.OwnerId);
                        
                        if(accEvAssociations.size() > 0){
                            for(Account_Event_Association__c aea : accEvAssociations){
                                if(aea.JJ_Janssen_Event__c == medEVt.Id){
                                    //New Account share with the Owner of the Janssen Event
                                    AccountShare newAccShare = new AccountShare();
                        
                                    newAccShare.AccountId = aea.JJ_Account__c;
                                    newAccShare.UserOrGroupId = medEVt.OwnerId;
                                    newAccShare.OpportunityAccessLevel = 'None';
                                    
                                    newAccShare.AccountAccessLevel = 'Edit';
                                    
                                    newAccountsShareList.add(newAccShare);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //Getting the old Janssen Events Shares for the old Rep Delegate 2 to be deleted
        if(oldRepDelegatesId.size() > 0 && eventsId.size() > 0){
            List<Medical_Event_vod__Share> oldSharingRules = [select Id
                                                              From Medical_Event_vod__Share
                                                              where ParentId in :eventsId AND UserOrGroupId in :oldRepDelegatesId];
            
            if(oldSharingRules.size() > 0){ 
                //Deleting the old Janssen Events Shares for the Rep Delegate 2                                     
                delete oldSharingRules;
            }
            //Getting the old Accounts Shares for the old Rep Delegate 2 to be deleted
            if(accountsId.size() > 0){
                List<AccountShare> oldAccSharingRules = [select Id From AccountShare where AccountId in :accountsId 
                                                         AND UserOrGroupId in :oldRepDelegatesId];
                
                if(oldAccSharingRules.size() > 0){  
                    //Deleting the old Accounts Shares for the Rep Delegate 2                                       
                    delete oldAccSharingRules;
                }
            }
        }
        //Getting the old Accounts Shares for the old Owners to be deleted
        if(oldOwnersId.size() > 0 && accountsId.size() > 0){
            List<AccountShare> oldAccSharingRules = [select Id
                                                     From AccountShare
                                                     where AccountId in :accountsId AND UserOrGroupId in :oldOwnersId
                                                     AND RowCause = 'Manual'];
            
            if(oldAccSharingRules.size() > 0){  
                //Deleting the old Accounts Shares for the Owners                                   
                delete oldAccSharingRules;
            }
        }

        if(newEventShareList.size() > 0){
            //Inserting the new Janssen Event shares for the new Rep Delegates 2
            insert newEventShareList;
        }
        if(newAccountsShareList.size() > 0){
            //Inserting the new Account shares for the new Rep Delegates 2 and Owners
            insert newAccountsShareList;
        }
    }
    
    //This part is run on Insert of Janssen Event only to share the Event with the Rep Delegate 2.
    else if(trigger.isInsert){
        for(Medical_Event_vod__c medEVt : trigger.new){
            if(medEVt.Rep_Delegate_2__c != null){
                //New Janssen Event share with the new Rep Delegate 2
                Medical_Event_vod__Share newMedShare = new Medical_Event_vod__Share();
                
                newMedShare.ParentId = medEVt.Id;
                newMedShare.UserOrGroupId = medEVt.Rep_Delegate_2__c;
                
                //Applying the same access permissions (if any) to the Events for the Rep Delegate 2
                if(medicalEventsOwnerAccessMap != null && medicalEventsOwnerAccessMap.size() > 0){
                    String accessLevl = medicalEventsOwnerAccessMap.get(medEVt.Id);
                    
                    if(accessLevl != ''){
                        if(accessLevl == 'All'){
                            newMedShare.AccessLevel = 'Edit';
                        }else if(accessLevl == 'Read'){
                            newMedShare.AccessLevel = 'Read';
                        }else if(accessLevl == 'Delete'){
                            newMedShare.AccessLevel = 'Edit';
                        }
                    }
                }else{
                    //If no existing permissions for the Owner of the Event, then give only Read access to the Rep Delegate 2
                    newMedShare.AccessLevel = 'Read';
                }
                
                newEventShareList.add(newMedShare);
            }
        }
        
        if(newEventShareList.size() > 0){
            //Inserting the new Janssen Event shares for the new Rep Delegates 2
            insert newEventShareList;
        }
    }
}