trigger JJ_ANZ_CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
if (TriggerDisabler__c.getInstance().JJ_ANZ_CaseTrigger__c) {return;}
    JJ_ANZ_CaseTriggerHandler handler = new JJ_ANZ_CaseTriggerHandler(true);

    /* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }      
    /* After Insert */
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
    /* Before Update */
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    }
    /* After Update */
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap);
    }
    /* Before Delete */
    else if(Trigger.isDelete && Trigger.isBefore){
      handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    /* After Delete */
    else if(Trigger.isDelete && Trigger.isAfter){
       handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
    }

    /* After Undelete */
    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);
    }
    If((Trigger.IsInsert||Trigger.IsUpdate)&&Trigger.IsBefore){
        handler.populatesalution(Trigger.new, Trigger.oldMap);
    }
    If(Trigger.IsAfter&&Trigger.IsUpdate){
        handler.populateSeriousness_Email(Trigger.new, Trigger.oldMap);
    }
                

}