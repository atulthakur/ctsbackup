/**
* Trigger to set the email address for the owners manager. Used by a worlflow because manager field is not availabe
* via default SFDC UI for email notifications
* Created: July 8 2013
* Modified July 8 2013
* Bluewolf  
**/

/************************************************************
Related requirements: DMP1 - Fetches the manager email for
                             workflow notifications
*************************************************************/
trigger JanssenEventTrigger on Medical_Event_vod__c (before insert, before update) {
    List<ID> users=new List<ID>();
    for(Medical_Event_vod__c ev:Trigger.new){
        users.add(ev.OwnerId);      
    }
    for(user u:[SELECT Id,Manager.Email FROM User WHERE Id in:users]){
        for(Medical_Event_vod__c ev:Trigger.new){
            if(ev.OwnerId==u.Id){
                ev.JJ_Manager_Email__c=u.Manager.Email;
            }
        }
    }
}