/**
* @author Noel Lim, Veeva Systems
* @date 06/12/2016
* @description 
* Prevent deletion of Grant records for all Profiles unless it is in Draft status
*/
trigger JJ_ANZ_GrantTriggers on JJ_ANZ_Grant__c (before delete) {
    if(trigger.isBefore && trigger.isDelete){
        for( JJ_ANZ_Grant__c grant :Trigger.old){
            if(grant.JJ_ANZ_Status__c != 'Draft'){
                grant.addError('Grants can only be deleted while in Draft status');
            }
        }
    }
}