trigger Sample_Quota_Before_UPDINS on JJ_ANZ_Sample_Quotas__c (Before Insert, Before Update) {
//setting external id to ensure uniqueness for user/product pairing accross records, as well as setting the owner to the rep for proper data visibility
For (JJ_ANZ_Sample_Quotas__c SQ : trigger.new){
        SQ.jj_anz_external_id__c = SQ.jj_anz_representative__c + '_' + SQ.jj_anz_sample_product__r.name;
        If (SQ.jj_anz_representative__c != null && SQ.jj_anz_representative__c != SQ.ownerid) {
            SQ.ownerid = SQ.jj_anz_representative__c;
            }
        
        }
}