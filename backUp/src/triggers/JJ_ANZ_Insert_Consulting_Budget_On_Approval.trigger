trigger JJ_ANZ_Insert_Consulting_Budget_On_Approval on Speaker_Agreement__c (after update) {
/*
   This trigger creates a line in Event Budget for Consulting Agreement when it has been approved.
   Planned Budget Amount = Consultant Hourly Rate * Anticipated max. # of hours.
*/

    List<Event_Budget__c> budgetToInsert = new List<Event_Budget__c>();
    
    for(Speaker_Agreement__c o : Trigger.new)  {
    
        //check if Status has been changed to Approved
        if (o.JJ_ANZ_Speaker_Status__c != Trigger.oldMap.get(o.Id).JJ_ANZ_Speaker_Status__c && o.JJ_ANZ_Speaker_Status__c == 'Approved') { 
            // check it is Consultant Agreement
            if (o.JJ_Type__c == 'Consultant Agreement') {

                Id recordtypeid = [SELECT ID FROM RecordType WHERE Name =: 'Fee for Service Payment' and SobjectType=:'Event_Budget__c'].Id;
                String dummyjanevtid = [select JJ_ANZ_Dummy_Event_FFS_Budget__c from JJ_ANZ_Janssen_Events__c limit 1].JJ_ANZ_Dummy_Event_FFS_Budget__c;

                // insert for first time approval only by checking if there are existing records in Event Budget for this Fee for Service agreement
                Event_Budget__c[] existingEB = [SELECT Id FROM Event_Budget__c WHERE JJ_ANZ_Fee_For_Service__c = :o.Id and RecordTypeId = :recordtypeid];
                if(existingEB.size() == 0) {
     
                    Event_Budget__c v = new Event_Budget__c(); 
    
                    v.RecordTypeId = recordtypeid;
                    v.Janssen_Event__c = dummyjanevtid;        // dummy Janssen Event Id
                    v.Account__c = o.JJ_Speaker__c;
                    v.JJ_ANZ_Fee_For_Service__c = o.Id; 
                    v.JJ_ANZ_Consulting_Group__c = o.JJ_ANZ_Consulting_Group__c;
                    v.JJ_Amount_Plan__c = o.JJ_ANZ_Consultant_Hourly_Rate__c * o.JJ_ANZ_Approx_of_Hours__c;
                    v.Type__c = 'Consultancy';
    
                    budgetToInsert.add(v);        
               }
            }
        }
    }

    try {
        insert budgetToInsert;
    } catch (system.Dmlexception e) {
        system.debug (e);
    }
    
}