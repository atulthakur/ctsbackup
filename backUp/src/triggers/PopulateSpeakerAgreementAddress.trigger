//ICBWORKS
/*
Speaker Agreement Address Trigger. 
We need the following fields to be placed on the Speaker Agreement object from the linked 
relevant Speaker's "Address object" Fields via JJ_Speaker__c on the Speaker agreement to go 
to are - Street_c, Street_2__c, Suburb__c, Postcode__c, State__c, Country__c
*/

trigger PopulateSpeakerAgreementAddress on Speaker_Agreement__c (before insert, before update) {
    set<Id> accIds = new set<Id>();
    for(Speaker_Agreement__c sp:trigger.new){
        accIds.add(sp.JJ_Speaker__c);
    }
    
    map<Id, Address_vod__c> mapaddress = new map<Id, Address_vod__c>();
    for(Account acc:[Select ID, (Select Name, Zip_vod__c, State_vod__c,Address_line_2_vod__c, Country_vod__c, City_vod__c, JJ_Core_Suburb__c From Address_vod__r Where Primary_vod__c = true limit 1) From Account where Id in:accIds]){
        for(Address_vod__c addr:acc.Address_vod__r){
            mapaddress.put(acc.Id, addr);
        }
    }
    
    
    for(Speaker_Agreement__c sp:trigger.new){
        sp.Street__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).Name;
        sp.Street2__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).Address_line_2_vod__c;
        sp.Suburb__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).City_vod__c;//JJ_Core_Suburb__c
        sp.Postcode__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).Zip_vod__c;
        sp.State__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).State_vod__c;
        sp.Country__c = (mapaddress.get(sp.JJ_Speaker__c) == null)?null:mapaddress.get(sp.JJ_Speaker__c).Country_vod__c;
    }
    
    
}