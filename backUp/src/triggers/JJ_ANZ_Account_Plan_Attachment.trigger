/* Loganathan Sekar 18/04/18
     The purpose of this trigger is to change the last modified date on account plan if any new attachment added in to system */


trigger JJ_ANZ_Account_Plan_Attachment on Attachment (after insert,after update) {
    
    list<Attachment> attach = new list<Attachment>();
    Set<ID> Accplids = new Set<ID>();
    
    for (Attachment att : Trigger.new) {    
         attach.add(att);                  
         Accplids.add(att.ParentID);     
        
    }
    
     List<Account_Plan_vod__c> Accid = [Select ID,LastModifiedById,LastModifiedDate,JJ_ANZ_Last_update_on_Attachment__c from Account_Plan_vod__c where ID IN: Accplids];
       
       for(Attachment at : attach){
           for(Account_Plan_vod__c Accpid : Accid){
               if(accpid.id == at.parentid){
            
                  Accpid.JJ_ANZ_Last_update_on_Attachment__c = at.LastModifiedDate;}
                
                 update Accpid;
                 } 
     }
}