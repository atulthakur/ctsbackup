//developped by Icbworks
//trigger to prevent user edit/delete speaker association when event status is not Planning or Rejected
trigger JJ_ANZ_Speaker_Association_ValidationError on JJ_ANZ_Speaker_Association__c (before delete, before update) {
    
    string errormsgstr = '';
    for(Message_vod__c m:[Select Text_vod__c From Message_vod__c where Name=:'Speaker Association Cannot Modify' and Category_vod__c =:'Events' and Active_vod__c =:true]){
        errormsgstr = m.Text_vod__c;
    }
    
    //get all events related to the speaker association
    set<Id> eventids = new set<Id>();    
    if(trigger.isUpdate){
        for(JJ_ANZ_Speaker_Association__c sa:trigger.new){
            eventids.add(sa.JJ_ANZ_Medical_Event__c);
        }
    }
    else if(trigger.isDelete){    
        for(JJ_ANZ_Speaker_Association__c sa:trigger.old){
            eventids.add(sa.JJ_ANZ_Medical_Event__c);       
        }
    }
    
    //event record types which cannot modify the speaker association
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Events_Speaker_Agreement_Lock_1__c, JJ_ANZ_Events_Speaker_Agreement_Lock_2__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.JJ_ANZ_Events_Speaker_Agreement_Lock_1__c == null)?'':je.JJ_ANZ_Events_Speaker_Agreement_Lock_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Events_Speaker_Agreement_Lock_2__c == null)?'':je.JJ_ANZ_Events_Speaker_Agreement_Lock_2__c;
        jerectype.addAll(s.split(';'));
    }
    
    //event status which cannot modify the speaker association
    set<string> setstatus = new set<String>(); 
    setstatus.add('Awaiting Approval');
    setstatus.add('Approved');
    setstatus.add('Close Out');
    setstatus.add('Reconciliation');
    setstatus.add('Completed'); 
    
    //check the event status and record type to see if there should be a validation error
    map<Id, boolean> mapvalerror = new map<Id, boolean>();
    for(Medical_Event_vod__c e:[select Id from Medical_Event_vod__c where Id in:eventids and RecordType.DeveloperName in:jerectype and JJ_ANZ_Event_Status__c in:setstatus]){
        mapvalerror.put(e.Id, true);
    }
        
    //display error in case there is validation error
    if(trigger.isUpdate){
        for(JJ_ANZ_Speaker_Association__c sa:trigger.new){
            if(mapvalerror.get(sa.JJ_ANZ_Medical_Event__c) != null){
                sa.addError(errormsgstr);
            }
        }
    }
    else if(trigger.isDelete){    
        for(JJ_ANZ_Speaker_Association__c sa:trigger.old){
            if(mapvalerror.get(sa.JJ_ANZ_Medical_Event__c) != null){
                sa.addError(errormsgstr);
            }
        }
    }
    
    
}