trigger UpdateCongaJanssenEventFieldsBeforeUpdateJanssenEvent on Medical_Event_vod__c (before update) {
    
    Set<Id> meetingEventsId = new Set<Id>();
    Map<Id, List<Janssen_Event_Agenda__c>> meetingEventAgendaMap = new Map<Id, List<Janssen_Event_Agenda__c>>();
    List<Janssen_Event_Agenda__c> agendaList = new List<Janssen_Event_Agenda__c>();
    integer counter = 0;
    Id previousMeetingEventId = null;
    Set<Id> accountsId = new Set<Id>();
    Set<Id> speakerAgsId = new Set<Id>();
    Map<Id, Account> accountsMap = new Map<Id, Account>();
    Map<Id, Speaker_Agreement__c> speakerAgreementsMap = new Map<Id, Speaker_Agreement__c>();
    Map<Id, List<JJ_ANZ_Speaker_Association__c>> acctEventAssociationsMap = new Map<Id, List<JJ_ANZ_Speaker_Association__c>>();
    List<JJ_ANZ_Speaker_Association__c> acctEventAssociationsList = new List<JJ_ANZ_Speaker_Association__c>();
    Set<Id> rsvpContacts1Id = new Set<Id>();
    Set<Id> rsvpContacts2Id = new Set<Id>();
    Set<Id> rsvpContacts3Id = new Set<Id>();
    Map<Id, User> rsvpContacts1Map = new Map<Id, User>();
    Map<Id, User> rsvpContacts2Map = new Map<Id, User>();
    Map<Id, User> rsvpContacts3Map = new Map<Id, User>();
    Set<Id> orgsId = new Set<Id>();
    Map<Id, Address_vod__c> orgAddressMap = new Map<Id, Address_vod__c>();
    
    for(Medical_Event_vod__c me : trigger.new){
        if(me.JJ_ANZ_Event_Status__c == 'Approved' || me.Process_Status__c == 'Approved'){
            meetingEventsId.add(me.Id);
            rsvpContacts1Id.add(me.RSVP_Contact_1__c);
            rsvpContacts2Id.add(me.RSVP_Contact_2__c);
            rsvpContacts3Id.add(me.JJ_RSVP_Contact_3__c);
            orgsId.add(me.JJ_Organization__c);
        }
    }
    
    if(meetingEventsId.size() > 0){
        List<Janssen_Event_Agenda__c> eventAgenda = [select Id, Time__c, Janssen_Event__c, Description__c 
                                                      From Janssen_Event_Agenda__c
                                                      where Janssen_Event__c in :meetingEventsId order by Janssen_Event__c,Time__c];
                                                      
        for(Janssen_Event_Agenda__c evAgenda : eventAgenda){
            if(counter == 0){
                previousMeetingEventId = evAgenda.Janssen_Event__c;
            }
            if(previousMeetingEventId == evAgenda.Janssen_Event__c){
                agendaList.add(evAgenda);
            }else{
                meetingEventAgendaMap.put(previousMeetingEventId, agendaList);
                
                previousMeetingEventId = evAgenda.Janssen_Event__c;
                agendaList = new List<Janssen_Event_Agenda__c>();
                agendaList.add(evAgenda);
            }
            if(counter == eventAgenda.size() - 1){
                meetingEventAgendaMap.put(previousMeetingEventId, agendaList);
            }
            counter++;
        }
        
        counter = 0;
        previousMeetingEventId = null;

        List<JJ_ANZ_Speaker_Association__c> accountEventAssociations = [select Id, JJ_ANZ_Speaker_Agreement__c, JJ_ANZ_Medical_Event__c, JJ_ANZ_Account__c
                                                                        From JJ_ANZ_Speaker_Association__c
                                                                        where JJ_ANZ_Medical_Event__c in :meetingEventsId
                                                                        order by JJ_ANZ_Medical_Event__c];
        
        /* -- code with reference to old object 
        List<JJ_ANZ_Speaker_Association__c> accountEventAssociations = [select Id, JJ_Speaker_Agreement__c, JJ_Janssen_Event__c, JJ_Account__c
                                                                       From JJ_ANZ_Speaker_Association__c
                                                                       where JJ_Janssen_Event__c in :meetingEventsId 
                                                                       order by JJ_Janssen_Event__c];
        */
                                                                       
        for(JJ_ANZ_Speaker_Association__c aea : accountEventAssociations){
            accountsId.add(aea.JJ_ANZ_Account__c);
            speakerAgsId.add(aea.JJ_ANZ_Speaker_Agreement__c);
        }
        
        if(accountsId.size() > 0){
            List<Account> accounts = [select Id, LastName, FirstName, Salutation from Account where Id in :accountsId];
            
            for(Account acc : accounts){
                accountsMap.put(acc.Id, acc);
            }
        }
        
        if(speakerAgsId.size() > 0){
            List<Speaker_Agreement__c> speakerAgreements = [select Id, JJ_Speaker_Credentials__c, JJ_Bio__c, JJ_Bio_Approved__c, JJ_Speaker__c 
                                                            From Speaker_Agreement__c
                                                            where Id in :speakerAgsId order by JJ_Start_Date__c];
                                                            
            for(Speaker_Agreement__c spa : speakerAgreements){
                speakerAgreementsMap.put(spa.Id, spa);
            }
        }
        
        for(JJ_ANZ_Speaker_Association__c aea : accountEventAssociations){
            if(counter == 0){
                previousMeetingEventId = aea.JJ_ANZ_Medical_Event__c;
            }
            if(previousMeetingEventId == aea.JJ_ANZ_Medical_Event__c){
                acctEventAssociationsList.add(aea);
            }else{
                acctEventAssociationsMap.put(previousMeetingEventId, acctEventAssociationsList);
                
                previousMeetingEventId = aea.JJ_ANZ_Medical_Event__c;
                acctEventAssociationsList = new List<JJ_ANZ_Speaker_Association__c>();
                
                acctEventAssociationsList.add(aea);
            }
            if(counter == accountEventAssociations.size() - 1){
                acctEventAssociationsMap.put(previousMeetingEventId, acctEventAssociationsList);
            }
            counter++;
        }
        
        if(rsvpContacts1Id.size() > 0){
            List<User> rsvp1Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts1Id];
            
            for(User user : rsvp1Contacts){
                rsvpContacts1Map.put(user.Id, user);
            }
        }
        
        if(rsvpContacts2Id.size() > 0){
            List<User> rsvp2Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts2Id];
            
            for(User user : rsvp2Contacts){
                rsvpContacts2Map.put(user.Id, user);
            }
        }
        
        if(rsvpContacts3Id.size() > 0){
            List<User> rsvp3Contacts = [select Id, Name, MobilePhone, Email From User where Id in :rsvpContacts3Id];
            
            for(User user : rsvp3Contacts){
                rsvpContacts3Map.put(user.Id, user);
            }
        }
        
        if(orgsId.size() > 0){
            List<Address_vod__c> organisationAddresses = [select Id, Account_vod__c, Zip_vod__c, State_vod__c, Primary_vod__c, Name, City_vod__c, Address_line_2_vod__c 
                                                          From Address_vod__c
                                                          where Account_vod__c in :orgsId AND Primary_vod__c = true
                                                          order by Account_vod__c];
                                                          
            for(Address_vod__c add : organisationAddresses){
                orgAddressMap.put(add.Account_vod__c, add);
            }
        }
        
        agendaList = new List<Janssen_Event_Agenda__c>();
        acctEventAssociationsList = new List<JJ_ANZ_Speaker_Association__c>();
        
        for(Medical_Event_vod__c me : trigger.new){
            if(meetingEventAgendaMap != null){
                agendaList = meetingEventAgendaMap.get(me.Id);
                
                if(agendaList != null && agendaList.size() > 0){
                    integer countAgenda = 1;
                    
                    for(Janssen_Event_Agenda__c ev : agendaList){
                        if(countAgenda == 1){
                            me.JJ_Agenda01__c = ev.Description__c;
                            me.JJ_Agenda01_Time__c = ev.Time__c;
                        }else if(countAgenda == 2){
                            me.JJ_Agenda02__c = ev.Description__c;
                            me.JJ_Agenda02_Time__c = ev.Time__c;
                        }else if(countAgenda == 3){
                            me.JJ_Agenda03__c = ev.Description__c;
                            me.JJ_Agenda03_Time__c = ev.Time__c;
                        }else if(countAgenda == 4){
                            me.JJ_Agenda04__c = ev.Description__c;
                            me.JJ_Agenda04_Time__c = ev.Time__c;
                        }else if(countAgenda == 5){
                            me.JJ_Agenda05__c = ev.Description__c;
                            me.JJ_Agenda05_Time__c = ev.Time__c;
                        }else if(countAgenda == 6){
                            me.JJ_Agenda06__c = ev.Description__c;
                            me.JJ_Agenda06_Time__c = ev.Time__c;
                        }else if(countAgenda == 7){
                            me.JJ_Agenda07__c = ev.Description__c;
                            me.JJ_Agenda07_Time__c = ev.Time__c;
                        }else if(countAgenda == 8){
                            me.JJ_Agenda08__c = ev.Description__c;
                            me.JJ_Agenda08_Time__c = ev.Time__c;
                        }else if(countAgenda == 9){
                            me.JJ_Agenda09__c = ev.Description__c;
                            me.JJ_Agenda09_Time__c = ev.Time__c;
                        }else if(countAgenda == 10){
                            me.JJ_Agenda10__c = ev.Description__c;
                            me.JJ_Agenda10_Time__c = ev.Time__c;
                        }
                        countAgenda++;
                    }
                }
            }
            
            if(acctEventAssociationsMap != null && acctEventAssociationsMap.size() > 0){
                acctEventAssociationsList = acctEventAssociationsMap.get(me.Id);
                
                if(acctEventAssociationsList != null && acctEventAssociationsList.size() > 0){
                    integer countAEA = 1;
                    
                    for(JJ_ANZ_Speaker_Association__c aea : acctEventAssociationsList){
                        Speaker_Agreement__c speaker = speakerAgreementsMap.get(aea.JJ_ANZ_Speaker_Agreement__c);
                        
                        if(speaker != null){
                            Account acc = accountsMap.get(speaker.JJ_Speaker__c);
                            
                            if(acc != null){
                                if(countAEA == 1){
                                    me.JJ_Speaker1_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                    me.JJ_Speaker1_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                    me.JJ_Speaker1_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                    me.JJ_Speaker1_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                }else if(countAEA == 2){
                                    me.JJ_Speaker2_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                    me.JJ_Speaker2_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                    me.JJ_Speaker2_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                    me.JJ_Speaker2_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                }else if(countAEA == 3){
                                    me.JJ_Speaker3_Name__c = ((acc.Salutation == '') ? '':acc.Salutation) + ' ' + ((acc.FirstName == '') ? '':acc.FirstName) + ' ' + ((acc.LastName == '') ? '':acc.LastName);
                                    me.JJ_Speaker3_Credentials__c = (speaker.JJ_Speaker_Credentials__c == '') ? '':speaker.JJ_Speaker_Credentials__c;
                                    me.JJ_Speaker3_Bio__c = (speaker.JJ_Bio__c == '') ? '':speaker.JJ_Bio__c;
                                    me.JJ_Speaker3_Bio_Approved__c = speaker.JJ_Bio_Approved__c;
                                }
                                countAEA++;
                            }
                        }
                    }
                }
            }
            
            if(rsvpContacts1Map.size() > 0){
                User rsvpContact1 = rsvpContacts1Map.get(me.RSVP_Contact_1__c);
                
                if(rsvpContact1 != null){
                    me.JJ_RSVP1_Name__c = rsvpContact1.Name;
                    me.JJ_RSVP1_Email__c = rsvpContact1.Email;
                    me.JJ_RSVP1_Mobile__c = rsvpContact1.MobilePhone;
                }
            }
            
            if(rsvpContacts2Map.size() > 0){
                User rsvpContact2 = rsvpContacts2Map.get(me.RSVP_Contact_2__c);
                
                if(rsvpContact2 != null){
                    me.JJ_RSVP2_Name__c = rsvpContact2.Name;
                    me.JJ_RSVP2_Email__c = rsvpContact2.Email;
                    me.JJ_RSVP2_Mobile__c = rsvpContact2.MobilePhone;
                }
            }
            
            if(rsvpContacts3Map.size() > 0){
                User rsvpContact3 = rsvpContacts3Map.get(me.JJ_RSVP_Contact_3__c);
                
                if(rsvpContact3 != null){
                    me.JJ_RSVP3_Name__c = rsvpContact3.Name;
                    me.JJ_RSVP3_Email__c = rsvpContact3.Email;
                    me.JJ_RSVP3_Mobile__c = rsvpContact3.MobilePhone;
                }
            }
            
            if(orgAddressMap != null){
                if(me.JJ_Organization__c != null){
                    Address_vod__c add = orgAddressMap.get(me.JJ_Organization__c);
                    
                    if(add != null){
                        me.JJ_Organisation_Street__c = add.Name;
                        me.JJ_Organisation_Street2__c = add.Address_line_2_vod__c;
                        me.JJ_Organisation_Suburb__c = add.City_vod__c;
                        me.JJ_Organisation_State__c = add.State_vod__c;
                        me.JJ_Organisation_Postcode__c = add.Zip_vod__c;
                    }
                }
            }
        }
    }
}