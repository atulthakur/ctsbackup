trigger JJ_ANZ_AFTER_INSUPDEL on JJ_ANZ_Territory_Management__c (after insert, after update, before delete) {

    /* Nathan Silver 03/06/13 
     The purpose of this trigger is to allow for multiple brick records on the Territory Management object
     to look up to the same brick to terr record. These multiple records are maintained separately by 
     individual teams within Janssen, and the separate territory values held in these fields are concatenated
     correctly in the one record on the brick to territory table needed for territory alignment rules to
     function properly.*/
    List<brick_to_terr_vod__c> updates = new List<brick_to_terr_vod__c>();
    
    If(trigger.isupdate){
    for (Integer i = 0 ;  i < Trigger.new.size(); i++)  {
        If(trigger.new[i].jj_anz_territory__c != trigger.old[i].jj_anz_territory__c){
            jj_anz_territory_management__c TerrUpd = (jj_anz_territory_management__c)trigger.new[i];
            String terrlist = [select id , territory_vod__c from brick_to_terr_vod__c
                                                       where id = :TerrUpd.jj_anz_brick__c].territory_vod__c;
            string oldterr = trigger.old[i].jj_anz_territory__c;
            string newterr = trigger.new[i].jj_anz_territory__c;
            terrlist = terrlist.replace(oldterr , newterr);
            Brick_to_terr_vod__c updbrick = new Brick_to_terr_vod__c(id = trigger.new[i].jj_anz_brick__c , territory_vod__c = terrlist);
            updates.add(updbrick);
        }
    }
    }
    If(trigger.isinsert){
        for (jj_anz_territory_management__c TerrMan :trigger.new){
            String terrlist = [select id , territory_vod__c from brick_to_terr_vod__c
                                                       where id = :TerrMan.jj_anz_brick__c].territory_vod__c;
            string newterr = TerrMan.jj_anz_territory__c;
            If(terrlist == null){
                terrlist = ';';
            }
           
            If(!terrlist.contains(newterr)){
                terrlist += newterr;
                terrlist += ';';
            Brick_to_terr_vod__c updbrick = new Brick_to_terr_vod__c(id = TerrMan.jj_anz_brick__c , territory_vod__c = terrlist);
            updates.add(updbrick);
        }
        }
    }
        If(trigger.isdelete){
            for(jj_anz_territory_management__c terrdel : trigger.old){
            String terrlist = [select id , territory_vod__c from brick_to_terr_vod__c
                                                       where id = :Terrdel.jj_anz_brick__c].territory_vod__c;
            string oldterr = terrdel.jj_anz_territory__c;
            If(terrlist.contains(oldterr)){
            terrlist = terrlist.remove(oldterr + ';');
            }
            Brick_to_terr_vod__c updbrick = new Brick_to_terr_vod__c(id = terrdel.jj_anz_brick__c , territory_vod__c = terrlist);
            updates.add(updbrick);
        }
    }
    
    update updates;
            
            
            
        
        
}