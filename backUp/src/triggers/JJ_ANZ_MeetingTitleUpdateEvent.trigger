/**
Developped by ICBWORKS
Trigger to Request Approval Process for each Event related to the Meeting Title
*/
trigger JJ_ANZ_MeetingTitleUpdateEvent on Meeting_Titles__c (after update) {
   
    //save old meeting values o be compared to new values
    map<Id,Meeting_Titles__c> mapoldmeet = trigger.oldmap;
    
    //save the meeting Ids whose meeting title Status changes from Awaiting Approval to Approved OR Rejected OR Exception
    set<Id> meetIds = new set<Id>();    
    for(Meeting_Titles__c meet:trigger.new){
        if(meet.JJ_Meeting_Title_Status__c != mapoldmeet.get(meet.Id).JJ_Meeting_Title_Status__c){
            meetIds.add(meet.Id);        
        }
    }
        
    //query the custom setting to know which janssen event record types to query for approval submission
    set<string> jerectype = new set<string>();
    for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Meeting_Title_Event_RT_update_1__c, JJ_ANZ_Meeting_Title_Event_RT_update_2__c, JJ_ANZ_Meeting_Title_Event_RT_update_3__c, JJ_ANZ_Meeting_Title_Event_RT_update_4__c From JJ_ANZ_Janssen_Events__c limit 1]){
        string s = (je.JJ_ANZ_Meeting_Title_Event_RT_update_1__c == null)?'':je.JJ_ANZ_Meeting_Title_Event_RT_update_1__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Meeting_Title_Event_RT_update_2__c == null)?'':je.JJ_ANZ_Meeting_Title_Event_RT_update_2__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Meeting_Title_Event_RT_update_3__c == null)?'':je.JJ_ANZ_Meeting_Title_Event_RT_update_3__c;
        jerectype.addAll(s.split(';'));
        s = (je.JJ_ANZ_Meeting_Title_Event_RT_update_4__c == null)?'':je.JJ_ANZ_Meeting_Title_Event_RT_update_4__c;
        jerectype.addAll(s.split(';'));
    }
     
    //select all events related to the meeting title
    map<Id, Id> medevtIds = new map<Id, Id>();
    for(Medical_Event_vod__c janevent:[Select Id,OwnerId From Medical_Event_vod__c where RecordType.DeveloperName in:jerectype and (JJ_ANZ_Event_Status__c='Planning' OR JJ_ANZ_Event_Status__c='Awaiting Approval' OR JJ_ANZ_Event_Status__c='Approved') and Meeting_Title__c in:meetIds]){
        medevtIds.put(janevent.Id, janevent.OwnerId);
    }
    
    //reject the Awaiting Approval records to be re submitted
    List<Approval.ProcessWorkitemRequest> rejectReqList = new List<Approval.ProcessWorkitemRequest>();
    set<Id> rejmedevtIds = new set<Id>();
    for(ProcessInstanceWorkitem workItem:[Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId in:medevtIds.keySet()]){
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Meeting Title status changed.');
        req.setAction('Approve');
        req.setWorkitemId(workItem.Id);
        rejectReqList.add(req);
        rejmedevtIds.add(workItem.ProcessInstance.TargetObjectId);
    }   
    List<Approval.ProcessResult> resultrejList = Approval.process(rejectReqList);
    
    List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>(); 
    for(Id i:rejmedevtIds){
       // create the new approval request to submit    
       Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
       req.setComments('Event system Approval Processing');
       req.setObjectId(i);
       req.setSubmitterId(medevtIds.get(i));
       approvalReqList.add(req);
    }

    // submit the approval request for processing        
    List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);
    
}