trigger JJ_ANZ_SpeakerAgreementUpdateEvent on Speaker_Agreement__c (before insert, before update, after update) {
    
    if(trigger.isAfter && trigger.isUpdate){    
        //save old speaker values o be compared to new values
        map<Id,Speaker_Agreement__c> mapoldspeaker = trigger.oldmap;
        
        //save the speaker Ids whose Speaker Status changes from Awaiting Approval to Approved OR Rejected OR Exception
        set<Id> speakeragreeIds = new set<Id>();    
        for(Speaker_Agreement__c spk:trigger.new){
            if( spk.JJ_ANZ_Speaker_Status__c != mapoldspeaker.get(spk.Id).JJ_ANZ_Speaker_Status__c){
                speakeragreeIds.add(spk.Id);        
            }
        }
        
        
        //query the custom setting to know which janssen event record types to query for approval submission
        set<string> jerectype = new set<string>();
        for(JJ_ANZ_Janssen_Events__c je:[Select Speaker_Agreement_Event_RT_update_4__c,Speaker_Agreement_Event_RT_update_3__c,Speaker_Agreement_Event_RT_update_2__c, Speaker_Agreement_Event_RT_update_1__c From JJ_ANZ_Janssen_Events__c limit 1]){
            string s = (je.Speaker_Agreement_Event_RT_update_1__c == null)?'':je.Speaker_Agreement_Event_RT_update_1__c;
            jerectype.addAll(s.split(';'));
            s = (je.Speaker_Agreement_Event_RT_update_2__c == null)?'':je.Speaker_Agreement_Event_RT_update_2__c;
            jerectype.addAll(s.split(';'));
            s = (je.Speaker_Agreement_Event_RT_update_3__c == null)?'':je.Speaker_Agreement_Event_RT_update_3__c;
            jerectype.addAll(s.split(';'));
            s = (je.Speaker_Agreement_Event_RT_update_4__c == null)?'':je.Speaker_Agreement_Event_RT_update_4__c;
            jerectype.addAll(s.split(';'));
        }
        
        set<Id> janevtIds = new set<Id>();
        set<Id> apkagrIds = new set<Id>();
        List<JJ_ANZ_Speaker_Association__c> lstAccEvtAsc = [Select JJ_ANZ_Medical_Event__c, JJ_ANZ_Speaker_Agreement__c From JJ_ANZ_Speaker_Association__c where (JJ_ANZ_Medical_Event__r.JJ_ANZ_Event_Status__c='Planning' OR JJ_ANZ_Medical_Event__r.JJ_ANZ_Event_Status__c='Awaiting Approval' OR JJ_ANZ_Medical_Event__r.JJ_ANZ_Event_Status__c='Approved') and JJ_ANZ_Medical_Event__r.RecordType.DeveloperName in:jerectype and JJ_ANZ_Speaker_Agreement__c in:speakeragreeIds];
        
        for(JJ_ANZ_Speaker_Association__c aea:lstAccEvtAsc){
            janevtIds.add(aea.JJ_ANZ_Medical_Event__c);
            apkagrIds.add(aea.JJ_ANZ_Speaker_Agreement__c);
        }
        
        
        map<Id, Medical_Event_vod__c> mapjanevt = new map<Id, Medical_Event_vod__c>([select Id, RecordType.DeveloperName, JJ_ANZ_Event_Status__c, JJ_ANZ_Speak_Agree_Statuses__c, OwnerId from Medical_Event_vod__c where Id in:janevtIds]);  
        map<Id, Speaker_Agreement__c> mapspkagr = new map<Id, Speaker_Agreement__c>([Select JJ_ANZ_Speaker_Status__c, Name, Id From Speaker_Agreement__c where Id in:apkagrIds]);
        
        Medical_Event_vod__c tempjanEvt = new Medical_Event_vod__c();
        Speaker_Agreement__c tempspkagr = new Speaker_Agreement__c();
        List<Medical_Event_vod__c> updmedEvnt = new List<Medical_Event_vod__c>();
        string statuslst = '';
        set<Id> checkdup = new set<Id>();
        for(JJ_ANZ_Speaker_Association__c aea:lstAccEvtAsc){
            tempjanEvt = mapjanevt.get(aea.JJ_ANZ_Medical_Event__c);
            tempspkagr = mapspkagr.get(aea.JJ_ANZ_Speaker_Agreement__c);
            if(tempjanEvt != null && tempspkagr != null){
                statuslst = (tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == null)?'':tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c;
                tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c = '';
                for(string s:statuslst.split(',')){
                    List<String> tempstrlst = s.split(':');
                    s = (tempstrlst[0] == tempspkagr.Name)?tempspkagr.Name + ':' + tempspkagr.JJ_ANZ_Speaker_Status__c:s;
                    tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c += (s == '' || tempjanEvt.JJ_ANZ_Speak_Agree_Statuses__c == '')?s:',' + s;                
                }
                if(!checkdup.contains(tempjanEvt.Id)){
                    updmedEvnt.add(tempjanEvt);
                    checkdup.add(tempjanEvt.Id);
                }
            }
        }
        update updmedEvnt;
        
        //select all events related to the meeting title
        map<Id, Id> medevtIds = new map<Id, Id>();
        for(Medical_Event_vod__c janevent:updmedEvnt){
            medevtIds.put(janevent.Id, janevent.OwnerId);
        }
        
        //reject the Awaiting Approval records to be re submitted
        List<Approval.ProcessWorkitemRequest> rejectReqList = new List<Approval.ProcessWorkitemRequest>();
        set<Id> rejmedevtIds = new set<Id>();
        for(ProcessInstanceWorkitem workItem:[Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId in:medevtIds.keySet()]){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments('Related Speaker Agreement status changed.');
            req.setAction('Approve');
            req.setWorkitemId(workItem.Id);
            rejectReqList.add(req);
            rejmedevtIds.add(workItem.ProcessInstance.TargetObjectId);
        }   
        List<Approval.ProcessResult> resultrejList = Approval.process(rejectReqList);
        
        List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>(); 
        for(Id i:rejmedevtIds){
           // create the new approval request to submit    
           Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();     
           req.setComments('Event System Approval Processing');
           req.setObjectId(i);
           req.setSubmitterId(medevtIds.get(i));
           approvalReqList.add(req);
        }

        // submit the approval request for processing        
        List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);
                
    }
    
    final String triggerName = 'JJ_ANZ_SpeakerAgreementUpdateEvent';
    final String skipReason = TriggerUtils.SKIP_TRIGGER_MAP.get(triggerName);
    
    if (skipReason == null && trigger.isBefore){
        //for 12 month agreement records, prevent start and end date overlaping
        map<String, Set<String>> rectypemap = new map<String, Set<String>>();
        set<String> set1;
        Set<String> alltypeset = new Set<String>();
        for(JJ_ANZ_Janssen_Events__c je:[Select JJ_ANZ_Speaker_Agreement_No_Overlap_RT__c From JJ_ANZ_Janssen_Events__c limit 1]){
            for(string s:je.JJ_ANZ_Speaker_Agreement_No_Overlap_RT__c.split('~')){             
                set1 = new set<String>();
                set1.addAll(s.split(';'));
                for(string r:s.split(';')){                 
                    rectypemap.put(r, set1);
                    alltypeset.add(r);
                }
            }
        }
        
        map<Id, String> maprtdevname = new map<Id, String>();
        for(RecordType rt:[select Id, DeveloperName from RecordType where SobjectType=:'Speaker_Agreement__c' and DeveloperName in:alltypeset]){
            maprtdevname.put(rt.Id, rt.DeveloperName);
        }       
        
        set<Id> setrecordtypes = new set<Id>();
        for(RecordType rt:[select Id from RecordType where SobjectType=:'Speaker_Agreement__c' and (DeveloperName=:'JJ_ANZ_No_Honorarium_Approved' OR DeveloperName=:'JJ_ANZ_No_Honorarium' OR DeveloperName=:'JJ_ANZ_12_Month_Approved' OR DeveloperName=:'JJ_ANZ_12_Month_Std')]){
            setrecordtypes.add(rt.Id);
        }
        
        List<Speaker_Agreement__c> speakerlst = new List<Speaker_Agreement__c>();
        Date startdate = null;
        Date enddate = null;
        set<Id> spkids = new set<Id>();
        set<Id> speakerIds = new set<Id>();
        //get speaker being created or updated
        for(Speaker_Agreement__c spk:trigger.new){      
            if(setrecordtypes.contains(spk.RecordTypeId)){
                spk.JJ_End_Date__c = spk.JJ_Start_Date__c + 1094;
            }           
            if((spk.JJ_ANZ_Speaker_Status__c == 'Draft' || spk.JJ_ANZ_Speaker_Status__c == 'Rejected') && alltypeset.contains(maprtdevname.get(spk.RecordTypeId))){
                speakerlst.add(spk);
                spkids.add(spk.Id);
                speakerIds.add(spk.JJ_Speaker__c);
                startdate = (startdate == null || startdate > spk.JJ_Start_Date__c)?spk.JJ_Start_Date__c:startdate;
                enddate = (enddate == null || enddate < spk.JJ_End_Date__c)?spk.JJ_End_Date__c:enddate;
            }
        }
        
        string errormsg = '';            
        string errormsgstr = 'An existing agreement already exists for this Speaker:';
        
        /*** Error text updated - simpler. Admins will not update via VMessage
        string errormsgstr = '';
        for(Message_vod__c m:[Select Text_vod__c From Message_vod__c where Name=:'Speaker Agreement Error Msg 1' and Category_vod__c =:'Events' and Active_vod__c =:true]){
            errormsgstr = m.Text_vod__c;
        } */
        
        Speaker_Agreement__c speakererror = new Speaker_Agreement__c();
        //select existing speakers and compared to the one being created/updated to see if their dates overlaps
        List<Speaker_Agreement__c> existingagreements = [select Name, JJ_Start_Date__c, JJ_ANZ_Speaker_Group__c, JJ_End_Date__c, JJ_Speaker__c, RecordType.DeveloperName from Speaker_Agreement__c where JJ_Speaker__c in:speakerIds and ((JJ_Start_Date__c >=: startdate and JJ_Start_Date__c <=: enddate) OR (JJ_End_Date__c >=: startdate and JJ_End_Date__c <=: enddate) OR (JJ_Start_Date__c >=: startdate and JJ_End_Date__c <=: enddate) OR (JJ_Start_Date__c <=: startdate and JJ_End_Date__c >=: enddate)) and Id not in:spkids and RecordTypeId in: maprtdevname.keySet() and (JJ_ANZ_Speaker_Status__c = 'Draft' OR JJ_ANZ_Speaker_Status__c = 'Awaiting Approval' OR JJ_ANZ_Speaker_Status__c = 'Exception' OR JJ_ANZ_Speaker_Status__c = 'Approved')];
        for(Speaker_Agreement__c s:speakerlst){
            for(Speaker_Agreement__c spk:existingagreements){if(spk.JJ_ANZ_Speaker_Group__c == s.JJ_ANZ_Speaker_Group__c && spk.JJ_Speaker__c == s.JJ_Speaker__c && ((spk.JJ_Start_Date__c >= s.JJ_Start_Date__c && spk.JJ_Start_Date__c <= s.JJ_End_Date__c) || (spk.JJ_End_Date__c >= s.JJ_Start_Date__c && spk.JJ_End_Date__c <= s.JJ_End_Date__c) || (spk.JJ_Start_Date__c >= s.JJ_Start_Date__c && spk.JJ_End_Date__c <= s.JJ_End_Date__c) || (spk.JJ_Start_Date__c <= s.JJ_Start_Date__c && spk.JJ_End_Date__c >= s.JJ_End_Date__c)) && rectypemap.get(maprtdevname.get(s.RecordTypeId)).contains(spk.RecordType.DeveloperName)){
                    
                    errormsg += (errormsg == '')?spk.Name: ',' + spk.Name;
                    speakererror = s;
                }
            }
            //display error in case of date overlaping
            if(errormsg != null){      
                speakererror.adderror(errormsgstr +' ' + errormsg);
            }
        }
        
    }
}