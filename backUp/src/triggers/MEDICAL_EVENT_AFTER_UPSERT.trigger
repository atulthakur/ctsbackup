trigger MEDICAL_EVENT_AFTER_UPSERT on Medical_Event_vod__c (after insert, after update) {
    // get new Medical Event Data
    Medical_Event_vod__c [] triggerMedicalEvents =  Trigger.new;
    
    for (Medical_Event_vod__c medicalEvent : triggerMedicalEvents) {
        
        Account_Event_Association__c [] eventAssociationList = [select Id, JJ_ANZ_Event_Date_Text__c, JJ_ANZ_Event_Owner__c from Account_Event_Association__c where JJ_Janssen_Event__c = :medicalEvent.Id];
        if (eventAssociationList.size()>0){
            for (Account_Event_Association__c eventAssociation : eventAssociationList){
                eventAssociation.JJ_ANZ_Event_Date_Text__c = medicalEvent.Start_Date_vod__c;
                eventAssociation.JJ_ANZ_Event_Owner__c = medicalEvent.OwnerId;
            }
        Update eventAssociationList;
        }
    }   
}