/*****
***     Developed by: Khodabocus Zameer (ICBWORKS)
***     Description: This trigger is used to share visibility of accounts (JJ_Speaker__c) with Speaker Liason on Speaker Agreement whenever
***                  the Speaker Liaison is updated.    
*****/

trigger ShareAccountsAfterUpsertSpeakerAgreements on Speaker_Agreement__c (before update) {
    
    Map<Id, Speaker_Agreement__c> previousSpeakerLiasonMap = new Map<Id, Speaker_Agreement__c>();
    Map<Id, Id> speakersAccountsMap = new Map<Id, Id>();
    Set<Id> oldSpLaisionsId = new Set<Id>();
    List<AccountShare> newAccountsShareList = new List<AccountShare>();
    
    for(Speaker_Agreement__c sa : trigger.new){
        //map of all Speakers (Accounts) for the Speaker Agreement being updated
        speakersAccountsMap.put(sa.Id, sa.JJ_Speaker__c);
    }
    
    if(trigger.isUpdate){
        for(Speaker_Agreement__c sa : trigger.old){
            if(sa.JJ_Speaker__c != null){
                //map of Speaker Agreements before the new changes are saved
                previousSpeakerLiasonMap.put(sa.Id, sa);
            }
        }
        
        for(Speaker_Agreement__c sa : trigger.new){
            if(previousSpeakerLiasonMap.size() > 0){
                Speaker_Agreement__c speakerAggOld = previousSpeakerLiasonMap.get(sa.Id);
                
                if(speakerAggOld != null){
                    //Compare the value of old Speaker Liason with the new one being saved.
                    //If they are not same, share the Speaker Account with the new Liason and delete the previous share with the Old Liason
                    if(speakerAggOld.JJ_Speaker_Liason__c != sa.JJ_Speaker_Liason__c){
                        if(speakerAggOld.JJ_Speaker_Liason__c != null){
                            //set of the old Speaker Liason Ids to be used to delete previous share
                            oldSpLaisionsId.add(speakerAggOld.JJ_Speaker_Liason__c);
                        }
                        //New Account share with the new Speaker Liason
                        AccountShare newAccShare = new AccountShare();
                            
                        newAccShare.AccountId = speakersAccountsMap.get(sa.Id);
                        newAccShare.UserOrGroupId = sa.JJ_Speaker_Liason__c;
                        newAccShare.OpportunityAccessLevel = 'None';
                        
                        newAccShare.AccountAccessLevel = 'Edit';
                        
                        newAccountsShareList.add(newAccShare);
                    }
                }
            }
        }
        
        //Getting the list of old account shared with the old Speaker Liason 
        if(oldSpLaisionsId.size() > 0 && speakersAccountsMap.size() > 0){
            List<AccountShare> oldAccSharingRules = [select Id
                                                     From AccountShare
                                                     where AccountId in :speakersAccountsMap.values() AND UserOrGroupId in :oldSpLaisionsId
                                                     AND RowCause = 'Manual'];
                                                     
            if(oldAccSharingRules.size() > 0){
                //deleting visibility of the Speaker accounts for the old Speaker Liason
                delete oldAccSharingRules;
            }
        }
        
        if(newAccountsShareList.size() > 0){
            //insertng the new account shares for the new Speaker Liason
            insert newAccountsShareList;
        }
    }
}