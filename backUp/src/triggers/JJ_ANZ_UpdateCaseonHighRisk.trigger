/**
 *      @author Mahesh Somineni
 *      @date   April 5 2016
        @description    Trigger to update High Risk Checkbox in Cases.  
        
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Mahesh Somineni                 5/04/2016         Original Version
 */

trigger JJ_ANZ_UpdateCaseonHighRisk on JJ_ANZ_HighRiskInformation__c (after insert) {

if (TriggerDisabler__c.getInstance().JJ_ANZ_UpdateCaseonHighRisk__c) {return;}


    List<Id> CaseIds = new List<Id>();
    Public static String Closed  = 'Closed';
    for(JJ_ANZ_HighRiskInformation__c highRiskInfo : Trigger.new)
    {
        if(highRiskInfo.JJ_ANZ_Status__c !=Closed&&highRiskInfo.JJ_ANZ_Case__c!=null)
        {
            CaseIds.add(highRiskInfo.JJ_ANZ_Case__c);
        }    
    }
    
    List<Case> UpdateCases = new List<Case>();
    
    for(Id caseId : CaseIds)
    {
        Case updateCase = new Case();
        updateCase.Id = caseId;
        updateCase.JJ_ANZ_Is_highrisk__c = true;
        
        UpdateCases.add(updateCase);
    }
    
    if(!UpdateCases.isEmpty())
    {
        try
        {
            Update UpdateCases;
        }
        catch(DMLException de)
        {
            // Log a DML Exception if Update fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =de.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =de.getTypeName();
                Database.insert(newErrorRecord,false);  
        }
        catch(Exception e)
        {
             // Log a Exception if Update fails
                JJ_ANZ_Error_Log__c newErrorRecord = new JJ_ANZ_Error_Log__c();
                newErrorRecord.JJ_ANZ_Error_Description__c =e.getMessage();
                newErrorRecord.JJ_ANZ_Exception_Type__c =e.getTypeName();
                Database.insert(newErrorRecord,false);
        }
        
    }
}