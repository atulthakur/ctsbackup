trigger JJ_ANZ_Post_Clues_To_Chatter on Call2_vod__c (after insert, after update) {

// creates a Chatter post when Call with Clues is Submitted
// post once on parent call Account only, e.g. 
//      for Individual Call it is on Person Account, 
//      for Group Calls it is on the Business Account
// link to Call record
    
    List<feedItem> postFeed = new List<feedItem>();
    for(Call2_vod__c o : Trigger.new){
        if(o.Status_vod__c == 'Submitted_vod' && Trigger.oldMap.get(o.id).Status_vod__c != 'Submitted_vod' && o.JJ_ANZ_Clues__c != null && o.Parent_Call_vod__c == null){
            String oppURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + o.id;
            String status = '#Clues ' + o.JJ_ANZ_Clues__c;
            
            FeedItem post = new FeedItem();
            post.ParentId = o.Account_vod__c;
            post.Title = o.Name;
            post.Body = status;
            post.LinkUrl = oppURL;
            post.Type = 'LinkPost';
            
            postFeed.add(post);
        }
    }
    if (!postFeed.isEmpty()) {
        try{ 
         insert postFeed;
      }
       catch(system.exception e){
          system.debug('Error: '+e);
      }
    }
}