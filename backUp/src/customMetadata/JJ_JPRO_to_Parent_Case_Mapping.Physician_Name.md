<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Physician Name</label>
    <protected>false</protected>
    <values>
        <field>JJ_Field_Name__c</field>
        <value xsi:type="xsd:string">JJ_ANZ_Secondary_First_Name__c</value>
    </values>
    <values>
        <field>JJ_Json_Response__c</field>
        <value xsi:type="xsd:string">JJ_ANZ_Secondary_First_Name</value>
    </values>
</CustomMetadata>
