<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>State or City NZ</label>
    <protected>false</protected>
    <values>
        <field>JJ_Field_Name__c</field>
        <value xsi:type="xsd:string">JJ_State_or_City_NZ__c</value>
    </values>
    <values>
        <field>JJ_Json_Response__c</field>
        <value xsi:type="xsd:string">JJ_State_or_City_NZ</value>
    </values>
    <values>
        <field>JJ_Number_Capture__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
