<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>iConnect Console</label>
    <logo>images/ANZHeaderImage.png</logo>
    <tab>standard-Chatter</tab>
    <tab>iConnect_Support__c</tab>
    <tab>JJ_ANZ_FAQ__c</tab>
    <tab>Future_Enhancements__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
